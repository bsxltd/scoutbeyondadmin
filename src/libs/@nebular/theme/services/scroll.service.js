import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { share } from 'rxjs/operators';
/**
 * Layout scroll service. Provides information about current scroll position,
 * as well as methods to update position of the scroll.
 *
 * The reason we added this service is that in Nebular there are two scroll modes:
 * - the default mode when scroll is on body
 * - and the `withScroll` mode, when scroll is removed from the body and moved to an element inside of the
 * `nb-layout` component
 */
var NbLayoutScrollService = /** @class */ (function () {
    function NbLayoutScrollService() {
        this.scrollPositionReq$ = new Subject();
        this.manualScroll$ = new Subject();
        this.scroll$ = new Subject();
    }
    /**
     * Returns scroll position
     *
     * @returns {Observable<NbScrollPosition>}
     */
    /**
       * Returns scroll position
       *
       * @returns {Observable<NbScrollPosition>}
       */
    NbLayoutScrollService.prototype.getPosition = /**
       * Returns scroll position
       *
       * @returns {Observable<NbScrollPosition>}
       */
    function () {
        var _this = this;
        return Observable.create(function (observer) {
            var listener = new Subject();
            listener.subscribe(observer);
            _this.scrollPositionReq$.next({ listener: listener });
            return function () { return listener.complete(); };
        });
    };
    /**
     * Sets scroll position
     *
     * @param {number} x
     * @param {number} y
     */
    /**
       * Sets scroll position
       *
       * @param {number} x
       * @param {number} y
       */
    NbLayoutScrollService.prototype.scrollTo = /**
       * Sets scroll position
       *
       * @param {number} x
       * @param {number} y
       */
    function (x, y) {
        if (x === void 0) { x = null; }
        if (y === void 0) { y = null; }
        this.manualScroll$.next({ x: x, y: y });
    };
    /**
     * Returns a stream of scroll events
     *
     * @returns {Observable<any>}
     */
    /**
       * Returns a stream of scroll events
       *
       * @returns {Observable<any>}
       */
    NbLayoutScrollService.prototype.onScroll = /**
       * Returns a stream of scroll events
       *
       * @returns {Observable<any>}
       */
    function () {
        return this.scroll$.pipe(share());
    };
    /**
     * @private
     * @returns Observable<NbScrollPosition>.
     */
    /**
       * @private
       * @returns Observable<NbScrollPosition>.
       */
    NbLayoutScrollService.prototype.onManualScroll = /**
       * @private
       * @returns Observable<NbScrollPosition>.
       */
    function () {
        return this.manualScroll$.pipe(share());
    };
    /**
     * @private
     * @returns {Subject<any>}
     */
    /**
       * @private
       * @returns {Subject<any>}
       */
    NbLayoutScrollService.prototype.onGetPosition = /**
       * @private
       * @returns {Subject<any>}
       */
    function () {
        return this.scrollPositionReq$;
    };
    /**
     * @private
     * @param {any} event
     */
    /**
       * @private
       * @param {any} event
       */
    NbLayoutScrollService.prototype.fireScrollChange = /**
       * @private
       * @param {any} event
       */
    function (event) {
        this.scroll$.next(event);
    };
    NbLayoutScrollService.decorators = [
        { type: Injectable },
    ];
    return NbLayoutScrollService;
}());
export { NbLayoutScrollService };
//# sourceMappingURL=scroll.service.js.map
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
/**
 * Simple helper service to return Layout dimensions
 * Depending of current Layout scroll mode (default or `withScroll` when scroll is moved to an element
 * inside of the layout) corresponding dimensions will be returns  - of `documentElement` in first case and
 * `.scrollable-container` in the second.
 */
var NbLayoutRulerService = /** @class */ (function () {
    function NbLayoutRulerService() {
        this.contentDimensionsReq$ = new Subject();
    }
    /**
     * Content dimensions
     * @returns {Observable<NbLayoutDimensions>}
     */
    /**
       * Content dimensions
       * @returns {Observable<NbLayoutDimensions>}
       */
    NbLayoutRulerService.prototype.getDimensions = /**
       * Content dimensions
       * @returns {Observable<NbLayoutDimensions>}
       */
    function () {
        var _this = this;
        return Observable.create(function (observer) {
            var listener = new Subject();
            listener.subscribe(observer);
            _this.contentDimensionsReq$.next({ listener: listener });
            return function () { return listener.complete(); };
        });
    };
    /**
     * @private
     * @returns {Subject<any>}
     */
    /**
       * @private
       * @returns {Subject<any>}
       */
    NbLayoutRulerService.prototype.onGetDimensions = /**
       * @private
       * @returns {Subject<any>}
       */
    function () {
        return this.contentDimensionsReq$;
    };
    NbLayoutRulerService.decorators = [
        { type: Injectable },
    ];
    return NbLayoutRulerService;
}());
export { NbLayoutRulerService };
//# sourceMappingURL=ruler.service.js.map
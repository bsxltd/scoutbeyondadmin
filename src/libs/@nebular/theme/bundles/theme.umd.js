(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('rxjs'), require('rxjs/operators'), require('@angular/forms'), require('@angular/router'), require('@angular/cdk/a11y'), require('@angular/cdk/portal'), require('@angular/cdk/overlay'), require('@angular/cdk/platform'), require('@angular/cdk/bidi'), require('@angular/animations'), require('@angular/platform-browser'), require('intersection-observer')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common', 'rxjs', 'rxjs/operators', '@angular/forms', '@angular/router', '@angular/cdk/a11y', '@angular/cdk/portal', '@angular/cdk/overlay', '@angular/cdk/platform', '@angular/cdk/bidi', '@angular/animations', '@angular/platform-browser', 'intersection-observer'], factory) :
	(factory((global.nb = global.nb || {}, global.nb.theme = global.nb.theme || {}),global.ng.core,global.ng.common,global.Rx,global.Rx.operators,global.ng.forms,global.ng.router,global.ng.cdk.a11y,global.ng.cdk.portal,global.ng.cdk.overlay,global.ng.cdk.platform,global.ng.cdk.bidi,global.ng.animations,global.ng.platformBrowser));
}(this, (function (exports,i0,i1,rxjs,rxjs_operators,_angular_forms,_angular_router,_angular_cdk_a11y,_angular_cdk_portal,_angular_cdk_overlay,_angular_cdk_platform,_angular_cdk_bidi,_angular_animations,_angular_platformBrowser) { 'use strict';

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_THEME_OPTIONS = new i0.InjectionToken('Nebular Theme Options');
var NB_MEDIA_BREAKPOINTS = new i0.InjectionToken('Nebular Media Breakpoints');
var NB_BUILT_IN_JS_THEMES = new i0.InjectionToken('Nebular Built-in JS Themes');
var NB_JS_THEMES = new i0.InjectionToken('Nebular JS Themes');
/**
 * We're providing browser apis with tokens to improve testing capabilities.
 * */
var NB_WINDOW = new i0.InjectionToken('Window');
var NB_DOCUMENT = new i0.InjectionToken('Document');

var NbColorHelper = /** @class */ (function () {
    function NbColorHelper() {
    }
    NbColorHelper.shade = function (color, weight) {
        return NbColorHelper.mix('#000000', color, weight);
    };
    NbColorHelper.tint = function (color, weight) {
        return NbColorHelper.mix('#ffffff', color, weight);
    };
    NbColorHelper.mix = function (color1, color2, weight) {
        var d2h = function (d) { return d.toString(16); };
        var h2d = function (h) { return parseInt(h, 16); };
        var result = '#';
        for (var i = 1; i < 7; i += 2) {
            var firstPart = h2d(color1.substr(i, 2));
            var secondPart = h2d(color2.substr(i, 2));
            var resultPart = d2h(Math.floor(secondPart + (firstPart - secondPart) * (weight / 100.0)));
            result += ('0' + resultPart).slice(-2);
        }
        return result;
    };
    NbColorHelper.hexToRgbA = function (hex, alpha) {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split('');
            if (c.length === 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = '0x' + c.join('');
            return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
        }
        throw new Error('Bad Hex');
    };
    return NbColorHelper;
}());

var palette = {
    primary: '#8a7fff',
    success: '#40dc7e',
    info: '#4ca6ff',
    warning: '#ffa100',
    danger: '#ff4c6a',
};
var DEFAULT_THEME = {
    name: 'default',
    variables: {
        fontMain: '"Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif',
        fontSecondary: '"Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif',
        bg: '#ffffff',
        fg: '#a4abb3',
        fgHeading: '#2a2a2a',
        fgText: '#3b3b3b',
        fgHighlight: '#41d974',
        layoutBg: '#ebeff5',
        separator: '#ebeef2',
        primary: palette.primary,
        success: palette.success,
        info: palette.info,
        warning: palette.warning,
        danger: palette.danger,
        primaryLight: NbColorHelper.tint(palette.primary, 15),
        successLight: NbColorHelper.tint(palette.success, 15),
        infoLight: NbColorHelper.tint(palette.info, 15),
        warningLight: NbColorHelper.tint(palette.warning, 15),
        dangerLight: NbColorHelper.tint(palette.danger, 15),
    },
};

var palette$1 = {
    primary: '#7659ff',
    success: '#00d977',
    info: '#0088ff',
    warning: '#ffa100',
    danger: '#ff386a',
};
var COSMIC_THEME = {
    name: 'cosmic',
    base: 'default',
    variables: {
        bg: '#3d3780',
        fg: '#a1a1e5',
        fgHeading: '#ffffff',
        fgText: '#d1d1ff',
        fgHighlight: '#00f9a6',
        layoutBg: '#2f296b',
        separator: '#342e73',
        primary: palette$1.primary,
        success: palette$1.success,
        info: palette$1.info,
        warning: palette$1.warning,
        danger: palette$1.danger,
        primaryLight: NbColorHelper.tint(palette$1.primary, 20),
        successLight: NbColorHelper.tint(palette$1.success, 20),
        infoLight: NbColorHelper.tint(palette$1.info, 20),
        warningLight: NbColorHelper.tint(palette$1.warning, 20),
        dangerLight: NbColorHelper.tint(palette$1.danger, 20),
    },
};

var palette$2 = {
    primary: '#73a1ff',
    success: '#5dcfe3',
    info: '#ba7fec',
    warning: '#ffa36b',
    danger: '#ff6b83',
};
var CORPORATE_THEME = {
    name: 'corporate',
    base: 'default',
    variables: {
        fg: '#f1f5f8',
        bg: '#ffffff',
        fgHeading: '#181818',
        fgText: '#4b4b4b',
        fgHighlight: '#a4abb3',
        layoutBg: '#f1f5f8',
        separator: '#cdd5dc',
        primary: palette$2.primary,
        success: palette$2.success,
        info: palette$2.info,
        warning: palette$2.warning,
        danger: palette$2.danger,
        primaryLight: NbColorHelper.tint(palette$2.primary, 15),
        successLight: NbColorHelper.tint(palette$2.success, 15),
        infoLight: NbColorHelper.tint(palette$2.info, 15),
        warningLight: NbColorHelper.tint(palette$2.warning, 15),
        dangerLight: NbColorHelper.tint(palette$2.danger, 15),
    },
};

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var BUILT_IN_THEMES = [
    DEFAULT_THEME,
    COSMIC_THEME,
    CORPORATE_THEME,
];
/**
 * Js Themes registry - provides access to the JS themes' variables.
 * Usually shouldn't be used directly, but through the NbThemeService class methods (getJsTheme).
 */
var NbJSThemesRegistry = /** @class */ (function () {
    function NbJSThemesRegistry(builtInThemes, newThemes) {
        if (newThemes === void 0) { newThemes = []; }
        var _this = this;
        this.themes = {};
        var themes = this.combineByNames(newThemes, builtInThemes);
        themes.forEach(function (theme) {
            _this.register(theme, theme.name, theme.base);
        });
    }
    /**
     * Registers a new JS theme
     * @param config any
     * @param themeName string
     * @param baseTheme string
     */
    /**
       * Registers a new JS theme
       * @param config any
       * @param themeName string
       * @param baseTheme string
       */
    NbJSThemesRegistry.prototype.register = /**
       * Registers a new JS theme
       * @param config any
       * @param themeName string
       * @param baseTheme string
       */
    function (config, themeName, baseTheme) {
        var base = this.has(baseTheme) ? this.get(baseTheme) : {};
        this.themes[themeName] = this.mergeDeep({}, base, config);
    };
    /**
     * Checks whether the theme is registered
     * @param themeName
     * @returns boolean
     */
    /**
       * Checks whether the theme is registered
       * @param themeName
       * @returns boolean
       */
    NbJSThemesRegistry.prototype.has = /**
       * Checks whether the theme is registered
       * @param themeName
       * @returns boolean
       */
    function (themeName) {
        return !!this.themes[themeName];
    };
    /**
     * Return a theme
     * @param themeName
     * @returns NbJSThemeOptions
     */
    /**
       * Return a theme
       * @param themeName
       * @returns NbJSThemeOptions
       */
    NbJSThemesRegistry.prototype.get = /**
       * Return a theme
       * @param themeName
       * @returns NbJSThemeOptions
       */
    function (themeName) {
        if (!this.themes[themeName]) {
            throw Error("NbThemeConfig: no theme '" + themeName + "' found registered.");
        }
        return JSON.parse(JSON.stringify(this.themes[themeName]));
    };
    NbJSThemesRegistry.prototype.combineByNames = function (newThemes, oldThemes) {
        var _this = this;
        if (newThemes) {
            var mergedThemes_1 = [];
            newThemes.forEach(function (theme) {
                var sameOld = oldThemes.find(function (tm) { return tm.name === theme.name; })
                    || {};
                var mergedTheme = _this.mergeDeep({}, sameOld, theme);
                mergedThemes_1.push(mergedTheme);
            });
            oldThemes.forEach(function (theme) {
                if (!mergedThemes_1.find(function (tm) { return tm.name === theme.name; })) {
                    mergedThemes_1.push(theme);
                }
            });
            return mergedThemes_1;
        }
        return oldThemes;
    };
    NbJSThemesRegistry.prototype.isObject = function (item) {
        return item && typeof item === 'object' && !Array.isArray(item);
    };
    // TODO: move to helpers
    // TODO: move to helpers
    NbJSThemesRegistry.prototype.mergeDeep = 
    // TODO: move to helpers
    function (target) {
        var sources = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            sources[_i - 1] = arguments[_i];
        }
        if (!sources.length) {
            return target;
        }
        var source = sources.shift();
        if (this.isObject(target) && this.isObject(source)) {
            for (var key in source) {
                if (this.isObject(source[key])) {
                    if (!target[key]) {
                        Object.assign(target, (_a = {}, _a[key] = {}, _a));
                    }
                    this.mergeDeep(target[key], source[key]);
                }
                else {
                    Object.assign(target, (_b = {}, _b[key] = source[key], _b));
                }
            }
        }
        return this.mergeDeep.apply(this, [target].concat(sources));
        var _a, _b;
    };
    NbJSThemesRegistry.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbJSThemesRegistry.ctorParameters = function () { return [
        { type: Array, decorators: [{ type: i0.Inject, args: [NB_BUILT_IN_JS_THEMES,] },] },
        { type: Array, decorators: [{ type: i0.Inject, args: [NB_JS_THEMES,] },] },
    ]; };
    return NbJSThemesRegistry;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var DEFAULT_MEDIA_BREAKPOINTS = [
    {
        name: 'xs',
        width: 0,
    },
    {
        name: 'is',
        width: 400,
    },
    {
        name: 'sm',
        width: 576,
    },
    {
        name: 'md',
        width: 768,
    },
    {
        name: 'lg',
        width: 992,
    },
    {
        name: 'xl',
        width: 1200,
    },
    {
        name: 'xxl',
        width: 1400,
    },
    {
        name: 'xxxl',
        width: 1600,
    },
];
/**
 * Manages media breakpoints
 *
 * Provides access to available media breakpoints to convert window width to a configured breakpoint,
 * e.g. 200px - *xs* breakpoint
 */
var NbMediaBreakpointsService = /** @class */ (function () {
    function NbMediaBreakpointsService(breakpoints) {
        this.breakpoints = breakpoints;
        this.breakpointsMap = this.breakpoints.reduce(function (res, b) {
            res[b.name] = b.width;
            return res;
        }, {});
    }
    /**
     * Returns a configured breakpoint by width
     * @param width number
     * @returns {Z|{name: string, width: number}}
     */
    /**
       * Returns a configured breakpoint by width
       * @param width number
       * @returns {Z|{name: string, width: number}}
       */
    NbMediaBreakpointsService.prototype.getByWidth = /**
       * Returns a configured breakpoint by width
       * @param width number
       * @returns {Z|{name: string, width: number}}
       */
    function (width) {
        var unknown = { name: 'unknown', width: width };
        var breakpoints = this.getBreakpoints();
        return breakpoints
            .find(function (point, index) {
            var next = breakpoints[index + 1];
            return width >= point.width && (!next || width < next.width);
        }) || unknown;
    };
    /**
     * Returns a configured breakpoint by name
     * @param name string
     * @returns NbMediaBreakpoint
     */
    /**
       * Returns a configured breakpoint by name
       * @param name string
       * @returns NbMediaBreakpoint
       */
    NbMediaBreakpointsService.prototype.getByName = /**
       * Returns a configured breakpoint by name
       * @param name string
       * @returns NbMediaBreakpoint
       */
    function (name) {
        var unknown = { name: 'unknown', width: NaN };
        var breakpoints = this.getBreakpoints();
        return breakpoints.find(function (point) { return name === point.name; }) || unknown;
    };
    /**
     * Returns a list of configured breakpoints for the theme
     * @returns NbMediaBreakpoint[]
     */
    /**
       * Returns a list of configured breakpoints for the theme
       * @returns NbMediaBreakpoint[]
       */
    NbMediaBreakpointsService.prototype.getBreakpoints = /**
       * Returns a list of configured breakpoints for the theme
       * @returns NbMediaBreakpoint[]
       */
    function () {
        return this.breakpoints;
    };
    /**
     * Returns a map of configured breakpoints for the theme
     * @returns {[p: string]: number}
     */
    /**
       * Returns a map of configured breakpoints for the theme
       * @returns {[p: string]: number}
       */
    NbMediaBreakpointsService.prototype.getBreakpointsMap = /**
       * Returns a map of configured breakpoints for the theme
       * @returns {[p: string]: number}
       */
    function () {
        return this.breakpointsMap;
    };
    NbMediaBreakpointsService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbMediaBreakpointsService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_MEDIA_BREAKPOINTS,] },] },
    ]; };
    return NbMediaBreakpointsService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Main Nebular service. Includes various helper methods.
 */
var NbThemeService = /** @class */ (function () {
    function NbThemeService(options, breakpointService, jsThemesRegistry) {
        this.options = options;
        this.breakpointService = breakpointService;
        this.jsThemesRegistry = jsThemesRegistry;
        this.themeChanges$ = new rxjs.ReplaySubject(1);
        this.appendLayoutClass$ = new rxjs.Subject();
        this.removeLayoutClass$ = new rxjs.Subject();
        this.changeWindowWidth$ = new rxjs.ReplaySubject(2);
        if (options && options.name) {
            this.changeTheme(options.name);
        }
    }
    /**
     * Change current application theme
     * @param {string} name
     */
    /**
       * Change current application theme
       * @param {string} name
       */
    NbThemeService.prototype.changeTheme = /**
       * Change current application theme
       * @param {string} name
       */
    function (name) {
        this.themeChanges$.next({ name: name, previous: this.currentTheme });
        this.currentTheme = name;
    };
    NbThemeService.prototype.changeWindowWidth = function (width) {
        this.changeWindowWidth$.next(width);
    };
    /**
     * Returns a theme object with variables (color/paddings/etc) on a theme change.
     * Once subscribed - returns current theme.
     *
     * @returns {Observable<NbJSThemeOptions>}
     */
    /**
       * Returns a theme object with variables (color/paddings/etc) on a theme change.
       * Once subscribed - returns current theme.
       *
       * @returns {Observable<NbJSThemeOptions>}
       */
    NbThemeService.prototype.getJsTheme = /**
       * Returns a theme object with variables (color/paddings/etc) on a theme change.
       * Once subscribed - returns current theme.
       *
       * @returns {Observable<NbJSThemeOptions>}
       */
    function () {
        var _this = this;
        return this.onThemeChange().pipe(rxjs_operators.map(function (theme) {
            return _this.jsThemesRegistry.get(theme.name);
        }));
    };
    /**
     * Triggers media query breakpoint change
     * Returns a pair where the first item is previous media breakpoint and the second item is current breakpoit.
     * ```ts
     *  [{ name: 'xs', width: 0 }, { name: 'md', width: 768 }] // change from `xs` to `md`
     * ```
     * @returns {Observable<[NbMediaBreakpoint, NbMediaBreakpoint]>}
     */
    /**
       * Triggers media query breakpoint change
       * Returns a pair where the first item is previous media breakpoint and the second item is current breakpoit.
       * ```ts
       *  [{ name: 'xs', width: 0 }, { name: 'md', width: 768 }] // change from `xs` to `md`
       * ```
       * @returns {Observable<[NbMediaBreakpoint, NbMediaBreakpoint]>}
       */
    NbThemeService.prototype.onMediaQueryChange = /**
       * Triggers media query breakpoint change
       * Returns a pair where the first item is previous media breakpoint and the second item is current breakpoit.
       * ```ts
       *  [{ name: 'xs', width: 0 }, { name: 'md', width: 768 }] // change from `xs` to `md`
       * ```
       * @returns {Observable<[NbMediaBreakpoint, NbMediaBreakpoint]>}
       */
    function () {
        var _this = this;
        return this.changeWindowWidth$
            .pipe(rxjs_operators.startWith(undefined), rxjs_operators.pairwise(), rxjs_operators.map(function (_a) {
            var prevWidth = _a[0], width = _a[1];
            return [
                _this.breakpointService.getByWidth(prevWidth),
                _this.breakpointService.getByWidth(width),
            ];
        }), rxjs_operators.filter(function (_a) {
            var prevPoint = _a[0], point = _a[1];
            return prevPoint.name !== point.name;
        }), rxjs_operators.distinctUntilChanged(null, function (params) { return params[0].name + params[1].name; }), rxjs_operators.share());
    };
    /**
     * Triggered when current theme is changed
     * @returns {Observable<any>}
     */
    /**
       * Triggered when current theme is changed
       * @returns {Observable<any>}
       */
    NbThemeService.prototype.onThemeChange = /**
       * Triggered when current theme is changed
       * @returns {Observable<any>}
       */
    function () {
        return this.themeChanges$.pipe(rxjs_operators.share());
    };
    /**
     * Append a class to nb-layout
     * @param {string} className
     */
    /**
       * Append a class to nb-layout
       * @param {string} className
       */
    NbThemeService.prototype.appendLayoutClass = /**
       * Append a class to nb-layout
       * @param {string} className
       */
    function (className) {
        this.appendLayoutClass$.next(className);
    };
    /**
     * Triggered when a new class is added to nb-layout through `appendLayoutClass` method
     * @returns {Observable<any>}
     */
    /**
       * Triggered when a new class is added to nb-layout through `appendLayoutClass` method
       * @returns {Observable<any>}
       */
    NbThemeService.prototype.onAppendLayoutClass = /**
       * Triggered when a new class is added to nb-layout through `appendLayoutClass` method
       * @returns {Observable<any>}
       */
    function () {
        return this.appendLayoutClass$.pipe(rxjs_operators.share());
    };
    /**
     * Removes a class from nb-layout
     * @param {string} className
     */
    /**
       * Removes a class from nb-layout
       * @param {string} className
       */
    NbThemeService.prototype.removeLayoutClass = /**
       * Removes a class from nb-layout
       * @param {string} className
       */
    function (className) {
        this.removeLayoutClass$.next(className);
    };
    /**
     * Triggered when a class is removed from nb-layout through `removeLayoutClass` method
     * @returns {Observable<any>}
     */
    /**
       * Triggered when a class is removed from nb-layout through `removeLayoutClass` method
       * @returns {Observable<any>}
       */
    NbThemeService.prototype.onRemoveLayoutClass = /**
       * Triggered when a class is removed from nb-layout through `removeLayoutClass` method
       * @returns {Observable<any>}
       */
    function () {
        return this.removeLayoutClass$.pipe(rxjs_operators.share());
    };
    NbThemeService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbThemeService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_THEME_OPTIONS,] },] },
        { type: NbMediaBreakpointsService, },
        { type: NbJSThemesRegistry, },
    ]; };
    return NbThemeService;
}());

/**
 * Service to control the global page spinner.
 */
var NbSpinnerService = /** @class */ (function () {
    function NbSpinnerService(document) {
        this.document = document;
        this.loaders = [];
        this.selector = 'nb-global-spinner';
    }
    /**
     * Appends new loader to the list of loader to be completed before
     * spinner will be hidden
     * @param method Promise<any>
     */
    /**
       * Appends new loader to the list of loader to be completed before
       * spinner will be hidden
       * @param method Promise<any>
       */
    NbSpinnerService.prototype.registerLoader = /**
       * Appends new loader to the list of loader to be completed before
       * spinner will be hidden
       * @param method Promise<any>
       */
    function (method) {
        this.loaders.push(method);
    };
    /**
     * Clears the list of loader
     */
    /**
       * Clears the list of loader
       */
    NbSpinnerService.prototype.clear = /**
       * Clears the list of loader
       */
    function () {
        this.loaders = [];
    };
    /**
     * Start the loader process, show spinnder and execute loaders
     */
    /**
       * Start the loader process, show spinnder and execute loaders
       */
    NbSpinnerService.prototype.load = /**
       * Start the loader process, show spinnder and execute loaders
       */
    function () {
        this.showSpinner();
        this.executeAll();
    };
    NbSpinnerService.prototype.executeAll = function (done) {
        var _this = this;
        if (done === void 0) { done = function () { }; }
        Promise.all(this.loaders).then(function (values) {
            _this.hideSpinner();
            done.call(null, values);
        })
            .catch(function (error) {
            // TODO: Promise.reject
            console.error(error);
        });
    };
    // TODO is there any better way of doing this?
    // TODO is there any better way of doing this?
    NbSpinnerService.prototype.showSpinner = 
    // TODO is there any better way of doing this?
    function () {
        var el = this.getSpinnerElement();
        if (el) {
            el.style['display'] = 'block';
        }
    };
    NbSpinnerService.prototype.hideSpinner = function () {
        var el = this.getSpinnerElement();
        if (el) {
            el.style['display'] = 'none';
        }
    };
    NbSpinnerService.prototype.getSpinnerElement = function () {
        return this.document.getElementById(this.selector);
    };
    NbSpinnerService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbSpinnerService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
    ]; };
    return NbSpinnerService;
}());

/**
 * Layout direction.
 * */
/**
 * Layout direction.
 * */

/**
 * Layout direction.
 * */
(function (NbLayoutDirection) {
    NbLayoutDirection["LTR"] = "ltr";
    NbLayoutDirection["RTL"] = "rtl";
})(exports.NbLayoutDirection || (exports.NbLayoutDirection = {}));

/**
 * Layout direction setting injection token.
 * */
var NB_LAYOUT_DIRECTION = new i0.InjectionToken('Layout direction');
/**
 * Layout Direction Service.
 * Allows to set or get layout direction and listen to its changes
 */
var NbLayoutDirectionService = /** @class */ (function () {
    function NbLayoutDirectionService(direction) {
        if (direction === void 0) { direction = exports.NbLayoutDirection.LTR; }
        this.direction = direction;
        this.$directionChange = new rxjs.ReplaySubject(1);
        this.setDirection(direction);
    }
    /**
     * Returns true if layout direction set to left to right.
     * @returns boolean.
     * */
    /**
       * Returns true if layout direction set to left to right.
       * @returns boolean.
       * */
    NbLayoutDirectionService.prototype.isLtr = /**
       * Returns true if layout direction set to left to right.
       * @returns boolean.
       * */
    function () {
        return this.direction === exports.NbLayoutDirection.LTR;
    };
    /**
     * Returns true if layout direction set to right to left.
     * @returns boolean.
     * */
    /**
       * Returns true if layout direction set to right to left.
       * @returns boolean.
       * */
    NbLayoutDirectionService.prototype.isRtl = /**
       * Returns true if layout direction set to right to left.
       * @returns boolean.
       * */
    function () {
        return this.direction === exports.NbLayoutDirection.RTL;
    };
    /**
     * Returns current layout direction.
     * @returns NbLayoutDirection.
     * */
    /**
       * Returns current layout direction.
       * @returns NbLayoutDirection.
       * */
    NbLayoutDirectionService.prototype.getDirection = /**
       * Returns current layout direction.
       * @returns NbLayoutDirection.
       * */
    function () {
        return this.direction;
    };
    /**
     * Sets layout direction
     * @param {NbLayoutDirection} direction
     */
    /**
       * Sets layout direction
       * @param {NbLayoutDirection} direction
       */
    NbLayoutDirectionService.prototype.setDirection = /**
       * Sets layout direction
       * @param {NbLayoutDirection} direction
       */
    function (direction) {
        this.direction = direction;
        this.$directionChange.next(direction);
    };
    /**
     * Triggered when direction was changed.
     * @returns Observable<NbLayoutDirection>.
     */
    /**
       * Triggered when direction was changed.
       * @returns Observable<NbLayoutDirection>.
       */
    NbLayoutDirectionService.prototype.onDirectionChange = /**
       * Triggered when direction was changed.
       * @returns Observable<NbLayoutDirection>.
       */
    function () {
        return this.$directionChange.pipe(rxjs_operators.share());
    };
    NbLayoutDirectionService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbLayoutDirectionService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Optional }, { type: i0.Inject, args: [NB_LAYOUT_DIRECTION,] },] },
    ]; };
    return NbLayoutDirectionService;
}());

/**
 * Layout scroll service. Provides information about current scroll position,
 * as well as methods to update position of the scroll.
 *
 * The reason we added this service is that in Nebular there are two scroll modes:
 * - the default mode when scroll is on body
 * - and the `withScroll` mode, when scroll is removed from the body and moved to an element inside of the
 * `nb-layout` component
 */
var NbLayoutScrollService = /** @class */ (function () {
    function NbLayoutScrollService() {
        this.scrollPositionReq$ = new rxjs.Subject();
        this.manualScroll$ = new rxjs.Subject();
        this.scroll$ = new rxjs.Subject();
    }
    /**
     * Returns scroll position
     *
     * @returns {Observable<NbScrollPosition>}
     */
    /**
       * Returns scroll position
       *
       * @returns {Observable<NbScrollPosition>}
       */
    NbLayoutScrollService.prototype.getPosition = /**
       * Returns scroll position
       *
       * @returns {Observable<NbScrollPosition>}
       */
    function () {
        var _this = this;
        return rxjs.Observable.create(function (observer) {
            var listener = new rxjs.Subject();
            listener.subscribe(observer);
            _this.scrollPositionReq$.next({ listener: listener });
            return function () { return listener.complete(); };
        });
    };
    /**
     * Sets scroll position
     *
     * @param {number} x
     * @param {number} y
     */
    /**
       * Sets scroll position
       *
       * @param {number} x
       * @param {number} y
       */
    NbLayoutScrollService.prototype.scrollTo = /**
       * Sets scroll position
       *
       * @param {number} x
       * @param {number} y
       */
    function (x, y) {
        if (x === void 0) { x = null; }
        if (y === void 0) { y = null; }
        this.manualScroll$.next({ x: x, y: y });
    };
    /**
     * Returns a stream of scroll events
     *
     * @returns {Observable<any>}
     */
    /**
       * Returns a stream of scroll events
       *
       * @returns {Observable<any>}
       */
    NbLayoutScrollService.prototype.onScroll = /**
       * Returns a stream of scroll events
       *
       * @returns {Observable<any>}
       */
    function () {
        return this.scroll$.pipe(rxjs_operators.share());
    };
    /**
     * @private
     * @returns Observable<NbScrollPosition>.
     */
    /**
       * @private
       * @returns Observable<NbScrollPosition>.
       */
    NbLayoutScrollService.prototype.onManualScroll = /**
       * @private
       * @returns Observable<NbScrollPosition>.
       */
    function () {
        return this.manualScroll$.pipe(rxjs_operators.share());
    };
    /**
     * @private
     * @returns {Subject<any>}
     */
    /**
       * @private
       * @returns {Subject<any>}
       */
    NbLayoutScrollService.prototype.onGetPosition = /**
       * @private
       * @returns {Subject<any>}
       */
    function () {
        return this.scrollPositionReq$;
    };
    /**
     * @private
     * @param {any} event
     */
    /**
       * @private
       * @param {any} event
       */
    NbLayoutScrollService.prototype.fireScrollChange = /**
       * @private
       * @param {any} event
       */
    function (event) {
        this.scroll$.next(event);
    };
    NbLayoutScrollService.decorators = [
        { type: i0.Injectable },
    ];
    return NbLayoutScrollService;
}());

/**
 * Simple helper service to return Layout dimensions
 * Depending of current Layout scroll mode (default or `withScroll` when scroll is moved to an element
 * inside of the layout) corresponding dimensions will be returns  - of `documentElement` in first case and
 * `.scrollable-container` in the second.
 */
var NbLayoutRulerService = /** @class */ (function () {
    function NbLayoutRulerService() {
        this.contentDimensionsReq$ = new rxjs.Subject();
    }
    /**
     * Content dimensions
     * @returns {Observable<NbLayoutDimensions>}
     */
    /**
       * Content dimensions
       * @returns {Observable<NbLayoutDimensions>}
       */
    NbLayoutRulerService.prototype.getDimensions = /**
       * Content dimensions
       * @returns {Observable<NbLayoutDimensions>}
       */
    function () {
        var _this = this;
        return rxjs.Observable.create(function (observer) {
            var listener = new rxjs.Subject();
            listener.subscribe(observer);
            _this.contentDimensionsReq$.next({ listener: listener });
            return function () { return listener.complete(); };
        });
    };
    /**
     * @private
     * @returns {Subject<any>}
     */
    /**
       * @private
       * @returns {Subject<any>}
       */
    NbLayoutRulerService.prototype.onGetDimensions = /**
       * @private
       * @returns {Subject<any>}
       */
    function () {
        return this.contentDimensionsReq$;
    };
    NbLayoutRulerService.decorators = [
        { type: i0.Injectable },
    ];
    return NbLayoutRulerService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbSharedModule = /** @class */ (function () {
    function NbSharedModule() {
    }
    NbSharedModule.decorators = [
        { type: i0.NgModule, args: [{
                    exports: [
                        i1.CommonModule,
                        _angular_forms.FormsModule,
                        _angular_router.RouterModule,
                    ],
                },] },
    ];
    return NbSharedModule;
}());

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Overrides angular cdk focus trap to keep restore functionality inside trap.
 * */
var NbFocusTrap = /** @class */ (function (_super) {
    __extends(NbFocusTrap, _super);
    function NbFocusTrap(element, checker, ngZone, document, deferAnchors) {
        var _this = _super.call(this, element, checker, ngZone, document, deferAnchors) || this;
        _this.element = element;
        _this.checker = checker;
        _this.ngZone = ngZone;
        _this.document = document;
        _this.savePreviouslyFocusedElement();
        return _this;
    }
    NbFocusTrap.prototype.restoreFocus = function () {
        this.previouslyFocusedElement.focus();
        this.destroy();
    };
    NbFocusTrap.prototype.blurPreviouslyFocusedElement = function () {
        this.previouslyFocusedElement.blur();
    };
    NbFocusTrap.prototype.savePreviouslyFocusedElement = function () {
        this.previouslyFocusedElement = this.document.activeElement;
    };
    return NbFocusTrap;
}(_angular_cdk_a11y.FocusTrap));
var NbFocusTrapFactoryService = /** @class */ (function (_super) {
    __extends(NbFocusTrapFactoryService, _super);
    function NbFocusTrapFactoryService(checker, ngZone, document) {
        var _this = _super.call(this, checker, ngZone, document) || this;
        _this.checker = checker;
        _this.ngZone = ngZone;
        _this.document = document;
        return _this;
    }
    NbFocusTrapFactoryService.prototype.create = function (element, deferCaptureElements) {
        return new NbFocusTrap(element, this.checker, this.ngZone, this.document, deferCaptureElements);
    };
    NbFocusTrapFactoryService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbFocusTrapFactoryService.ctorParameters = function () { return [
        { type: _angular_cdk_a11y.InteractivityChecker, },
        { type: i0.NgZone, },
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
    ]; };
    return NbFocusTrapFactoryService;
}(_angular_cdk_a11y.FocusTrapFactory));

var NbA11yModule = /** @class */ (function () {
    function NbA11yModule() {
    }
    NbA11yModule.forRoot = function () {
        return {
            ngModule: NbA11yModule,
            providers: [NbFocusTrapFactoryService],
        };
    };
    NbA11yModule.decorators = [
        { type: i0.NgModule, args: [{},] },
    ];
    return NbA11yModule;
}());

var __extends$1 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NbPortalDirective = /** @class */ (function (_super) {
    __extends$1(NbPortalDirective, _super);
    function NbPortalDirective() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPortalDirective.decorators = [
        { type: i0.Directive, args: [{ selector: '[nbPortal]' },] },
    ];
    return NbPortalDirective;
}(_angular_cdk_portal.CdkPortal));
var NbPortalOutletDirective = /** @class */ (function (_super) {
    __extends$1(NbPortalOutletDirective, _super);
    function NbPortalOutletDirective() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPortalOutletDirective.decorators = [
        { type: i0.Directive, args: [{ selector: '[nbPortalOutlet]' },] },
    ];
    return NbPortalOutletDirective;
}(_angular_cdk_portal.CdkPortalOutlet));
var NbComponentPortal = /** @class */ (function (_super) {
    __extends$1(NbComponentPortal, _super);
    function NbComponentPortal(component, vcr, injector, cfr) {
        var _this = _super.call(this, component, vcr, injector) || this;
        _this.cfr = cfr;
        return _this;
    }
    return NbComponentPortal;
}(_angular_cdk_portal.ComponentPortal));
/**
 * TODO remove after @angular/cdk@7.0.0 relased
 * */
var NbDomPortalOutlet = /** @class */ (function (_super) {
    __extends$1(NbDomPortalOutlet, _super);
    function NbDomPortalOutlet(/** Element into which the content is projected. */
    outletElement, componentFactoryResolver, appRef, defaultInjector) {
        var _this = _super.call(this, outletElement, componentFactoryResolver, appRef, defaultInjector) || this;
        _this.outletElement = outletElement;
        _this.componentFactoryResolver = componentFactoryResolver;
        _this.appRef = appRef;
        _this.defaultInjector = defaultInjector;
        return _this;
    }
    /**
     * Attach the given ComponentPortal to DOM element using the ComponentFactoryResolver.
     * @param portal Portal to be attached
     * @returns Reference to the created component.
     */
    /**
       * Attach the given ComponentPortal to DOM element using the ComponentFactoryResolver.
       * @param portal Portal to be attached
       * @returns Reference to the created component.
       */
    NbDomPortalOutlet.prototype.attachComponentPortal = /**
       * Attach the given ComponentPortal to DOM element using the ComponentFactoryResolver.
       * @param portal Portal to be attached
       * @returns Reference to the created component.
       */
    function (portal) {
        var _this = this;
        var resolver = portal.cfr || this.componentFactoryResolver;
        var componentFactory = resolver.resolveComponentFactory(portal.component);
        var componentRef;
        // If the portal specifies a ViewContainerRef, we will use that as the attachment point
        // for the component (in terms of Angular's component tree, not rendering).
        // When the ViewContainerRef is missing, we use the factory to create the component directly
        // and then manually attach the view to the application.
        if (portal.viewContainerRef) {
            componentRef = portal.viewContainerRef.createComponent(componentFactory, portal.viewContainerRef.length, portal.injector || portal.viewContainerRef.parentInjector);
            this.setDisposeFn(function () { return componentRef.destroy(); });
        }
        else {
            componentRef = componentFactory.create(portal.injector || this.defaultInjector);
            this.appRef.attachView(componentRef.hostView);
            this.setDisposeFn(function () {
                _this.appRef.detachView(componentRef.hostView);
                componentRef.destroy();
            });
        }
        // At this point the component has been instantiated, so we move it to the location in the DOM
        // where we want it to be rendered.
        this.outletElement.appendChild(this.getComponentRootNode(componentRef));
        return componentRef;
    };
    /** Gets the root HTMLElement for an instantiated component. */
    /** Gets the root HTMLElement for an instantiated component. */
    NbDomPortalOutlet.prototype.getComponentRootNode = /** Gets the root HTMLElement for an instantiated component. */
    function (componentRef) {
        return componentRef.hostView.rootNodes[0];
    };
    return NbDomPortalOutlet;
}(_angular_cdk_portal.DomPortalOutlet));
var NbOverlay = /** @class */ (function (_super) {
    __extends$1(NbOverlay, _super);
    function NbOverlay(/** Scrolling strategies that can be used when creating an overlay. */
    scrollStrategies, overlayContainer, componentFactoryResolver, positionBuilder, keyboardDispatcher, injector, ngZone, document, directionality) {
        var _this = _super.call(this, scrollStrategies, overlayContainer, componentFactoryResolver, positionBuilder, keyboardDispatcher, injector, ngZone, document, directionality) || this;
        _this.scrollStrategies = scrollStrategies;
        _this.overlayContainer = overlayContainer;
        _this.componentFactoryResolver = componentFactoryResolver;
        _this.positionBuilder = positionBuilder;
        _this.keyboardDispatcher = keyboardDispatcher;
        _this.injector = injector;
        _this.ngZone = ngZone;
        _this.document = document;
        _this.directionality = directionality;
        return _this;
    }
    /**
     * Creates an overlay.
     * @param config Configuration applied to the overlay.
     * @returns Reference to the created overlay.
     */
    /**
       * Creates an overlay.
       * @param config Configuration applied to the overlay.
       * @returns Reference to the created overlay.
       */
    NbOverlay.prototype.create = /**
       * Creates an overlay.
       * @param config Configuration applied to the overlay.
       * @returns Reference to the created overlay.
       */
    function (config) {
        var host = this.createHostElement();
        var pane = this.createPaneElement(host);
        var portalOutlet = this.createPortalOutlet(pane);
        var overlayConfig = new _angular_cdk_overlay.OverlayConfig(config);
        overlayConfig.direction = overlayConfig.direction || this.directionality.value;
        return new _angular_cdk_overlay.OverlayRef(portalOutlet, host, pane, overlayConfig, this.ngZone, this.keyboardDispatcher, this.document);
    };
    /**
     * Creates the DOM element for an overlay and appends it to the overlay container.
     * @returns Newly-created pane element
     */
    /**
       * Creates the DOM element for an overlay and appends it to the overlay container.
       * @returns Newly-created pane element
       */
    NbOverlay.prototype.createPaneElement = /**
       * Creates the DOM element for an overlay and appends it to the overlay container.
       * @returns Newly-created pane element
       */
    function (host) {
        var pane = this.document.createElement('div');
        pane.id = "cdk-overlay-" + NbOverlay.nextUniqueId++;
        pane.classList.add('cdk-overlay-pane');
        host.appendChild(pane);
        return pane;
    };
    /**
     * Creates the host element that wraps around an overlay
     * and can be used for advanced positioning.
     * @returns Newly-create host element.
     */
    /**
       * Creates the host element that wraps around an overlay
       * and can be used for advanced positioning.
       * @returns Newly-create host element.
       */
    NbOverlay.prototype.createHostElement = /**
       * Creates the host element that wraps around an overlay
       * and can be used for advanced positioning.
       * @returns Newly-create host element.
       */
    function () {
        var host = this.document.createElement('div');
        this.overlayContainer.getContainerElement().appendChild(host);
        return host;
    };
    /**
     * Create a DomPortalOutlet into which the overlay content can be loaded.
     * @param pane The DOM element to turn into a portal outlet.
     * @returns A portal outlet for the given DOM element.
     */
    /**
       * Create a DomPortalOutlet into which the overlay content can be loaded.
       * @param pane The DOM element to turn into a portal outlet.
       * @returns A portal outlet for the given DOM element.
       */
    NbOverlay.prototype.createPortalOutlet = /**
       * Create a DomPortalOutlet into which the overlay content can be loaded.
       * @param pane The DOM element to turn into a portal outlet.
       * @returns A portal outlet for the given DOM element.
       */
    function (pane) {
        // We have to resolve the ApplicationRef later in order to allow people
        // to use overlay-based providers during app initialization.
        if (!this.appRef) {
            this.appRef = this.injector.get(i0.ApplicationRef);
        }
        return new NbDomPortalOutlet(pane, this.componentFactoryResolver, this.appRef, this.injector);
    };
    NbOverlay.nextUniqueId = 0;
    NbOverlay.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbOverlay.ctorParameters = function () { return [
        { type: _angular_cdk_overlay.ScrollStrategyOptions, },
        { type: _angular_cdk_overlay.OverlayContainer, },
        { type: i0.ComponentFactoryResolver, },
        { type: _angular_cdk_overlay.OverlayPositionBuilder, },
        { type: _angular_cdk_overlay.OverlayKeyboardDispatcher, },
        { type: i0.Injector, },
        { type: i0.NgZone, },
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: _angular_cdk_bidi.Directionality, },
    ]; };
    return NbOverlay;
}(_angular_cdk_overlay.Overlay));
var NbPlatform = /** @class */ (function (_super) {
    __extends$1(NbPlatform, _super);
    function NbPlatform() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPlatform.decorators = [
        { type: i0.Injectable },
    ];
    return NbPlatform;
}(_angular_cdk_platform.Platform));
var NbOverlayPositionBuilder = /** @class */ (function (_super) {
    __extends$1(NbOverlayPositionBuilder, _super);
    function NbOverlayPositionBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbOverlayPositionBuilder.decorators = [
        { type: i0.Injectable },
    ];
    return NbOverlayPositionBuilder;
}(_angular_cdk_overlay.OverlayPositionBuilder));
var NbTemplatePortal = /** @class */ (function (_super) {
    __extends$1(NbTemplatePortal, _super);
    function NbTemplatePortal(template, viewContainerRef, context) {
        return _super.call(this, template, viewContainerRef, context) || this;
    }
    return NbTemplatePortal;
}(_angular_cdk_portal.TemplatePortal));
var NbOverlayContainer = /** @class */ (function (_super) {
    __extends$1(NbOverlayContainer, _super);
    function NbOverlayContainer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbOverlayContainer.ngInjectableDef = i0.defineInjectable({ factory: function NbOverlayContainer_Factory() { return new NbOverlayContainer(i0.inject(i1.DOCUMENT)); }, token: NbOverlayContainer, providedIn: "root" });
    return NbOverlayContainer;
}(_angular_cdk_overlay.OverlayContainer));
var NbFlexibleConnectedPositionStrategy = /** @class */ (function (_super) {
    __extends$1(NbFlexibleConnectedPositionStrategy, _super);
    function NbFlexibleConnectedPositionStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbFlexibleConnectedPositionStrategy;
}(_angular_cdk_overlay.FlexibleConnectedPositionStrategy));
var NbPortalInjector = /** @class */ (function (_super) {
    __extends$1(NbPortalInjector, _super);
    function NbPortalInjector() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NbPortalInjector;
}(_angular_cdk_portal.PortalInjector));
var CDK_MODULES = [_angular_cdk_overlay.OverlayModule, _angular_cdk_portal.PortalModule];
/**
 * This module helps us to keep all angular/cdk deps inside our cdk module via providing aliases.
 * Approach will help us move cdk in separate npm package and refactor nebular/theme code.
 * */
var NbCdkMappingModule = /** @class */ (function () {
    function NbCdkMappingModule() {
    }
    NbCdkMappingModule.forRoot = function () {
        return {
            ngModule: NbCdkMappingModule,
            providers: [
                NbOverlay,
                NbPlatform,
                NbOverlayPositionBuilder,
            ],
        };
    };
    NbCdkMappingModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: CDK_MODULES.slice(),
                    exports: CDK_MODULES.concat([
                        NbPortalDirective,
                        NbPortalOutletDirective,
                    ]),
                    declarations: [NbPortalDirective, NbPortalOutletDirective],
                },] },
    ];
    return NbCdkMappingModule;
}());

var __extends$3 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NbViewportRulerAdapter = /** @class */ (function (_super) {
    __extends$3(NbViewportRulerAdapter, _super);
    function NbViewportRulerAdapter(platform, ngZone, ruler, scroll) {
        var _this = _super.call(this, platform, ngZone) || this;
        _this.ruler = ruler;
        _this.scroll = scroll;
        return _this;
    }
    NbViewportRulerAdapter.prototype.getViewportSize = function () {
        var res;
        /*
            * getDimensions call is really synchronous operation.
            * And we have to conform with the interface of the original service.
            * */
        this.ruler.getDimensions()
            .pipe(rxjs_operators.map(function (dimensions) { return ({ width: dimensions.clientWidth, height: dimensions.clientHeight }); }))
            .subscribe(function (rect) { return res = rect; });
        return res;
    };
    NbViewportRulerAdapter.prototype.getViewportScrollPosition = function () {
        var res;
        /*
            * getPosition call is really synchronous operation.
            * And we have to conform with the interface of the original service.
            * */
        this.scroll.getPosition()
            .pipe(rxjs_operators.map(function (position) { return ({ top: position.y, left: position.x }); }))
            .subscribe(function (position) { return res = position; });
        return res;
    };
    NbViewportRulerAdapter.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbViewportRulerAdapter.ctorParameters = function () { return [
        { type: NbPlatform, },
        { type: i0.NgZone, },
        { type: NbLayoutRulerService, },
        { type: NbLayoutScrollService, },
    ]; };
    return NbViewportRulerAdapter;
}(_angular_cdk_overlay.ViewportRuler));

(function (NbGlobalLogicalPosition) {
    NbGlobalLogicalPosition["TOP_START"] = "top-start";
    NbGlobalLogicalPosition["TOP_END"] = "top-end";
    NbGlobalLogicalPosition["BOTTOM_START"] = "bottom-start";
    NbGlobalLogicalPosition["BOTTOM_END"] = "bottom-end";
})(exports.NbGlobalLogicalPosition || (exports.NbGlobalLogicalPosition = {}));

(function (NbGlobalPhysicalPosition) {
    NbGlobalPhysicalPosition["TOP_RIGHT"] = "top-right";
    NbGlobalPhysicalPosition["TOP_LEFT"] = "top-left";
    NbGlobalPhysicalPosition["BOTTOM_RIGHT"] = "bottom-right";
    NbGlobalPhysicalPosition["BOTTOM_LEFT"] = "bottom-left";
})(exports.NbGlobalPhysicalPosition || (exports.NbGlobalPhysicalPosition = {}));
var NbPositionHelper = /** @class */ (function () {
    function NbPositionHelper(layoutDirection) {
        this.layoutDirection = layoutDirection;
    }
    NbPositionHelper.prototype.toLogicalPosition = function (position) {
        if (Object.values(exports.NbGlobalLogicalPosition).includes(position)) {
            return position;
        }
        if (this.layoutDirection.isLtr()) {
            return this.toLogicalPositionWhenLtr(position);
        }
        else {
            return this.toLogicalPositionWhenRtl(position);
        }
    };
    NbPositionHelper.prototype.toPhysicalPosition = function (position) {
        if (Object.values(exports.NbGlobalPhysicalPosition).includes(position)) {
            return position;
        }
        if (this.layoutDirection.isLtr()) {
            return this.toPhysicalPositionWhenLtr(position);
        }
        else {
            return this.toPhysicalPositionWhenRtl(position);
        }
    };
    NbPositionHelper.prototype.isTopPosition = function (position) {
        var logicalPosition = this.toLogicalPosition(position);
        return logicalPosition === exports.NbGlobalLogicalPosition.TOP_END
            || logicalPosition === exports.NbGlobalLogicalPosition.TOP_START;
    };
    NbPositionHelper.prototype.isRightPosition = function (position) {
        var physicalPosition = this.toPhysicalPosition(position);
        return physicalPosition === exports.NbGlobalPhysicalPosition.TOP_RIGHT
            || physicalPosition === exports.NbGlobalPhysicalPosition.BOTTOM_RIGHT;
    };
    NbPositionHelper.prototype.toLogicalPositionWhenLtr = function (position) {
        switch (position) {
            case exports.NbGlobalPhysicalPosition.TOP_RIGHT:
                return exports.NbGlobalLogicalPosition.TOP_END;
            case exports.NbGlobalPhysicalPosition.TOP_LEFT:
                return exports.NbGlobalLogicalPosition.TOP_START;
            case exports.NbGlobalPhysicalPosition.BOTTOM_RIGHT:
                return exports.NbGlobalLogicalPosition.BOTTOM_END;
            case exports.NbGlobalPhysicalPosition.BOTTOM_LEFT:
                return exports.NbGlobalLogicalPosition.BOTTOM_START;
        }
    };
    NbPositionHelper.prototype.toLogicalPositionWhenRtl = function (position) {
        switch (position) {
            case exports.NbGlobalPhysicalPosition.TOP_RIGHT:
                return exports.NbGlobalLogicalPosition.TOP_START;
            case exports.NbGlobalPhysicalPosition.TOP_LEFT:
                return exports.NbGlobalLogicalPosition.TOP_END;
            case exports.NbGlobalPhysicalPosition.BOTTOM_RIGHT:
                return exports.NbGlobalLogicalPosition.BOTTOM_START;
            case exports.NbGlobalPhysicalPosition.BOTTOM_LEFT:
                return exports.NbGlobalLogicalPosition.BOTTOM_END;
        }
    };
    NbPositionHelper.prototype.toPhysicalPositionWhenLtr = function (position) {
        switch (position) {
            case exports.NbGlobalLogicalPosition.TOP_START:
                return exports.NbGlobalPhysicalPosition.TOP_LEFT;
            case exports.NbGlobalLogicalPosition.TOP_END:
                return exports.NbGlobalPhysicalPosition.TOP_RIGHT;
            case exports.NbGlobalLogicalPosition.BOTTOM_START:
                return exports.NbGlobalPhysicalPosition.BOTTOM_LEFT;
            case exports.NbGlobalLogicalPosition.BOTTOM_END:
                return exports.NbGlobalPhysicalPosition.BOTTOM_RIGHT;
        }
    };
    NbPositionHelper.prototype.toPhysicalPositionWhenRtl = function (position) {
        switch (position) {
            case exports.NbGlobalLogicalPosition.TOP_START:
                return exports.NbGlobalPhysicalPosition.TOP_RIGHT;
            case exports.NbGlobalLogicalPosition.TOP_END:
                return exports.NbGlobalPhysicalPosition.TOP_LEFT;
            case exports.NbGlobalLogicalPosition.BOTTOM_START:
                return exports.NbGlobalPhysicalPosition.BOTTOM_RIGHT;
            case exports.NbGlobalLogicalPosition.BOTTOM_END:
                return exports.NbGlobalPhysicalPosition.BOTTOM_LEFT;
        }
    };
    NbPositionHelper.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbPositionHelper.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
    ]; };
    return NbPositionHelper;
}());

var __extends$2 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

(function (NbAdjustment) {
    NbAdjustment["NOOP"] = "noop";
    NbAdjustment["CLOCKWISE"] = "clockwise";
    NbAdjustment["COUNTERCLOCKWISE"] = "counterclockwise";
    NbAdjustment["VERTICAL"] = "vertical";
    NbAdjustment["HORIZONTAL"] = "horizontal";
})(exports.NbAdjustment || (exports.NbAdjustment = {}));

(function (NbPosition) {
    NbPosition["TOP"] = "top";
    NbPosition["BOTTOM"] = "bottom";
    NbPosition["LEFT"] = "left";
    NbPosition["RIGHT"] = "right";
    NbPosition["START"] = "start";
    NbPosition["END"] = "end";
})(exports.NbPosition || (exports.NbPosition = {}));
var POSITIONS = (_a = {},
    _a[exports.NbPosition.RIGHT] = function (offset) {
        return { originX: 'end', originY: 'center', overlayX: 'start', overlayY: 'center', offsetX: offset };
    },
    _a[exports.NbPosition.BOTTOM] = function (offset) {
        return { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top', offsetY: offset };
    },
    _a[exports.NbPosition.LEFT] = function (offset) {
        return { originX: 'start', originY: 'center', overlayX: 'end', overlayY: 'center', offsetX: -offset };
    },
    _a[exports.NbPosition.TOP] = function (offset) {
        return { originX: 'center', originY: 'top', overlayX: 'center', overlayY: 'bottom', offsetY: -offset };
    },
    _a);
var COUNTER_CLOCKWISE_POSITIONS = [exports.NbPosition.TOP, exports.NbPosition.LEFT, exports.NbPosition.BOTTOM, exports.NbPosition.RIGHT];
var NOOP_POSITIONS = [exports.NbPosition.TOP, exports.NbPosition.BOTTOM, exports.NbPosition.LEFT, exports.NbPosition.RIGHT];
var CLOCKWISE_POSITIONS = [exports.NbPosition.TOP, exports.NbPosition.RIGHT, exports.NbPosition.BOTTOM, exports.NbPosition.LEFT];
var VERTICAL_POSITIONS = [exports.NbPosition.BOTTOM, exports.NbPosition.TOP];
var HORIZONTAL_POSITIONS = [exports.NbPosition.START, exports.NbPosition.END];
function comparePositions(p1, p2) {
    return p1.originX === p2.originX
        && p1.originY === p2.originY
        && p1.overlayX === p2.overlayX
        && p1.overlayY === p2.overlayY;
}
/**
 * The main idea of the adjustable connected strategy is to provide predefined set of positions for your overlay.
 * You have to provide adjustment and appropriate strategy will be chosen in runtime.
 * */
var NbAdjustableConnectedPositionStrategy = /** @class */ (function (_super) {
    __extends$2(NbAdjustableConnectedPositionStrategy, _super);
    function NbAdjustableConnectedPositionStrategy() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this._offset = 15;
        _this.positionChange = _this.positionChanges.pipe(rxjs_operators.map(function (positionChange) { return positionChange.connectionPair; }), rxjs_operators.map(function (connectionPair) {
            return _this.appliedPositions.find(function (_a) {
                var connectedPosition = _a.connectedPosition;
                return comparePositions(connectedPosition, connectionPair);
            }).key;
        }));
        return _this;
    }
    NbAdjustableConnectedPositionStrategy.prototype.attach = function (overlayRef) {
        /**
             * We have to apply positions before attach because super.attach() validates positions and crashes app
             * if no positions provided.
             * */
        this.applyPositions();
        _super.prototype.attach.call(this, overlayRef);
    };
    NbAdjustableConnectedPositionStrategy.prototype.apply = function () {
        this.applyPositions();
        _super.prototype.apply.call(this);
    };
    NbAdjustableConnectedPositionStrategy.prototype.position = function (position) {
        this._position = position;
        return this;
    };
    NbAdjustableConnectedPositionStrategy.prototype.adjustment = function (adjustment) {
        this._adjustment = adjustment;
        return this;
    };
    NbAdjustableConnectedPositionStrategy.prototype.offset = function (offset) {
        this._offset = offset;
        return this;
    };
    NbAdjustableConnectedPositionStrategy.prototype.applyPositions = function () {
        var positions = this.createPositions();
        this.persistChosenPositions(positions);
        this.withPositions(this.appliedPositions.map(function (_a) {
            var connectedPosition = _a.connectedPosition;
            return connectedPosition;
        }));
    };
    NbAdjustableConnectedPositionStrategy.prototype.createPositions = function () {
        var _this = this;
        switch (this._adjustment) {
            case exports.NbAdjustment.NOOP:
                return NOOP_POSITIONS.filter(function (position) { return _this._position === position; });
            case exports.NbAdjustment.CLOCKWISE:
                return this.reorderPreferredPositions(CLOCKWISE_POSITIONS);
            case exports.NbAdjustment.COUNTERCLOCKWISE:
                return this.reorderPreferredPositions(COUNTER_CLOCKWISE_POSITIONS);
            case exports.NbAdjustment.HORIZONTAL:
                return this.reorderPreferredPositions(HORIZONTAL_POSITIONS);
            case exports.NbAdjustment.VERTICAL:
                return this.reorderPreferredPositions(VERTICAL_POSITIONS);
        }
    };
    NbAdjustableConnectedPositionStrategy.prototype.persistChosenPositions = function (positions) {
        var _this = this;
        this.appliedPositions = positions.map(function (position) {
            return ({
                key: position,
                connectedPosition: POSITIONS[position](_this._offset),
            });
        });
    };
    NbAdjustableConnectedPositionStrategy.prototype.reorderPreferredPositions = function (positions) {
        var cpy = positions.slice();
        var startIndex = positions.indexOf(this._position);
        var start = cpy.splice(startIndex);
        return start.concat.apply(start, cpy);
    };
    return NbAdjustableConnectedPositionStrategy;
}(NbFlexibleConnectedPositionStrategy));
var NbGlobalPositionStrategy = /** @class */ (function (_super) {
    __extends$2(NbGlobalPositionStrategy, _super);
    function NbGlobalPositionStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbGlobalPositionStrategy.prototype.position = function (position) {
        switch (position) {
            case exports.NbGlobalLogicalPosition.TOP_START:
                return this.top().left();
            case exports.NbGlobalLogicalPosition.TOP_END:
                return this.top().right();
            case exports.NbGlobalLogicalPosition.BOTTOM_START:
                return this.bottom().left();
            case exports.NbGlobalLogicalPosition.BOTTOM_END:
                return this.bottom().right();
        }
    };
    return NbGlobalPositionStrategy;
}(_angular_cdk_overlay.GlobalPositionStrategy));
var NbPositionBuilderService = /** @class */ (function () {
    function NbPositionBuilderService(document, viewportRuler, platform, positionBuilder) {
        this.document = document;
        this.viewportRuler = viewportRuler;
        this.platform = platform;
        this.positionBuilder = positionBuilder;
    }
    NbPositionBuilderService.prototype.global = function () {
        return new NbGlobalPositionStrategy();
    };
    NbPositionBuilderService.prototype.connectedTo = function (elementRef) {
        return new NbAdjustableConnectedPositionStrategy(elementRef, this.viewportRuler, this.document, this.platform)
            .withFlexibleDimensions(false)
            .withPush(false);
    };
    NbPositionBuilderService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbPositionBuilderService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: NbViewportRulerAdapter, },
        { type: NbPlatform, },
        { type: NbOverlayPositionBuilder, },
    ]; };
    return NbPositionBuilderService;
}());
var _a;

var NbPositionedContainer = /** @class */ (function () {
    function NbPositionedContainer() {
    }
    Object.defineProperty(NbPositionedContainer.prototype, "top", {
        get: function () {
            return this.position === exports.NbPosition.TOP;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbPositionedContainer.prototype, "right", {
        get: function () {
            return this.position === exports.NbPosition.RIGHT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbPositionedContainer.prototype, "bottom", {
        get: function () {
            return this.position === exports.NbPosition.BOTTOM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbPositionedContainer.prototype, "left", {
        get: function () {
            return this.position === exports.NbPosition.LEFT;
        },
        enumerable: true,
        configurable: true
    });
    NbPositionedContainer.propDecorators = {
        "position": [{ type: i0.Input },],
        "top": [{ type: i0.HostBinding, args: ['class.nb-overlay-top',] },],
        "right": [{ type: i0.HostBinding, args: ['class.nb-overlay-right',] },],
        "bottom": [{ type: i0.HostBinding, args: ['class.nb-overlay-bottom',] },],
        "left": [{ type: i0.HostBinding, args: ['class.nb-overlay-left',] },],
    };
    return NbPositionedContainer;
}());
var NbOverlayContainerComponent = /** @class */ (function () {
    function NbOverlayContainerComponent(vcr, injector) {
        this.vcr = vcr;
        this.injector = injector;
    }
    Object.defineProperty(NbOverlayContainerComponent.prototype, "isStringContent", {
        get: function () {
            return !!this.content;
        },
        enumerable: true,
        configurable: true
    });
    NbOverlayContainerComponent.prototype.attachComponentPortal = function (portal) {
        var factory = portal.cfr.resolveComponentFactory(portal.component);
        var injector = this.createChildInjector(portal.cfr);
        return this.vcr.createComponent(factory, null, injector);
    };
    NbOverlayContainerComponent.prototype.attachTemplatePortal = function (portal) {
        return this.vcr.createEmbeddedView(portal.templateRef, portal.context);
    };
    NbOverlayContainerComponent.prototype.attachStringContent = function (content) {
        this.content = content;
    };
    NbOverlayContainerComponent.prototype.createChildInjector = function (cfr) {
        return new NbPortalInjector(this.injector, new WeakMap([
            [i0.ComponentFactoryResolver, cfr],
        ]));
    };
    NbOverlayContainerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-overlay-container',
                    template: "\n    <div *ngIf=\"isStringContent\" class=\"primitive-overlay\">{{ content }}</div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbOverlayContainerComponent.ctorParameters = function () { return [
        { type: i0.ViewContainerRef, },
        { type: i0.Injector, },
    ]; };
    return NbOverlayContainerComponent;
}());

function patch(container, containerContext) {
    Object.assign(container.instance, containerContext);
    container.changeDetectorRef.detectChanges();
    return container;
}
function createContainer(ref, container, context, componentFactoryResolver) {
    var containerRef = ref.attach(new NbComponentPortal(container, null, null, componentFactoryResolver));
    patch(containerRef, context);
    return containerRef;
}
var NbOverlayService = /** @class */ (function () {
    function NbOverlayService(overlay, layoutDirection) {
        this.overlay = overlay;
        this.layoutDirection = layoutDirection;
    }
    Object.defineProperty(NbOverlayService.prototype, "scrollStrategies", {
        get: function () {
            return this.overlay.scrollStrategies;
        },
        enumerable: true,
        configurable: true
    });
    NbOverlayService.prototype.create = function (config) {
        var overlayRef = this.overlay.create(config);
        this.layoutDirection.onDirectionChange()
            .subscribe(function (dir) { return overlayRef.setDirection(dir); });
        return overlayRef;
    };
    NbOverlayService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbOverlayService.ctorParameters = function () { return [
        { type: NbOverlay, },
        { type: NbLayoutDirectionService, },
    ]; };
    return NbOverlayService;
}());

var __extends$4 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Provides nb-layout as overlay container.
 * Container has to be cleared when layout destroys.
 * Another way previous version of the container will be used
 * but it isn't inserted in DOM and exists in memory only.
 * This case important only if you switch between multiple layouts.
 * */
var NbOverlayContainerAdapter = /** @class */ (function (_super) {
    __extends$4(NbOverlayContainerAdapter, _super);
    function NbOverlayContainerAdapter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbOverlayContainerAdapter.prototype.setContainer = function (container) {
        this.container = container;
    };
    NbOverlayContainerAdapter.prototype.clearContainer = function () {
        this.container = null;
        this._containerElement = null;
    };
    NbOverlayContainerAdapter.prototype._createContainer = function () {
        var container = this._document.createElement('div');
        container.classList.add('cdk-overlay-container');
        this.container.appendChild(container);
        this._containerElement = container;
    };
    NbOverlayContainerAdapter.decorators = [
        { type: i0.Injectable },
    ];
    return NbOverlayContainerAdapter;
}(NbOverlayContainer));

var __extends$5 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NbScrollDispatcherAdapter = /** @class */ (function (_super) {
    __extends$5(NbScrollDispatcherAdapter, _super);
    function NbScrollDispatcherAdapter(ngZone, platform, scrollService) {
        var _this = _super.call(this, ngZone, platform) || this;
        _this.scrollService = scrollService;
        return _this;
    }
    NbScrollDispatcherAdapter.prototype.scrolled = function (auditTimeInMs) {
        return this.scrollService.onScroll();
    };
    NbScrollDispatcherAdapter.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbScrollDispatcherAdapter.ctorParameters = function () { return [
        { type: i0.NgZone, },
        { type: NbPlatform, },
        { type: NbLayoutScrollService, },
    ]; };
    return NbScrollDispatcherAdapter;
}(_angular_cdk_overlay.ScrollDispatcher));

var __extends$6 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var NbBlockScrollStrategyAdapter = /** @class */ (function (_super) {
    __extends$6(NbBlockScrollStrategyAdapter, _super);
    function NbBlockScrollStrategyAdapter(ruler, document) {
        return _super.call(this, ruler, document) || this;
    }
    NbBlockScrollStrategyAdapter.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbBlockScrollStrategyAdapter.ctorParameters = function () { return [
        { type: NbViewportRulerAdapter, },
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
    ]; };
    return NbBlockScrollStrategyAdapter;
}(_angular_cdk_overlay.BlockScrollStrategy));

var NbCdkAdapterModule = /** @class */ (function () {
    function NbCdkAdapterModule() {
    }
    NbCdkAdapterModule.forRoot = function () {
        return {
            ngModule: NbCdkAdapterModule,
            providers: [
                NbViewportRulerAdapter,
                NbOverlayContainerAdapter,
                NbBlockScrollStrategyAdapter,
                { provide: _angular_cdk_overlay.OverlayContainer, useExisting: NbOverlayContainerAdapter },
                { provide: _angular_cdk_overlay.ScrollDispatcher, useClass: NbScrollDispatcherAdapter },
            ],
        };
    };
    NbCdkAdapterModule.decorators = [
        { type: i0.NgModule, args: [{},] },
    ];
    return NbCdkAdapterModule;
}());

var NbOverlayModule = /** @class */ (function () {
    function NbOverlayModule() {
    }
    NbOverlayModule.forRoot = function () {
        return {
            ngModule: NbOverlayModule,
            providers: [
                NbPositionBuilderService,
                NbOverlayService,
                NbPositionHelper
            ].concat(NbCdkMappingModule.forRoot().providers, NbCdkAdapterModule.forRoot().providers, NbA11yModule.forRoot().providers),
        };
    };
    NbOverlayModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbCdkMappingModule,
                        NbSharedModule,
                    ],
                    declarations: [NbOverlayContainerComponent],
                    exports: [
                        NbCdkMappingModule,
                        NbCdkAdapterModule,
                        NbOverlayContainerComponent,
                    ],
                },] },
    ];
    return NbOverlayModule;
}());

var __extends$7 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

(function (NbTrigger) {
    NbTrigger["CLICK"] = "click";
    NbTrigger["HOVER"] = "hover";
    NbTrigger["HINT"] = "hint";
    NbTrigger["FOCUS"] = "focus";
})(exports.NbTrigger || (exports.NbTrigger = {}));
/**
 * Provides entity with two event stream: show and hide.
 * Each stream provides different events depends on implementation.
 * We have three main trigger strategies: click, hint and hover.
 * */
/**
 * TODO maybe we have to use renderer.listen instead of observableFromEvent?
 * Renderer provides capability use it in service worker, ssr and so on.
 * */
var NbTriggerStrategy = /** @class */ (function () {
    function NbTriggerStrategy(document, host, container) {
        this.document = document;
        this.host = host;
        this.container = container;
    }
    return NbTriggerStrategy;
}());
/**
 * Creates show and hide event streams.
 * Fires toggle event when the click was performed on the host element.
 * Fires close event when the click was performed on the document but
 * not on the host or container.
 * */
var NbClickTriggerStrategy = /** @class */ (function (_super) {
    __extends$7(NbClickTriggerStrategy, _super);
    function NbClickTriggerStrategy(document, host, container) {
        var _this = _super.call(this, document, host, container) || this;
        _this.document = document;
        _this.host = host;
        _this.container = container;
        _this.show = new rxjs.Subject();
        _this.show$ = _this.show.asObservable();
        _this.hide = new rxjs.Subject();
        _this.hide$ = rxjs.merge(_this.hide.asObservable(), rxjs.fromEvent(_this.document, 'click')
            .pipe(rxjs_operators.filter(function (event) { return _this.isNotHostOrContainer(event); })));
        _this.subscribeOnHostClick();
        return _this;
    }
    NbClickTriggerStrategy.prototype.subscribeOnHostClick = function () {
        var _this = this;
        rxjs.fromEvent(this.host, 'click')
            .subscribe(function (event) {
            if (_this.isContainerExists()) {
                _this.hide.next(event);
            }
            else {
                _this.show.next(event);
            }
        });
    };
    NbClickTriggerStrategy.prototype.isContainerExists = function () {
        return !!this.container();
    };
    NbClickTriggerStrategy.prototype.isNotHostOrContainer = function (event) {
        return !this.host.contains(event.target)
            && this.isContainerExists()
            && !this.container().location.nativeElement.contains(event.target);
    };
    return NbClickTriggerStrategy;
}(NbTriggerStrategy));
/**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element and stops out of the host and popover container.
 * */
var NbHoverTriggerStrategy = /** @class */ (function (_super) {
    __extends$7(NbHoverTriggerStrategy, _super);
    function NbHoverTriggerStrategy() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.show$ = rxjs.fromEvent(_this.host, 'mouseenter')
            .pipe(rxjs_operators.delay(100), rxjs_operators.takeUntil(rxjs.fromEvent(_this.host, 'mouseleave')), rxjs_operators.repeat());
        _this.hide$ = rxjs.fromEvent(_this.host, 'mouseleave')
            .pipe(rxjs_operators.switchMap(function () {
            return rxjs.fromEvent(_this.document, 'mousemove')
                .pipe(rxjs_operators.debounceTime(100), rxjs_operators.takeWhile(function () { return !!_this.container(); }), rxjs_operators.filter(function (event) {
                return !_this.host.contains(event.target)
                    && !_this.container().location.nativeElement.contains(event.target);
            }));
        }));
        return _this;
    }
    return NbHoverTriggerStrategy;
}(NbTriggerStrategy));
/**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element.
 * */
var NbHintTriggerStrategy = /** @class */ (function (_super) {
    __extends$7(NbHintTriggerStrategy, _super);
    function NbHintTriggerStrategy() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.show$ = rxjs.fromEvent(_this.host, 'mouseenter')
            .pipe(rxjs_operators.delay(100), rxjs_operators.takeUntil(rxjs.fromEvent(_this.host, 'mouseleave')), rxjs_operators.repeat());
        _this.hide$ = rxjs.fromEvent(_this.host, 'mouseleave');
        return _this;
    }
    return NbHintTriggerStrategy;
}(NbTriggerStrategy));
/**
 * Creates show and hide event streams.
 * Fires open event when a focus is on the host element and stay over at least 100 milliseconds.
 * Fires close event when the focus leaves the host element.
 * */
var NbFocusTriggerStrategy = /** @class */ (function (_super) {
    __extends$7(NbFocusTriggerStrategy, _super);
    function NbFocusTriggerStrategy() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.focusOut$ = rxjs.fromEvent(_this.host, 'focusout')
            .pipe(rxjs_operators.switchMap(function () {
            return rxjs.fromEvent(_this.document, 'focusin')
                .pipe(rxjs_operators.takeWhile(function () { return !!_this.container(); }), rxjs_operators.filter(function (event) { return _this.isNotOnHostOrContainer(event); }));
        }));
        _this.clickIn$ = rxjs.fromEvent(_this.host, 'click')
            .pipe(rxjs_operators.filter(function () { return !_this.container(); }));
        _this.clickOut$ = rxjs.fromEvent(_this.document, 'click')
            .pipe(rxjs_operators.filter(function () { return !!_this.container(); }), rxjs_operators.filter(function (event) { return _this.isNotOnHostOrContainer(event); }));
        _this.tabKeyPress$ = rxjs.fromEvent(_this.document, 'keydown')
            .pipe(rxjs_operators.filter(function (event) { return event.keyCode === 9; }), rxjs_operators.filter(function () { return !!_this.container(); }));
        _this.show$ = rxjs.merge(rxjs.fromEvent(_this.host, 'focusin'), _this.clickIn$)
            .pipe(rxjs_operators.filter(function () { return !_this.container(); }), rxjs_operators.debounceTime(100), rxjs_operators.takeUntil(rxjs.fromEvent(_this.host, 'focusout')), rxjs_operators.repeat());
        _this.hide$ = rxjs.merge(_this.focusOut$, _this.tabKeyPress$, _this.clickOut$);
        return _this;
    }
    NbFocusTriggerStrategy.prototype.isNotOnHostOrContainer = function (event) {
        return !this.isOnHost(event) && !this.isOnContainer(event);
    };
    NbFocusTriggerStrategy.prototype.isOnHost = function (_a) {
        var target = _a.target;
        return this.host.contains(target);
    };
    NbFocusTriggerStrategy.prototype.isOnContainer = function (_a) {
        var target = _a.target;
        return this.container() && this.container().location.nativeElement.contains(target);
    };
    return NbFocusTriggerStrategy;
}(NbTriggerStrategy));
var NbTriggerStrategyBuilder = /** @class */ (function () {
    function NbTriggerStrategyBuilder() {
    }
    NbTriggerStrategyBuilder.prototype.document = function (document) {
        this._document = document;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.trigger = function (trigger$$1) {
        this._trigger = trigger$$1;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.host = function (host) {
        this._host = host;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.container = function (container) {
        this._container = container;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.build = function () {
        switch (this._trigger) {
            case exports.NbTrigger.CLICK:
                return new NbClickTriggerStrategy(this._document, this._host, this._container);
            case exports.NbTrigger.HINT:
                return new NbHintTriggerStrategy(this._document, this._host, this._container);
            case exports.NbTrigger.HOVER:
                return new NbHoverTriggerStrategy(this._document, this._host, this._container);
            case exports.NbTrigger.FOCUS:
                return new NbFocusTriggerStrategy(this._document, this._host, this._container);
            default:
                throw new Error('Trigger have to be provided');
        }
    };
    return NbTriggerStrategyBuilder;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
function nbWindowFactory() {
    return window;
}
var NbThemeModule = /** @class */ (function () {
    function NbThemeModule() {
    }
    // TODO: check the options (throw exception?)
    /**
     * Main Theme Module
     *
     * @param nbThemeOptions {NbThemeOptions} Main theme options
     * @param nbJSThemes {NbJSThemeOptions[]} List of JS Themes, will be merged with default themes
     * @param nbMediaBreakpoints {NbMediaBreakpoint} Available media breakpoints
     * @param layoutDirection {NbLayoutDirection} Layout direction
     *
     * @returns {ModuleWithProviders}
     */
    // TODO: check the options (throw exception?)
    /**
       * Main Theme Module
       *
       * @param nbThemeOptions {NbThemeOptions} Main theme options
       * @param nbJSThemes {NbJSThemeOptions[]} List of JS Themes, will be merged with default themes
       * @param nbMediaBreakpoints {NbMediaBreakpoint} Available media breakpoints
       * @param layoutDirection {NbLayoutDirection} Layout direction
       *
       * @returns {ModuleWithProviders}
       */
    NbThemeModule.forRoot = 
    // TODO: check the options (throw exception?)
    /**
       * Main Theme Module
       *
       * @param nbThemeOptions {NbThemeOptions} Main theme options
       * @param nbJSThemes {NbJSThemeOptions[]} List of JS Themes, will be merged with default themes
       * @param nbMediaBreakpoints {NbMediaBreakpoint} Available media breakpoints
       * @param layoutDirection {NbLayoutDirection} Layout direction
       *
       * @returns {ModuleWithProviders}
       */
    function (nbThemeOptions, nbJSThemes, nbMediaBreakpoints, layoutDirection) {
        return {
            ngModule: NbThemeModule,
            providers: [
                { provide: NB_THEME_OPTIONS, useValue: nbThemeOptions || {} },
                { provide: NB_BUILT_IN_JS_THEMES, useValue: BUILT_IN_THEMES },
                { provide: NB_JS_THEMES, useValue: nbJSThemes || [] },
                { provide: NB_MEDIA_BREAKPOINTS, useValue: nbMediaBreakpoints || DEFAULT_MEDIA_BREAKPOINTS },
                { provide: NB_WINDOW, useFactory: nbWindowFactory },
                { provide: NB_DOCUMENT, useExisting: i1.DOCUMENT },
                NbJSThemesRegistry,
                NbThemeService,
                NbMediaBreakpointsService,
                NbSpinnerService,
                { provide: NB_LAYOUT_DIRECTION, useValue: layoutDirection || exports.NbLayoutDirection.LTR },
                NbLayoutDirectionService,
                NbLayoutScrollService,
                NbLayoutRulerService
            ].concat(NbOverlayModule.forRoot().providers),
        };
    };
    NbThemeModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        i1.CommonModule,
                    ],
                    exports: [],
                },] },
    ];
    return NbThemeModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Component intended to be used within the `<nb-card>` component.
 * It adds styles for a preset header section.
 *
 * @styles
 *
 * card-header-font-family:
 * card-header-font-size:
 * card-header-font-weight:
 * card-header-fg:
 * card-header-fg-heading:
 * card-header-active-bg:
 * card-header-active-fg:
 * card-header-disabled-bg:
 * card-header-primary-bg:
 * card-header-info-bg:
 * card-header-success-bg:
 * card-header-warning-bg:
 * card-header-danger-bg:
 */
var NbCardHeaderComponent = /** @class */ (function () {
    function NbCardHeaderComponent() {
    }
    NbCardHeaderComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-card-header',
                    template: "<ng-content></ng-content>",
                },] },
    ];
    return NbCardHeaderComponent;
}());
/**
 * Component intended to be used within  the `<nb-card>` component.
 * Adds styles for a preset body section.
 */
var NbCardBodyComponent = /** @class */ (function () {
    function NbCardBodyComponent() {
    }
    NbCardBodyComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-card-body',
                    template: "<ng-content></ng-content>",
                },] },
    ];
    return NbCardBodyComponent;
}());
/**
 * Component intended to be used within  the `<nb-card>` component.
 * Adds styles for a preset footer section.
 */
var NbCardFooterComponent = /** @class */ (function () {
    function NbCardFooterComponent() {
    }
    NbCardFooterComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-card-footer',
                    template: "<ng-content></ng-content>",
                },] },
    ];
    return NbCardFooterComponent;
}());
/**
 * Basic content container component.
 *
 * Basic card example:
 * @stacked-example(Showcase, card/card-showcase.component)
 *
 * Basic card configuration:
 *
 * ```html
 * <nb-card>
 *   <nb-card-body>
 *     Card
 *   </nb-card-body>
 * </nb-card>
 * ```
 *
 * ### Installation
 *
 * Import `NbCardModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbCardModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Card with header and footer:
 * @stacked-example(With Header & Footer, card/card-full.component)
 *
 * Most of the time main card content goes to `nb-card-body`,
 * so it is styled and aligned in accordance with the header and footer.
 * In case you need a higher level of control, you can pass contend directly to `nb-card`,
 * so `nb-card-body` styling will not be applied.
 *
 * Consider an example with `nb-list` component:
 * @stacked-example(Showcase, card/card-without-body.component)
 *
 * Colored cards could be simply configured by providing a `status` property:
 * @stacked-example(Colored Card, card/card-colors.component)
 *
 * It is also possible to assign an `accent` property for a slight card highlight
 * as well as combine it with `status`:
 * @stacked-example(Accent Card, card/card-accents.component)
 *
 * @additional-example(Multiple Sizes, card/card-sizes.component)
 *
 * @styles
 *
 * card-line-height:
 * card-font-weight:
 * card-fg-text:
 * card-bg:
 * card-height-xxsmall:
 * card-height-xsmall:
 * card-height-small:
 * card-height-medium:
 * card-height-large:
 * card-height-xlarge:
 * card-height-xxlarge:
 * card-shadow:
 * card-border-radius:
 * card-padding:
 * card-margin:
 * card-separator:
 *
 */
var NbCardComponent = /** @class */ (function () {
    function NbCardComponent() {
    }
    Object.defineProperty(NbCardComponent.prototype, "xxsmall", {
        get: function () {
            return this.size === NbCardComponent.SIZE_XXSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbCardComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "small", {
        get: function () {
            return this.size === NbCardComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "medium", {
        get: function () {
            return this.size === NbCardComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "large", {
        get: function () {
            return this.size === NbCardComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "xlarge", {
        get: function () {
            return this.size === NbCardComponent.SIZE_XLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "xxlarge", {
        get: function () {
            return this.size === NbCardComponent.SIZE_XXLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "active", {
        get: function () {
            return this.status === NbCardComponent.STATUS_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "disabled", {
        get: function () {
            return this.status === NbCardComponent.STATUS_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "primary", {
        get: function () {
            return this.status === NbCardComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "info", {
        get: function () {
            return this.status === NbCardComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "success", {
        get: function () {
            return this.status === NbCardComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "warning", {
        get: function () {
            return this.status === NbCardComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "danger", {
        get: function () {
            return this.status === NbCardComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "hasAccent", {
        get: function () {
            return this.accent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "primaryAccent", {
        get: function () {
            return this.accent === NbCardComponent.ACCENT_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "infoAccent", {
        get: function () {
            return this.accent === NbCardComponent.ACCENT_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "successAccent", {
        get: function () {
            return this.accent === NbCardComponent.ACCENT_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "warningAccent", {
        get: function () {
            return this.accent === NbCardComponent.ACCENT_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "dangerAccent", {
        get: function () {
            return this.accent === NbCardComponent.ACCENT_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "activeAccent", {
        get: function () {
            return this.accent === NbCardComponent.ACCENT_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "disabledAccent", {
        get: function () {
            return this.accent === NbCardComponent.ACCENT_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "setSize", {
        set: /**
           * Card size, available sizes:
           * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "setStatus", {
        set: /**
           * Card status (adds specific styles):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCardComponent.prototype, "setAccent", {
        set: /**
           * Card accent (color of the top border):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.accent = val;
        },
        enumerable: true,
        configurable: true
    });
    NbCardComponent.SIZE_XXSMALL = 'xxsmall';
    NbCardComponent.SIZE_XSMALL = 'xsmall';
    NbCardComponent.SIZE_SMALL = 'small';
    NbCardComponent.SIZE_MEDIUM = 'medium';
    NbCardComponent.SIZE_LARGE = 'large';
    NbCardComponent.SIZE_XLARGE = 'xlarge';
    NbCardComponent.SIZE_XXLARGE = 'xxlarge';
    NbCardComponent.STATUS_ACTIVE = 'active';
    NbCardComponent.STATUS_DISABLED = 'disabled';
    NbCardComponent.STATUS_PRIMARY = 'primary';
    NbCardComponent.STATUS_INFO = 'info';
    NbCardComponent.STATUS_SUCCESS = 'success';
    NbCardComponent.STATUS_WARNING = 'warning';
    NbCardComponent.STATUS_DANGER = 'danger';
    NbCardComponent.ACCENT_ACTIVE = 'active';
    NbCardComponent.ACCENT_DISABLED = 'disabled';
    NbCardComponent.ACCENT_PRIMARY = 'primary';
    NbCardComponent.ACCENT_INFO = 'info';
    NbCardComponent.ACCENT_SUCCESS = 'success';
    NbCardComponent.ACCENT_WARNING = 'warning';
    NbCardComponent.ACCENT_DANGER = 'danger';
    NbCardComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-card',
                    styles: [":host{display:flex;flex-direction:column} "],
                    template: "\n    <ng-content select=\"nb-card-header\"></ng-content>\n    <ng-content select=\"nb-card-body\"></ng-content>\n    <ng-content></ng-content>\n    <ng-content select=\"nb-card-footer\"></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbCardComponent.propDecorators = {
        "xxsmall": [{ type: i0.HostBinding, args: ['class.xxsmall-card',] },],
        "xsmall": [{ type: i0.HostBinding, args: ['class.xsmall-card',] },],
        "small": [{ type: i0.HostBinding, args: ['class.small-card',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium-card',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large-card',] },],
        "xlarge": [{ type: i0.HostBinding, args: ['class.xlarge-card',] },],
        "xxlarge": [{ type: i0.HostBinding, args: ['class.xxlarge-card',] },],
        "active": [{ type: i0.HostBinding, args: ['class.active-card',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled-card',] },],
        "primary": [{ type: i0.HostBinding, args: ['class.primary-card',] },],
        "info": [{ type: i0.HostBinding, args: ['class.info-card',] },],
        "success": [{ type: i0.HostBinding, args: ['class.success-card',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.warning-card',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.danger-card',] },],
        "hasAccent": [{ type: i0.HostBinding, args: ['class.accent',] },],
        "primaryAccent": [{ type: i0.HostBinding, args: ['class.accent-primary',] },],
        "infoAccent": [{ type: i0.HostBinding, args: ['class.accent-info',] },],
        "successAccent": [{ type: i0.HostBinding, args: ['class.accent-success',] },],
        "warningAccent": [{ type: i0.HostBinding, args: ['class.accent-warning',] },],
        "dangerAccent": [{ type: i0.HostBinding, args: ['class.accent-danger',] },],
        "activeAccent": [{ type: i0.HostBinding, args: ['class.accent-active',] },],
        "disabledAccent": [{ type: i0.HostBinding, args: ['class.accent-disabled',] },],
        "setSize": [{ type: i0.Input, args: ['size',] },],
        "setStatus": [{ type: i0.Input, args: ['status',] },],
        "setAccent": [{ type: i0.Input, args: ['accent',] },],
    };
    return NbCardComponent;
}());

/**
 *
 * Reveal card example:
 * @stacked-example(My example, reveal-card/reveal-card-showcase.component)
 *
 * As a content Reveal card accepts two instances of `nb-card` - for front and back sides.
 *
 * Basic reveal card configuration:
 *
 * ```html
 * <nb-reveal-card>
 *   <nb-card-front>
 *     <nb-card>
 *       <nb-card-body>
 *         Front
 *       </nb-card-body>
 *     </nb-card>
 *   </nb-card-front>
 *   <nb-card-back>
 *     <nb-card>
 *       <nb-card-body>
 *         Back
 *       </nb-card-body>
 *     </nb-card>
 *   </nb-card-back>
 * </nb-reveal-card>
 * ```
 *
 * ### Installation
 *
 * Import `NbCardModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbCardModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Reveal Card with header and footer:
 * @stacked-example(With Header & Footer, reveal-card/reveal-card-full.component)
 *
 * Colored reveal-cards could be simply configured by providing a `status` property:
 * @stacked-example(Colored Card, reveal-card/reveal-card-colors.component)
 *
 * It is also possible to assign an `accent` property for a slight card highlight
 * as well as combine it with `status`:
 * @stacked-example(Accent Card, reveal-card/reveal-card-accents.component)
 *
 * @additional-example(Multiple Sizes, reveal-card/reveal-card-sizes.component)
 */
var NbRevealCardComponent = /** @class */ (function () {
    function NbRevealCardComponent() {
        /**
           * Reveal state
           * @type boolean
           */
        this.revealed = false;
        /**
           * Show/hide toggle button to be able to control toggle from your code
           * @type {boolean}
           */
        this.showToggleButton = true;
    }
    NbRevealCardComponent.prototype.toggle = function () {
        this.revealed = !this.revealed;
    };
    NbRevealCardComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-reveal-card',
                    styles: [":host{display:block;position:relative;overflow:hidden}:host.revealed .second-card-container{top:0;transition:none}:host.revealed .second-card-container /deep/ nb-card-back{top:0}:host.revealed .reveal-button{transform:none}:host /deep/ nb-card-front{display:block;height:100%}:host .second-card-container{position:absolute;top:100%;right:0;left:0;overflow:hidden;transition:top 0s 0.5s}:host .second-card-container /deep/ nb-card-back{position:absolute;left:0;top:100%;width:100%;transition:top 0.5s}:host .reveal-button{cursor:pointer;position:absolute;right:0;bottom:0;transform:rotate(180deg);transition:transform 0.3s} "],
                    template: "\n    <ng-content select=\"nb-card-front\"></ng-content>\n    <div class=\"second-card-container\">\n      <ng-content select=\"nb-card-back\"></ng-content>\n    </div>\n    <a *ngIf=\"showToggleButton\" class=\"reveal-button\" (click)=\"toggle()\">\n      <i class=\"nb-arrow-dropdown\" aria-hidden=\"true\"></i>\n    </a>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbRevealCardComponent.propDecorators = {
        "revealed": [{ type: i0.Input }, { type: i0.HostBinding, args: ['class.revealed',] },],
        "showToggleButton": [{ type: i0.Input },],
    };
    return NbRevealCardComponent;
}());

/**
 *
 * Flip card example:
 * @stacked-example(Showcase, flip-card/flip-card-showcase.component)
 *
 * As a content Flip card accepts two instances of `nb-card` - for front and back sides.
 *
 * Basic flip card configuration:
 *
 * ```html
 * <nb-flip-card>
 *   <nb-card-front>
 *     <nb-card>
 *       <nb-card-body>
 *         Front
 *       </nb-card-body>
 *     </nb-card>
 *   </nb-card-front>
 *   <nb-card-back>
 *     <nb-card>
 *       <nb-card-body>
 *         Back
 *       </nb-card-body>
 *     </nb-card>
 *   </nb-card-back>
 * </nb-flip-card>
 * ```
 *
 * ### Installation
 *
 * Import `NbCardModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbCardModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Flip Card with header and footer:
 * @stacked-example(With Header & Footer, flip-card/flip-card-full.component.ts)
 *
 * Colored flip-cards could be simply configured by providing a `status` property:
 * @stacked-example(Colored Card, flip-card/flip-card-colors.component)
 *
 * It is also possible to assign an `accent` property for a slight card highlight
 * as well as combine it with `status`:
 * @stacked-example(Accent Card, flip-card/flip-card-accents.component)
 *
 * @additional-example(Multiple Sizes, flip-card/flip-card-sizes.component)
 *
 */
var NbFlipCardComponent = /** @class */ (function () {
    function NbFlipCardComponent() {
        /**
           * Flip state
           * @type boolean
           */
        this.flipped = false;
        /**
           * Show/hide toggle button to be able to control toggle from your code
           * @type {boolean}
           */
        this.showToggleButton = true;
    }
    NbFlipCardComponent.prototype.toggle = function () {
        this.flipped = !this.flipped;
    };
    NbFlipCardComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-flip-card',
                    styles: [":host{display:block;perspective:1200px;position:relative}:host-context(.flipped) .flipcard-body{transform:rotateY(-180deg)}:host-context(.flipped) .flipcard-body .front-container{opacity:0;transition:opacity 0s 0.25s;backface-visibility:hidden}:host-context(.flipped) .flipcard-body .front-container .flip-button{opacity:0;z-index:-1}:host-context(.flipped) .flipcard-body .back-container{backface-visibility:visible}.flipcard-body{display:flex;transition:transform 0.5s;transform-style:preserve-3d}.flipcard-body .front-container,.flipcard-body .back-container{flex:1}.flipcard-body .front-container .flip-button,.flipcard-body .back-container .flip-button{cursor:pointer;position:absolute;right:0;bottom:0;opacity:1;transition:opacity 0s 0.15s}.flipcard-body .front-container{backface-visibility:visible;transition:opacity 0s 0.2s}.flipcard-body .back-container{backface-visibility:hidden;transform:rotateY(180deg)} "],
                    template: "\n    <div class=\"flipcard-body\">\n      <div class=\"front-container\">\n        <ng-content select=\"nb-card-front\"></ng-content>\n        <a *ngIf=\"showToggleButton\" class=\"flip-button\" (click)=\"toggle()\">\n          <i class=\"nb-arrow-dropleft\" aria-hidden=\"true\"></i>\n        </a>\n      </div>\n      <div class=\"back-container\">\n        <ng-content select=\"nb-card-back\"></ng-content>\n        <a *ngIf=\"showToggleButton\" class=\"flip-button\" (click)=\"toggle()\">\n          <i class=\"nb-arrow-dropleft\" aria-hidden=\"true\"></i>\n        </a>\n      </div>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbFlipCardComponent.propDecorators = {
        "flipped": [{ type: i0.Input }, { type: i0.HostBinding, args: ['class.flipped',] },],
        "showToggleButton": [{ type: i0.Input },],
    };
    return NbFlipCardComponent;
}());

/**
 * Component intended to be used within the `<nb-flip-card>` and `<nb-reveal-card>` components.
 *
 * Use it as a container for the front card.
 */
var NbCardFrontComponent = /** @class */ (function () {
    function NbCardFrontComponent() {
    }
    NbCardFrontComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-card-front',
                    template: '<ng-content select="nb-card"></ng-content>',
                },] },
    ];
    return NbCardFrontComponent;
}());
/**
 * Component intended to be used within the `<nb-flip-card>` and `<nb-reveal-card>` components.
 *
 * Use it as a container for the back card.
 */
var NbCardBackComponent = /** @class */ (function () {
    function NbCardBackComponent() {
    }
    NbCardBackComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-card-back',
                    template: '<ng-content select="nb-card"></ng-content>',
                },] },
    ];
    return NbCardBackComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_CARD_COMPONENTS = [
    NbCardComponent,
    NbCardBodyComponent,
    NbCardFooterComponent,
    NbCardHeaderComponent,
    NbRevealCardComponent,
    NbFlipCardComponent,
    NbCardFrontComponent,
    NbCardBackComponent,
];
var NbCardModule = /** @class */ (function () {
    function NbCardModule() {
    }
    NbCardModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: NB_CARD_COMPONENTS.slice(),
                    exports: NB_CARD_COMPONENTS.slice(),
                },] },
    ];
    return NbCardModule;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbDateService = /** @class */ (function () {
    function NbDateService() {
        this.DAYS_IN_WEEK = 7;
    }
    NbDateService.prototype.setLocale = function (locale) {
        this.locale = locale;
    };
    /**
     * Checks if the date is between the start date and the end date.
     * */
    /**
       * Checks if the date is between the start date and the end date.
       * */
    NbDateService.prototype.isBetween = /**
       * Checks if the date is between the start date and the end date.
       * */
    function (date, start, end) {
        return this.compareDates(date, start) > 0 && this.compareDates(date, end) < 0;
    };
    
    /**
     * Checks is two dates have the same day.
     * */
    /**
       * Checks is two dates have the same day.
       * */
    NbDateService.prototype.isSameDaySafe = /**
       * Checks is two dates have the same day.
       * */
    function (date1, date2) {
        return date1 && date2 && this.isSameDay(date1, date2);
    };
    
    /**
     * Checks is two dates have the same month.
     * */
    /**
       * Checks is two dates have the same month.
       * */
    NbDateService.prototype.isSameMonthSafe = /**
       * Checks is two dates have the same month.
       * */
    function (date1, date2) {
        return date1 && date2 && this.isSameMonth(date1, date2);
    };
    /**
     * Checks is two dates have the same year.
     * */
    /**
       * Checks is two dates have the same year.
       * */
    NbDateService.prototype.isSameYearSafe = /**
       * Checks is two dates have the same year.
       * */
    function (date1, date2) {
        return date1 && date2 && this.isSameYear(date1, date2);
    };
    return NbDateService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var batch = function (target, batchSize, offset) {
    if (offset === void 0) { offset = 0; }
    return target.reduce(function (res, item, index) {
        var chunkIndex = Math.floor((index + offset) / batchSize);
        if (!res[chunkIndex]) {
            res[chunkIndex] = [];
        }
        res[chunkIndex].push(item);
        return res;
    }, []);
};
/**
 * returns array with numbers from zero to bound.
 * */
var range = function (bound, producer) {
    if (producer === void 0) { producer = function (i) { return i; }; }
    var arr = [];
    for (var i = 0; i < bound; i++) {
        arr.push(producer(i));
    }
    return arr;
};

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarMonthModelService = /** @class */ (function () {
    function NbCalendarMonthModelService(dateService) {
        this.dateService = dateService;
    }
    NbCalendarMonthModelService.prototype.createDaysGrid = function (activeMonth, boundingMonth) {
        if (boundingMonth === void 0) { boundingMonth = true; }
        var weeks = this.createDates(activeMonth);
        return this.withBoundingMonths(weeks, activeMonth, boundingMonth);
    };
    NbCalendarMonthModelService.prototype.createDates = function (activeMonth) {
        var days = this.createDateRangeForMonth(activeMonth);
        var startOfWeekDayDiff = this.getStartOfWeekDayDiff(activeMonth);
        return batch(days, this.dateService.DAYS_IN_WEEK, startOfWeekDayDiff);
    };
    NbCalendarMonthModelService.prototype.withBoundingMonths = function (weeks, activeMonth, boundingMonth) {
        var withBoundingMonths = weeks;
        if (this.isShouldAddPrevBoundingMonth(withBoundingMonths)) {
            withBoundingMonths = this.addPrevBoundingMonth(withBoundingMonths, activeMonth, boundingMonth);
        }
        if (this.isShouldAddNextBoundingMonth(withBoundingMonths)) {
            withBoundingMonths = this.addNextBoundingMonth(withBoundingMonths, activeMonth, boundingMonth);
        }
        return withBoundingMonths;
    };
    NbCalendarMonthModelService.prototype.addPrevBoundingMonth = function (weeks, activeMonth, boundingMonth) {
        var firstWeek = weeks.shift();
        var requiredItems = this.dateService.DAYS_IN_WEEK - firstWeek.length;
        firstWeek.unshift.apply(firstWeek, this.createPrevBoundingDays(activeMonth, boundingMonth, requiredItems));
        return [firstWeek].concat(weeks);
    };
    NbCalendarMonthModelService.prototype.addNextBoundingMonth = function (weeks, activeMonth, boundingMonth) {
        var lastWeek = weeks.pop();
        var requiredItems = this.dateService.DAYS_IN_WEEK - lastWeek.length;
        lastWeek.push.apply(lastWeek, this.createNextBoundingDays(activeMonth, boundingMonth, requiredItems));
        return weeks.concat([lastWeek]);
    };
    NbCalendarMonthModelService.prototype.createPrevBoundingDays = function (activeMonth, boundingMonth, requiredItems) {
        var month = this.dateService.addMonth(activeMonth, -1);
        var daysInMonth = this.dateService.getNumberOfDaysInMonth(month);
        return this.createDateRangeForMonth(month)
            .slice(daysInMonth - requiredItems)
            .map(function (date) { return boundingMonth ? date : null; });
    };
    NbCalendarMonthModelService.prototype.createNextBoundingDays = function (activeMonth, boundingMonth, requiredItems) {
        var month = this.dateService.addMonth(activeMonth, 1);
        return this.createDateRangeForMonth(month)
            .slice(0, requiredItems)
            .map(function (date) { return boundingMonth ? date : null; });
    };
    NbCalendarMonthModelService.prototype.getStartOfWeekDayDiff = function (date) {
        var startOfMonth = this.dateService.getMonthStart(date);
        return this.getWeekStartDiff(startOfMonth);
    };
    NbCalendarMonthModelService.prototype.getWeekStartDiff = function (date) {
        return (7 - this.dateService.getFirstDayOfWeek() + this.dateService.getDayOfWeek(date)) % 7;
    };
    NbCalendarMonthModelService.prototype.isShouldAddPrevBoundingMonth = function (weeks) {
        return weeks[0].length < this.dateService.DAYS_IN_WEEK;
    };
    NbCalendarMonthModelService.prototype.isShouldAddNextBoundingMonth = function (weeks) {
        return weeks[weeks.length - 1].length < this.dateService.DAYS_IN_WEEK;
    };
    NbCalendarMonthModelService.prototype.createDateRangeForMonth = function (date) {
        var _this = this;
        var daysInMonth = this.dateService.getNumberOfDaysInMonth(date);
        return range(daysInMonth, function (i) {
            var year = _this.dateService.getYear(date);
            var month = _this.dateService.getMonth(date);
            return _this.dateService.createDate(year, month, i + 1);
        });
    };
    NbCalendarMonthModelService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbCalendarMonthModelService.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    return NbCalendarMonthModelService;
}());

var __extends$8 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbNativeDateService` is basic implementation of `NbDateService` using
 * native js date objects and angular localization services.
 * */
var NbNativeDateService = /** @class */ (function (_super) {
    __extends$8(NbNativeDateService, _super);
    function NbNativeDateService(locale) {
        var _this = _super.call(this) || this;
        _this.setLocale(locale);
        return _this;
    }
    NbNativeDateService.prototype.setLocale = function (locale) {
        _super.prototype.setLocale.call(this, locale);
        this.datePipe = new i1.DatePipe(locale);
    };
    NbNativeDateService.prototype.isValidDateString = function (date, format) {
        return !isNaN(this.parse(date, format).getTime());
    };
    NbNativeDateService.prototype.today = function () {
        return new Date();
    };
    NbNativeDateService.prototype.getDate = function (date) {
        return date.getDate();
    };
    NbNativeDateService.prototype.getMonth = function (date) {
        return date.getMonth();
    };
    NbNativeDateService.prototype.getYear = function (date) {
        return date.getFullYear();
    };
    NbNativeDateService.prototype.getDayOfWeek = function (date) {
        return date.getDay();
    };
    /**
     * returns first day of the week, it can be 1 if week starts from monday
     * and 0 if from sunday and so on.
     * */
    /**
       * returns first day of the week, it can be 1 if week starts from monday
       * and 0 if from sunday and so on.
       * */
    NbNativeDateService.prototype.getFirstDayOfWeek = /**
       * returns first day of the week, it can be 1 if week starts from monday
       * and 0 if from sunday and so on.
       * */
    function () {
        return i1.getLocaleFirstDayOfWeek(this.locale);
    };
    NbNativeDateService.prototype.getMonthName = function (date, style$$1) {
        if (style$$1 === void 0) { style$$1 = i1.TranslationWidth.Abbreviated; }
        var index = date.getMonth();
        return this.getMonthNameByIndex(index, style$$1);
    };
    NbNativeDateService.prototype.getMonthNameByIndex = function (index, style$$1) {
        if (style$$1 === void 0) { style$$1 = i1.TranslationWidth.Abbreviated; }
        return i1.getLocaleMonthNames(this.locale, i1.FormStyle.Format, style$$1)[index];
    };
    NbNativeDateService.prototype.getDayOfWeekNames = function () {
        return i1.getLocaleDayNames(this.locale, i1.FormStyle.Format, i1.TranslationWidth.Short);
    };
    NbNativeDateService.prototype.format = function (date, format) {
        return this.datePipe.transform(date, format);
    };
    /**
     * We haven't got capability to parse date using formatting without third party libraries.
     * */
    /**
       * We haven't got capability to parse date using formatting without third party libraries.
       * */
    NbNativeDateService.prototype.parse = /**
       * We haven't got capability to parse date using formatting without third party libraries.
       * */
    function (date, format) {
        return new Date(Date.parse(date));
    };
    NbNativeDateService.prototype.addDay = function (date, num) {
        return this.createDate(date.getFullYear(), date.getMonth(), date.getDate() + num);
    };
    NbNativeDateService.prototype.addMonth = function (date, num) {
        return this.createDate(date.getFullYear(), date.getMonth() + num, date.getDate());
    };
    NbNativeDateService.prototype.addYear = function (date, num) {
        return this.createDate(date.getFullYear() + num, date.getMonth(), date.getDate());
    };
    NbNativeDateService.prototype.clone = function (date) {
        return new Date(date.getTime());
    };
    NbNativeDateService.prototype.compareDates = function (date1, date2) {
        return date1.getTime() - date2.getTime();
    };
    NbNativeDateService.prototype.createDate = function (year, month, date) {
        var result = new Date(year, month, date);
        // We need to correct for the fact that JS native Date treats years in range [0, 99] as
        // abbreviations for 19xx.
        if (year >= 0 && year < 100) {
            result.setFullYear(result.getFullYear() - 1900);
        }
        return result;
    };
    NbNativeDateService.prototype.getMonthEnd = function (date) {
        return this.createDate(date.getFullYear(), date.getMonth() + 1, 0);
    };
    NbNativeDateService.prototype.getMonthStart = function (date) {
        return this.createDate(date.getFullYear(), date.getMonth(), 1);
    };
    NbNativeDateService.prototype.getNumberOfDaysInMonth = function (date) {
        return this.getMonthEnd(date).getDate();
    };
    NbNativeDateService.prototype.getYearEnd = function (date) {
        return this.createDate(date.getFullYear(), 11, 31);
    };
    NbNativeDateService.prototype.getYearStart = function (date) {
        return this.createDate(date.getFullYear(), 0, 1);
    };
    NbNativeDateService.prototype.isSameDay = function (date1, date2) {
        return this.isSameMonth(date1, date2) &&
            date1.getDate() === date2.getDate();
    };
    NbNativeDateService.prototype.isSameMonth = function (date1, date2) {
        return this.isSameYear(date1, date2) &&
            date1.getMonth() === date2.getMonth();
    };
    NbNativeDateService.prototype.isSameYear = function (date1, date2) {
        return date1.getFullYear() === date2.getFullYear();
    };
    NbNativeDateService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbNativeDateService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [i0.LOCALE_ID,] },] },
    ]; };
    return NbNativeDateService;
}(NbDateService));

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarHeaderComponent = /** @class */ (function () {
    function NbCalendarHeaderComponent(directionService, dateService) {
        this.directionService = directionService;
        this.dateService = dateService;
        this.navigateToday = new i0.EventEmitter();
        this.date = this.dateService.today();
    }
    Object.defineProperty(NbCalendarHeaderComponent.prototype, "isRtl", {
        get: function () {
            return this.directionService.isRtl();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarHeaderComponent.prototype, "isLtr", {
        get: function () {
            return this.directionService.isLtr();
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarHeaderComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-header',
                    template: "\n    <div class=\"header\">\n      <span class=\"title\" (click)=\"navigateToday.emit()\">\n        {{ date | nbCalendarDate }}\n        <i [ngClass]=\"{ 'nb-arrow-dropright': isLtr, 'nb-arrow-dropleft': isRtl }\"></i>\n      </span>\n      <span class=\"sub-title\">Today</span>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbCalendarHeaderComponent.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
        { type: NbDateService, },
    ]; };
    NbCalendarHeaderComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "navigateToday": [{ type: i0.Output },],
    };
    return NbCalendarHeaderComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarDayCellComponent = /** @class */ (function () {
    function NbCalendarDayCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new i0.EventEmitter(true);
    }
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameDaySafe(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "boundingMonth", {
        get: function () {
            return !this.dateService.isSameMonthSafe(this.date, this.visibleDate);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "selected", {
        get: function () {
            return this.dateService.isSameDaySafe(this.date, this.selectedValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "empty", {
        get: function () {
            return !this.date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax() || this.dontFitFilter();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "day", {
        get: function () {
            return this.date && this.dateService.getDate(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarDayCellComponent.prototype.onClick = function () {
        if (this.disabled || this.empty) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarDayCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.date, this.min) < 0;
    };
    NbCalendarDayCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.date, this.max) > 0;
    };
    NbCalendarDayCellComponent.prototype.dontFitFilter = function () {
        return this.date && this.filter && !this.filter(this.date);
    };
    NbCalendarDayCellComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-day-cell',
                    template: '{{ day }}',
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'day-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarDayCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarDayCellComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "selectedValue": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "select": [{ type: i0.Output },],
        "today": [{ type: i0.HostBinding, args: ['class.today',] },],
        "boundingMonth": [{ type: i0.HostBinding, args: ['class.bounding-month',] },],
        "selected": [{ type: i0.HostBinding, args: ['class.selected',] },],
        "empty": [{ type: i0.HostBinding, args: ['class.empty',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbCalendarDayCellComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

(function (NbCalendarViewMode) {
    NbCalendarViewMode["YEAR"] = "year";
    NbCalendarViewMode["MONTH"] = "month";
    NbCalendarViewMode["DATE"] = "date";
})(exports.NbCalendarViewMode || (exports.NbCalendarViewMode = {}));

(function (NbCalendarSize) {
    NbCalendarSize["MEDIUM"] = "medium";
    NbCalendarSize["LARGE"] = "large";
})(exports.NbCalendarSize || (exports.NbCalendarSize = {}));

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarYearCellComponent = /** @class */ (function () {
    function NbCalendarYearCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new i0.EventEmitter(true);
    }
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "selected", {
        get: function () {
            return this.dateService.isSameYearSafe(this.date, this.selectedValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameYearSafe(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "year", {
        get: function () {
            return this.dateService.getYear(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarYearCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarYearCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.yearEnd(), this.min) < 0;
    };
    NbCalendarYearCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.yearStart(), this.max) > 0;
    };
    NbCalendarYearCellComponent.prototype.yearStart = function () {
        return this.dateService.getYearStart(this.date);
    };
    NbCalendarYearCellComponent.prototype.yearEnd = function () {
        return this.dateService.getYearEnd(this.date);
    };
    NbCalendarYearCellComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-year-cell',
                    template: "{{ year }}",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'year-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarYearCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarYearCellComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "selectedValue": [{ type: i0.Input },],
        "select": [{ type: i0.Output },],
        "selected": [{ type: i0.HostBinding, args: ['class.selected',] },],
        "today": [{ type: i0.HostBinding, args: ['class.today',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbCalendarYearCellComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var defaultYearCount = 20;
var NbCalendarYearPickerComponent = /** @class */ (function () {
    function NbCalendarYearPickerComponent(dateService) {
        this.dateService = dateService;
        this.cellComponent = NbCalendarYearCellComponent;
        this.size = exports.NbCalendarSize.MEDIUM;
        this.yearChange = new i0.EventEmitter();
    }
    Object.defineProperty(NbCalendarYearPickerComponent.prototype, "_cellComponent", {
        set: function (cellComponent) {
            if (cellComponent) {
                this.cellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearPickerComponent.prototype, "medium", {
        get: function () {
            return this.size === exports.NbCalendarSize.MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearPickerComponent.prototype, "large", {
        get: function () {
            return this.size === exports.NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarYearPickerComponent.prototype.ngOnChanges = function () {
        this.initYears();
    };
    NbCalendarYearPickerComponent.prototype.initYears = function () {
        var _this = this;
        var selectedYear = this.dateService.getYear(this.year);
        var startYear = Math.ceil(selectedYear - defaultYearCount / 2);
        var years = range(defaultYearCount).map(function (i) { return _this.createYearDateByIndex(i + startYear); });
        this.years = batch(years, 4);
    };
    NbCalendarYearPickerComponent.prototype.onSelect = function (year) {
        this.yearChange.emit(year);
    };
    NbCalendarYearPickerComponent.prototype.createYearDateByIndex = function (i) {
        return this.dateService.createDate(i, this.dateService.getMonth(this.year), this.dateService.getDate(this.year));
    };
    NbCalendarYearPickerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-year-picker',
                    template: "\n    <nb-calendar-picker\n      [data]=\"years\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      [selectedValue]=\"date\"\n      [visibleDate]=\"year\"\n      [cellComponent]=\"cellComponent\"\n      (select)=\"onSelect($event)\">\n    </nb-calendar-picker>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarYearPickerComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarYearPickerComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "_cellComponent": [{ type: i0.Input, args: ['cellComponent',] },],
        "size": [{ type: i0.Input },],
        "year": [{ type: i0.Input },],
        "yearChange": [{ type: i0.Output },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large',] },],
    };
    return NbCalendarYearPickerComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarMonthCellComponent = /** @class */ (function () {
    function NbCalendarMonthCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new i0.EventEmitter(true);
    }
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "selected", {
        get: function () {
            return this.dateService.isSameMonthSafe(this.date, this.selectedValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameMonthSafe(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "month", {
        get: function () {
            return this.dateService.getMonthName(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarMonthCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarMonthCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.monthEnd(), this.min) < 0;
    };
    NbCalendarMonthCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.monthStart(), this.max) > 0;
    };
    NbCalendarMonthCellComponent.prototype.monthStart = function () {
        return this.dateService.getMonthStart(this.date);
    };
    NbCalendarMonthCellComponent.prototype.monthEnd = function () {
        return this.dateService.getMonthEnd(this.date);
    };
    NbCalendarMonthCellComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-month-cell',
                    template: "{{ month }}",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'month-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarMonthCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarMonthCellComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "selectedValue": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "select": [{ type: i0.Output },],
        "selected": [{ type: i0.HostBinding, args: ['class.selected',] },],
        "today": [{ type: i0.HostBinding, args: ['class.today',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbCalendarMonthCellComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarMonthPickerComponent = /** @class */ (function () {
    function NbCalendarMonthPickerComponent(dateService) {
        this.dateService = dateService;
        this.size = exports.NbCalendarSize.MEDIUM;
        this.monthChange = new i0.EventEmitter();
        this.cellComponent = NbCalendarMonthCellComponent;
    }
    Object.defineProperty(NbCalendarMonthPickerComponent.prototype, "_cellComponent", {
        set: function (cellComponent) {
            if (cellComponent) {
                this.cellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthPickerComponent.prototype, "medium", {
        get: function () {
            return this.size === exports.NbCalendarSize.MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthPickerComponent.prototype, "large", {
        get: function () {
            return this.size === exports.NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarMonthPickerComponent.prototype.ngOnInit = function () {
        this.initMonths();
    };
    NbCalendarMonthPickerComponent.prototype.initMonths = function () {
        var _this = this;
        var months = range(12).map(function (i) { return _this.createMonthDateByIndex(i); });
        this.months = batch(months, 4);
    };
    NbCalendarMonthPickerComponent.prototype.onSelect = function (month) {
        this.monthChange.emit(month);
    };
    NbCalendarMonthPickerComponent.prototype.createMonthDateByIndex = function (i) {
        return this.dateService.createDate(this.dateService.getYear(this.month), i, this.dateService.getDate(this.month));
    };
    NbCalendarMonthPickerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-month-picker',
                    template: "\n    <nb-calendar-picker\n      [data]=\"months\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      [selectedValue]=\"month\"\n      [cellComponent]=\"cellComponent\"\n      (select)=\"onSelect($event)\">\n    </nb-calendar-picker>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarMonthPickerComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarMonthPickerComponent.propDecorators = {
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "size": [{ type: i0.Input },],
        "month": [{ type: i0.Input },],
        "monthChange": [{ type: i0.Output },],
        "_cellComponent": [{ type: i0.Input, args: ['cellComponent',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large',] },],
    };
    return NbCalendarMonthPickerComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Provides capability pick days.
 * */
var NbCalendarDayPickerComponent = /** @class */ (function () {
    function NbCalendarDayPickerComponent(monthModel) {
        this.monthModel = monthModel;
        /**
           * Defines if we should render previous and next months
           * in the current month view.
           * */
        this.boundingMonths = true;
        this.cellComponent = NbCalendarDayCellComponent;
        /**
           * Size of the component.
           * Can be 'medium' which is default or 'large'.
           * */
        this.size = exports.NbCalendarSize.MEDIUM;
        /**
           * Fires newly selected date.
           * */
        this.dateChange = new i0.EventEmitter();
    }
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "setCellComponent", {
        set: /**
           * Custom day cell component. Have to implement `NbCalendarCell` interface.
           * */
        function (cellComponent) {
            if (cellComponent) {
                this.cellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "medium", {
        get: function () {
            return this.size === exports.NbCalendarSize.MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "large", {
        get: function () {
            return this.size === exports.NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarDayPickerComponent.prototype.ngOnChanges = function (_a) {
        var visibleDate = _a.visibleDate;
        if (visibleDate) {
            this.weeks = this.monthModel.createDaysGrid(this.visibleDate, this.boundingMonths);
        }
    };
    NbCalendarDayPickerComponent.prototype.onSelect = function (day) {
        this.dateChange.emit(day);
    };
    NbCalendarDayPickerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-day-picker',
                    styles: [" :host { display: block; } "],
                    template: "\n    <nb-calendar-days-names></nb-calendar-days-names>\n    <nb-calendar-picker\n      [data]=\"weeks\"\n      [visibleDate]=\"visibleDate\"\n      [selectedValue]=\"date\"\n      [cellComponent]=\"cellComponent\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      (select)=\"onSelect($event)\">\n    </nb-calendar-picker>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarDayPickerComponent.ctorParameters = function () { return [
        { type: NbCalendarMonthModelService, },
    ]; };
    NbCalendarDayPickerComponent.propDecorators = {
        "visibleDate": [{ type: i0.Input },],
        "boundingMonths": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "setCellComponent": [{ type: i0.Input, args: ['cellComponent',] },],
        "size": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
        "dateChange": [{ type: i0.Output },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large',] },],
    };
    return NbCalendarDayPickerComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarNavigationComponent = /** @class */ (function () {
    function NbCalendarNavigationComponent() {
        this.changeMode = new i0.EventEmitter(true);
    }
    NbCalendarNavigationComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-navigation',
                    styles: ["\n    :host {\n      display: flex;\n      justify-content: center;\n    }\n\n    :host button {\n      height: 3.125rem;\n    }\n  "],
                    template: "\n    <button nbButton (click)=\"changeMode.emit()\">\n      {{ date | nbCalendarDate }}\n    </button>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarNavigationComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "changeMode": [{ type: i0.Output },],
    };
    return NbCalendarNavigationComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarPageableNavigationComponent = /** @class */ (function () {
    function NbCalendarPageableNavigationComponent(directionService) {
        this.directionService = directionService;
        this.changeMode = new i0.EventEmitter();
        this.next = new i0.EventEmitter();
        this.prev = new i0.EventEmitter();
    }
    Object.defineProperty(NbCalendarPageableNavigationComponent.prototype, "isRtl", {
        get: function () {
            return this.directionService.isRtl();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarPageableNavigationComponent.prototype, "isLtr", {
        get: function () {
            return this.directionService.isLtr();
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarPageableNavigationComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-pageable-navigation',
                    styles: [":host{display:flex;align-items:center;justify-content:space-between}:host i{font-size:1.5rem;cursor:pointer} "],
                    template: "\n    <i [ngClass]=\"{'nb-arrow-left': isLtr, 'nb-arrow-right': isRtl }\" (click)=\"prev.emit()\"></i>\n    <nb-calendar-navigation [date]=\"date\" (changeMode)=\"changeMode.emit()\"></nb-calendar-navigation>\n    <i [ngClass]=\"{'nb-arrow-right': isLtr, 'nb-arrow-left': isRtl }\" (click)=\"next.emit()\"></i>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbCalendarPageableNavigationComponent.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
    ]; };
    NbCalendarPageableNavigationComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "changeMode": [{ type: i0.Output },],
        "next": [{ type: i0.Output },],
        "prev": [{ type: i0.Output },],
    };
    return NbCalendarPageableNavigationComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarDaysNamesComponent = /** @class */ (function () {
    function NbCalendarDaysNamesComponent(dateService) {
        this.dateService = dateService;
    }
    NbCalendarDaysNamesComponent.prototype.ngOnInit = function () {
        var days = this.createDaysNames();
        this.days = this.shiftStartOfWeek(days);
    };
    NbCalendarDaysNamesComponent.prototype.createDaysNames = function () {
        return this.dateService.getDayOfWeekNames()
            .map(this.markIfHoliday);
    };
    NbCalendarDaysNamesComponent.prototype.shiftStartOfWeek = function (days) {
        for (var i = 0; i < this.dateService.getFirstDayOfWeek(); i++) {
            days.push(days.shift());
        }
        return days;
    };
    NbCalendarDaysNamesComponent.prototype.markIfHoliday = function (name, i) {
        return { name: name, isHoliday: i % 6 === 0 };
    };
    NbCalendarDaysNamesComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-days-names',
                    styles: [":host{display:flex;justify-content:space-between}:host .day{display:flex;align-items:center;justify-content:center;margin:1px} "],
                    template: "\n    <div class=\"day\" *ngFor=\"let day of days\" [class.holiday]=\"day.isHoliday\">{{ day.name }}</div>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarDaysNamesComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    return NbCalendarDaysNamesComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarPickerRowComponent = /** @class */ (function () {
    function NbCalendarPickerRowComponent(cfr) {
        this.cfr = cfr;
        this.select = new i0.EventEmitter();
    }
    NbCalendarPickerRowComponent.prototype.ngOnChanges = function () {
        var _this = this;
        var factory = this.cfr.resolveComponentFactory(this.component);
        this.containerRef.clear();
        this.row.forEach(function (date) {
            var component = _this.containerRef.createComponent(factory);
            _this.patchWithContext(component.instance, date);
            component.changeDetectorRef.detectChanges();
        });
    };
    NbCalendarPickerRowComponent.prototype.patchWithContext = function (component, date) {
        component.visibleDate = this.visibleDate;
        component.selectedValue = this.selectedValue;
        component.date = date;
        component.min = this.min;
        component.max = this.max;
        component.filter = this.filter;
        component.select.subscribe(this.select.emit.bind(this.select));
    };
    NbCalendarPickerRowComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-picker-row',
                    styles: ["\n    :host {\n      display: flex;\n      justify-content: space-between;\n    }\n  "],
                    template: '<ng-template></ng-template>',
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarPickerRowComponent.ctorParameters = function () { return [
        { type: i0.ComponentFactoryResolver, },
    ]; };
    NbCalendarPickerRowComponent.propDecorators = {
        "row": [{ type: i0.Input },],
        "selectedValue": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "component": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "select": [{ type: i0.Output },],
        "containerRef": [{ type: i0.ViewChild, args: [i0.TemplateRef, { read: i0.ViewContainerRef },] },],
    };
    return NbCalendarPickerRowComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarPickerComponent = /** @class */ (function () {
    function NbCalendarPickerComponent() {
        this.select = new i0.EventEmitter();
    }
    NbCalendarPickerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-picker',
                    template: "\n    <nb-calendar-picker-row\n      *ngFor=\"let row of data\"\n      [row]=\"row\"\n      [visibleDate]=\"visibleDate\"\n      [selectedValue]=\"selectedValue\"\n      [component]=\"cellComponent\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      (select)=\"select.emit($event)\">\n    </nb-calendar-picker-row>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarPickerComponent.propDecorators = {
        "data": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "selectedValue": [{ type: i0.Input },],
        "cellComponent": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "select": [{ type: i0.Output },],
    };
    return NbCalendarPickerComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarDatePipe = /** @class */ (function () {
    function NbCalendarDatePipe(dateService) {
        this.dateService = dateService;
    }
    NbCalendarDatePipe.prototype.transform = function (date) {
        return date ? this.dateService.getMonthName(date) + " " + this.dateService.getYear(date) : '';
    };
    NbCalendarDatePipe.decorators = [
        { type: i0.Pipe, args: [{ name: 'nbCalendarDate' },] },
    ];
    /** @nocollapse */
    NbCalendarDatePipe.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    return NbCalendarDatePipe;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
function convertToBoolProperty(val) {
    if (typeof val === 'string') {
        val = val.toLowerCase().trim();
        return (val === 'true' || val === '');
    }
    return !!val;
}

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Basic button component.
 *
 * Default button size is `medium` and status color is `primary`:
 * @stacked-example(Button Showcase, button/button-showcase.component)
 *
 * ```html
 * <button nbButton></button>
 * ```
 * ### Installation
 *
 * Import `NbButtonModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbButtonModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Buttons are available in multiple colors using `status` property:
 * @stacked-example(Button Colors, button/button-colors.component.html)
 *
 * There are three button sizes:
 *
 * @stacked-example(Button Sizes, button/button-sizes.component.html)
 *
 * And two additional style types - `outline`:
 *
 * @stacked-example(Outline Buttons, button/button-outline.component.html)
 *
 * and `hero`:
 *
 * @stacked-example(Button Colors, button/button-hero.component.html)
 *
 * Buttons available in different shapes, which could be combined with the other properties:
 * @stacked-example(Button Shapes, button/button-shapes.component)
 *
 * `nbButton` could be applied to the following selectors - `button`, `input[type="button"]`, `input[type="submit"]`
 * and `a`:
 * @stacked-example(Button Elements, button/button-types.component.html)
 *
 * Button can be made `fullWidth`:
 * @stacked-example(Full Width Button, button/button-full-width.component.html)
 *
 * @styles
 *
 * btn-fg:
 * btn-font-family:
 * btn-line-height:
 * btn-disabled-opacity:
 * btn-cursor:
 * btn-primary-bg:
 * btn-secondary-bg:
 * btn-info-bg:
 * btn-success-bg:
 * btn-warning-bg:
 * btn-danger-bg:
 * btn-secondary-border:
 * btn-secondary-border-width:
 * btn-padding-y-lg:
 * btn-padding-x-lg:
 * btn-font-size-lg:
 * btn-padding-y-md:
 * btn-padding-x-md:
 * btn-font-size-md:
 * btn-padding-y-sm:
 * btn-padding-x-sm:
 * btn-font-size-sm:
 * btn-padding-y-xs:
 * btn-padding-x-xs:
 * btn-font-size-xs:
 * btn-border-radius:
 * btn-rectangle-border-radius:
 * btn-semi-round-border-radius:
 * btn-round-border-radius:
 * btn-hero-shadow:
 * btn-hero-text-shadow:
 * btn-hero-bevel-size:
 * btn-hero-glow-size:
 * btn-hero-primary-glow-size:
 * btn-hero-success-glow-size:
 * btn-hero-warning-glow-size:
 * btn-hero-info-glow-size:
 * btn-hero-danger-glow-size:
 * btn-hero-secondary-glow-size:
 * btn-hero-degree:
 * btn-hero-primary-degree:
 * btn-hero-success-degree:
 * btn-hero-warning-degree:
 * btn-hero-info-degree:
 * btn-hero-danger-degree:
 * btn-hero-secondary-degree:
 * btn-hero-border-radius:
 * btn-outline-fg:
 * btn-outline-hover-fg:
 * btn-outline-focus-fg:
 */
var NbButtonComponent = /** @class */ (function () {
    function NbButtonComponent() {
        this.fullWidth = false;
    }
    Object.defineProperty(NbButtonComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "small", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "medium", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "large", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "primary", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "info", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "success", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "warning", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "danger", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "rectangle", {
        get: function () {
            return this.shape === NbButtonComponent.SHAPE_RECTANGLE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "round", {
        get: function () {
            return this.shape === NbButtonComponent.SHAPE_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "semiRound", {
        get: function () {
            return this.shape === NbButtonComponent.SHAPE_SEMI_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "tabbable", {
        get: function () {
            return this.disabled ? '-1' : '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setSize", {
        set: /**
           * Button size, available sizes:
           * `xxsmall`, `xsmall`, `small`, `medium`, `large`
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setStatus", {
        set: /**
           * Button status (adds specific styles):
           * `primary`, `info`, `success`, `warning`, `danger`
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setShape", {
        set: /**
           * Button shapes: `rectangle`, `round`, `semi-round`
           * @param {string} val
           */
        function (val) {
            this.shape = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setHero", {
        set: /**
           * Adds `hero` styles
           * @param {boolean} val
           */
        function (val) {
            this.hero = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setDisabled", {
        set: /**
           * Disables the button
           * @param {boolean} val
           */
        function (val) {
            this.disabled = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setFullWidth", {
        set: /**
           * If set element will fill its container
           * @param {boolean}
           */
        function (value) {
            this.fullWidth = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setOutline", {
        set: /**
           * Adds `outline` styles
           * @param {boolean} val
           */
        function (val) {
            this.outline = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbButtonComponent.prototype.onClick = function (event) {
        if (this.disabled) {
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    };
    NbButtonComponent.SIZE_XSMALL = 'xsmall';
    NbButtonComponent.SIZE_SMALL = 'small';
    NbButtonComponent.SIZE_MEDIUM = 'medium';
    NbButtonComponent.SIZE_LARGE = 'large';
    NbButtonComponent.STATUS_PRIMARY = 'primary';
    NbButtonComponent.STATUS_INFO = 'info';
    NbButtonComponent.STATUS_SUCCESS = 'success';
    NbButtonComponent.STATUS_WARNING = 'warning';
    NbButtonComponent.STATUS_DANGER = 'danger';
    NbButtonComponent.SHAPE_RECTANGLE = 'rectangle';
    NbButtonComponent.SHAPE_ROUND = 'round';
    NbButtonComponent.SHAPE_SEMI_ROUND = 'semi-round';
    NbButtonComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'button[nbButton],a[nbButton],input[type="button"][nbButton],input[type="submit"][nbButton]',
                    styles: [":host{text-transform:uppercase;letter-spacing:0.4px;border:2px solid transparent;transition:none;cursor:pointer;-webkit-appearance:none;-moz-appearance:none;text-align:center;text-decoration:none;display:inline-block;white-space:nowrap;vertical-align:middle;user-select:none}:host:hover,:host:focus{text-decoration:none}:host.btn-full-width{width:100%} "],
                    template: "\n    <ng-content></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbButtonComponent.propDecorators = {
        "xsmall": [{ type: i0.HostBinding, args: ['class.btn-xsmall',] },],
        "small": [{ type: i0.HostBinding, args: ['class.btn-small',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.btn-medium',] },],
        "large": [{ type: i0.HostBinding, args: ['class.btn-large',] },],
        "primary": [{ type: i0.HostBinding, args: ['class.btn-primary',] },],
        "info": [{ type: i0.HostBinding, args: ['class.btn-info',] },],
        "success": [{ type: i0.HostBinding, args: ['class.btn-success',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.btn-warning',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.btn-danger',] },],
        "rectangle": [{ type: i0.HostBinding, args: ['class.btn-rectangle',] },],
        "round": [{ type: i0.HostBinding, args: ['class.btn-round',] },],
        "semiRound": [{ type: i0.HostBinding, args: ['class.btn-semi-round',] },],
        "hero": [{ type: i0.HostBinding, args: ['class.btn-hero',] },],
        "outline": [{ type: i0.HostBinding, args: ['class.btn-outline',] },],
        "disabled": [{ type: i0.HostBinding, args: ['attr.aria-disabled',] }, { type: i0.HostBinding, args: ['class.btn-disabled',] },],
        "tabbable": [{ type: i0.HostBinding, args: ['attr.tabindex',] },],
        "fullWidth": [{ type: i0.HostBinding, args: ['class.btn-full-width',] },],
        "setSize": [{ type: i0.Input, args: ['size',] },],
        "setStatus": [{ type: i0.Input, args: ['status',] },],
        "setShape": [{ type: i0.Input, args: ['shape',] },],
        "setHero": [{ type: i0.Input, args: ['hero',] },],
        "setDisabled": [{ type: i0.Input, args: ['disabled',] },],
        "setFullWidth": [{ type: i0.Input, args: ['fullWidth',] },],
        "setOutline": [{ type: i0.Input, args: ['outline',] },],
        "onClick": [{ type: i0.HostListener, args: ['click', ['$event'],] },],
    };
    return NbButtonComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_BUTTON_COMPONENTS = [
    NbButtonComponent,
];
var NbButtonModule = /** @class */ (function () {
    function NbButtonModule() {
    }
    NbButtonModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: NB_BUTTON_COMPONENTS.slice(),
                    exports: NB_BUTTON_COMPONENTS.slice(),
                },] },
    ];
    return NbButtonModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var SERVICES = [
    { provide: NbDateService, useClass: NbNativeDateService },
    i1.DatePipe,
    NbCalendarMonthModelService,
];
var COMPONENTS = [
    NbCalendarHeaderComponent,
    NbCalendarNavigationComponent,
    NbCalendarPageableNavigationComponent,
    NbCalendarDaysNamesComponent,
    NbCalendarYearPickerComponent,
    NbCalendarMonthPickerComponent,
    NbCalendarDayPickerComponent,
    NbCalendarDayCellComponent,
    NbCalendarMonthCellComponent,
    NbCalendarYearCellComponent,
    NbCalendarPickerRowComponent,
    NbCalendarPickerComponent,
];
var PIPES = [
    NbCalendarDatePipe,
];
/**
 * `NbCalendarKitModule` is a module that contains multiple useful components for building custom calendars.
 * So if you think our calendars is not enough powerful for you just use calendar-kit and build your own calendar!
 *
 * Available components:
 * - `NbCalendarDayPicker`
 * - `NbCalendarDayCell`
 * - `NbCalendarMonthPicker`
 * - `NbCalendarMonthCell`
 * - `NbCalendarYearPicker`
 * - `NbCalendarYearCell`
 * - `NbCalendarHeader`
 * - `NbCalendarNavigation`
 * - `NbCalendarPageableNavigation`
 *
 * For example you can easily build full calendar:
 * @stacked-example(Full calendar, calendar-kit/calendar-kit-full-calendar.component)
 * */
var NbCalendarKitModule = /** @class */ (function () {
    function NbCalendarKitModule() {
    }
    NbCalendarKitModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbSharedModule, NbButtonModule],
                    exports: COMPONENTS.concat(PIPES),
                    declarations: COMPONENTS.concat(PIPES),
                    providers: SERVICES.slice(),
                    entryComponents: [
                        NbCalendarDayCellComponent,
                        NbCalendarMonthCellComponent,
                        NbCalendarYearCellComponent,
                    ],
                },] },
    ];
    return NbCalendarKitModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Calendar component provides a capability to choose a date.
 *
 * ```html
 * <nb-calendar [(date)]="date"></nb-calendar>
 * <nb-calendar [date]="date" (dateChange)="handleDateChange($event)"></nb-calendar>
 * ```
 *
 * Basic usage example
 * @stacked-example(Showcase, calendar/calendar-showcase.component)
 *
 * ### Installation
 *
 * Import `NbCalendarModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbCalendarModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * If you want to select ranges you can use `NbCalendarRangeComponent`.
 *
 * ```html
 * <nb-calendar-range [(range)]="range"></nb-calendar-range>
 * <nb-calendar-range [range]="range" (rangeChange)="handleRangeChange($event)"></nb-calendar-range>
 * ```
 *
 * In order to use it, you have to import `NbCalendarRangeModule`.
 * @stacked-example(Range, calendar/calendar-range-showcase.component)
 *
 * The calendar component is supplied with a calendar header that contains navigate today button.
 * If you do not want to use it you can hide calendar header using `showHeader` property.
 * @stacked-example(Header, calendar/calendar-without-header.component)
 *
 * As you can see in the basic usage example calendar contains previous and next month days
 * which can be disabled using `boundingMonth` property.
 * @stacked-example(Bounding months, calendar/calendar-bounding-month.component)
 *
 * You can define starting view of the calendar by setting `startView` property.
 * Available values: year, month and date.
 * @stacked-example(Start view, calendar/calendar-start-view.component)
 *
 * You can use a larger version of the calendar by defining size property.
 * Available values: medium(which is default) and large.
 * @stacked-example(Size, calendar/calendar-size.component)
 *
 * Calendar supports min and max dates which disables values out of min-max range.
 * @stacked-example(Borders, calendar/calendar-min-max.component)
 *
 * Also, you can define custom filter property that should be predicate which receives
 * date and returns false if this date has to be disabled. In this example, we provide the filter
 * which disables weekdays.
 * @stacked-example(Filter, calendar/calendar-filter.component)
 *
 * If you need create custom cells you can easily provide custom components for
 * calendar. For examples if you want to show any average price under each date you can
 * just provide custom `dayCellComponent`. Custom cells for month and year can be provided
 * the same way, check API reference.
 * @stacked-example(Custom day cell, calendar/calendar-custom-day-cell-showcase.component)
 *
 * @styles
 *
 * calendar-width
 * calendar-body-height
 * calendar-header-title-font-size
 * calendar-header-title-font-weight
 * calendar-header-sub-title-font-size
 * calendar-header-sub-title-font-weight
 * calendar-navigation-button-width
 * calendar-selected-item-bg
 * calendar-hover-item-bg
 * calendar-today-item-bg
 * calendar-active-item-bg
 * calendar-fg
 * calendar-selected-fg
 * calendar-day-cell-width
 * calendar-day-cell-height
 * calendar-month-cell-width
 * calendar-month-cell-height
 * calendar-year-cell-width
 * calendar-year-cell-height
 * calendar-inactive-opacity
 * calendar-disabled-opacity
 * calendar-border-radius
 * calendar-weekday-width
 * calendar-weekday-height
 * calendar-weekday-font-size
 * calendar-weekday-font-weight
 * calendar-weekday-fg
 * calendar-weekday-holiday-fg
 * calendar-range-bg-in-range
 * calendar-large-width
 * calendar-large-body-height
 * calendar-day-cell-large-width
 * calendar-day-cell-large-height
 * calendar-month-cell-large-width
 * calendar-month-cell-large-height
 * calendar-year-cell-large-width
 * calendar-year-cell-large-height
 * */
var NbCalendarComponent = /** @class */ (function () {
    function NbCalendarComponent() {
        /**
           * Defines if we should render previous and next months
           * in the current month view.
           * */
        this.boundingMonth = true;
        /**
           * Defines starting view for calendar.
           * */
        this.startView = exports.NbCalendarViewMode.DATE;
        /**
           * Size of the calendar and entire components.
           * Can be 'medium' which is default or 'large'.
           * */
        this.size = exports.NbCalendarSize.MEDIUM;
        /**
           * Determines should we show calendars header or not.
           * */
        this.showHeader = true;
        /**
           * Emits date when selected.
           * */
        this.dateChange = new i0.EventEmitter();
    }
    NbCalendarComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar',
                    template: "\n    <nb-base-calendar\n      [boundingMonth]=\"boundingMonth\"\n      [startView]=\"startView\"\n      [date]=\"date\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      [dayCellComponent]=\"dayCellComponent\"\n      [monthCellComponent]=\"monthCellComponent\"\n      [yearCellComponent]=\"yearCellComponent\"\n      [size]=\"size\"\n      [visibleDate]=\"visibleDate\"\n      [showHeader]=\"showHeader\"\n      (dateChange)=\"dateChange.emit($event)\"\n    ></nb-base-calendar>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbCalendarComponent.propDecorators = {
        "boundingMonth": [{ type: i0.Input },],
        "startView": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "dayCellComponent": [{ type: i0.Input },],
        "monthCellComponent": [{ type: i0.Input },],
        "yearCellComponent": [{ type: i0.Input },],
        "size": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "showHeader": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
        "dateChange": [{ type: i0.Output },],
    };
    return NbCalendarComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The basis for calendar and range calendar components.
 * Encapsulates common behavior - store calendar state and perform navigation
 * between pickers.
 * */
var NbBaseCalendarComponent = /** @class */ (function () {
    function NbBaseCalendarComponent(dateService) {
        this.dateService = dateService;
        /**
           * Defines if we should render previous and next months
           * in the current month view.
           * */
        this.boundingMonth = true;
        /**
           * Defines active view for calendar.
           * */
        this.activeViewMode = exports.NbCalendarViewMode.DATE;
        /**
           * Size of the calendar and entire components.
           * Can be 'medium' which is default or 'large'.
           * */
        this.size = exports.NbCalendarSize.MEDIUM;
        /**
           * Determines should we show calendars header or not.
           * */
        this.showHeader = true;
        /**
           * Emits date when selected.
           * */
        this.dateChange = new i0.EventEmitter();
        this.ViewMode = exports.NbCalendarViewMode;
    }
    NbBaseCalendarComponent.prototype.ngOnInit = function () {
        if (!this.visibleDate) {
            this.visibleDate = this.dateService.today();
        }
    };
    Object.defineProperty(NbBaseCalendarComponent.prototype, "medium", {
        get: function () {
            return this.size === exports.NbCalendarSize.MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbBaseCalendarComponent.prototype, "large", {
        get: function () {
            return this.size === exports.NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbBaseCalendarComponent.prototype.setViewMode = function (viewMode) {
        this.activeViewMode = viewMode;
    };
    NbBaseCalendarComponent.prototype.setVisibleDate = function (visibleDate) {
        this.visibleDate = visibleDate;
    };
    NbBaseCalendarComponent.prototype.prevMonth = function () {
        this.changeVisibleMonth(-1);
    };
    NbBaseCalendarComponent.prototype.nextMonth = function () {
        this.changeVisibleMonth(1);
    };
    NbBaseCalendarComponent.prototype.prevYears = function () {
        this.changeVisibleYear(-1);
    };
    NbBaseCalendarComponent.prototype.nextYears = function () {
        this.changeVisibleYear(1);
    };
    NbBaseCalendarComponent.prototype.navigateToday = function () {
        this.setViewMode(exports.NbCalendarViewMode.DATE);
        this.visibleDate = this.dateService.today();
    };
    NbBaseCalendarComponent.prototype.changeVisibleMonth = function (direction) {
        this.visibleDate = this.dateService.addMonth(this.visibleDate, direction);
    };
    NbBaseCalendarComponent.prototype.changeVisibleYear = function (direction) {
        this.visibleDate = this.dateService.addYear(this.visibleDate, direction * 20);
    };
    NbBaseCalendarComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-base-calendar',
                    template: "<nb-card> <nb-card-header *ngIf=\"showHeader\"> <nb-calendar-header (navigateToday)=\"navigateToday()\"></nb-calendar-header> </nb-card-header> <nb-card-body [ngSwitch]=\"activeViewMode\"> <ng-container *ngSwitchCase=\"ViewMode.DATE\"> <nb-calendar-pageable-navigation *ngSwitchCase=\"ViewMode.DATE\" [date]=\"visibleDate\" (next)=\"nextMonth()\" (prev)=\"prevMonth()\" (changeMode)=\"setViewMode(ViewMode.YEAR)\"> </nb-calendar-pageable-navigation> <nb-calendar-day-picker [boundingMonths]=\"boundingMonth\" [cellComponent]=\"dayCellComponent\" [min]=\"min\" [max]=\"max\" [filter]=\"filter\" [visibleDate]=\"visibleDate\" [size]=\"size\" [date]=\"date\" (dateChange)=\"dateChange.emit($event)\"> </nb-calendar-day-picker> </ng-container> <ng-container *ngSwitchCase=\"ViewMode.YEAR\"> <nb-calendar-pageable-navigation [date]=\"visibleDate\" (next)=\"nextYears()\" (prev)=\"prevYears()\" (changeMode)=\"setViewMode(ViewMode.DATE)\"> </nb-calendar-pageable-navigation> <nb-calendar-year-picker [cellComponent]=\"yearCellComponent\" [date]=\"date\" [min]=\"min\" [max]=\"max\" [filter]=\"filter\" [size]=\"size\" [year]=\"visibleDate\" (yearChange)=\"setVisibleDate($event); setViewMode(ViewMode.MONTH)\"> </nb-calendar-year-picker> </ng-container> <ng-container *ngSwitchCase=\"ViewMode.MONTH\"> <nb-calendar-navigation [date]=\"visibleDate\" (changeMode)=\"setViewMode(ViewMode.DATE)\"> </nb-calendar-navigation> <nb-calendar-month-picker [cellComponent]=\"monthCellComponent\" [min]=\"min\" [max]=\"max\" [filter]=\"filter\" [size]=\"size\" [month]=\"visibleDate\" (monthChange)=\"setVisibleDate($event); setViewMode(ViewMode.DATE)\"> </nb-calendar-month-picker> </ng-container> </nb-card-body> </nb-card> ",
                },] },
    ];
    /** @nocollapse */
    NbBaseCalendarComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbBaseCalendarComponent.propDecorators = {
        "boundingMonth": [{ type: i0.Input },],
        "activeViewMode": [{ type: i0.Input, args: ['startView',] },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "dayCellComponent": [{ type: i0.Input },],
        "monthCellComponent": [{ type: i0.Input },],
        "yearCellComponent": [{ type: i0.Input },],
        "size": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "showHeader": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
        "dateChange": [{ type: i0.Output },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large',] },],
    };
    return NbBaseCalendarComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbBaseCalendarModule = /** @class */ (function () {
    function NbBaseCalendarModule() {
    }
    NbBaseCalendarModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbCalendarKitModule, NbSharedModule, NbCardModule],
                    exports: [NbBaseCalendarComponent],
                    declarations: [NbBaseCalendarComponent],
                },] },
    ];
    return NbBaseCalendarModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarModule = /** @class */ (function () {
    function NbCalendarModule() {
    }
    NbCalendarModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbBaseCalendarModule],
                    exports: [NbCalendarComponent],
                    declarations: [NbCalendarComponent],
                },] },
    ];
    return NbCalendarModule;
}());

var NbCalendarRangeDayCellComponent = /** @class */ (function () {
    function NbCalendarRangeDayCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new i0.EventEmitter(true);
    }
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "inRange", {
        get: function () {
            return this.date && this.selectedValue
                && (this.selectedValue.start && this.dateService.compareDates(this.date, this.selectedValue.start) >= 0)
                && (this.selectedValue.end && this.dateService.compareDates(this.date, this.selectedValue.end) <= 0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "start", {
        get: function () {
            return this.date && this.selectedValue && this.selectedValue.end
                && (this.selectedValue.start && this.dateService.isSameDay(this.date, this.selectedValue.start));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "end", {
        get: function () {
            return this.date && this.selectedValue &&
                (this.selectedValue.end && this.dateService.isSameDay(this.date, this.selectedValue.end));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "today", {
        get: function () {
            return this.date && this.dateService.isSameDay(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "boundingMonth", {
        get: function () {
            return !this.dateService.isSameMonthSafe(this.date, this.visibleDate);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "selected", {
        get: function () {
            return this.date && this.selectedValue
                && (this.selectedValue.start && this.dateService.isSameDay(this.date, this.selectedValue.start)) || this.end;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "empty", {
        get: function () {
            return !this.date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax() || this.dontFitFilter();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "day", {
        get: function () {
            return this.date && this.dateService.getDate(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeDayCellComponent.prototype.onClick = function () {
        if (this.disabled || this.empty) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarRangeDayCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.date, this.min) < 0;
    };
    NbCalendarRangeDayCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.date, this.max) > 0;
    };
    NbCalendarRangeDayCellComponent.prototype.dontFitFilter = function () {
        return this.date && this.filter && !this.filter(this.date);
    };
    NbCalendarRangeDayCellComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-range-day-cell',
                    template: "\n    <div\n      class=\"day-cell\"\n      [class.today]=\"today\"\n      [class.selected]=\"selected\"\n      [class.bounding-month]=\"boundingMonth\"\n      [class.start]=\"start\"\n      [class.end]=\"end\"\n      [class.in-range]=\"inRange\"\n      [class.disabled]=\"disabled\">\n      {{ day }}\n    </div>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    host: { '(click)': 'onClick()', 'class': 'range-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarRangeDayCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarRangeDayCellComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "selectedValue": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "select": [{ type: i0.Output },],
        "inRange": [{ type: i0.HostBinding, args: ['class.in-range',] },],
        "start": [{ type: i0.HostBinding, args: ['class.start',] },],
        "end": [{ type: i0.HostBinding, args: ['class.end',] },],
    };
    return NbCalendarRangeDayCellComponent;
}());
var NbCalendarRangeYearCellComponent = /** @class */ (function () {
    function NbCalendarRangeYearCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new i0.EventEmitter(true);
    }
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "selected", {
        get: function () {
            return this.selectedValue && this.dateService.isSameYear(this.date, this.selectedValue.start);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "today", {
        get: function () {
            return this.date && this.dateService.isSameYear(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "year", {
        get: function () {
            return this.dateService.getYear(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeYearCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarRangeYearCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.yearEnd(), this.min) < 0;
    };
    NbCalendarRangeYearCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.yearStart(), this.max) > 0;
    };
    NbCalendarRangeYearCellComponent.prototype.yearStart = function () {
        return this.dateService.getYearStart(this.date);
    };
    NbCalendarRangeYearCellComponent.prototype.yearEnd = function () {
        return this.dateService.getYearEnd(this.date);
    };
    NbCalendarRangeYearCellComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-range-year-cell',
                    template: "{{ year }}",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'year-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarRangeYearCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarRangeYearCellComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "selectedValue": [{ type: i0.Input },],
        "select": [{ type: i0.Output },],
        "selected": [{ type: i0.HostBinding, args: ['class.selected',] },],
        "today": [{ type: i0.HostBinding, args: ['class.today',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbCalendarRangeYearCellComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * CalendarRange component provides a capability to choose a date range.
 *
 * ```html
 * <nb-calendar [(date)]="date"></nb-calendar>
 * <nb-calendar [date]="date" (dateChange)="handleDateChange($event)"></nb-calendar>
 * ```
 *
 * Basic usage example
 * @stacked-example(Range, calendar/calendar-range-showcase.component)
 *
 * ### Installation
 *
 * Import `NbCalendarRangeModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbCalendarRangeModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 *
 * ### Usage
 *
 * CalendarRange component supports all of the Calendar component customization properties. More defails can be found
 * in the [Calendar component docs](docs/components/calendar).
 *
 * @styles
 *
 * calendar-width
 * calendar-body-height
 * calendar-header-title-font-size
 * calendar-header-title-font-weight
 * calendar-header-sub-title-font-size
 * calendar-header-sub-title-font-weight
 * calendar-navigation-button-width
 * calendar-selected-item-bg
 * calendar-hover-item-bg
 * calendar-today-item-bg
 * calendar-active-item-bg
 * calendar-fg
 * calendar-selected-fg
 * calendar-day-cell-width
 * calendar-day-cell-height
 * calendar-month-cell-width
 * calendar-month-cell-height
 * calendar-year-cell-width
 * calendar-year-cell-height
 * calendar-inactive-opacity
 * calendar-disabled-opacity
 * calendar-border-radius
 * calendar-weekday-width
 * calendar-weekday-height
 * calendar-weekday-font-size
 * calendar-weekday-font-weight
 * calendar-weekday-fg
 * calendar-weekday-holiday-fg
 * calendar-range-bg-in-range
 * calendar-large-width
 * calendar-large-body-height
 * calendar-day-cell-large-width
 * calendar-day-cell-large-height
 * calendar-month-cell-large-width
 * calendar-month-cell-large-height
 * calendar-year-cell-large-width
 * calendar-year-cell-large-height
 * */
var NbCalendarRangeComponent = /** @class */ (function () {
    function NbCalendarRangeComponent(dateService) {
        this.dateService = dateService;
        /**
           * Defines if we should render previous and next months
           * in the current month view.
           * */
        this.boundingMonth = true;
        /**
           * Defines starting view for the calendar.
           * */
        this.startView = exports.NbCalendarViewMode.DATE;
        this.dayCellComponent = NbCalendarRangeDayCellComponent;
        this.yearCellComponent = NbCalendarRangeYearCellComponent;
        /**
           * Size of the calendar and entire components.
           * Can be 'medium' which is default or 'large'.
           * */
        this.size = exports.NbCalendarSize.MEDIUM;
        /**
           * Determines should we show calendars header or not.
           * */
        this.showHeader = true;
        /**
           * Emits range when start selected and emits again when end selected.
           * */
        this.rangeChange = new i0.EventEmitter();
    }
    Object.defineProperty(NbCalendarRangeComponent.prototype, "_cellComponent", {
        set: /**
           * Custom day cell component. Have to implement `NbCalendarCell` interface.
           * */
        function (cellComponent) {
            if (cellComponent) {
                this.dayCellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeComponent.prototype, "_yearCellComponent", {
        set: /**
           * Custom year cell component. Have to implement `NbCalendarCell` interface.
           * */
        function (cellComponent) {
            if (cellComponent) {
                this.yearCellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeComponent.prototype.onChange = function (date) {
        this.initDateIfNull();
        this.handleSelected(date);
    };
    NbCalendarRangeComponent.prototype.initDateIfNull = function () {
        if (!this.range) {
            this.range = { start: null, end: null };
        }
    };
    NbCalendarRangeComponent.prototype.handleSelected = function (date) {
        if (this.selectionStarted()) {
            this.selectEnd(date);
        }
        else {
            this.selectStart(date);
        }
    };
    NbCalendarRangeComponent.prototype.selectionStarted = function () {
        var _a = this.range, start = _a.start, end = _a.end;
        return start && !end;
    };
    NbCalendarRangeComponent.prototype.selectStart = function (start) {
        this.selectRange({ start: start });
    };
    NbCalendarRangeComponent.prototype.selectEnd = function (date) {
        var start = this.range.start;
        if (this.dateService.compareDates(date, start) > 0) {
            this.selectRange({ start: start, end: date });
        }
        else {
            this.selectRange({ start: date, end: start });
        }
    };
    NbCalendarRangeComponent.prototype.selectRange = function (range) {
        this.range = range;
        this.rangeChange.emit(range);
    };
    NbCalendarRangeComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-calendar-range',
                    template: "\n    <nb-base-calendar\n      [date]=\"range\"\n      (dateChange)=\"onChange($event)\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      [startView]=\"startView\"\n      [boundingMonth]=\"boundingMonth\"\n      [dayCellComponent]=\"dayCellComponent\"\n      [monthCellComponent]=\"monthCellComponent\"\n      [yearCellComponent]=\"yearCellComponent\"\n      [visibleDate]=\"visibleDate\"\n      [showHeader]=\"showHeader\"\n      [size]=\"size\"\n    ></nb-base-calendar>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbCalendarRangeComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarRangeComponent.propDecorators = {
        "boundingMonth": [{ type: i0.Input },],
        "startView": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "_cellComponent": [{ type: i0.Input, args: ['dayCellComponent',] },],
        "monthCellComponent": [{ type: i0.Input },],
        "_yearCellComponent": [{ type: i0.Input, args: ['yearCellComponent',] },],
        "size": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "showHeader": [{ type: i0.Input },],
        "range": [{ type: i0.Input },],
        "rangeChange": [{ type: i0.Output },],
    };
    return NbCalendarRangeComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCalendarRangeModule = /** @class */ (function () {
    function NbCalendarRangeModule() {
    }
    NbCalendarRangeModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbBaseCalendarModule],
                    exports: [NbCalendarRangeComponent],
                    declarations: [
                        NbCalendarRangeComponent,
                        NbCalendarRangeDayCellComponent,
                        NbCalendarRangeYearCellComponent,
                    ],
                    entryComponents: [NbCalendarRangeDayCellComponent, NbCalendarRangeYearCellComponent],
                },] },
    ];
    return NbCalendarRangeModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
function isUrlPathEqual(path, link) {
    var locationPath = getPathPartOfUrl(path);
    return link === locationPath;
}
function isUrlPathContain(path, link) {
    var locationPath = getPathPartOfUrl(path);
    var endOfUrlSegmentRegExp = /\/|^$/;
    return locationPath.startsWith(link) &&
        locationPath.slice(link.length).charAt(0).search(endOfUrlSegmentRegExp) !== -1;
}
function getPathPartOfUrl(url) {
    return url.match(/.*?(?=[?;#]|$)/)[0];
}

/**
 * This service determines whether we should scroll the layout back to top.
 * This occurs when the page is changed, so when current url PATH is not equal to the previous one.
 *
 *  TODO: this is most likely a temporary solutions as recently Angular introduces ViewportScroll
 *  and scroll restoration process
 */
var NbRestoreScrollTopHelper = /** @class */ (function () {
    function NbRestoreScrollTopHelper(router) {
        this.router = router;
    }
    NbRestoreScrollTopHelper.prototype.shouldRestore = function () {
        var _this = this;
        return this.router.events
            .pipe(rxjs_operators.startWith(null), rxjs_operators.filter(function (event) { return event === null || event instanceof _angular_router.NavigationEnd; }), rxjs_operators.pairwise(), rxjs_operators.map(function (_a) {
            var prev = _a[0], current = _a[1];
            return _this.pageChanged(prev, current);
        }), rxjs_operators.filter(function (res) { return !!res; }));
    };
    NbRestoreScrollTopHelper.prototype.pageChanged = function (prev, current) {
        return !prev || getPathPartOfUrl(prev.url) !== getPathPartOfUrl(current.url);
    };
    NbRestoreScrollTopHelper.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbRestoreScrollTopHelper.ctorParameters = function () { return [
        { type: _angular_router.Router, },
    ]; };
    return NbRestoreScrollTopHelper;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * A container component which determines a content position inside of the layout.
 * The layout could contain unlimited columns (not including the sidebars).
 *
 * By default the columns are ordered from the left to the right,
 * but it's also possible to overwrite this behavior by setting a `left` attribute to the column,
 * moving it to the very first position:
 *
 * @stacked-example(Column Left, layout/layout-column-left.component)
 */
var NbLayoutColumnComponent = /** @class */ (function () {
    function NbLayoutColumnComponent() {
    }
    Object.defineProperty(NbLayoutColumnComponent.prototype, "left", {
        set: /**
           * Move the column to the very left position in the layout.
           * @param {boolean} val
           */
        function (val) {
            this.leftValue = convertToBoolProperty(val);
            this.startValue = false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbLayoutColumnComponent.prototype, "start", {
        set: /**
           * Make columnt first in the layout.
           * @param {boolean} val
           */
        function (val) {
            this.startValue = convertToBoolProperty(val);
            this.leftValue = false;
        },
        enumerable: true,
        configurable: true
    });
    NbLayoutColumnComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-layout-column',
                    template: "\n    <ng-content></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbLayoutColumnComponent.propDecorators = {
        "leftValue": [{ type: i0.HostBinding, args: ['class.left',] },],
        "startValue": [{ type: i0.HostBinding, args: ['class.start',] },],
        "left": [{ type: i0.Input },],
        "start": [{ type: i0.Input },],
    };
    return NbLayoutColumnComponent;
}());
/**
 * Page header component.
 * Located on top of the page above the layout columns and sidebars.
 * Could be made `fixed` by setting the corresponding property. In the fixed mode the header becomes
 * sticky to the top of the nb-layout (to of the page). Here's an example:
 *
 * @stacked-example(Fixed Header, layout/layout-fixed-header.component)
 *
 * In a pair with sidebar it is possible to setup a configuration when header is placed on a side of the sidebar
 * and not on top of it. To achieve this simply put a `subheader` property to the header like this:
 * ```html
 * <nb-layout-header subheader></nb-layout-header>
 * ```
 * @stacked-example(Subheader, layout/layout-sidebar-subheader.component)
 * Note that in such configuration sidebar shadow is removed and header cannot be make `fixed`.
 *
 * Same way you can put both `fixed` and `clipped` headers adding creating a sub-header for your app:
 *
 * @stacked-example(Subheader, layout/layout-subheader.component)
 *
 * @styles
 *
 * header-font-family
 * header-line-height
 * header-fg
 * header-bg
 * header-height
 * header-padding
 * header-shadow
 */
var NbLayoutHeaderComponent = /** @class */ (function () {
    // tslint:disable-next-line
    function NbLayoutHeaderComponent(layout) {
        this.layout = layout;
    }
    Object.defineProperty(NbLayoutHeaderComponent.prototype, "fixed", {
        set: /**
           * Makes the header sticky to the top of the nb-layout.
           * @param {boolean} val
           */
        function (val) {
            this.fixedValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbLayoutHeaderComponent.prototype, "subheader", {
        set: /**
           * Places header on a side of the sidebar, and not above.
           * Disables fixed mode for this header and remove a shadow from the sidebar.
           * @param {boolean} val
           */
        function (val) {
            this.subheaderValue = convertToBoolProperty(val);
            this.fixedValue = false;
            this.layout.withSubheader = this.subheaderValue;
        },
        enumerable: true,
        configurable: true
    });
    NbLayoutHeaderComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-layout-header',
                    template: "\n    <nav [class.fixed]=\"fixedValue\">\n      <ng-content></ng-content>\n    </nav>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbLayoutHeaderComponent.ctorParameters = function () { return [
        { type: NbLayoutComponent, decorators: [{ type: i0.Inject, args: [i0.forwardRef(function () { return NbLayoutComponent; }),] },] },
    ]; };
    NbLayoutHeaderComponent.propDecorators = {
        "fixedValue": [{ type: i0.HostBinding, args: ['class.fixed',] },],
        "subheaderValue": [{ type: i0.HostBinding, args: ['class.subheader',] },],
        "fixed": [{ type: i0.Input },],
        "subheader": [{ type: i0.Input },],
    };
    return NbLayoutHeaderComponent;
}());
/**
 * Page footer.
 * Located under the nb-layout content (specifically, under the columns).
 * Could be made `fixed`, becoming sticky to the bottom of the view port (window).
 *
 * @styles
 *
 * footer-height
 * footer-padding
 * footer-fg
 * footer-bg
 * footer-separator
 * footer-shadow
 */
var NbLayoutFooterComponent = /** @class */ (function () {
    function NbLayoutFooterComponent() {
    }
    Object.defineProperty(NbLayoutFooterComponent.prototype, "fixed", {
        set: /**
           * Makes the footer sticky to the bottom of the window.
           * @param {boolean} val
           */
        function (val) {
            this.fixedValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbLayoutFooterComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-layout-footer',
                    template: "\n    <nav [class.fixed]=\"fixedValue\">\n      <ng-content></ng-content>\n    </nav>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbLayoutFooterComponent.propDecorators = {
        "fixedValue": [{ type: i0.HostBinding, args: ['class.fixed',] },],
        "fixed": [{ type: i0.Input },],
    };
    return NbLayoutFooterComponent;
}());
/**
 * Layout container component.
 * When using with Nebular Theme System it is required that all child components should be placed inside.
 *
 * Basic example of two column layout with header:
 *
 * @stacked-example(Showcase, layout/layout-showcase.component)
 *
 * Can contain the following components inside:
 *
 * ```html
 * <nb-layout>
 *  <nb-layout-header></nb-layout-header>
 *  <nb-layout-footer></nb-layout-column>
 *  <nb-layout-column></nb-layout-column>
 *  <nb-sidebar></nb-sidebar>
 * </nb-layout>
 * ```
 * ### Installation
 *
 * Import `NbLayoutModule.forRoot()` to your app module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbLayoutModule.forRoot(),
 *   ],
 * })
 * export class AppModule { }
 * ```
 * and `NbLayoutModule` to your feature module where the component should be shown:
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbLayoutModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 * By default the layout fills up the whole view-port.
 * The window scrollbars are disabled on the body and moved inside of the nb-layout, so that the scrollbars
 * won't mess with the fixed nb-header.
 *
 * The child components are projected into a flexible layout structure allowing to adjust the layout behavior
 * based on the settings provided.
 *
 * The layout content (columns) becomes centered when the window width is more than
 * the value specified in the theme variable `layout-content-width`.
 *
 * The layout also contains the area on the very top (the first child of the nb-layout), which could be used
 * to dynamically append some components like modals or spinners/loaders
 * so that they are located on top of the elements hierarchy.
 * More details are under the `ThemeService` section.
 *
 * The layout component is also responsible for changing application themes.
 * It listens to the `themeChange` event and change a theme CSS class appended to body.
 * Based on the class appended, specific CSS-theme is applied to the application.
 * More details of the Theme System could be found here [Enabling Theme System](#/docs/concepts/theme-system)
 *
 * A simple layout with footer:
 *
 * @stacked-example(Layout With Footer, layout/layout-w-footer.component)
 *
 * It is possible to ask the layout to center the columns (notice: we added a `center` attribute
 * to the layout:
 *
 * ```html
 * <nb-layout center>
 *   <nb-layout-header>Awesome Company</nb-layout-header>
 *
 *   <nb-layout-column>
 *     Hello World!
 *   </nb-layout-column>
 *
 *   <nb-layout-footer>Contact us</nb-layout-footer>
 * </nb-layout>
 * ```
 *
 * @styles
 *
 * layout-font-family
 * layout-font-size
 * layout-line-height
 * layout-fg
 * layout-bg
 * layout-min-height
 * layout-content-width
 * layout-window-mode-min-width
 * layout-window-mode-max-width: window mode only, after this value layout turns into a floating window
 * layout-window-mode-bg: window mode only, background
 * layout-window-mode-padding-top: window mode only, max padding from top
 * layout-window-shadow: window mode shadow
 * layout-padding
 * layout-medium-padding
 * layout-small-padding
 */
var NbLayoutComponent = /** @class */ (function () {
    function NbLayoutComponent(themeService, spinnerService, elementRef, renderer, window, document, platformId, layoutDirectionService, scrollService, rulerService, scrollTop, overlayContainer) {
        var _this = this;
        this.themeService = themeService;
        this.spinnerService = spinnerService;
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.window = window;
        this.document = document;
        this.platformId = platformId;
        this.layoutDirectionService = layoutDirectionService;
        this.scrollService = scrollService;
        this.rulerService = rulerService;
        this.scrollTop = scrollTop;
        this.overlayContainer = overlayContainer;
        this.centerValue = false;
        this.restoreScrollTopValue = true;
        this.windowModeValue = false;
        this.withScrollValue = false;
        this.withSubheader = false;
        this.afterViewInit$ = new rxjs.BehaviorSubject(null);
        this.alive = true;
        this.registerAsOverlayContainer();
        this.themeService.onThemeChange()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (theme) {
            var body = _this.document.getElementsByTagName('body')[0];
            if (theme.previous) {
                _this.renderer.removeClass(body, "nb-theme-" + theme.previous);
            }
            _this.renderer.addClass(body, "nb-theme-" + theme.name);
        });
        this.themeService.onAppendLayoutClass()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (className) {
            _this.renderer.addClass(_this.elementRef.nativeElement, className);
        });
        this.themeService.onRemoveLayoutClass()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (className) {
            _this.renderer.removeClass(_this.elementRef.nativeElement, className);
        });
        this.spinnerService.registerLoader(new Promise(function (resolve, reject) {
            _this.afterViewInit$
                .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
                .subscribe(function (_) { return resolve(); });
        }));
        this.spinnerService.load();
        this.rulerService.onGetDimensions()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (_a) {
            var listener = _a.listener;
            listener.next(_this.getDimensions());
            listener.complete();
        });
        this.scrollService.onGetPosition()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (_a) {
            var listener = _a.listener;
            listener.next(_this.getScrollPosition());
            listener.complete();
        });
        this.scrollTop
            .shouldRestore()
            .pipe(rxjs_operators.filter(function () { return _this.restoreScrollTopValue; }), rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function () {
            _this.scroll(0, 0);
        });
        if (i1.isPlatformBrowser(this.platformId)) {
            // trigger first time so that after the change we have the initial value
            this.themeService.changeWindowWidth(this.window.innerWidth);
        }
    }
    Object.defineProperty(NbLayoutComponent.prototype, "center", {
        set: /**
           * Defines whether the layout columns will be centered after some width
           * @param {boolean} val
           */
        function (val) {
            this.centerValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbLayoutComponent.prototype, "windowMode", {
        set: /**
           * Defines whether the layout enters a 'window' mode, when the layout content (including sidebars and fixed header)
           * becomes centered by width with a margin from the top of the screen, like a floating window.
           * Automatically enables `withScroll` mode, as in the window mode scroll must be inside the layout and cannot be on
           * window. (TODO: check this)
           * @param {boolean} val
           */
        function (val) {
            this.windowModeValue = convertToBoolProperty(val);
            this.withScroll = this.windowModeValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbLayoutComponent.prototype, "withScroll", {
        set: /**
           * Defines whether to move the scrollbars to layout or leave it at the body level.
           * Automatically set to true when `windowMode` is enabled.
           * @param {boolean} val
           */
        function (val) {
            this.withScrollValue = convertToBoolProperty(val);
            // TODO: is this the best way of doing it? as we don't have access to body from theme styles
            // TODO: add e2e test
            var body = this.document.getElementsByTagName('body')[0];
            if (this.withScrollValue) {
                this.renderer.setStyle(body, 'overflow', 'hidden');
            }
            else {
                this.renderer.setStyle(body, 'overflow', 'initial');
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbLayoutComponent.prototype, "restoreScrollTop", {
        set: /**
           * Restores scroll to the top of the page after navigation
           * @param {boolean} val
           */
        function (val) {
            this.restoreScrollTopValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbLayoutComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.layoutDirectionService.onDirectionChange()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (direction) {
            _this.renderer.setProperty(_this.document, 'dir', direction);
        });
        this.scrollService.onManualScroll()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (_a) {
            var x = _a.x, y = _a.y;
            return _this.scroll(x, y);
        });
        this.afterViewInit$.next(true);
    };
    NbLayoutComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
        this.unregisterAsOverlayContainer();
    };
    NbLayoutComponent.prototype.onScroll = function ($event) {
        this.scrollService.fireScrollChange($event);
    };
    NbLayoutComponent.prototype.onResize = function (event) {
        this.themeService.changeWindowWidth(event.target.innerWidth);
    };
    /**
     * Returns scroll and client height/width
     *
     * Depending on the current scroll mode (`withScroll=true`) returns sizes from the body element
     * or from the `.scrollable-container`
     * @returns {NbLayoutDimensions}
     */
    /**
       * Returns scroll and client height/width
       *
       * Depending on the current scroll mode (`withScroll=true`) returns sizes from the body element
       * or from the `.scrollable-container`
       * @returns {NbLayoutDimensions}
       */
    NbLayoutComponent.prototype.getDimensions = /**
       * Returns scroll and client height/width
       *
       * Depending on the current scroll mode (`withScroll=true`) returns sizes from the body element
       * or from the `.scrollable-container`
       * @returns {NbLayoutDimensions}
       */
    function () {
        var clientWidth, clientHeight, scrollWidth, scrollHeight = 0;
        if (this.withScrollValue) {
            var container = this.scrollableContainerRef.nativeElement;
            clientWidth = container.clientWidth;
            clientHeight = container.clientHeight;
            scrollWidth = container.scrollWidth;
            scrollHeight = container.scrollHeight;
        }
        else {
            var _a = this.document, documentElement = _a.documentElement, body = _a.body;
            clientWidth = documentElement.clientWidth || body.clientWidth;
            clientHeight = documentElement.clientHeight || body.clientHeight;
            scrollWidth = documentElement.scrollWidth || body.scrollWidth;
            scrollHeight = documentElement.scrollHeight || body.scrollHeight;
        }
        return {
            clientWidth: clientWidth,
            clientHeight: clientHeight,
            scrollWidth: scrollWidth,
            scrollHeight: scrollHeight,
        };
    };
    /**
     * Returns scroll position of current scroll container.
     *
     * If `withScroll` = true, returns scroll position of the `.scrollable-container` element,
     * otherwise - of the scrollable element of the window (which may be different depending of a browser)
     *
     * @returns {NbScrollPosition}
     */
    /**
       * Returns scroll position of current scroll container.
       *
       * If `withScroll` = true, returns scroll position of the `.scrollable-container` element,
       * otherwise - of the scrollable element of the window (which may be different depending of a browser)
       *
       * @returns {NbScrollPosition}
       */
    NbLayoutComponent.prototype.getScrollPosition = /**
       * Returns scroll position of current scroll container.
       *
       * If `withScroll` = true, returns scroll position of the `.scrollable-container` element,
       * otherwise - of the scrollable element of the window (which may be different depending of a browser)
       *
       * @returns {NbScrollPosition}
       */
    function () {
        if (this.withScrollValue) {
            var container = this.scrollableContainerRef.nativeElement;
            return { x: container.scrollLeft, y: container.scrollTop };
        }
        var documentRect = this.document.documentElement.getBoundingClientRect();
        var x = -documentRect.left || this.document.body.scrollLeft || this.window.scrollX ||
            this.document.documentElement.scrollLeft || 0;
        var y = -documentRect.top || this.document.body.scrollTop || this.window.scrollY ||
            this.document.documentElement.scrollTop || 0;
        return { x: x, y: y };
    };
    NbLayoutComponent.prototype.registerAsOverlayContainer = function () {
        if (this.overlayContainer.setContainer) {
            this.overlayContainer.setContainer(this.elementRef.nativeElement);
        }
    };
    NbLayoutComponent.prototype.unregisterAsOverlayContainer = function () {
        if (this.overlayContainer.clearContainer) {
            this.overlayContainer.clearContainer();
        }
    };
    NbLayoutComponent.prototype.scroll = function (x, y) {
        if (x === void 0) { x = null; }
        if (y === void 0) { y = null; }
        var _a = this.getScrollPosition(), currentX = _a.x, currentY = _a.y;
        x = x == null ? currentX : x;
        y = y == null ? currentY : y;
        if (!i1.isPlatformBrowser(this.platformId)) {
            return;
        }
        if (this.withScrollValue) {
            var scrollable = this.scrollableContainerRef.nativeElement;
            if (scrollable.scrollTo) {
                scrollable.scrollTo(x, y);
            }
            else {
                scrollable.scrollLeft = x;
                scrollable.scrollTop = y;
            }
        }
        else {
            this.window.scrollTo(x, y);
        }
    };
    NbLayoutComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-layout',
                    styles: [":host{-webkit-font-smoothing:antialiased}[dir=ltr] :host{text-align:left}[dir=rtl] :host{text-align:right}:host .layout{display:flex;flex-direction:column}:host /deep/ nb-layout-header{display:block}:host /deep/ nb-layout-header nav{align-items:center;justify-content:flex-start;display:flex}:host /deep/ nb-layout-header.fixed{position:fixed;left:0;right:0;z-index:1040}:host .layout-container{display:flex;flex:1;-ms-flex:1 1 auto;flex-direction:row}[dir=ltr] :host .layout-container /deep/ nb-sidebar.left{order:0}[dir=rtl] :host .layout-container /deep/ nb-sidebar.left{order:2}[dir=ltr] :host .layout-container /deep/ nb-sidebar.right{order:2}[dir=rtl] :host .layout-container /deep/ nb-sidebar.right{order:0}:host .layout-container /deep/ nb-sidebar.end{order:2}:host .layout-container /deep/ nb-sidebar .fixed{position:fixed;width:100%;overflow-y:auto;height:100%}:host .layout-container .content{display:flex;flex:1;-ms-flex:1 1 auto;flex-direction:column;min-width:0}:host .layout-container .content.center{max-width:100%;position:relative;margin-left:auto;margin-right:auto}:host .layout-container .content .columns{display:flex;flex:1;-ms-flex:1 1 auto;flex-direction:row;width:100%}:host .layout-container .content .columns /deep/ nb-layout-column{order:1;flex:1 0;min-width:0}[dir=ltr] :host .layout-container .content .columns /deep/ nb-layout-column.left{order:0}[dir=rtl] :host .layout-container .content .columns /deep/ nb-layout-column.left{order:2}:host .layout-container .content .columns /deep/ nb-layout-column.start{order:0}:host .layout-container .content /deep/ nb-layout-footer{display:block;margin-top:auto}:host .layout-container .content /deep/ nb-layout-footer nav{justify-content:center;display:flex} "],
                    template: "\n    <div class=\"scrollable-container\" #scrollableContainer (scroll)=\"onScroll($event)\">\n      <div class=\"layout\">\n        <ng-content select=\"nb-layout-header:not([subheader])\"></ng-content>\n        <div class=\"layout-container\">\n          <ng-content select=\"nb-sidebar\"></ng-content>\n          <div class=\"content\" [class.center]=\"centerValue\">\n            <ng-content select=\"nb-layout-header[subheader]\"></ng-content>\n            <div class=\"columns\">\n              <ng-content select=\"nb-layout-column\"></ng-content>\n            </div>\n            <ng-content select=\"nb-layout-footer\"></ng-content>\n          </div>\n        </div>\n      </div>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbLayoutComponent.ctorParameters = function () { return [
        { type: NbThemeService, },
        { type: NbSpinnerService, },
        { type: i0.ElementRef, },
        { type: i0.Renderer2, },
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_WINDOW,] },] },
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: Object, decorators: [{ type: i0.Inject, args: [i0.PLATFORM_ID,] },] },
        { type: NbLayoutDirectionService, },
        { type: NbLayoutScrollService, },
        { type: NbLayoutRulerService, },
        { type: NbRestoreScrollTopHelper, },
        { type: NbOverlayContainerAdapter, },
    ]; };
    NbLayoutComponent.propDecorators = {
        "windowModeValue": [{ type: i0.HostBinding, args: ['class.window-mode',] },],
        "withScrollValue": [{ type: i0.HostBinding, args: ['class.with-scroll',] },],
        "withSubheader": [{ type: i0.HostBinding, args: ['class.with-subheader',] },],
        "center": [{ type: i0.Input },],
        "windowMode": [{ type: i0.Input },],
        "withScroll": [{ type: i0.Input },],
        "restoreScrollTop": [{ type: i0.Input },],
        "veryTopRef": [{ type: i0.ViewChild, args: ['layoutTopDynamicArea', { read: i0.ViewContainerRef },] },],
        "scrollableContainerRef": [{ type: i0.ViewChild, args: ['scrollableContainer', { read: i0.ElementRef },] },],
        "onScroll": [{ type: i0.HostListener, args: ['window:scroll', ['$event'],] },],
        "onResize": [{ type: i0.HostListener, args: ['window:resize', ['$event'],] },],
    };
    return NbLayoutComponent;
}());

var NB_LAYOUT_COMPONENTS = [
    NbLayoutComponent,
    NbLayoutColumnComponent,
    NbLayoutFooterComponent,
    NbLayoutHeaderComponent,
];
var NbLayoutModule = /** @class */ (function () {
    function NbLayoutModule() {
    }
    NbLayoutModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: NB_LAYOUT_COMPONENTS.slice(),
                    providers: [
                        NbRestoreScrollTopHelper,
                    ],
                    exports: NB_LAYOUT_COMPONENTS.slice(),
                },] },
    ];
    return NbLayoutModule;
}());

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var itemClick$ = new rxjs.Subject();
var addItems$ = new rxjs.ReplaySubject(1);
var navigateHome$ = new rxjs.ReplaySubject(1);
var getSelectedItem$ = new rxjs.ReplaySubject(1);
var itemSelect$ = new rxjs.ReplaySubject(1);
var itemHover$ = new rxjs.ReplaySubject(1);
var submenuToggle$ = new rxjs.ReplaySubject(1);
var collapseAll$ = new rxjs.ReplaySubject(1);
// TODO: check if we need both URL and LINK
/**
 * Menu Item options
 */
var NbMenuItem = /** @class */ (function () {
    function NbMenuItem() {
        /**
           * Children items height
           * @type {number}
           */
        this.subMenuHeight = 0;
        /**
           * Item is selected when partly or fully equal to the current url
           * @type {string}
           */
        this.pathMatch = 'full';
    }
    /**
     * @returns item parents in top-down order
     */
    /**
       * @returns item parents in top-down order
       */
    NbMenuItem.getParents = /**
       * @returns item parents in top-down order
       */
    function (item) {
        var parents = [];
        var parent = item.parent;
        while (parent) {
            parents.unshift(parent);
            parent = parent.parent;
        }
        return parents;
    };
    NbMenuItem.isParent = function (item, possibleChild) {
        return possibleChild.parent
            ? possibleChild.parent === item || this.isParent(item, possibleChild.parent)
            : false;
    };
    return NbMenuItem;
}());
// TODO: map select events to router change events
// TODO: review the interface
/**
 * Menu Service. Allows you to listen to menu events, or to interact with a menu.
 */
var NbMenuService = /** @class */ (function () {
    function NbMenuService() {
    }
    /**
     * Add items to the end of the menu items list
     * @param {List<NbMenuItem>} items
     * @param {string} tag
     */
    /**
       * Add items to the end of the menu items list
       * @param {List<NbMenuItem>} items
       * @param {string} tag
       */
    NbMenuService.prototype.addItems = /**
       * Add items to the end of the menu items list
       * @param {List<NbMenuItem>} items
       * @param {string} tag
       */
    function (items, tag) {
        addItems$.next({ tag: tag, items: items });
    };
    /**
     * Collapses all menu items
     * @param {string} tag
     */
    /**
       * Collapses all menu items
       * @param {string} tag
       */
    NbMenuService.prototype.collapseAll = /**
       * Collapses all menu items
       * @param {string} tag
       */
    function (tag) {
        collapseAll$.next({ tag: tag });
    };
    /**
     * Navigate to the home menu item
     * @param {string} tag
     */
    /**
       * Navigate to the home menu item
       * @param {string} tag
       */
    NbMenuService.prototype.navigateHome = /**
       * Navigate to the home menu item
       * @param {string} tag
       */
    function (tag) {
        navigateHome$.next({ tag: tag });
    };
    /**
     * Returns currently selected item. Won't subscribe to the future events.
     * @param {string} tag
     * @returns {Observable<{tag: string; item: NbMenuItem}>}
     */
    /**
       * Returns currently selected item. Won't subscribe to the future events.
       * @param {string} tag
       * @returns {Observable<{tag: string; item: NbMenuItem}>}
       */
    NbMenuService.prototype.getSelectedItem = /**
       * Returns currently selected item. Won't subscribe to the future events.
       * @param {string} tag
       * @returns {Observable<{tag: string; item: NbMenuItem}>}
       */
    function (tag) {
        var listener = new rxjs.BehaviorSubject(null);
        getSelectedItem$.next({ tag: tag, listener: listener });
        return listener.asObservable();
    };
    NbMenuService.prototype.onItemClick = function () {
        return itemClick$.pipe(rxjs_operators.share());
    };
    NbMenuService.prototype.onItemSelect = function () {
        return itemSelect$.pipe(rxjs_operators.share());
    };
    NbMenuService.prototype.onItemHover = function () {
        return itemHover$.pipe(rxjs_operators.share());
    };
    NbMenuService.prototype.onSubmenuToggle = function () {
        return submenuToggle$.pipe(rxjs_operators.share());
    };
    NbMenuService.decorators = [
        { type: i0.Injectable },
    ];
    return NbMenuService;
}());
var NbMenuInternalService = /** @class */ (function () {
    function NbMenuInternalService(location) {
        this.location = location;
    }
    NbMenuInternalService.prototype.prepareItems = function (items) {
        var _this = this;
        var defaultItem = new NbMenuItem();
        items.forEach(function (i) {
            _this.applyDefaults(i, defaultItem);
            _this.setParent(i);
        });
    };
    NbMenuInternalService.prototype.selectFromUrl = function (items, tag, collapseOther) {
        if (collapseOther === void 0) { collapseOther = false; }
        var selectedItem = this.findItemByUrl(items);
        if (selectedItem) {
            this.selectItem(selectedItem, items, collapseOther, tag);
        }
    };
    NbMenuInternalService.prototype.selectItem = function (item, items, collapseOther, tag) {
        if (collapseOther === void 0) { collapseOther = false; }
        var unselectedItems = this.resetSelection(items);
        var collapsedItems = collapseOther ? this.collapseItems(items) : [];
        for (var _i = 0, _a = NbMenuItem.getParents(item); _i < _a.length; _i++) {
            var parent_1 = _a[_i];
            parent_1.selected = true;
            // emit event only for items that weren't selected before ('unselectedItems' contains items that were selected)
            if (!unselectedItems.includes(parent_1)) {
                this.itemSelect(parent_1, tag);
            }
            var wasNotExpanded = !parent_1.expanded;
            parent_1.expanded = true;
            var i = collapsedItems.indexOf(parent_1);
            // emit event only for items that weren't expanded before.
            // 'collapsedItems' contains items that were expanded, so no need to emit event.
            // in case 'collapseOther' is false, 'collapsedItems' will be empty,
            // so also check if item isn't expanded already ('wasNotExpanded').
            if (i === -1 && wasNotExpanded) {
                this.submenuToggle(parent_1, tag);
            }
            else {
                collapsedItems.splice(i, 1);
            }
        }
        item.selected = true;
        // emit event only for items that weren't selected before ('unselectedItems' contains items that were selected)
        if (!unselectedItems.includes(item)) {
            this.itemSelect(item, tag);
        }
        // remaining items which wasn't expanded back after expanding all currently selected items
        for (var _b = 0, collapsedItems_1 = collapsedItems; _b < collapsedItems_1.length; _b++) {
            var collapsedItem = collapsedItems_1[_b];
            this.submenuToggle(collapsedItem, tag);
        }
    };
    NbMenuInternalService.prototype.collapseAll = function (items, tag, except) {
        var collapsedItems = this.collapseItems(items, except);
        for (var _i = 0, collapsedItems_2 = collapsedItems; _i < collapsedItems_2.length; _i++) {
            var item = collapsedItems_2[_i];
            this.submenuToggle(item, tag);
        }
    };
    NbMenuInternalService.prototype.onAddItem = function () {
        return addItems$.pipe(rxjs_operators.share());
    };
    NbMenuInternalService.prototype.onNavigateHome = function () {
        return navigateHome$.pipe(rxjs_operators.share());
    };
    NbMenuInternalService.prototype.onCollapseAll = function () {
        return collapseAll$.pipe(rxjs_operators.share());
    };
    NbMenuInternalService.prototype.onGetSelectedItem = function () {
        return getSelectedItem$.pipe(rxjs_operators.share());
    };
    NbMenuInternalService.prototype.itemHover = function (item, tag) {
        itemHover$.next({ tag: tag, item: item });
    };
    NbMenuInternalService.prototype.submenuToggle = function (item, tag) {
        submenuToggle$.next({ tag: tag, item: item });
    };
    NbMenuInternalService.prototype.itemSelect = function (item, tag) {
        itemSelect$.next({ tag: tag, item: item });
    };
    NbMenuInternalService.prototype.itemClick = function (item, tag) {
        itemClick$.next({ tag: tag, item: item });
    };
    /**
     * Unselect all given items deeply.
     * @param items array of items to unselect.
     * @returns items which selected value was changed.
     */
    /**
       * Unselect all given items deeply.
       * @param items array of items to unselect.
       * @returns items which selected value was changed.
       */
    NbMenuInternalService.prototype.resetSelection = /**
       * Unselect all given items deeply.
       * @param items array of items to unselect.
       * @returns items which selected value was changed.
       */
    function (items) {
        var unselectedItems = [];
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            if (item.selected) {
                unselectedItems.push(item);
            }
            item.selected = false;
            if (item.children) {
                unselectedItems.push.apply(unselectedItems, this.resetSelection(item.children));
            }
        }
        return unselectedItems;
    };
    /**
     * Collapse all given items deeply.
     * @param items array of items to collapse.
     * @param except menu item which shouldn't be collapsed, also disables collapsing for parents of this item.
     * @returns items which expanded value was changed.
     */
    /**
       * Collapse all given items deeply.
       * @param items array of items to collapse.
       * @param except menu item which shouldn't be collapsed, also disables collapsing for parents of this item.
       * @returns items which expanded value was changed.
       */
    NbMenuInternalService.prototype.collapseItems = /**
       * Collapse all given items deeply.
       * @param items array of items to collapse.
       * @param except menu item which shouldn't be collapsed, also disables collapsing for parents of this item.
       * @returns items which expanded value was changed.
       */
    function (items, except) {
        var collapsedItems = [];
        for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
            var item = items_2[_i];
            if (except && (item === except || NbMenuItem.isParent(item, except))) {
                continue;
            }
            if (item.expanded) {
                collapsedItems.push(item);
            }
            item.expanded = false;
            if (item.children) {
                collapsedItems.push.apply(collapsedItems, this.collapseItems(item.children));
            }
        }
        return collapsedItems;
    };
    NbMenuInternalService.prototype.applyDefaults = function (item, defaultItem) {
        var _this = this;
        var menuItem = __assign({}, item);
        Object.assign(item, defaultItem, menuItem);
        item.children && item.children.forEach(function (child) {
            _this.applyDefaults(child, defaultItem);
        });
    };
    NbMenuInternalService.prototype.setParent = function (item) {
        var _this = this;
        item.children && item.children.forEach(function (child) {
            child.parent = item;
            _this.setParent(child);
        });
    };
    /**
     * Find deepest item which link matches current URL path.
     * @param items array of items to search in.
     * @returns found item of undefined.
     */
    /**
       * Find deepest item which link matches current URL path.
       * @param items array of items to search in.
       * @returns found item of undefined.
       */
    NbMenuInternalService.prototype.findItemByUrl = /**
       * Find deepest item which link matches current URL path.
       * @param items array of items to search in.
       * @returns found item of undefined.
       */
    function (items) {
        var _this = this;
        var selectedItem;
        items.some(function (item) {
            if (item.children) {
                selectedItem = _this.findItemByUrl(item.children);
            }
            if (!selectedItem && _this.isSelectedInUrl(item)) {
                selectedItem = item;
            }
            return selectedItem;
        });
        return selectedItem;
    };
    NbMenuInternalService.prototype.isSelectedInUrl = function (item) {
        var exact = item.pathMatch === 'full';
        return exact
            ? isUrlPathEqual(this.location.path(), item.link)
            : isUrlPathContain(this.location.path(), item.link);
    };
    NbMenuInternalService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbMenuInternalService.ctorParameters = function () { return [
        { type: i1.Location, },
    ]; };
    return NbMenuInternalService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbToggleStates;
(function (NbToggleStates) {
    NbToggleStates["Expanded"] = "expanded";
    NbToggleStates["Collapsed"] = "collapsed";
})(NbToggleStates || (NbToggleStates = {}));
var NbMenuItemComponent = /** @class */ (function () {
    function NbMenuItemComponent(menuService) {
        this.menuService = menuService;
        this.menuItem = null;
        this.hoverItem = new i0.EventEmitter();
        this.toggleSubMenu = new i0.EventEmitter();
        this.selectItem = new i0.EventEmitter();
        this.itemClick = new i0.EventEmitter();
        this.alive = true;
    }
    NbMenuItemComponent.prototype.ngDoCheck = function () {
        this.toggleState = this.menuItem.expanded ? NbToggleStates.Expanded : NbToggleStates.Collapsed;
    };
    NbMenuItemComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.menuService.onSubmenuToggle()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (_a) {
            var item = _a.item;
            return item === _this.menuItem;
        }), rxjs_operators.map(function (_a) {
            var item = _a.item;
            return item.expanded;
        }))
            .subscribe(function (isExpanded) { return _this.toggleState = isExpanded ? NbToggleStates.Expanded : NbToggleStates.Collapsed; });
    };
    NbMenuItemComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbMenuItemComponent.prototype.onToggleSubMenu = function (item) {
        this.toggleSubMenu.emit(item);
    };
    NbMenuItemComponent.prototype.onHoverItem = function (item) {
        this.hoverItem.emit(item);
    };
    NbMenuItemComponent.prototype.onSelectItem = function (item) {
        this.selectItem.emit(item);
    };
    NbMenuItemComponent.prototype.onItemClick = function (item) {
        this.itemClick.emit(item);
    };
    NbMenuItemComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: '[nbMenuItem]',
                    template: "<span *ngIf=\"menuItem.group\"> <i class=\"menu-icon {{ menuItem.icon }}\" *ngIf=\"menuItem.icon\"></i> {{ menuItem.title }} </span> <a *ngIf=\"menuItem.link && !menuItem.url && !menuItem.children && !menuItem.group\" [routerLink]=\"menuItem.link\" [queryParams]=\"menuItem.queryParams\" [fragment]=\"menuItem.fragment\" [attr.target]=\"menuItem.target\" [attr.title]=\"menuItem.title\" [class.active]=\"menuItem.selected\" (mouseenter)=\"onHoverItem(menuItem)\" (click)=\"onItemClick(menuItem);\"> <i class=\"menu-icon {{ menuItem.icon }}\" *ngIf=\"menuItem.icon\"></i> <span class=\"menu-title\">{{ menuItem.title }}</span> </a> <a *ngIf=\"menuItem.url && !menuItem.children && !menuItem.link && !menuItem.group\" [attr.href]=\"menuItem.url\" [attr.target]=\"menuItem.target\" [attr.title]=\"menuItem.title\" [class.active]=\"menuItem.selected\" (mouseenter)=\"onHoverItem(menuItem)\" (click)=\"onSelectItem(menuItem)\"> <i class=\"menu-icon {{ menuItem.icon }}\" *ngIf=\"menuItem.icon\"></i> <span class=\"menu-title\">{{ menuItem.title }}</span> </a> <a *ngIf=\"!menuItem.children && !menuItem.link && !menuItem.url && !menuItem.group\" [attr.target]=\"menuItem.target\" [attr.title]=\"menuItem.title\" [class.active]=\"menuItem.selected\" (mouseenter)=\"onHoverItem(menuItem)\" (click)=\"$event.preventDefault(); onItemClick(menuItem);\"> <i class=\"menu-icon {{ menuItem.icon }}\" *ngIf=\"menuItem.icon\"></i> <span class=\"menu-title\">{{ menuItem.title }}</span> </a> <a *ngIf=\"menuItem.children\" (click)=\"$event.preventDefault(); onToggleSubMenu(menuItem);\" [attr.target]=\"menuItem.target\" [attr.title]=\"menuItem.title\" [class.active]=\"menuItem.selected\" (mouseenter)=\"onHoverItem(menuItem)\" href=\"#\"> <i class=\"menu-icon {{ menuItem.icon }}\" *ngIf=\"menuItem.icon\"></i> <span class=\"menu-title\">{{ menuItem.title }}</span> <i class=\"ion chevron\" [class.ion-chevron-left]=\"!menuItem.expanded\" [class.ion-chevron-down]=\"menuItem.expanded\"></i> </a> <ul *ngIf=\"menuItem.children\" [class.collapsed]=\"!(menuItem.children && menuItem.expanded)\" [class.expanded]=\"menuItem.expanded\" [@toggle]=\"toggleState\" class=\"menu-items\"> <ng-container *ngFor=\"let item of menuItem.children\"> <li nbMenuItem *ngIf=\"!item.hidden\" [menuItem]=\"item\" [class.menu-group]=\"item.group\" (hoverItem)=\"onHoverItem($event)\" (toggleSubMenu)=\"onToggleSubMenu($event)\" (selectItem)=\"onSelectItem($event)\" (itemClick)=\"onItemClick($event)\" class=\"menu-item\"> </li> </ng-container> </ul> ",
                    animations: [
                        _angular_animations.trigger('toggle', [
                            _angular_animations.state(NbToggleStates.Collapsed, _angular_animations.style({ height: '0' })),
                            _angular_animations.state(NbToggleStates.Expanded, _angular_animations.style({ height: '*' })),
                            _angular_animations.transition(NbToggleStates.Collapsed + " <=> " + NbToggleStates.Expanded, _angular_animations.animate(300)),
                        ]),
                    ],
                },] },
    ];
    /** @nocollapse */
    NbMenuItemComponent.ctorParameters = function () { return [
        { type: NbMenuService, },
    ]; };
    NbMenuItemComponent.propDecorators = {
        "menuItem": [{ type: i0.Input },],
        "hoverItem": [{ type: i0.Output },],
        "toggleSubMenu": [{ type: i0.Output },],
        "selectItem": [{ type: i0.Output },],
        "itemClick": [{ type: i0.Output },],
    };
    return NbMenuItemComponent;
}());
/**
 * Vertical menu component.
 *
 * Accepts a list of menu items and renders them accordingly. Supports multi-level menus.
 *
 * Basic example
 * @stacked-example(Showcase, menu/menu-showcase.component)
 *
 * ```ts
 * // ...
 * items: NbMenuItem[] = [
 *  {
 *    title: home,
 *    link: '/'
 *  },
 *  {
 *    title: dashboard,
 *    link: 'dashboard'
 *  }
 * ];
 * // ...
 * <nb-menu [items]="items"></nb-menu>
 * ```
 * ### Installation
 *
 * Import `NbMenuModule.forRoot()` to your app module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbMenuModule.forRoot(),
 *   ],
 * })
 * export class AppModule { }
 * ```
 * and `NbMenuModule` to your feature module where the component should be shown:
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbMenuModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Two-level menu example
 * @stacked-example(Two Levels, menu/menu-children.component)
 *
 * @styles
 *
 * menu-font-family:
 * menu-font-size:
 * menu-font-weight:
 * menu-fg:
 * menu-bg:
 * menu-active-fg:
 * menu-active-bg:
 * menu-active-font-weight:
 * menu-submenu-bg:
 * menu-submenu-fg:
 * menu-submenu-active-fg:
 * menu-submenu-active-bg:
 * menu-submenu-active-border-color:
 * menu-submenu-active-shadow:
 * menu-submenu-hover-fg:
 * menu-submenu-hover-bg:
 * menu-submenu-item-border-width:
 * menu-submenu-item-border-radius:
 * menu-submenu-item-padding:
 * menu-submenu-item-container-padding:
 * menu-submenu-padding:
 * menu-group-font-weight:
 * menu-group-font-size:
 * menu-group-fg:
 * menu-group-padding
 * menu-item-padding:
 * menu-item-separator:
 * menu-icon-font-size:
 * menu-icon-margin:
 * menu-icon-color:
 * menu-icon-active-color:
 */
var NbMenuComponent = /** @class */ (function () {
    function NbMenuComponent(window, menuInternalService, router) {
        this.window = window;
        this.menuInternalService = menuInternalService;
        this.router = router;
        this.alive = true;
        this.autoCollapseValue = false;
    }
    Object.defineProperty(NbMenuComponent.prototype, "inverse", {
        set: /**
           * Makes colors inverse based on current theme
           * @type boolean
           */
        function (val) {
            this.inverseValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbMenuComponent.prototype, "autoCollapse", {
        set: /**
           * Collapse all opened submenus on the toggle event
           * Default value is "false"
           * @type boolean
           */
        function (val) {
            this.autoCollapseValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbMenuComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuInternalService.prepareItems(this.items);
        this.menuInternalService
            .onAddItem()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (data) { return _this.compareTag(data.tag); }))
            .subscribe(function (data) { return _this.onAddItem(data); });
        this.menuInternalService
            .onNavigateHome()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (data) { return _this.compareTag(data.tag); }))
            .subscribe(function () { return _this.navigateHome(); });
        this.menuInternalService
            .onGetSelectedItem()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (data) { return _this.compareTag(data.tag); }))
            .subscribe(function (data) {
            data.listener.next({ tag: _this.tag, item: _this.getSelectedItem(_this.items) });
        });
        this.menuInternalService
            .onCollapseAll()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (data) { return _this.compareTag(data.tag); }))
            .subscribe(function () { return _this.collapseAll(); });
        this.router.events
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (event) { return event instanceof _angular_router.NavigationEnd; }))
            .subscribe(function () {
            _this.menuInternalService.selectFromUrl(_this.items, _this.tag, _this.autoCollapseValue);
        });
    };
    NbMenuComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.menuInternalService.selectFromUrl(_this.items, _this.tag, _this.autoCollapseValue); });
    };
    NbMenuComponent.prototype.onAddItem = function (data) {
        (_a = this.items).push.apply(_a, data.items);
        this.menuInternalService.prepareItems(this.items);
        this.menuInternalService.selectFromUrl(this.items, this.tag, this.autoCollapseValue);
        var _a;
    };
    NbMenuComponent.prototype.onHoverItem = function (item) {
        this.menuInternalService.itemHover(item, this.tag);
    };
    NbMenuComponent.prototype.onToggleSubMenu = function (item) {
        if (this.autoCollapseValue) {
            this.menuInternalService.collapseAll(this.items, this.tag, item);
        }
        item.expanded = !item.expanded;
        this.menuInternalService.submenuToggle(item, this.tag);
    };
    // TODO: is not fired on page reload
    // TODO: is not fired on page reload
    NbMenuComponent.prototype.onSelectItem = 
    // TODO: is not fired on page reload
    function (item) {
        this.menuInternalService.selectItem(item, this.items, this.autoCollapseValue, this.tag);
    };
    NbMenuComponent.prototype.onItemClick = function (item) {
        this.menuInternalService.itemClick(item, this.tag);
    };
    NbMenuComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbMenuComponent.prototype.navigateHome = function () {
        var homeItem = this.getHomeItem(this.items);
        if (homeItem) {
            if (homeItem.link) {
                this.router.navigate([homeItem.link], { queryParams: homeItem.queryParams, fragment: homeItem.fragment });
            }
            if (homeItem.url) {
                this.window.location.href = homeItem.url;
            }
        }
    };
    NbMenuComponent.prototype.collapseAll = function () {
        this.menuInternalService.collapseAll(this.items, this.tag);
    };
    NbMenuComponent.prototype.getHomeItem = function (items) {
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            if (item.home) {
                return item;
            }
            var homeItem = item.children && this.getHomeItem(item.children);
            if (homeItem) {
                return homeItem;
            }
        }
    };
    NbMenuComponent.prototype.compareTag = function (tag) {
        return !tag || tag === this.tag;
    };
    NbMenuComponent.prototype.getSelectedItem = function (items) {
        var _this = this;
        var selected = null;
        items.forEach(function (item) {
            if (item.selected) {
                selected = item;
            }
            if (item.selected && item.children && item.children.length > 0) {
                selected = _this.getSelectedItem(item.children);
            }
        });
        return selected;
    };
    NbMenuComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-menu',
                    styles: [":host /deep/ {display:block}:host /deep/ .menu-items,:host /deep/ .menu-item>.menu-items{list-style-type:none;overflow:hidden}:host /deep/ .menu-item a{display:flex;color:inherit;text-decoration:none;align-items:center}:host /deep/ .menu-item a .menu-title{flex:1}[dir=rtl] :host /deep/ .menu-item a .menu-title{text-align:right} "],
                    template: "\n    <ul class=\"menu-items\">\n      <ng-container *ngFor=\"let item of items\">\n        <li nbMenuItem *ngIf=\"!item.hidden\"\n            [menuItem]=\"item\"\n            [class.menu-group]=\"item.group\"\n            (hoverItem)=\"onHoverItem($event)\"\n            (toggleSubMenu)=\"onToggleSubMenu($event)\"\n            (selectItem)=\"onSelectItem($event)\"\n            (itemClick)=\"onItemClick($event)\"\n            class=\"menu-item\">\n        </li>\n      </ng-container>\n    </ul>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbMenuComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_WINDOW,] },] },
        { type: NbMenuInternalService, },
        { type: _angular_router.Router, },
    ]; };
    NbMenuComponent.propDecorators = {
        "inverseValue": [{ type: i0.HostBinding, args: ['class.inverse',] },],
        "tag": [{ type: i0.Input },],
        "items": [{ type: i0.Input },],
        "inverse": [{ type: i0.Input },],
        "autoCollapse": [{ type: i0.Input },],
    };
    return NbMenuComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var nbMenuComponents = [NbMenuComponent, NbMenuItemComponent];
var NB_MENU_PROVIDERS = [NbMenuService, NbMenuInternalService];
var NbMenuModule = /** @class */ (function () {
    function NbMenuModule() {
    }
    NbMenuModule.forRoot = function () {
        return {
            ngModule: NbMenuModule,
            providers: NB_MENU_PROVIDERS.slice(),
        };
    };
    NbMenuModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbSharedModule],
                    declarations: nbMenuComponents.slice(),
                    exports: nbMenuComponents.slice(),
                },] },
    ];
    return NbMenuModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Route tabset components.
 * Renders tabs inside of a router-outlet.
 *
 * ```ts
 *  tabs = [
 *  {
 *    title: 'Route tab #1',
 *    route: '/pages/description',
 *    icon: 'nb-home',
 *    responsive: true, // hide title before `route-tabs-icon-only-max-width` value
 *  },
 *  {
 *    title: 'Route tab #2',
 *    route: '/pages/images',
 *    }
 *  ];
 *
 *  <nb-route-tabset [tabs]="tabs"></nb-route-tabset>
 * ```
 * ### Installation
 *
 * Import `NbRouteTabsetModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbRouteTabsetModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 *
 * @stacked-example(Route Tabset, tabset/route-tabset-showcase.component)
 *
 * @styles
 *
 * route-tabs-font-family:
 * route-tabs-font-size:
 * route-tabs-active-bg:
 * route-tabs-active-font-weight:
 * route-tabs-padding:
 * route-tabs-header-bg:
 * route-tabs-separator:
 * route-tabs-fg:
 * route-tabs-fg-heading:
 * route-tabs-bg:
 * route-tabs-selected:
 * route-tabs-icon-only-max-width:
 */
var NbRouteTabsetComponent = /** @class */ (function () {
    function NbRouteTabsetComponent(router) {
        this.router = router;
        this.fullWidthValue = false;
        /**
           * Emits when tab is selected
           * @type {EventEmitter<any>}
           */
        this.changeTab = new i0.EventEmitter();
    }
    Object.defineProperty(NbRouteTabsetComponent.prototype, "fullWidth", {
        set: /**
           * Take full width of a parent
           * @param {boolean} val
           */
        function (val) {
            this.fullWidthValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbRouteTabsetComponent.prototype.selectTab = function (tab) {
        this.changeTab.emit(tab);
        this.router.navigate([tab.route]);
    };
    NbRouteTabsetComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-route-tabset',
                    styles: ["ul{display:flex;flex-direction:row;list-style-type:none;margin:0}ul li{cursor:pointer;margin-bottom:-1px;text-align:center}ul li.active a::before{display:block}ul li a{position:relative;text-decoration:none;display:inline-block}ul li a::before{display:none;position:absolute;content:'';width:100%;height:6px;border-radius:3px;bottom:-2px;left:0}ul li a i{font-size:1.5rem;vertical-align:middle}[dir=ltr] ul li a i+span{margin-left:.5rem}[dir=rtl] ul li a i+span{margin-right:.5rem}:host.full-width ul{justify-content:space-around} "],
                    template: "\n    <ul>\n      <li *ngFor=\"let tab of tabs\"\n          (click)=\"$event.preventDefault(); selectTab(tab)\"\n          routerLink=\"{{tab.route}}\"\n          routerLinkActive=\"active\"\n          [routerLinkActiveOptions]=\"{ exact: true }\"\n          [class.responsive]=\"tab.responsive\">\n        <a href>\n          <i *ngIf=\"tab.icon\" [class]=\"tab.icon\"></i>\n          <span *ngIf=\"tab.title\">{{ tab.title }}</span>\n        </a>\n      </li>\n    </ul>\n    <router-outlet></router-outlet>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbRouteTabsetComponent.ctorParameters = function () { return [
        { type: _angular_router.Router, },
    ]; };
    NbRouteTabsetComponent.propDecorators = {
        "fullWidthValue": [{ type: i0.HostBinding, args: ['class.full-width',] },],
        "tabs": [{ type: i0.Input },],
        "fullWidth": [{ type: i0.Input },],
        "changeTab": [{ type: i0.Output },],
    };
    return NbRouteTabsetComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbRouteTabsetModule = /** @class */ (function () {
    function NbRouteTabsetModule() {
    }
    NbRouteTabsetModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: [
                        NbRouteTabsetComponent,
                    ],
                    exports: [
                        NbRouteTabsetComponent,
                    ],
                },] },
    ];
    return NbRouteTabsetModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Sidebar service.
 *
 * Root module service to control the sidebar from any part of the app.
 *
 * Allows you to change sidebar state dynamically from any part of the app:
 * @stacked-example(Sidebar State, sidebar/sidebar-toggle.component)
 */
var NbSidebarService = /** @class */ (function () {
    function NbSidebarService() {
        this.toggle$ = new rxjs.Subject();
        this.expand$ = new rxjs.Subject();
        this.collapse$ = new rxjs.Subject();
    }
    /**
     * Subscribe to toggle events
     *
     * @returns Observable<{ compact: boolean, tag: string }>
     */
    /**
       * Subscribe to toggle events
       *
       * @returns Observable<{ compact: boolean, tag: string }>
       */
    NbSidebarService.prototype.onToggle = /**
       * Subscribe to toggle events
       *
       * @returns Observable<{ compact: boolean, tag: string }>
       */
    function () {
        return this.toggle$.pipe(rxjs_operators.share());
    };
    /**
     * Subscribe to expand events
     * @returns Observable<{ tag: string }>
     */
    /**
       * Subscribe to expand events
       * @returns Observable<{ tag: string }>
       */
    NbSidebarService.prototype.onExpand = /**
       * Subscribe to expand events
       * @returns Observable<{ tag: string }>
       */
    function () {
        return this.expand$.pipe(rxjs_operators.share());
    };
    /**
     * Subscribe to collapse evens
     * @returns Observable<{ tag: string }>
     */
    /**
       * Subscribe to collapse evens
       * @returns Observable<{ tag: string }>
       */
    NbSidebarService.prototype.onCollapse = /**
       * Subscribe to collapse evens
       * @returns Observable<{ tag: string }>
       */
    function () {
        return this.collapse$.pipe(rxjs_operators.share());
    };
    /**
     * Toggle a sidebar
     * @param {boolean} compact
     * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
     * to specify which sidebar you want to control
     */
    /**
       * Toggle a sidebar
       * @param {boolean} compact
       * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
       * to specify which sidebar you want to control
       */
    NbSidebarService.prototype.toggle = /**
       * Toggle a sidebar
       * @param {boolean} compact
       * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
       * to specify which sidebar you want to control
       */
    function (compact, tag) {
        if (compact === void 0) { compact = false; }
        this.toggle$.next({ compact: compact, tag: tag });
    };
    /**
     * Expands a sidebar
     * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
     * to specify which sidebar you want to control
     */
    /**
       * Expands a sidebar
       * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
       * to specify which sidebar you want to control
       */
    NbSidebarService.prototype.expand = /**
       * Expands a sidebar
       * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
       * to specify which sidebar you want to control
       */
    function (tag) {
        this.expand$.next({ tag: tag });
    };
    /**
     * Collapses a sidebar
     * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
     * to specify which sidebar you want to control
     */
    /**
       * Collapses a sidebar
       * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
       * to specify which sidebar you want to control
       */
    NbSidebarService.prototype.collapse = /**
       * Collapses a sidebar
       * @param {string} tag If you have multiple sidebars on the page, mark them with `tag` input property and pass it here
       * to specify which sidebar you want to control
       */
    function (tag) {
        this.collapse$.next({ tag: tag });
    };
    NbSidebarService.decorators = [
        { type: i0.Injectable },
    ];
    return NbSidebarService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Sidebar header container.
 *
 * Placeholder which contains a sidebar header content,
 * placed at the very top of the sidebar outside of the scroll area.
 */
var NbSidebarHeaderComponent = /** @class */ (function () {
    function NbSidebarHeaderComponent() {
    }
    NbSidebarHeaderComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-sidebar-header',
                    template: "\n    <ng-content></ng-content>\n  ",
                },] },
    ];
    return NbSidebarHeaderComponent;
}());
/**
 * Sidebar footer container.
 *
 * Placeholder which contains a sidebar footer content,
 * placed at the very bottom of the sidebar outside of the scroll area.
 */
var NbSidebarFooterComponent = /** @class */ (function () {
    function NbSidebarFooterComponent() {
    }
    NbSidebarFooterComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-sidebar-footer',
                    template: "\n    <ng-content></ng-content>\n  ",
                },] },
    ];
    return NbSidebarFooterComponent;
}());
/**
 * Layout sidebar component.
 *
 * @stacked-example(Showcase, sidebar/sidebar-showcase.component)
 *
 * ### Installation
 *
 * Import `NbSidebarModule.forRoot()` to your app module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbSidebarModule.forRoot(),
 *   ],
 * })
 * export class AppModule { }
 * ```
 * and `NbSidebarModule` to your feature module where the component should be shown:
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbSidebarModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Sidebar can be placed on the left or the right side of the layout,
 * or on start/end position of layout (depends on document direction, left to right or right to left)
 * It can be fixed (shown above the content) or can push the layout when opened.
 *
 * There are three states - `expanded`, `collapsed`, `compacted`.
 * By default sidebar content is fixed and saves its position while the page is being scrolled.
 *
 * Compacted sidebar example:
 * @stacked-example(Compacted Sidebar, sidebar/sidebar-compacted.component)
 *
 * Sidebar also supports a `responsive` behavior, listening to window size change and changing its size respectably.
 *
 * In a pair with header it is possible to setup a configuration when header is placed on a side of the sidebar
 * and not on top of it. To achieve this simply put a `subheader` property to the header like this:
 * ```html
 * <nb-layout-header subheader></nb-layout-header>
 * ```
 * @stacked-example(Subheader, layout/layout-sidebar-subheader.component)
 * Note that in such configuration sidebar shadow is removed and header cannot be make `fixed`.
 *
 * @additional-example(Right Sidebar, sidebar/sidebar-right.component)
 * @additional-example(Fixed Sidebar, sidebar/sidebar-fixed.component)
 *
 * @styles
 *
 * sidebar-font-size: Sidebar content font size
 * sidebar-line-height: Sidebar content line height
 * sidebar-fg: Foreground color
 * sidebar-bg: Background color
 * sidebar-height: Content height
 * sidebar-width: Expanded width
 * sidebar-width-compact: Compacted width
 * sidebar-padding: Sidebar content padding
 * sidebar-header-height: Sidebar header height
 * sidebar-footer-height: Sidebar footer height
 * sidebar-shadow: Sidebar container shadow
 *
 */
var NbSidebarComponent = /** @class */ (function () {
    function NbSidebarComponent(sidebarService, themeService, element) {
        this.sidebarService = sidebarService;
        this.themeService = themeService;
        this.element = element;
        this.responsiveValue = false;
        this.alive = true;
        this.containerFixedValue = true;
        this.fixedValue = false;
        this.rightValue = false;
        this.leftValue = true;
        this.startValue = false;
        this.endValue = false;
        // TODO: get width by the key and define only max width for the tablets and mobiles
        /**
           * Controls on which screen sizes sidebar should be switched to compacted state.
           * Works only when responsive mode is on.
           * Default values are `['xs', 'is', 'sm', 'md', 'lg']`.
           *
           * @type string[]
           */
        this.compactedBreakpoints = ['xs', 'is', 'sm', 'md', 'lg'];
        /**
           * Controls on which screen sizes sidebar should be switched to collapsed state.
           * Works only when responsive mode is on.
           * Default values are `['xs', 'is']`.
           *
           * @type string[]
           */
        this.collapsedBreakpoints = ['xs', 'is'];
        this.responsiveState = NbSidebarComponent.RESPONSIVE_STATE_PC;
    }
    Object.defineProperty(NbSidebarComponent.prototype, "expanded", {
        get: 
        // TODO: rename stateValue to state (take a look to the card component)
        function () {
            return this.stateValue === NbSidebarComponent.STATE_EXPANDED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "collapsed", {
        get: function () {
            return this.stateValue === NbSidebarComponent.STATE_COLLAPSED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "compacted", {
        get: function () {
            return this.stateValue === NbSidebarComponent.STATE_COMPACTED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "right", {
        set: /**
           * Places sidebar on the right side
           * @type {boolean}
           */
        function (val) {
            this.rightValue = convertToBoolProperty(val);
            this.leftValue = !this.rightValue;
            this.startValue = false;
            this.endValue = false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "left", {
        set: /**
           * Places sidebar on the left side
           * @type {boolean}
           */
        function (val) {
            this.leftValue = convertToBoolProperty(val);
            this.rightValue = !this.leftValue;
            this.startValue = false;
            this.endValue = false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "start", {
        set: /**
           * Places sidebar on the start edge of layout
           * @type {boolean}
           */
        function (val) {
            this.startValue = convertToBoolProperty(val);
            this.endValue = !this.startValue;
            this.leftValue = false;
            this.rightValue = false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "end", {
        set: /**
           * Places sidebar on the end edge of layout
           * @type {boolean}
           */
        function (val) {
            this.endValue = convertToBoolProperty(val);
            this.startValue = !this.endValue;
            this.leftValue = false;
            this.rightValue = false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "fixed", {
        set: /**
           * Makes sidebar fixed (shown above the layout content)
           * @type {boolean}
           */
        function (val) {
            this.fixedValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "containerFixed", {
        set: /**
           * Makes sidebar container fixed
           * @type {boolean}
           */
        function (val) {
            this.containerFixedValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "state", {
        set: /**
           * Initial sidebar state, `expanded`|`collapsed`|`compacted`
           * @type {string}
           */
        function (val) {
            this.stateValue = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSidebarComponent.prototype, "responsive", {
        set: /**
           * Makes sidebar listen to media query events and change its behaviour
           * @type {boolean}
           */
        function (val) {
            this.responsiveValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbSidebarComponent.prototype.toggleResponsive = function (enabled) {
        if (enabled) {
            this.mediaQuerySubscription = this.onMediaQueryChanges();
        }
        else if (this.mediaQuerySubscription) {
            this.mediaQuerySubscription.unsubscribe();
        }
    };
    NbSidebarComponent.prototype.ngOnChanges = function (changes) {
        if (changes.responsive) {
            this.toggleResponsive(this.responsiveValue);
        }
    };
    NbSidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sidebarService.onToggle()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (data) {
            if (!_this.tag || _this.tag === data.tag) {
                _this.toggle(data.compact);
            }
        });
        this.sidebarService.onExpand()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (data) {
            if (!_this.tag || _this.tag === data.tag) {
                _this.expand();
            }
        });
        this.sidebarService.onCollapse()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (data) {
            if (!_this.tag || _this.tag === data.tag) {
                _this.collapse();
            }
        });
    };
    NbSidebarComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
        if (this.mediaQuerySubscription) {
            this.mediaQuerySubscription.unsubscribe();
        }
    };
    // TODO: this is more of a workaround, should be a better way to make components communicate to each other
    // TODO: this is more of a workaround, should be a better way to make components communicate to each other
    NbSidebarComponent.prototype.onClick = 
    // TODO: this is more of a workaround, should be a better way to make components communicate to each other
    function (event) {
        var menu = this.element.nativeElement.querySelector('nb-menu');
        if (menu && menu.contains(event.target)) {
            var link = event.target;
            var linkChildren = ['span', 'i'];
            // if we clicked on span - get the link
            if (linkChildren.includes(link.tagName.toLowerCase()) && link.parentNode) {
                link = event.target.parentNode;
            }
            // we only expand if an item has children
            if (link && link.nextElementSibling && link.nextElementSibling.classList.contains('menu-items')) {
                this.expand();
            }
        }
    };
    /**
     * Collapses the sidebar
     */
    /**
       * Collapses the sidebar
       */
    NbSidebarComponent.prototype.collapse = /**
       * Collapses the sidebar
       */
    function () {
        this.state = NbSidebarComponent.STATE_COLLAPSED;
    };
    /**
     * Expands the sidebar
     */
    /**
       * Expands the sidebar
       */
    NbSidebarComponent.prototype.expand = /**
       * Expands the sidebar
       */
    function () {
        this.state = NbSidebarComponent.STATE_EXPANDED;
    };
    /**
     * Compacts the sidebar (minimizes)
     */
    /**
       * Compacts the sidebar (minimizes)
       */
    NbSidebarComponent.prototype.compact = /**
       * Compacts the sidebar (minimizes)
       */
    function () {
        this.state = NbSidebarComponent.STATE_COMPACTED;
    };
    /**
     * Toggles sidebar state (expanded|collapsed|compacted)
     * @param {boolean} compact If true, then sidebar state will be changed between expanded & compacted,
     * otherwise - between expanded & collapsed. False by default.
     *
     * Toggle sidebar state
     *
     * ```ts
     * this.sidebar.toggle(true);
     * ```
     */
    /**
       * Toggles sidebar state (expanded|collapsed|compacted)
       * @param {boolean} compact If true, then sidebar state will be changed between expanded & compacted,
       * otherwise - between expanded & collapsed. False by default.
       *
       * Toggle sidebar state
       *
       * ```ts
       * this.sidebar.toggle(true);
       * ```
       */
    NbSidebarComponent.prototype.toggle = /**
       * Toggles sidebar state (expanded|collapsed|compacted)
       * @param {boolean} compact If true, then sidebar state will be changed between expanded & compacted,
       * otherwise - between expanded & collapsed. False by default.
       *
       * Toggle sidebar state
       *
       * ```ts
       * this.sidebar.toggle(true);
       * ```
       */
    function (compact) {
        if (compact === void 0) { compact = false; }
        if (this.responsiveEnabled()) {
            if (this.responsiveState === NbSidebarComponent.RESPONSIVE_STATE_MOBILE) {
                compact = false;
            }
        }
        var closedStates = [NbSidebarComponent.STATE_COMPACTED, NbSidebarComponent.STATE_COLLAPSED];
        if (compact) {
            this.state = closedStates.includes(this.stateValue) ?
                NbSidebarComponent.STATE_EXPANDED : NbSidebarComponent.STATE_COMPACTED;
        }
        else {
            this.state = closedStates.includes(this.stateValue) ?
                NbSidebarComponent.STATE_EXPANDED : NbSidebarComponent.STATE_COLLAPSED;
        }
    };
    NbSidebarComponent.prototype.onMediaQueryChanges = function () {
        var _this = this;
        return this.themeService.onMediaQueryChange()
            .subscribe(function (_a) {
            var prev = _a[0], current = _a[1];
            var isCollapsed = _this.collapsedBreakpoints.includes(current.name);
            var isCompacted = _this.compactedBreakpoints.includes(current.name);
            if (isCompacted) {
                _this.fixed = _this.containerFixedValue;
                _this.compact();
                _this.responsiveState = NbSidebarComponent.RESPONSIVE_STATE_TABLET;
            }
            if (isCollapsed) {
                _this.fixed = true;
                _this.collapse();
                _this.responsiveState = NbSidebarComponent.RESPONSIVE_STATE_MOBILE;
            }
            if (!isCollapsed && !isCollapsed && prev.width < current.width) {
                _this.expand();
                _this.fixed = false;
                _this.responsiveState = NbSidebarComponent.RESPONSIVE_STATE_PC;
            }
        });
    };
    NbSidebarComponent.prototype.responsiveEnabled = function () {
        return this.responsiveValue;
    };
    NbSidebarComponent.STATE_EXPANDED = 'expanded';
    NbSidebarComponent.STATE_COLLAPSED = 'collapsed';
    NbSidebarComponent.STATE_COMPACTED = 'compacted';
    NbSidebarComponent.RESPONSIVE_STATE_MOBILE = 'mobile';
    NbSidebarComponent.RESPONSIVE_STATE_TABLET = 'tablet';
    NbSidebarComponent.RESPONSIVE_STATE_PC = 'pc';
    NbSidebarComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-sidebar',
                    styles: [":host{display:flex;flex-direction:column;overflow:hidden;z-index:auto;order:0}:host .scrollable{overflow-y:auto;overflow-x:hidden;flex:1}:host .main-container{transform:translate3d(0, 0, 0);display:flex;flex-direction:column}:host .main-container-fixed{position:fixed}:host.right{margin-right:0;margin-left:auto}[dir=ltr] :host.right{order:4}[dir=rtl] :host.right{order:0}:host.end{order:4}[dir=ltr] :host.end{margin-right:0;margin-left:auto}[dir=rtl] :host.end{margin-left:0;margin-right:auto}:host.fixed{position:fixed;height:100%;z-index:999;top:0;bottom:0;left:0}:host.fixed.right{right:0}[dir=ltr] :host.fixed.start{left:0}[dir=rtl] :host.fixed.start{right:0}[dir=ltr] :host.fixed.end{right:0}[dir=rtl] :host.fixed.end{left:0}:host /deep/ nb-sidebar-footer{margin-top:auto;display:block}:host /deep/ nb-sidebar-header{display:block} "],
                    template: "\n    <div class=\"main-container\"\n         [class.main-container-fixed]=\"containerFixedValue\">\n      <ng-content select=\"nb-sidebar-header\"></ng-content>\n      <div class=\"scrollable\" (click)=\"onClick($event)\">\n        <ng-content></ng-content>\n      </div>\n      <ng-content select=\"nb-sidebar-footer\"></ng-content>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbSidebarComponent.ctorParameters = function () { return [
        { type: NbSidebarService, },
        { type: NbThemeService, },
        { type: i0.ElementRef, },
    ]; };
    NbSidebarComponent.propDecorators = {
        "fixedValue": [{ type: i0.HostBinding, args: ['class.fixed',] },],
        "rightValue": [{ type: i0.HostBinding, args: ['class.right',] },],
        "leftValue": [{ type: i0.HostBinding, args: ['class.left',] },],
        "startValue": [{ type: i0.HostBinding, args: ['class.start',] },],
        "endValue": [{ type: i0.HostBinding, args: ['class.end',] },],
        "expanded": [{ type: i0.HostBinding, args: ['class.expanded',] },],
        "collapsed": [{ type: i0.HostBinding, args: ['class.collapsed',] },],
        "compacted": [{ type: i0.HostBinding, args: ['class.compacted',] },],
        "right": [{ type: i0.Input },],
        "left": [{ type: i0.Input },],
        "start": [{ type: i0.Input },],
        "end": [{ type: i0.Input },],
        "fixed": [{ type: i0.Input },],
        "containerFixed": [{ type: i0.Input },],
        "state": [{ type: i0.Input },],
        "responsive": [{ type: i0.Input },],
        "tag": [{ type: i0.Input },],
        "compactedBreakpoints": [{ type: i0.Input },],
        "collapsedBreakpoints": [{ type: i0.Input },],
    };
    return NbSidebarComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_SIDEBAR_COMPONENTS = [
    NbSidebarComponent,
    NbSidebarFooterComponent,
    NbSidebarHeaderComponent,
];
var NB_SIDEBAR_PROVIDERS = [
    NbSidebarService,
];
var NbSidebarModule = /** @class */ (function () {
    function NbSidebarModule() {
    }
    NbSidebarModule.forRoot = function () {
        return {
            ngModule: NbSidebarModule,
            providers: NB_SIDEBAR_PROVIDERS.slice(),
        };
    };
    NbSidebarModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: NB_SIDEBAR_COMPONENTS.slice(),
                    exports: NB_SIDEBAR_COMPONENTS.slice(),
                },] },
    ];
    return NbSidebarModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Specific tab container.
 *
 * ```ts
 * <nb-tab tabTitle="Users"
 *   badgeText="99+"
 *   badgeStatus="danger">
 *   <p>List of <strong>users</strong>.</p>
 * </nb-tab>
 ```
 */
var NbTabComponent = /** @class */ (function () {
    function NbTabComponent() {
        this.activeValue = false;
        this.responsiveValue = false;
        this.init = false;
    }
    Object.defineProperty(NbTabComponent.prototype, "responsive", {
        get: function () {
            return this.responsiveValue;
        },
        set: /**
           * Show only icons when width is smaller than `tabs-icon-only-max-width`
           * @type {boolean}
           */
        function (val) {
            this.responsiveValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbTabComponent.prototype, "active", {
        get: /**
           * Specifies active tab
           * @returns {boolean}
           */
        function () {
            return this.activeValue;
        },
        set: function (val) {
            this.activeValue = convertToBoolProperty(val);
            if (this.activeValue) {
                this.init = true;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbTabComponent.prototype, "lazyLoad", {
        set: /**
           * Lazy load content before tab selection
           * TODO: rename, as lazy is by default, and this is more `instant load`
           * @param {boolean} val
           */
        function (val) {
            this.init = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbTabComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-tab',
                    template: "\n    <ng-container *ngIf=\"init\">\n      <ng-content></ng-content>\n    </ng-container>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbTabComponent.propDecorators = {
        "tabTitle": [{ type: i0.Input },],
        "tabIcon": [{ type: i0.Input },],
        "responsive": [{ type: i0.Input },],
        "route": [{ type: i0.Input },],
        "activeValue": [{ type: i0.HostBinding, args: ['class.content-active',] },],
        "active": [{ type: i0.Input },],
        "lazyLoad": [{ type: i0.Input },],
        "badgeText": [{ type: i0.Input },],
        "badgeStatus": [{ type: i0.Input },],
        "badgePosition": [{ type: i0.Input },],
    };
    return NbTabComponent;
}());
// TODO: Combine tabset with route-tabset, so that we can:
// - have similar interface
// - easy to migrate from one to another
// - can mix them both (route/content tab)
/**
 *
 * Dynamic tabset component.
 * @stacked-example(Showcase, tabset/tabset-showcase.component)
 *
 * Basic tabset example
 *
 * ```html
 * <nb-tabset>
 *  <nb-tab tabTitle="Simple Tab #1">
 *    Tab content 1
 *  </nb-tab>
 *  <nb-tab tabTitle="Simple Tab #2">
 *    Tab content 2
 *  </nb-tab>
 * </nb-tabset>
 * ```
 *
 * ### Installation
 *
 * Import `NbTabsetModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbTabsetModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * It is also possible to set a badge to a particular tab:
 * @stacked-example(Tab With Badge, tabset/tabset-badge.component)
 *
 * and we can set it to full a width of a parent component
 * @stacked-example(Full Width, tabset/tabset-width.component)
 *
 * `tabIcon` should be used to add an icon to the tab. Icon can also be combined with title.
 * `responsive` tab property if set allows you to hide the title on smaller screens
 * (`tabs-icon-only-max-width` property) for better responsive behaviour. You can open the following example and make
 * your screen smaller - titles will be hidden in the last tabset in the list:
 *
 * @stacked-example(Icon, tabset/tabset-icon.component)
 *
 * @styles
 *
 * tabs-font-family:
 * tabs-font-size:
 * tabs-content-font-family:
 * tabs-content-font-size:
 * tabs-active-bg:
 * tabs-active-font-weight:
 * tabs-padding:
 * tabs-content-padding:
 * tabs-header-bg:
 * tabs-separator:
 * tabs-fg:
 * tabs-fg-text:
 * tabs-fg-heading:
 * tabs-bg:
 * tabs-selected:
 * tabs-icon-only-max-width
 *
 */
var NbTabsetComponent = /** @class */ (function () {
    function NbTabsetComponent(route, changeDetectorRef) {
        this.route = route;
        this.changeDetectorRef = changeDetectorRef;
        this.fullWidthValue = false;
        /**
           * Emits when tab is selected
           * @type EventEmitter<any>
           */
        this.changeTab = new i0.EventEmitter();
    }
    Object.defineProperty(NbTabsetComponent.prototype, "fullWidth", {
        set: /**
           * Take full width of a parent
           * @param {boolean} val
           */
        function (val) {
            this.fullWidthValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    // TODO: refactoring this component, avoid change detection loop
    // TODO: refactoring this component, avoid change detection loop
    NbTabsetComponent.prototype.ngAfterContentInit = 
    // TODO: refactoring this component, avoid change detection loop
    function () {
        var _this = this;
        this.route.params
            .pipe(rxjs_operators.map(function (params) {
            return _this.tabs.find(function (tab) { return _this.routeParam ? tab.route === params[_this.routeParam] : tab.active; });
        }), rxjs_operators.delay(0))
            .subscribe(function (activeTab) {
            _this.selectTab(activeTab || _this.tabs.first);
            _this.changeDetectorRef.markForCheck();
        });
    };
    // TODO: navigate to routeParam
    // TODO: navigate to routeParam
    NbTabsetComponent.prototype.selectTab = 
    // TODO: navigate to routeParam
    function (selectedTab) {
        this.tabs.forEach(function (tab) { return tab.active = tab === selectedTab; });
        this.changeTab.emit(selectedTab);
    };
    NbTabsetComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-tabset',
                    styles: [":host{display:block}:host.full-width ul{justify-content:space-around}:host /deep/ nb-tab{flex:1;-ms-flex:1 1 auto;overflow:auto;display:none}:host /deep/ nb-tab.content-active{display:block}:host ul{display:flex;flex-direction:row;list-style-type:none;margin:0}:host ul li{cursor:pointer;margin-bottom:-1px;text-align:center;position:relative}:host ul li.active a::before{display:block}:host ul li a{display:flex;position:relative;text-decoration:none}:host ul li a::before{display:none;position:absolute;content:'';width:100%;height:6px;border-radius:3px;bottom:-2px;left:0}:host ul li a i{font-size:1.5rem;vertical-align:middle}[dir=ltr] :host ul li a i+span{margin-left:.5rem}[dir=rtl] :host ul li a i+span{margin-right:.5rem} "],
                    template: "\n    <ul>\n      <li *ngFor=\"let tab of tabs\"\n          (click)=\"selectTab(tab)\"\n          [class.responsive]=\"tab.responsive\"\n          [class.active]=\"tab.active\">\n        <a href (click)=\"$event.preventDefault()\">\n          <i *ngIf=\"tab.tabIcon\" [class]=\"tab.tabIcon\"></i>\n          <span *ngIf=\"tab.tabTitle\">{{ tab.tabTitle }}</span>\n        </a>\n        <nb-badge *ngIf=\"tab.badgeText\"\n          [text]=\"tab.badgeText\"\n          [status]=\"tab.badgeStatus\"\n          [position]=\"tab.badgePosition\">\n        </nb-badge>\n      </li>\n    </ul>\n    <ng-content select=\"nb-tab\"></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbTabsetComponent.ctorParameters = function () { return [
        { type: _angular_router.ActivatedRoute, },
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbTabsetComponent.propDecorators = {
        "tabs": [{ type: i0.ContentChildren, args: [NbTabComponent,] },],
        "fullWidthValue": [{ type: i0.HostBinding, args: ['class.full-width',] },],
        "fullWidth": [{ type: i0.Input },],
        "routeParam": [{ type: i0.Input },],
        "changeTab": [{ type: i0.Output },],
    };
    return NbTabsetComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Badge is a simple labeling component.
 * It can be used to add additional information to any content or highlight unread items.
 *
 * Element is absolute positioned, so parent should be
 * [positioned element](https://developer.mozilla.org/en-US/docs/Web/CSS/position).
 * It means parent `position` should be set to anything except `static`, e.g. `relative`,
 * `absolute`, `fixed`, or `sticky`.
 *
 * ### Installation
 *
 * Import `NbBadgeModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbBadgeModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Badge with default position and status(color):
 *
 * ```html
 * <nb-badge text="badgeText"></nb-badge>
 * ```
 *
 * For example, badge can be placed into nb-card header:
 * @stacked-example(Showcase, badge/badge-showcase.component)
 *
 * Badge located on the bottom right with warning status:
 *
 * ```html
 * <nb-badge text="badgeText" status="warning" position="bottom right">
 * </nb-badge>
 * ```
 *
 * @styles
 *
 * badge-fg-text:
 * badge-primary-bg-color:
 * badge-success-bg-color:
 * badge-info-bg-color:
 * badge-warning-bg-color:
 * badge-danger-bg-color:
 */
var NbBadgeComponent = /** @class */ (function () {
    function NbBadgeComponent(layoutDirectionService) {
        this.layoutDirectionService = layoutDirectionService;
        this.colorClass = NbBadgeComponent.STATUS_PRIMARY;
        /**
           * Text to display
           * @type string
           */
        this.text = '';
    }
    Object.defineProperty(NbBadgeComponent.prototype, "status", {
        set: /**
           * Badge status (adds specific styles):
           * 'primary', 'info', 'success', 'warning', 'danger'
           * @param {string} val
           * @type string
           */
        function (value) {
            if (value) {
                this.colorClass = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbBadgeComponent.prototype, "positionClass", {
        get: function () {
            if (!this.position) {
                return NbBadgeComponent.TOP_RIGHT;
            }
            var isLtr = this.layoutDirectionService.isLtr();
            var startValue = isLtr ? 'left' : 'right';
            var endValue = isLtr ? 'right' : 'left';
            return this.position
                .replace(/\bstart\b/, startValue)
                .replace(/\bend\b/, endValue);
        },
        enumerable: true,
        configurable: true
    });
    NbBadgeComponent.TOP_LEFT = 'top left';
    NbBadgeComponent.TOP_RIGHT = 'top right';
    NbBadgeComponent.BOTTOM_LEFT = 'bottom left';
    NbBadgeComponent.BOTTOM_RIGHT = 'bottom right';
    NbBadgeComponent.TOP_START = 'top start';
    NbBadgeComponent.TOP_END = 'top end';
    NbBadgeComponent.BOTTOM_START = 'bottom start';
    NbBadgeComponent.BOTTOM_END = 'bottom end';
    NbBadgeComponent.STATUS_PRIMARY = 'primary';
    NbBadgeComponent.STATUS_INFO = 'info';
    NbBadgeComponent.STATUS_SUCCESS = 'success';
    NbBadgeComponent.STATUS_WARNING = 'warning';
    NbBadgeComponent.STATUS_DANGER = 'danger';
    NbBadgeComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-badge',
                    styles: [":host .nb-badge{position:absolute;padding:0.25em 0.4em;font-size:75%;font-weight:700;line-height:1;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:0.25rem}:host .nb-badge.top{top:0}:host .nb-badge.right{right:0}:host .nb-badge.bottom{bottom:0}:host .nb-badge.left{left:0} "],
                    template: "\n    <span class=\"nb-badge {{positionClass}} nb-badge-{{colorClass}}\">{{text}}</span>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbBadgeComponent.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
    ]; };
    NbBadgeComponent.propDecorators = {
        "text": [{ type: i0.Input },],
        "position": [{ type: i0.Input },],
        "status": [{ type: i0.Input },],
    };
    return NbBadgeComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbBadgeModule = /** @class */ (function () {
    function NbBadgeModule() {
    }
    NbBadgeModule.decorators = [
        { type: i0.NgModule, args: [{
                    exports: [NbBadgeComponent],
                    declarations: [NbBadgeComponent],
                },] },
    ];
    return NbBadgeModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_TABSET_COMPONENTS = [
    NbTabsetComponent,
    NbTabComponent,
];
var NbTabsetModule = /** @class */ (function () {
    function NbTabsetModule() {
    }
    NbTabsetModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                        NbBadgeModule,
                    ],
                    declarations: NB_TABSET_COMPONENTS.slice(),
                    exports: NB_TABSET_COMPONENTS.slice(),
                },] },
    ];
    return NbTabsetModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Represents a component showing a user avatar (picture) with a user name on the right.
 * @stacked-example(Showcase, user/user-showcase.component)
 *
 * ```ts
 *   <nb-user name="Jonh Doe" title="Engineer"></nb-user>
 * ```
 *
 * ### Installation
 *
 * Import `NbUserModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbUserModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Available in multiple sizes:
 * @stacked-example(Multiple Sizes, user/user-sizes.component)
 *
 * @styles
 *
 * user-font-size:
 * user-line-height:
 * user-bg:
 * user-fg:
 * user-fg-highlight:
 * user-font-family-secondary:
 * user-size-small:
 * user-size-medium:
 * user-size-large:
 * user-size-xlarge:
 */
var NbUserComponent = /** @class */ (function () {
    function NbUserComponent(domSanitizer) {
        this.domSanitizer = domSanitizer;
        /**
           * Specifies a name to be shown on the right of a user picture
           * @type string
           */
        this.name = 'Anonymous';
        this.showNameValue = true;
        this.showTitleValue = true;
        this.showInitialsValue = true;
        this.isMenuShown = false;
    }
    Object.defineProperty(NbUserComponent.prototype, "small", {
        get: function () {
            return this.sizeValue === NbUserComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "medium", {
        get: function () {
            return this.sizeValue === NbUserComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "large", {
        get: function () {
            return this.sizeValue === NbUserComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "xlarge", {
        get: function () {
            return this.sizeValue === NbUserComponent.SIZE_XLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "picture", {
        set: /**
           * Absolute path to a user picture. Or base64 image
           * User name initials (JD for John Doe) will be shown if no picture specified
           * @type string
           */
        function (value) {
            this.imageBackgroundStyle = value ? this.domSanitizer.bypassSecurityTrustStyle("url(" + value + ")") : null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "size", {
        set: /**
           * Size of the component, small|medium|large
           * @type string
           */
        function (val) {
            this.sizeValue = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "showName", {
        set: /**
           * Whether to show a user name or not
           * @type boolean
           */
        function (val) {
            this.showNameValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "showTitle", {
        set: /**
           * Whether to show a user title or not
           * @type boolean
           */
        function (val) {
            this.showTitleValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "showInitials", {
        set: /**
           * Whether to show a user initials (if no picture specified) or not
           * @type boolean
           */
        function (val) {
            this.showInitialsValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "onlyPicture", {
        set: /**
           * Whether to show only a picture or also show the name and title
           * @type boolean
           */
        function (val) {
            this.showNameValue = this.showTitleValue = !convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbUserComponent.prototype, "inverse", {
        set: /**
           * Makes colors inverse based on current theme
           * @type boolean
           */
        function (val) {
            this.inverseValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbUserComponent.prototype.getInitials = function () {
        if (this.name) {
            var names = this.name.split(' ');
            return names.map(function (n) { return n.charAt(0); }).splice(0, 2).join('').toUpperCase();
        }
        return '';
    };
    // TODO: it makes sense use object instead of list of variables (or even enum)
    /*
        static readonly SIZE = {
         SMALL: 'small',
         MEDIUM: 'medium',
         LARGE: 'large',
        };
       */
    NbUserComponent.SIZE_SMALL = 'small';
    NbUserComponent.SIZE_MEDIUM = 'medium';
    NbUserComponent.SIZE_LARGE = 'large';
    NbUserComponent.SIZE_XLARGE = 'xlarge';
    NbUserComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-user',
                    styles: [":host{display:flex}:host .user-container{position:relative;display:flex;align-items:center}:host .user-picture{position:relative;border-radius:50%;flex-shrink:0}:host .user-picture.image{background-size:cover;background-repeat:no-repeat}:host .user-picture.background{display:flex;align-items:center;justify-content:center}:host .user-title{font-size:0.75rem}[dir=rtl] :host .user-name,[dir=rtl] :host .user-title{text-align:right}[dir=ltr] :host .info-container{margin-left:.5rem}[dir=rtl] :host .info-container{margin-right:.5rem} "],
                    template: "<div class=\"user-container\"> <div *ngIf=\"imageBackgroundStyle\" class=\"user-picture image\" [style.background-image]=\"imageBackgroundStyle\"> <nb-badge *ngIf=\"badgeText\" [text]=\"badgeText\" [status]=\"badgeStatus\" [position]=\"badgePosition\"></nb-badge> </div> <div *ngIf=\"!imageBackgroundStyle\" class=\"user-picture background\" [style.background-color]=\"color\"> <ng-container *ngIf=\"showInitialsValue\"> {{ getInitials() }} </ng-container> <nb-badge *ngIf=\"badgeText\" [text]=\"badgeText\" [status]=\"badgeStatus\" [position]=\"badgePosition\"></nb-badge> </div> <div class=\"info-container\"> <div *ngIf=\"showNameValue && name\" class=\"user-name\">{{ name }}</div> <div *ngIf=\"showTitleValue && title\" class=\"user-title\">{{ title }}</div> </div> </div> ",
                },] },
    ];
    /** @nocollapse */
    NbUserComponent.ctorParameters = function () { return [
        { type: _angular_platformBrowser.DomSanitizer, },
    ]; };
    NbUserComponent.propDecorators = {
        "inverseValue": [{ type: i0.HostBinding, args: ['class.inverse',] },],
        "small": [{ type: i0.HostBinding, args: ['class.small',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large',] },],
        "xlarge": [{ type: i0.HostBinding, args: ['class.xlarge',] },],
        "name": [{ type: i0.Input },],
        "title": [{ type: i0.Input },],
        "picture": [{ type: i0.Input },],
        "color": [{ type: i0.Input },],
        "size": [{ type: i0.Input },],
        "showName": [{ type: i0.Input },],
        "showTitle": [{ type: i0.Input },],
        "showInitials": [{ type: i0.Input },],
        "onlyPicture": [{ type: i0.Input },],
        "inverse": [{ type: i0.Input },],
        "badgeText": [{ type: i0.Input },],
        "badgeStatus": [{ type: i0.Input },],
        "badgePosition": [{ type: i0.Input },],
    };
    return NbUserComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_USER_COMPONENTS = [
    NbUserComponent,
];
var NbUserModule = /** @class */ (function () {
    function NbUserModule() {
    }
    NbUserModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                        NbBadgeModule,
                    ],
                    declarations: NB_USER_COMPONENTS.slice(),
                    exports: NB_USER_COMPONENTS.slice(),
                },] },
    ];
    return NbUserModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Action item, display a link with an icon, or any other content provided instead.
 */
var NbActionComponent = /** @class */ (function () {
    function NbActionComponent() {
        this.disabledValue = false;
    }
    Object.defineProperty(NbActionComponent.prototype, "disabled", {
        set: /**
           * Disables the item (changes item opacity and mouse cursor)
           * @type boolean
           */
        function (val) {
            this.disabledValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbActionComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-action',
                    template: "\n    <a class=\"icon-container\" href=\"#\" *ngIf=\"icon; else showContent\" (click)=\"$event.preventDefault()\">\n      <i class=\"control-icon {{ icon }}\"></i>\n    </a>\n    <ng-template #showContent>\n      <ng-content></ng-content>\n    </ng-template>\n    <nb-badge *ngIf=\"badgeText\" [text]=\"badgeText\" [status]=\"badgeStatus\" [position]=\"badgePosition\"></nb-badge>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbActionComponent.propDecorators = {
        "disabledValue": [{ type: i0.HostBinding, args: ['class.disabled',] },],
        "icon": [{ type: i0.Input },],
        "disabled": [{ type: i0.Input },],
        "badgeText": [{ type: i0.Input },],
        "badgeStatus": [{ type: i0.Input },],
        "badgePosition": [{ type: i0.Input },],
    };
    return NbActionComponent;
}());
/**
 * Shows a horizontal list of actions, available in multiple sizes.
 * Aligns items vertically.
 *
 * @stacked-example(Showcase, action/action-showcase.component)
 *
 * Basic actions setup:
 * ```html
 * <nb-actions size="small">
 *   <nb-action icon="nb-search"></nb-action>
 *   <nb-action icon="nb-power-circled"></nb-action>
 *   <nb-action icon="nb-person"></nb-action>
 * </nb-actions>
 * ```
 * ### Installation
 *
 * Import `NbActionsModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbActionsModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Multiple sizes example:
 * @stacked-example(Multiple Sizes, action/action-sizes.component)
 *
 * It is also possible to specify a `badge` value:
 *
 * @stacked-example(Action Badge, action/action-badge.component)
 *
 * and we can set it to full a width of a parent component
 * @stacked-example(Full Width, action/action-width.component)
 *
 * @styles
 *
 * actions-font-size:
 * actions-font-family:
 * actions-line-height:
 * actions-fg:
 * actions-bg:
 * actions-separator:
 * actions-padding:
 * actions-size-small:
 * actions-size-medium:
 * actions-size-large:
 */
var NbActionsComponent = /** @class */ (function () {
    function NbActionsComponent() {
        this.fullWidthValue = false;
    }
    Object.defineProperty(NbActionsComponent.prototype, "small", {
        get: function () {
            return this.sizeValue === NbActionsComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbActionsComponent.prototype, "medium", {
        get: function () {
            return this.sizeValue === NbActionsComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbActionsComponent.prototype, "large", {
        get: function () {
            return this.sizeValue === NbActionsComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbActionsComponent.prototype, "size", {
        set: /**
           * Size of the component, small|medium|large
           * @type string
           */
        function (val) {
            this.sizeValue = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbActionsComponent.prototype, "inverse", {
        set: /**
           * Makes colors inverse based on current theme
           * @type boolean
           */
        function (val) {
            this.inverseValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbActionsComponent.prototype, "fullWidth", {
        set: /**
           * Component will fill full width of the container
           * @type boolean
           */
        function (val) {
            this.fullWidthValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbActionsComponent.SIZE_SMALL = 'small';
    NbActionsComponent.SIZE_MEDIUM = 'medium';
    NbActionsComponent.SIZE_LARGE = 'large';
    NbActionsComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-actions',
                    styles: [":host{display:flex;align-items:center}:host /deep/ nb-action{display:flex;flex-wrap:wrap;align-items:center;position:relative}:host /deep/ nb-action i.control-icon:hover{cursor:pointer}:host /deep/ nb-action.disabled{cursor:not-allowed}:host /deep/ nb-action.disabled>*{opacity:0.5}:host /deep/ nb-action.disabled a,:host /deep/ nb-action.disabled i{cursor:not-allowed !important} "],
                    template: "\n    <ng-content select=\"nb-action\"></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbActionsComponent.propDecorators = {
        "inverseValue": [{ type: i0.HostBinding, args: ['class.inverse',] },],
        "small": [{ type: i0.HostBinding, args: ['class.small',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large',] },],
        "fullWidthValue": [{ type: i0.HostBinding, args: ['class.full-width',] },],
        "size": [{ type: i0.Input },],
        "inverse": [{ type: i0.Input },],
        "fullWidth": [{ type: i0.Input },],
    };
    return NbActionsComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_ACTIONS_COMPONENTS = [
    NbActionComponent,
    NbActionsComponent,
];
var NbActionsModule = /** @class */ (function () {
    function NbActionsModule() {
    }
    NbActionsModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                        NbBadgeModule,
                    ],
                    declarations: NB_ACTIONS_COMPONENTS.slice(),
                    exports: NB_ACTIONS_COMPONENTS.slice(),
                },] },
    ];
    return NbActionsModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Search component service, connects your code to a page-level search component.
 */
var NbSearchService = /** @class */ (function () {
    function NbSearchService() {
        this.searchSubmittings$ = new rxjs.Subject();
        this.searchActivations$ = new rxjs.Subject();
        this.searchDeactivations$ = new rxjs.Subject();
    }
    /***
     * Activate (open) search component
     * @param {string} searchType
     * @param {string} tag
     */
    /***
       * Activate (open) search component
       * @param {string} searchType
       * @param {string} tag
       */
    NbSearchService.prototype.activateSearch = /***
       * Activate (open) search component
       * @param {string} searchType
       * @param {string} tag
       */
    function (searchType, tag) {
        this.searchActivations$.next({ searchType: searchType, tag: tag });
    };
    /**
     * Deactibate (close) search component
     * @param {string} searchType
     * @param {string} tag
     */
    /**
       * Deactibate (close) search component
       * @param {string} searchType
       * @param {string} tag
       */
    NbSearchService.prototype.deactivateSearch = /**
       * Deactibate (close) search component
       * @param {string} searchType
       * @param {string} tag
       */
    function (searchType, tag) {
        this.searchDeactivations$.next({ searchType: searchType, tag: tag });
    };
    /**
     * Trigger search submit
     * @param {string} term
     * @param {string} tag
     */
    /**
       * Trigger search submit
       * @param {string} term
       * @param {string} tag
       */
    NbSearchService.prototype.submitSearch = /**
       * Trigger search submit
       * @param {string} term
       * @param {string} tag
       */
    function (term, tag) {
        this.searchSubmittings$.next({ term: term, tag: tag });
    };
    /**
     * Subscribe to 'activate' event
     * @returns Observable<{searchType: string; tag?: string}>
     */
    /**
       * Subscribe to 'activate' event
       * @returns Observable<{searchType: string; tag?: string}>
       */
    NbSearchService.prototype.onSearchActivate = /**
       * Subscribe to 'activate' event
       * @returns Observable<{searchType: string; tag?: string}>
       */
    function () {
        return this.searchActivations$.pipe(rxjs_operators.share());
    };
    /**
     * Subscribe to 'deactivate' event
     * @returns Observable<{searchType: string; tag?: string}>
     */
    /**
       * Subscribe to 'deactivate' event
       * @returns Observable<{searchType: string; tag?: string}>
       */
    NbSearchService.prototype.onSearchDeactivate = /**
       * Subscribe to 'deactivate' event
       * @returns Observable<{searchType: string; tag?: string}>
       */
    function () {
        return this.searchDeactivations$.pipe(rxjs_operators.share());
    };
    /**
     * Subscribe to 'submit' event (when submit button clicked)
     * @returns Observable<{term: string; tag?: string}>
     */
    /**
       * Subscribe to 'submit' event (when submit button clicked)
       * @returns Observable<{term: string; tag?: string}>
       */
    NbSearchService.prototype.onSearchSubmit = /**
       * Subscribe to 'submit' event (when submit button clicked)
       * @returns Observable<{term: string; tag?: string}>
       */
    function () {
        return this.searchSubmittings$.pipe(rxjs_operators.share());
    };
    NbSearchService.decorators = [
        { type: i0.Injectable },
    ];
    return NbSearchService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * search-field-component is used under the hood by nb-search component
 * can't be used itself
 */
var NbSearchFieldComponent = /** @class */ (function () {
    function NbSearchFieldComponent() {
        this.show = false;
        this.close = new i0.EventEmitter();
        this.search = new i0.EventEmitter();
    }
    Object.defineProperty(NbSearchFieldComponent.prototype, "showClass", {
        get: function () {
            return this.show;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSearchFieldComponent.prototype, "modalZoomin", {
        get: function () {
            return this.type === NbSearchFieldComponent.TYPE_MODAL_ZOOMIN;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSearchFieldComponent.prototype, "rotateLayout", {
        get: function () {
            return this.type === NbSearchFieldComponent.TYPE_ROTATE_LAYOUT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSearchFieldComponent.prototype, "modalMove", {
        get: function () {
            return this.type === NbSearchFieldComponent.TYPE_MODAL_MOVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSearchFieldComponent.prototype, "curtain", {
        get: function () {
            return this.type === NbSearchFieldComponent.TYPE_CURTAIN;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSearchFieldComponent.prototype, "columnCurtain", {
        get: function () {
            return this.type === NbSearchFieldComponent.TYPE_COLUMN_CURTAIN;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSearchFieldComponent.prototype, "modalDrop", {
        get: function () {
            return this.type === NbSearchFieldComponent.TYPE_MODAL_DROP;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSearchFieldComponent.prototype, "modalHalf", {
        get: function () {
            return this.type === NbSearchFieldComponent.TYPE_MODAL_HALF;
        },
        enumerable: true,
        configurable: true
    });
    NbSearchFieldComponent.prototype.ngOnChanges = function (_a) {
        var show = _a.show;
        var becameHidden = !show.isFirstChange() && show.currentValue === false;
        if (becameHidden && this.inputElement) {
            this.inputElement.nativeElement.value = '';
        }
        this.focusInput();
    };
    NbSearchFieldComponent.prototype.ngAfterViewInit = function () {
        this.focusInput();
    };
    NbSearchFieldComponent.prototype.emitClose = function () {
        this.close.emit();
    };
    NbSearchFieldComponent.prototype.submitSearch = function (term) {
        if (term) {
            this.search.emit(term);
        }
    };
    NbSearchFieldComponent.prototype.focusInput = function () {
        if (this.show && this.inputElement) {
            this.inputElement.nativeElement.focus();
        }
    };
    NbSearchFieldComponent.TYPE_MODAL_ZOOMIN = 'modal-zoomin';
    NbSearchFieldComponent.TYPE_ROTATE_LAYOUT = 'rotate-layout';
    NbSearchFieldComponent.TYPE_MODAL_MOVE = 'modal-move';
    NbSearchFieldComponent.TYPE_CURTAIN = 'curtain';
    NbSearchFieldComponent.TYPE_COLUMN_CURTAIN = 'column-curtain';
    NbSearchFieldComponent.TYPE_MODAL_DROP = 'modal-drop';
    NbSearchFieldComponent.TYPE_MODAL_HALF = 'modal-half';
    NbSearchFieldComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-search-field',
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    styles: [":host button{margin:0;padding:0;cursor:pointer;border:none;background:none}:host button:focus{box-shadow:none;outline:none}:host input{border-top:0;border-right:0;border-left:0;background:transparent;border-radius:0;line-height:1;display:inline-block;box-sizing:border-box;padding:0.05rem 0;-webkit-appearance:none}:host input:focus{outline:none}:host input::placeholder{opacity:0.3}:host span{font-size:90%;font-weight:bold;display:block;width:75%;margin:0 auto;padding:0.85rem 0;text-align:right}:host.modal-zoomin{display:block}:host.modal-zoomin .search{display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;position:fixed;z-index:1050;top:0;left:0;width:100%;height:100vh;pointer-events:none;opacity:0;transition:opacity 0.5s}:host.modal-zoomin .search::before,:host.modal-zoomin .search::after{content:'';position:absolute;width:calc(100% + 15px);height:calc(100% + 15px);pointer-events:none}:host.modal-zoomin .search::before{top:0;left:0;border-right-width:0;border-bottom-width:0;transform:translate3d(-15px, -15px, 0)}:host.modal-zoomin .search::after{right:0;bottom:0;border-top-width:0;border-left-width:0;transform:translate3d(15px, 15px, 0)}:host.modal-zoomin .search button{position:absolute;top:3rem;font-size:2.5rem}[dir=ltr] :host.modal-zoomin .search button{right:3rem}[dir=rtl] :host.modal-zoomin .search button{left:3rem}:host.modal-zoomin .search input{font-size:10vw;width:75%}:host.modal-zoomin .search button{opacity:0;transform:scale3d(0.8, 0.8, 1);transition:opacity 0.5s, transform 0.5s}:host.modal-zoomin .search form{opacity:0;transform:scale3d(0.8, 0.8, 1);transition:opacity 0.5s, transform 0.5s}:host.modal-zoomin.show .search{pointer-events:auto;opacity:1}:host.modal-zoomin.show .search::before,:host.modal-zoomin.show .search::after{transform:translate3d(0, 0, 0);transition:transform 0.5s}:host.modal-zoomin.show .search button{opacity:1;transform:scale3d(1, 1, 1)}:host.modal-zoomin.show .search form{opacity:1;transform:scale3d(1, 1, 1)}@media screen and (max-width: 40rem){:host.modal-zoomin form{margin:5rem 0 1rem}:host.modal-zoomin span{text-align:left}} ",
"/deep/ nb-layout.rotate-layout{position:fixed;overflow:hidden;width:100%}/deep/ nb-layout.rotate-layout .scrollable-container{position:relative;z-index:10001;transition:transform 0.5s cubic-bezier(0.2, 1, 0.3, 1)}/deep/ nb-layout.rotate-layout.with-search .scrollable-container{transition:transform 0.5s cubic-bezier(0.2, 1, 0.3, 1);transform-origin:50vw 50vh;transform:perspective(1000px) translate3d(0, 50vh, 0) rotate3d(1, 0, 0, 30deg);pointer-events:none}:host.rotate-layout{position:absolute;display:block;width:100vw;height:100vh;pointer-events:none;opacity:0;transition-property:opacity;transition-delay:0.4s}:host.rotate-layout .search{display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;z-index:1050;position:fixed;top:0;left:0;width:100%;height:50vh;pointer-events:none;opacity:0;transition:opacity 0.5s;transition-timing-function:cubic-bezier(0.2, 1, 0.3, 1)}:host.rotate-layout .search button{position:absolute;top:3rem;font-size:2.5rem;opacity:0;transform:scale3d(0.8, 0.8, 1);transition:opacity 0.5s, transform 0.5s;transition-timing-function:cubic-bezier(0.2, 1, 0.3, 1)}[dir=ltr] :host.rotate-layout .search button{right:3rem}[dir=rtl] :host.rotate-layout .search button{left:3rem}:host.rotate-layout .search form{margin:5rem 0;opacity:0;transform:scale3d(0.7, 0.7, 1);transition:opacity 0.5s, transform 0.5s;transition-timing-function:cubic-bezier(0.2, 1, 0.3, 1)}:host.rotate-layout .search input{font-size:7vw;width:75%}:host.rotate-layout.show{opacity:1;transition-delay:0s}:host.rotate-layout.show .search{pointer-events:auto;opacity:1}:host.rotate-layout.show .search button{opacity:1;transform:scale3d(1, 1, 1)}:host.rotate-layout.show .search form{opacity:1;transform:scale3d(1, 1, 1)} ",
"/deep/ nb-layout.modal-move .layout{transition:transform 0.5s}/deep/ nb-layout.modal-move.with-search .layout{transform:scale3d(0.8, 0.8, 1);pointer-events:none}:host.modal-move .search{display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;position:fixed;z-index:1050;top:0;left:0;width:100%;height:100vh;pointer-events:none;opacity:0;transition:opacity 0.5s}:host.modal-move .search button{position:absolute;top:3rem;font-size:2.5rem;opacity:0;transition:opacity 0.5s}[dir=ltr] :host.modal-move .search button{right:3rem}[dir=rtl] :host.modal-move .search button{left:3rem}:host.modal-move .search form{margin:5rem 0;opacity:0;transform:scale3d(0.8, 0.8, 1);transition:opacity 0.5s, transform 0.5s}:host.modal-move .search input{font-size:10vw;width:75%;transform:scale3d(0, 1, 1);transform-origin:0 50%;transition:transform 0.3s}:host.modal-move.show .search{pointer-events:auto;opacity:1}:host.modal-move.show .search button{opacity:1}:host.modal-move.show .search form{opacity:1;transform:scale3d(1, 1, 1)}:host.modal-move.show .search input{transform:scale3d(1, 1, 1);transition-duration:0.5s}@media screen and (max-width: 40rem){:host.modal-move span{text-align:left}} ",
":host.curtain .search{position:fixed;z-index:1050;top:0;left:100%;overflow:hidden;height:100vh;width:100%;padding:3rem;pointer-events:none;transition:transform 0.3s;transition-delay:0.4s;transition-timing-function:ease-out}:host.curtain .search::after{content:'';position:absolute;top:0;left:0;width:100%;height:100%;transition:transform 0.3s;transition-timing-function:ease-out}:host.curtain .search button{font-size:2.5rem;position:absolute;top:3rem;transition:opacity 0.1s;transition-delay:0.3s}[dir=ltr] :host.curtain .search button{right:3rem}[dir=rtl] :host.curtain .search button{left:3rem}:host.curtain .search form{width:50%;opacity:0;transform:scale3d(0.8, 0.8, 1);transition:opacity 0.5s, transform 0.5s}:host.curtain .search input{width:100%;font-size:6vw}:host.curtain.show .search{width:100%;pointer-events:auto;transform:translate3d(-100%, 0, 0);transition-delay:0s}:host.curtain.show .search::after{transform:translate3d(100%, 0, 0);transition-delay:0.4s}:host.curtain.show .search button{opacity:1;transform:scale3d(1, 1, 1)}:host.curtain.show .search form{opacity:1;transform:scale3d(1, 1, 1)}@media screen and (max-width: 40em){:host.curtain span{width:90%}:host.curtain input{font-size:2em;width:90%}}/deep/ nb-layout.curtain .scrollable-container{position:relative;z-index:0} ",
"/deep/ nb-layout.column-curtain.with-search .layout{pointer-events:none}:host.column-curtain{display:block;position:fixed;z-index:1050;top:0;left:50%;overflow:hidden;width:50%;height:100vh;pointer-events:none}:host.column-curtain::before{content:'';position:absolute;top:0;left:0;width:100%;height:100%;transform:scale3d(0, 1, 1);transform-origin:0 50%;transition:transform 0.3s;transition-timing-function:cubic-bezier(0.86, 0, 0.07, 1)}:host.column-curtain .search{position:relative;padding:2.5rem 1.5rem 0;background:transparent}:host.column-curtain .search button{position:absolute;top:2rem;font-size:2.5rem;opacity:0;transition:opacity 0.5s}[dir=ltr] :host.column-curtain .search button{right:2rem}[dir=rtl] :host.column-curtain .search button{left:2rem}:host.column-curtain .search form{width:85%;transform:translate3d(-150%, 0, 0);transition:transform 0.3s}:host.column-curtain .search input{font-size:2.5rem;width:100%}:host.column-curtain .search span{font-size:85%}:host.column-curtain.show{pointer-events:auto}:host.column-curtain.show::before{transform:scale3d(1, 1, 1)}:host.column-curtain.show .search form{transform:translate3d(0, 0, 0);transition-delay:0.15s;transition-timing-function:cubic-bezier(0.86, 0, 0.07, 1)}:host.column-curtain.show .search button{opacity:1;z-index:100}@media screen and (max-width: 40rem){:host.column-curtain span{width:90%}:host.column-curtain input{font-size:2rem;width:90%}} ",
"/deep/ nb-layout.modal-drop .layout{position:relative;transition:transform 0.4s, opacity 0.4s;transition-timing-function:cubic-bezier(0.4, 0, 0.2, 1)}/deep/ nb-layout.modal-drop.with-search .layout{opacity:0;transform:scale3d(0.9, 0.9, 1);pointer-events:none}:host.modal-drop .search{display:flex;flex-direction:column;justify-content:center;align-items:center;text-align:center;z-index:1050;position:fixed;top:0;left:0;width:100%;height:100vh;background:none;pointer-events:none}:host.modal-drop .search::before{content:'';position:absolute;top:0;right:0;width:100%;height:100%;opacity:0;transition:opacity 0.4s}:host.modal-drop .search button{font-size:2.5rem;position:absolute;top:3rem;display:block;opacity:0;transition:opacity 0.4s}[dir=ltr] :host.modal-drop .search button{right:3rem}[dir=rtl] :host.modal-drop .search button{left:3rem}:host.modal-drop .search form{position:relative;margin:5rem 0 2rem}:host.modal-drop .search input{font-size:6vw;width:60%;padding:0.25rem;text-align:center;opacity:0;transition:opacity 0.4s}:host.modal-drop .search span{position:relative;z-index:9;display:block;width:60%;padding:0.85rem 0;opacity:0;transform:translate3d(0, -50px, 0);transition:opacity 0.4s, transform 0.4s}:host.modal-drop .search .form-content{position:relative;z-index:10;overflow:hidden;transform:translate3d(0, -50px, 0);transition:transform 0.4s}:host.modal-drop .search .form-content::after{content:'';position:absolute;top:0;left:20%;width:60%;height:105%;opacity:0;transform-origin:50% 0}:host.modal-drop.show .search{pointer-events:auto}:host.modal-drop.show .search::before{opacity:1}:host.modal-drop.show .search button{opacity:1}:host.modal-drop.show .search .form-content{transform:translate3d(0, 0, 0);transition:none}:host.modal-drop.show .search .form-content::after{animation:scaleUpDown 0.8s cubic-bezier(0.4, 0, 0.2, 1) forwards}:host.modal-drop.show .search input{opacity:1;transition:opacity 0s 0.4s}:host.modal-drop.show .search span{opacity:1;transform:translate3d(0, 0, 0);transition-delay:0.4s;transition-timing-function:ease-out}@keyframes scaleUpDown{0%{opacity:1;transform:scale3d(1, 0, 1)}50%{transform:scale3d(1, 1, 1);transform-origin:50% 0;transition-timing-function:ease-out}50.1%{transform-origin:50% 100%;transition-timing-function:ease-out}100%{opacity:1;transform:scale3d(1, 0, 1);transform-origin:50% 100%;transition-timing-function:ease-out}}@media screen and (max-width: 40rem){:host.modal-drop form{margin:2rem 0}:host.modal-drop input{width:100%;left:0}} ",
"/deep/ nb-layout.modal-half .layout{transition:transform 0.6s, opacity 0.6s;transition-timing-function:cubic-bezier(0.2, 1, 0.3, 1)}/deep/ nb-layout.modal-half.with-search .layout{transform:scale3d(0.8, 0.8, 1);pointer-events:none}:host.modal-half .search{text-align:center;position:fixed;z-index:1050;top:0;left:0;overflow:hidden;width:100%;height:100vh;background:none;pointer-events:none}:host.modal-half .search::before{content:'';position:absolute;top:0;left:0;width:100%;height:100%;pointer-events:none;opacity:0;transition:opacity 0.6s;transition-timing-function:cubic-bezier(0.2, 1, 0.3, 1)}:host.modal-half .search button{font-size:2.5rem;position:absolute;top:3rem;display:block;z-index:100;opacity:0;transform:scale3d(0.8, 0.8, 1);transition:opacity 0.6s, transform 0.6s;transition-timing-function:cubic-bezier(0.2, 1, 0.3, 1)}[dir=ltr] :host.modal-half .search button{right:3rem}[dir=rtl] :host.modal-half .search button{left:3rem}:host.modal-half .search .form-wrapper{position:absolute;display:flex;justify-content:center;align-items:center;width:100%;height:50%;transition:transform 0.6s;transition-timing-function:cubic-bezier(0.2, 1, 0.3, 1);transform:translate3d(0, -100%, 0)}:host.modal-half .search form{width:75%;margin:0 auto}:host.modal-half .search input{font-size:7vw;width:100%}:host.modal-half.show .search{pointer-events:auto}:host.modal-half.show .search::before{opacity:1}:host.modal-half.show .search button{opacity:1;transform:scale3d(1, 1, 1)}:host.modal-half.show .search .form-wrapper{transform:translate3d(0, 0, 0)} "],
                    template: "\n    <div class=\"search\" (keyup.esc)=\"emitClose()\">\n      <button (click)=\"emitClose()\">\n        <i class=\"nb-close-circled\"></i>\n      </button>\n      <div class=\"form-wrapper\">\n        <form class=\"form\" (keyup.enter)=\"submitSearch(searchInput.value)\">\n          <div class=\"form-content\">\n            <input class=\"search-input\"\n                   #searchInput\n                   autocomplete=\"off\"\n                   [attr.placeholder]=\"placeholder\"\n                   tabindex=\"-1\"\n                   (blur)=\"focusInput()\"/>\n          </div>\n          <span class=\"info\">{{ hint }}</span>\n        </form>\n      </div>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbSearchFieldComponent.propDecorators = {
        "type": [{ type: i0.Input },],
        "placeholder": [{ type: i0.Input },],
        "hint": [{ type: i0.Input },],
        "show": [{ type: i0.Input },],
        "close": [{ type: i0.Output },],
        "search": [{ type: i0.Output },],
        "inputElement": [{ type: i0.ViewChild, args: ['searchInput',] },],
        "showClass": [{ type: i0.HostBinding, args: ['class.show',] },],
        "modalZoomin": [{ type: i0.HostBinding, args: ['class.modal-zoomin',] },],
        "rotateLayout": [{ type: i0.HostBinding, args: ['class.rotate-layout',] },],
        "modalMove": [{ type: i0.HostBinding, args: ['class.modal-move',] },],
        "curtain": [{ type: i0.HostBinding, args: ['class.curtain',] },],
        "columnCurtain": [{ type: i0.HostBinding, args: ['class.column-curtain',] },],
        "modalDrop": [{ type: i0.HostBinding, args: ['class.modal-drop',] },],
        "modalHalf": [{ type: i0.HostBinding, args: ['class.modal-half',] },],
    };
    return NbSearchFieldComponent;
}());
/**
 * Beautiful full-page search control.
 *
 * @stacked-example(Showcase, search/search-showcase.component)
 *
 * Basic setup:
 *
 * ```ts
 *  <nb-search type="rotate-layout"></nb-search>
 * ```
 * ### Installation
 *
 * Import `NbSearchModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbSearchModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Several animation types are available:
 * modal-zoomin, rotate-layout, modal-move, curtain, column-curtain, modal-drop, modal-half
 *
 * It is also possible to handle search event using `NbSearchService`:
 *
 * @stacked-example(Search Event, search/search-event.component)
 *
 * @styles
 *
 * search-btn-open-fg:
 * search-btn-close-fg:
 * search-bg:
 * search-bg-secondary:
 * search-text:
 * search-info:
 * search-dash:
 * search-placeholder:
 */
var NbSearchComponent = /** @class */ (function () {
    function NbSearchComponent(searchService, themeService, router, overlayService, changeDetector) {
        this.searchService = searchService;
        this.themeService = themeService;
        this.router = router;
        this.overlayService = overlayService;
        this.changeDetector = changeDetector;
        this.alive = true;
        this.showSearchField = false;
        /**
           * Search input placeholder
           * @type {string}
           */
        this.placeholder = 'Search...';
        /**
           * Hint showing under the input field to improve user experience
           *
           * @type {string}
           */
        this.hint = 'Hit enter to search';
    }
    NbSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (event) { return event instanceof _angular_router.NavigationEnd; }))
            .subscribe(function () { return _this.hideSearch(); });
        this.searchService.onSearchActivate()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (data) { return !_this.tag || data.tag === _this.tag; }))
            .subscribe(function () { return _this.openSearch(); });
        this.searchService.onSearchDeactivate()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (data) { return !_this.tag || data.tag === _this.tag; }))
            .subscribe(function () { return _this.hideSearch(); });
    };
    NbSearchComponent.prototype.ngOnDestroy = function () {
        if (this.overlayRef && this.overlayRef.hasAttached()) {
            this.removeLayoutClasses();
            this.overlayRef.detach();
        }
        this.alive = false;
    };
    NbSearchComponent.prototype.openSearch = function () {
        var _this = this;
        if (!this.overlayRef) {
            this.overlayRef = this.overlayService.create();
            this.overlayRef.attach(this.searchFieldPortal);
        }
        this.themeService.appendLayoutClass(this.type);
        rxjs.of(null).pipe(rxjs_operators.delay(0)).subscribe(function () {
            _this.themeService.appendLayoutClass('with-search');
            _this.showSearchField = true;
            _this.changeDetector.detectChanges();
        });
    };
    NbSearchComponent.prototype.hideSearch = function () {
        this.removeLayoutClasses();
        this.showSearchField = false;
        this.changeDetector.detectChanges();
        this.searchButton.nativeElement.focus();
    };
    NbSearchComponent.prototype.search = function (term) {
        this.searchService.submitSearch(term, this.tag);
        this.hideSearch();
    };
    NbSearchComponent.prototype.removeLayoutClasses = function () {
        var _this = this;
        this.themeService.removeLayoutClass('with-search');
        rxjs.of(null).pipe(rxjs_operators.delay(500)).subscribe(function () {
            _this.themeService.removeLayoutClass(_this.type);
        });
    };
    NbSearchComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-search',
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    styles: [":host button{font-size:2rem;margin:0 auto;padding:0;cursor:pointer;border:none;background:none}:host button:focus{box-shadow:none;outline:none}/deep/ nb-layout.with-search .scrollable-container{position:relative;z-index:0} "],
                    template: "\n    <button #searchButton class=\"start-search\" (click)=\"openSearch()\">\n      <i class=\"nb-search\"></i>\n    </button>\n    <nb-search-field\n      *nbPortal\n      [show]=\"showSearchField\"\n      [type]=\"type\"\n      [placeholder]=\"placeholder\"\n      [hint]=\"hint\"\n      (search)=\"search($event)\"\n      (close)=\"hideSearch()\">\n    </nb-search-field>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbSearchComponent.ctorParameters = function () { return [
        { type: NbSearchService, },
        { type: NbThemeService, },
        { type: _angular_router.Router, },
        { type: NbOverlayService, },
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbSearchComponent.propDecorators = {
        "tag": [{ type: i0.Input },],
        "placeholder": [{ type: i0.Input },],
        "hint": [{ type: i0.Input },],
        "type": [{ type: i0.Input },],
        "searchFieldPortal": [{ type: i0.ViewChild, args: [NbPortalDirective,] },],
        "searchButton": [{ type: i0.ViewChild, args: ['searchButton',] },],
    };
    return NbSearchComponent;
}());

var NbSearchModule = /** @class */ (function () {
    function NbSearchModule() {
    }
    NbSearchModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                        NbOverlayModule,
                    ],
                    declarations: [
                        NbSearchComponent,
                        NbSearchFieldComponent,
                    ],
                    exports: [
                        NbSearchComponent,
                        NbSearchFieldComponent,
                    ],
                    providers: [
                        NbSearchService,
                    ],
                    entryComponents: [
                        NbSearchFieldComponent,
                    ],
                },] },
    ];
    return NbSearchModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Styled checkbox component
 *
 * @stacked-example(Showcase, checkbox/checkbox-showcase.component)
 *
 * ### Installation
 *
 * Import `NbCheckboxComponent` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbCheckboxComponent,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Can have one of the following statuses: danger, success or warning
 *
 * @stacked-example(Colored Checkboxes, checkbox/checkbox-status.component)
 *
 * @additional-example(Disabled Checkbox, checkbox/checkbox-disabled.component)
 *
 * @styles
 *
 * checkbox-bg:
 * checkbox-size:
 * checkbox-border-size:
 * checkbox-border-color:
 * checkbox-checkmark:
 * checkbox-checked-bg:
 * checkbox-checked-size:
 * checkbox-checked-border-size:
 * checkbox-checked-border-color:
 * checkbox-checked-checkmark:
 * checkbox-disabled-bg:
 * checkbox-disabled-size:
 * checkbox-disabled-border-size:
 * checkbox-disabled-border-color:
 * checkbox-disabled-checkmark:
 */
var NbCheckboxComponent = /** @class */ (function () {
    function NbCheckboxComponent() {
        /**
           * Checkbox value
           * @type {boolean}
           * @private
           */
        this._value = false;
        this.disabled = false;
        this.onChange = function () { };
        this.onTouched = function () { };
    }
    Object.defineProperty(NbCheckboxComponent.prototype, "setDisabled", {
        set: function (val) {
            this.disabled = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCheckboxComponent.prototype, "setStatus", {
        set: /**
           * Checkbox status (success, warning, danger)
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCheckboxComponent.prototype, "success", {
        get: function () {
            return this.status === 'success';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCheckboxComponent.prototype, "warning", {
        get: function () {
            return this.status === 'warning';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCheckboxComponent.prototype, "danger", {
        get: function () {
            return this.status === 'danger';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCheckboxComponent.prototype, "value", {
        get: function () {
            return this._value;
        },
        set: function (val) {
            this._value = val;
            this.onChange(val);
            this.onTouched();
        },
        enumerable: true,
        configurable: true
    });
    NbCheckboxComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    NbCheckboxComponent.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    NbCheckboxComponent.prototype.writeValue = function (val) {
        this.value = val;
    };
    NbCheckboxComponent.prototype.setDisabledState = function (val) {
        this.disabled = convertToBoolProperty(val);
    };
    NbCheckboxComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-checkbox',
                    template: "\n    <label class=\"customised-control\">\n      <input type=\"checkbox\" class=\"customised-control-input\"\n             [disabled]=\"disabled\"\n             [checked]=\"value\"\n             (change)=\"value = !value\">\n      <span class=\"customised-control-indicator\"></span>\n      <span class=\"customised-control-description\">\n        <ng-content></ng-content>\n      </span>\n    </label>\n  ",
                    styles: [":host .customised-control{position:relative;display:inline-flex;margin:0;min-height:inherit;padding:0.375rem 1.5rem 0.375rem 0}:host .customised-control-input{position:absolute;opacity:0}:host .customised-control-input:disabled ~ .customised-control-indicator,:host .customised-control-input:disabled ~ .customised-control-description{opacity:0.5}:host .customised-control-indicator{border-radius:0.25rem;flex-shrink:0}:host .customised-control-indicator::before{content:'';border-style:solid;display:block;margin:0 auto;transform:rotate(45deg)}[dir=ltr] :host .customised-control-description{padding-left:.5rem}[dir=rtl] :host .customised-control-description{padding-right:.5rem} "],
                    providers: [{
                            provide: _angular_forms.NG_VALUE_ACCESSOR,
                            useExisting: i0.forwardRef(function () { return NbCheckboxComponent; }),
                            multi: true,
                        }],
                },] },
    ];
    /** @nocollapse */
    NbCheckboxComponent.propDecorators = {
        "_value": [{ type: i0.Input, args: ['value',] },],
        "setDisabled": [{ type: i0.Input, args: ['disabled',] },],
        "setStatus": [{ type: i0.Input, args: ['status',] },],
        "success": [{ type: i0.HostBinding, args: ['class.success',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.warning',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.danger',] },],
    };
    return NbCheckboxComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbCheckboxModule = /** @class */ (function () {
    function NbCheckboxModule() {
    }
    NbCheckboxModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: [NbCheckboxComponent],
                    exports: [NbCheckboxComponent],
                },] },
    ];
    return NbCheckboxModule;
}());

var __extends$9 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Overlay container.
 * Renders provided content inside.
 *
 * @styles
 *
 * popover-fg
 * popover-bg
 * popover-border
 * popover-shadow
 * */
var NbPopoverComponent = /** @class */ (function (_super) {
    __extends$9(NbPopoverComponent, _super);
    function NbPopoverComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbPopoverComponent.prototype.ngAfterViewInit = function () {
        if (this.content instanceof i0.TemplateRef) {
            this.attachTemplate();
        }
        else if (this.content instanceof i0.Type) {
            this.attachComponent();
        }
        else {
            this.attachString();
        }
    };
    NbPopoverComponent.prototype.attachTemplate = function () {
        this.overlayContainer.attachTemplatePortal(new NbTemplatePortal(this.content, null, this.context));
    };
    NbPopoverComponent.prototype.attachComponent = function () {
        var portal = new NbComponentPortal(this.content, null, null, this.cfr);
        var ref = this.overlayContainer.attachComponentPortal(portal);
        Object.assign(ref.instance, this.context);
        ref.changeDetectorRef.detectChanges();
    };
    NbPopoverComponent.prototype.attachString = function () {
        this.overlayContainer.attachStringContent(this.content);
    };
    NbPopoverComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-popover',
                    styles: [":host .arrow{position:absolute;width:0;height:0}:host /deep/ nb-overlay-container .primitive-overlay{padding:0.75rem 1rem} "],
                    template: "\n    <span class=\"arrow\"></span>\n    <nb-overlay-container></nb-overlay-container>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbPopoverComponent.propDecorators = {
        "overlayContainer": [{ type: i0.ViewChild, args: [NbOverlayContainerComponent,] },],
        "content": [{ type: i0.Input },],
        "context": [{ type: i0.Input },],
        "cfr": [{ type: i0.Input },],
    };
    return NbPopoverComponent;
}(NbPositionedContainer));

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Powerful popover directive, which provides the best UX for your users.
 *
 * @stacked-example(Showcase, popover/popover-showcase.component)
 *
 * Popover can accept different content such as:
 * TemplateRef
 *
 * ```html
 * <button [nbPopover]="templateRef"></button>
 * <ng-template #templateRef>
 *   <span>Hello, Popover!</span>
 * </ng-template>
 * ```
 * ### Installation
 *
 * Import `NbPopoverModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbPopoverModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Custom components
 *
 * ```html
 * <button [nbPopover]="MyPopoverComponent"></button>
 * ```
 *
 * Both custom components and templateRef popovers can receive *contentContext* property
 * that will be passed to the content props.
 *
 * Primitive types
 *
 * ```html
 * <button nbPopover="Hello, Popover!"></button>
 * ```
 *
 * Popover has different placements, such as: top, bottom, left, right, start and end
 * which can be used as following:
 *
 * @stacked-example(Placements, popover/popover-placements.component)
 *
 * By default popover will try to adjust itself to maximally fit viewport
 * and provide the best user experience. It will try to change position of the popover container.
 * If you wanna disable this behaviour just set it falsy value.
 *
 * ```html
 * <button nbPopover="Hello, Popover!" [nbPopoverAdjust]="false"></button>
 * ```
 *
 * Also popover has some different modes which provides capability show$ and hide$ popover in different ways:
 *
 * - Click mode popover shows when a user clicking on the host element and hides when the user clicks
 * somewhere on the document except popover.
 * - Hint mode provides capability show$ popover when the user hovers on the host element
 * and hide$ popover when user hovers out of the host.
 * - Hover mode works like hint mode with one exception - when the user moves mouse from host element to
 * the container element popover will not be hidden.
 *
 * @stacked-example(Available Modes, popover/popover-modes.component.html)
 *
 * @additional-example(Template Ref, popover/popover-template-ref.component)
 * @additional-example(Custom Component, popover/popover-custom-component.component)
 * */
var NbPopoverDirective = /** @class */ (function () {
    function NbPopoverDirective(document, hostRef, positionBuilder, overlay, componentFactoryResolver) {
        this.document = document;
        this.hostRef = hostRef;
        this.positionBuilder = positionBuilder;
        this.overlay = overlay;
        this.componentFactoryResolver = componentFactoryResolver;
        /**
           * Container content context. Will be applied to the rendered component.
           * */
        this.context = {};
        /**
           * Position will be calculated relatively host element based on the position.
           * Can be top, right, bottom, left, start or end.
           * */
        this.position = exports.NbPosition.TOP;
        /**
           * Container position will be changes automatically based on this strategy if container can't fit view port.
           * Set this property to any falsy value if you want to disable automatically adjustment.
           * Available values: clockwise, counterclockwise.
           * */
        this.adjustment = exports.NbAdjustment.CLOCKWISE;
        /**
           * Describes when the container will be shown.
           * Available options: click, hover and hint
           * */
        this.mode = exports.NbTrigger.CLICK;
        this.alive = true;
    }
    NbPopoverDirective.prototype.ngAfterViewInit = function () {
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({
            positionStrategy: this.positionStrategy,
            scrollStrategy: this.overlay.scrollStrategies.reposition(),
        });
        this.triggerStrategy = this.createTriggerStrategy();
        this.subscribeOnTriggers();
        this.subscribeOnPositionChange();
    };
    NbPopoverDirective.prototype.ngOnDestroy = function () {
        this.alive = false;
        this.hide();
    };
    NbPopoverDirective.prototype.show = function () {
        this.container = createContainer(this.ref, NbPopoverComponent, {
            position: this.position,
            content: this.content,
            context: this.context,
            cfr: this.componentFactoryResolver,
        }, this.componentFactoryResolver);
    };
    NbPopoverDirective.prototype.hide = function () {
        this.ref.detach();
        this.container = null;
    };
    NbPopoverDirective.prototype.toggle = function () {
        if (this.ref && this.ref.hasAttached()) {
            this.hide();
        }
        else {
            this.show();
        }
    };
    NbPopoverDirective.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(this.position)
            .adjustment(this.adjustment);
    };
    NbPopoverDirective.prototype.createTriggerStrategy = function () {
        var _this = this;
        return new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(this.mode)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.container; })
            .build();
    };
    NbPopoverDirective.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return patch(_this.container, { position: position }); });
    };
    NbPopoverDirective.prototype.subscribeOnTriggers = function () {
        var _this = this;
        this.triggerStrategy.show$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.show(); });
        this.triggerStrategy.hide$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.hide(); });
    };
    NbPopoverDirective.decorators = [
        { type: i0.Directive, args: [{ selector: '[nbPopover]' },] },
    ];
    /** @nocollapse */
    NbPopoverDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: i0.ElementRef, },
        { type: NbPositionBuilderService, },
        { type: NbOverlayService, },
        { type: i0.ComponentFactoryResolver, },
    ]; };
    NbPopoverDirective.propDecorators = {
        "content": [{ type: i0.Input, args: ['nbPopover',] },],
        "context": [{ type: i0.Input, args: ['nbPopoverContext',] },],
        "position": [{ type: i0.Input, args: ['nbPopoverPlacement',] },],
        "adjustment": [{ type: i0.Input, args: ['nbPopoverAdjustment',] },],
        "mode": [{ type: i0.Input, args: ['nbPopoverMode',] },],
    };
    return NbPopoverDirective;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbPopoverModule = /** @class */ (function () {
    function NbPopoverModule() {
    }
    NbPopoverModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbOverlayModule],
                    declarations: [NbPopoverDirective, NbPopoverComponent],
                    exports: [NbPopoverDirective],
                    entryComponents: [NbPopoverComponent],
                },] },
    ];
    return NbPopoverModule;
}());

var __extends$10 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Context menu component used as content within NbContextMenuDirective.
 *
 * @styles
 *
 * context-menu-fg
 * context-menu-active-fg
 * context-menu-active-bg
 * */
var NbContextMenuComponent = /** @class */ (function (_super) {
    __extends$10(NbContextMenuComponent, _super);
    function NbContextMenuComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.items = [];
        return _this;
    }
    NbContextMenuComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-context-menu',
                    styles: [":host .arrow{position:absolute;width:0;height:0}:host /deep/ nb-menu{display:inline;font-size:0.875rem;line-height:1.5rem}:host /deep/ nb-menu ul.menu-items{margin:0;padding:0.5rem 0}:host /deep/ nb-menu ul.menu-items .menu-item{border:none;white-space:nowrap}:host /deep/ nb-menu ul.menu-items .menu-item:first-child{border:none}:host /deep/ nb-menu ul.menu-items .menu-item a{cursor:pointer;border-radius:0;padding:0}:host /deep/ nb-menu ul.menu-items .menu-item a .menu-icon{font-size:1.5rem;width:auto}:host /deep/ nb-menu ul.menu-items .menu-item a .menu-title{padding:0.375rem 3rem}[dir=rtl] :host /deep/ nb-menu ul.menu-items .menu-item a .menu-title{text-align:right}[dir=ltr] :host /deep/ nb-menu ul.menu-items .menu-item a .menu-icon ~ .menu-title{padding-left:0}[dir=rtl] :host /deep/ nb-menu ul.menu-items .menu-item a .menu-icon ~ .menu-title{padding-right:0}[dir=ltr] :host /deep/ nb-menu ul.menu-items .menu-item a .menu-icon:first-child{padding-left:1rem}[dir=rtl] :host /deep/ nb-menu ul.menu-items .menu-item a .menu-icon:first-child{padding-right:1rem} "],
                    template: "\n    <span class=\"arrow\"></span>\n    <nb-menu class=\"context-menu\" [items]=\"items\" [tag]=\"tag\"></nb-menu>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbContextMenuComponent.propDecorators = {
        "items": [{ type: i0.Input },],
        "tag": [{ type: i0.Input },],
    };
    return NbContextMenuComponent;
}(NbPositionedContainer));

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Full featured context menu directive.
 *
 * @stacked-example(Showcase, context-menu/context-menu-showcase.component)
 *
 * Just pass menu items array:
 *
 * ```html
 * <button [nbContextMenu]="items"></button>
 * ...
 * items = [{ title: 'Profile' }, { title: 'Log out' }];
 * ```
 * ### Installation
 *
 * Import `NbContextMenuModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbContextMenuModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * If you want to handle context menu clicks you have to pass `nbContextMenuTag`
 * param and register to events using NbMenuService.
 * `NbContextMenu` renders plain `NbMenu` inside, so
 * you have to work with it just like with `NbMenu` component:
 *
 * @stacked-example(Menu item click, context-menu/context-menu-click.component)
 *
 * Context menu has different placements, such as: top, bottom, left and right
 * which can be used as following:
 *
 * ```html
 * <button [nbContextMenu]="items" nbContextMenuPlacement="right"></button>
 * ```
 *
 * ```ts
 * items = [{ title: 'Profile' }, { title: 'Log out' }];
 * ```
 *
 * By default context menu will try to adjust itself to maximally fit viewport
 * and provide the best user experience. It will try to change position of the context menu.
 * If you wanna disable this behaviour just set it falsy value.
 *
 * ```html
 * <button [nbContextMenu]="items" nbContextMenuAdjustment="counterclockwise"></button>
 * ```
 *
 * ```ts
 * items = [{ title: 'Profile' }, { title: 'Log out' }];
 * ```
 * */
var NbContextMenuDirective = /** @class */ (function () {
    function NbContextMenuDirective(document, menuService, hostRef, positionBuilder, overlay, componentFactoryResolver) {
        this.document = document;
        this.menuService = menuService;
        this.hostRef = hostRef;
        this.positionBuilder = positionBuilder;
        this.overlay = overlay;
        this.componentFactoryResolver = componentFactoryResolver;
        /**
           * Position will be calculated relatively host element based on the position.
           * Can be top, right, bottom and left.
           * */
        this.position = exports.NbPosition.BOTTOM;
        /**
           * Container position will be changes automatically based on this strategy if container can't fit view port.
           * Set this property to any falsy value if you want to disable automatically adjustment.
           * Available values: clockwise, counterclockwise.
           * */
        this.adjustment = exports.NbAdjustment.CLOCKWISE;
        this.alive = true;
        this.items = [];
    }
    Object.defineProperty(NbContextMenuDirective.prototype, "setItems", {
        set: /**
           * Basic menu items, will be passed to the internal NbMenuComponent.
           * */
        function (items) {
            this.validateItems(items);
            this.items = items;
        },
        enumerable: true,
        configurable: true
    });
    
    NbContextMenuDirective.prototype.ngAfterViewInit = function () {
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({
            positionStrategy: this.positionStrategy,
            scrollStrategy: this.overlay.scrollStrategies.reposition(),
        });
        this.triggerStrategy = this.createTriggerStrategy();
        this.subscribeOnTriggers();
        this.subscribeOnPositionChange();
        this.subscribeOnItemClick();
    };
    NbContextMenuDirective.prototype.ngOnDestroy = function () {
        this.alive = false;
        this.hide();
    };
    NbContextMenuDirective.prototype.show = function () {
        this.container = createContainer(this.ref, NbContextMenuComponent, {
            position: this.position,
            items: this.items,
            tag: this.tag,
        }, this.componentFactoryResolver);
    };
    NbContextMenuDirective.prototype.hide = function () {
        this.ref.detach();
        this.container = null;
    };
    NbContextMenuDirective.prototype.toggle = function () {
        if (this.ref && this.ref.hasAttached()) {
            this.hide();
        }
        else {
            this.show();
        }
    };
    NbContextMenuDirective.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(this.position)
            .adjustment(this.adjustment);
    };
    NbContextMenuDirective.prototype.createTriggerStrategy = function () {
        var _this = this;
        return new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(exports.NbTrigger.CLICK)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.container; })
            .build();
    };
    NbContextMenuDirective.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return patch(_this.container, { position: position }); });
    };
    NbContextMenuDirective.prototype.subscribeOnTriggers = function () {
        var _this = this;
        this.triggerStrategy.show$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.show(); });
        this.triggerStrategy.hide$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.hide(); });
    };
    /*
     * NbMenuComponent will crash if don't pass menu items to it.
     * So, we just validating them and throw custom obvious error.
     * */
    /*
       * NbMenuComponent will crash if don't pass menu items to it.
       * So, we just validating them and throw custom obvious error.
       * */
    NbContextMenuDirective.prototype.validateItems = /*
       * NbMenuComponent will crash if don't pass menu items to it.
       * So, we just validating them and throw custom obvious error.
       * */
    function (items) {
        if (!items || !items.length) {
            throw Error("List of menu items expected, but given: " + items);
        }
    };
    NbContextMenuDirective.prototype.subscribeOnItemClick = function () {
        var _this = this;
        this.menuService.onItemClick()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function (_a) {
            var tag = _a.tag;
            return tag === _this.tag;
        }))
            .subscribe(function () { return _this.hide(); });
    };
    NbContextMenuDirective.decorators = [
        { type: i0.Directive, args: [{ selector: '[nbContextMenu]' },] },
    ];
    /** @nocollapse */
    NbContextMenuDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: NbMenuService, },
        { type: i0.ElementRef, },
        { type: NbPositionBuilderService, },
        { type: NbOverlayService, },
        { type: i0.ComponentFactoryResolver, },
    ]; };
    NbContextMenuDirective.propDecorators = {
        "position": [{ type: i0.Input, args: ['nbContextMenuPlacement',] },],
        "adjustment": [{ type: i0.Input, args: ['nbContextMenuAdjustment',] },],
        "tag": [{ type: i0.Input, args: ['nbContextMenuTag',] },],
        "setItems": [{ type: i0.Input, args: ['nbContextMenu',] },],
    };
    return NbContextMenuDirective;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbContextMenuModule = /** @class */ (function () {
    function NbContextMenuModule() {
    }
    NbContextMenuModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [i1.CommonModule, NbOverlayModule, NbMenuModule],
                    exports: [NbContextMenuDirective],
                    declarations: [NbContextMenuDirective, NbContextMenuComponent],
                    entryComponents: [NbContextMenuComponent],
                },] },
    ];
    return NbContextMenuModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Progress Bar is a component for indicating progress.
 *
 * Simple usage:
 *
 * ```html
 * <nb-progress-bar [value]="50"></nb-progress-bar>
 * ```
 * ### Installation
 *
 * Import `NbProgressBarModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbProgressBarModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Progress bar accepts property `value` in range 0-100
 * @stacked-example(Progress bar, progress-bar/progress-bar-showcase.component)
 *
 * Progress bar background could be configured by providing a `status` property:
 * @stacked-example(Progress bar status, progress-bar/progress-bar-status.component)
 *
 * Progress bar size (height and font-size) could be configured by providing a `size` property:
 * @stacked-example(Progress bar size, progress-bar/progress-bar-size.component)
 *
 * `displayValue` property shows current value inside progress bar. It's also possible to add custom text inside:
 * @stacked-example(Progress bar value, progress-bar/progress-bar-value.component)
 *
 * Progress bar supports `width` and `background-color` transition:
 * @stacked-example(Progress bar interactive, progress-bar/progress-bar-interactive.component)
 *
 * @styles
 *
 * progress-bar-height-xlg:
 * progress-bar-height-lg:
 * progress-bar-height:
 * progress-bar-height-sm:
 * progress-bar-height-xs:
 * progress-bar-font-size-xlg:
 * progress-bar-font-size-lg:
 * progress-bar-font-size:
 * progress-bar-font-size-sm:
 * progress-bar-font-size-xs:
 * progress-bar-radius:
 * progress-bar-bg-color:
 * progress-bar-font-color:
 * progress-bar-font-weight:
 * progress-bar-default-bg-color:
 * progress-bar-primary-bg-color:
 * progress-bar-success-bg-color:
 * progress-bar-info-bg-color:
 * progress-bar-warning-bg-color:
 * progress-bar-danger-bg-color:
 */
var NbProgressBarComponent = /** @class */ (function () {
    function NbProgressBarComponent() {
        /**
           * Progress bar value in percent (0 - 100)
           * @type {number}
           * @private
           */
        this.value = 0;
        /**
           * Displays value inside progress bar
           * @param {string} val
           */
        this.displayValue = false;
    }
    NbProgressBarComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-progress-bar',
                    styles: [":host{display:block}.progress-container{overflow:hidden}.progress-value{height:100%;text-align:center;overflow:hidden} "],
                    template: "\n    <div class=\"progress-container {{ size ? '' + size : '' }}\">\n      <div class=\"progress-value {{ status ? '' + status : '' }}\" [style.width.%]=\"value\">\n        <span *ngIf=\"displayValue\">{{ value }}%</span>\n        <ng-content></ng-content>\n      </div>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbProgressBarComponent.propDecorators = {
        "value": [{ type: i0.Input },],
        "status": [{ type: i0.Input },],
        "size": [{ type: i0.Input },],
        "displayValue": [{ type: i0.Input },],
    };
    return NbProgressBarComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbProgressBarModule = /** @class */ (function () {
    function NbProgressBarModule() {
    }
    NbProgressBarModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: [NbProgressBarComponent],
                    exports: [NbProgressBarComponent],
                },] },
    ];
    return NbProgressBarModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Alert component.
 *
 * Basic alert example:
 * @stacked-example(Showcase, alert/alert-showcase.component)
 *
 * Alert configuration:
 *
 * ```html
 * <nb-alert status="success">
 *   You have been successfully authenticated!
 * </nb-alert>
 * ```
 * ### Installation
 *
 * Import `NbButtonModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbAlertModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Alert could additionally have a `close` button when `closable` property is set:
 * ```html
 * <nb-alert status="success" closable (close)="onClose()">
 *   You have been successfully authenticated!
 * </nb-alert>
 * ```
 *
 * Colored alerts could be simply configured by providing a `status` property:
 * @stacked-example(Colored Alert, alert/alert-colors.component)
 *
 * It is also possible to assign an `accent` property for a slight alert highlight
 * as well as combine it with `status`:
 * @stacked-example(Accent Alert, alert/alert-accents.component)
 *
 * And `outline` property:
 * @stacked-example(Outline Alert, alert/alert-outline.component)
 *
 * @additional-example(Multiple Sizes, alert/alert-sizes.component)
 *
 * @styles
 *
 * alert-font-size:
 * alert-line-height:
 * alert-font-weight:
 * alert-fg:
 * alert-outline-fg:
 * alert-bg:
 * alert-active-bg:
 * alert-disabled-bg:
 * alert-disabled-fg:
 * alert-primary-bg:
 * alert-info-bg:
 * alert-success-bg:
 * alert-warning-bg:
 * alert-danger-bg:
 * alert-height-xxsmall:
 * alert-height-xsmall:
 * alert-height-small:
 * alert-height-medium:
 * alert-height-large:
 * alert-height-xlarge:
 * alert-height-xxlarge:
 * alert-shadow:
 * alert-border-radius:
 * alert-padding:
 * alert-closable-padding:
 * alert-button-padding:
 * alert-margin:
 */
var NbAlertComponent = /** @class */ (function () {
    function NbAlertComponent() {
        this.closableValue = false;
        /**
           * Emits when chip is removed
           * @type EventEmitter<any>
           */
        this.close = new i0.EventEmitter();
    }
    Object.defineProperty(NbAlertComponent.prototype, "closable", {
        set: /**
           * Shows `close` icon
           */
        function (val) {
            this.closableValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xxsmall", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XXSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "small", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "medium", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "large", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xlarge", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xxlarge", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XXLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "active", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "disabled", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "primary", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "info", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "success", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "warning", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "danger", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "hasAccent", {
        get: function () {
            return this.accent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "hasStatus", {
        get: function () {
            return this.status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "primaryAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "infoAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "successAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "warningAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "dangerAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "activeAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "disabledAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "hasOutline", {
        get: function () {
            return this.outline;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "primaryOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "infoOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "successOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "warningOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "dangerOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "activeOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "disabledOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setSize", {
        set: /**
           * Alert size, available sizes:
           * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setStatus", {
        set: /**
           * Alert status (adds specific styles):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setAccent", {
        set: /**
           * Alert accent (color of the top border):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.accent = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setOutline", {
        set: /**
           * Alert outline (color of the border):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.outline = val;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Emits the removed chip event
     */
    /**
       * Emits the removed chip event
       */
    NbAlertComponent.prototype.onClose = /**
       * Emits the removed chip event
       */
    function () {
        this.close.emit();
    };
    NbAlertComponent.SIZE_XXSMALL = 'xxsmall';
    NbAlertComponent.SIZE_XSMALL = 'xsmall';
    NbAlertComponent.SIZE_SMALL = 'small';
    NbAlertComponent.SIZE_MEDIUM = 'medium';
    NbAlertComponent.SIZE_LARGE = 'large';
    NbAlertComponent.SIZE_XLARGE = 'xlarge';
    NbAlertComponent.SIZE_XXLARGE = 'xxlarge';
    NbAlertComponent.STATUS_ACTIVE = 'active';
    NbAlertComponent.STATUS_DISABLED = 'disabled';
    NbAlertComponent.STATUS_PRIMARY = 'primary';
    NbAlertComponent.STATUS_INFO = 'info';
    NbAlertComponent.STATUS_SUCCESS = 'success';
    NbAlertComponent.STATUS_WARNING = 'warning';
    NbAlertComponent.STATUS_DANGER = 'danger';
    NbAlertComponent.ACCENT_ACTIVE = 'active';
    NbAlertComponent.ACCENT_DISABLED = 'disabled';
    NbAlertComponent.ACCENT_PRIMARY = 'primary';
    NbAlertComponent.ACCENT_INFO = 'info';
    NbAlertComponent.ACCENT_SUCCESS = 'success';
    NbAlertComponent.ACCENT_WARNING = 'warning';
    NbAlertComponent.ACCENT_DANGER = 'danger';
    NbAlertComponent.OUTLINE_ACTIVE = 'active';
    NbAlertComponent.OUTLINE_DISABLED = 'disabled';
    NbAlertComponent.OUTLINE_PRIMARY = 'primary';
    NbAlertComponent.OUTLINE_INFO = 'info';
    NbAlertComponent.OUTLINE_SUCCESS = 'success';
    NbAlertComponent.OUTLINE_WARNING = 'warning';
    NbAlertComponent.OUTLINE_DANGER = 'danger';
    NbAlertComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-alert',
                    styles: [":host{display:flex;flex-direction:column;position:relative}.close{position:absolute;top:0;right:0;color:inherit;background-color:transparent;border:0;-webkit-appearance:none} "],
                    template: "\n    <button *ngIf=\"closableValue\" type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"onClose()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n    <ng-content></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbAlertComponent.propDecorators = {
        "closableValue": [{ type: i0.HostBinding, args: ['class.closable',] },],
        "closable": [{ type: i0.Input },],
        "xxsmall": [{ type: i0.HostBinding, args: ['class.xxsmall-alert',] },],
        "xsmall": [{ type: i0.HostBinding, args: ['class.xsmall-alert',] },],
        "small": [{ type: i0.HostBinding, args: ['class.small-alert',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium-alert',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large-alert',] },],
        "xlarge": [{ type: i0.HostBinding, args: ['class.xlarge-alert',] },],
        "xxlarge": [{ type: i0.HostBinding, args: ['class.xxlarge-alert',] },],
        "active": [{ type: i0.HostBinding, args: ['class.active-alert',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled-alert',] },],
        "primary": [{ type: i0.HostBinding, args: ['class.primary-alert',] },],
        "info": [{ type: i0.HostBinding, args: ['class.info-alert',] },],
        "success": [{ type: i0.HostBinding, args: ['class.success-alert',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.warning-alert',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.danger-alert',] },],
        "hasAccent": [{ type: i0.HostBinding, args: ['class.accent',] },],
        "hasStatus": [{ type: i0.HostBinding, args: ['class.status',] },],
        "primaryAccent": [{ type: i0.HostBinding, args: ['class.accent-primary',] },],
        "infoAccent": [{ type: i0.HostBinding, args: ['class.accent-info',] },],
        "successAccent": [{ type: i0.HostBinding, args: ['class.accent-success',] },],
        "warningAccent": [{ type: i0.HostBinding, args: ['class.accent-warning',] },],
        "dangerAccent": [{ type: i0.HostBinding, args: ['class.accent-danger',] },],
        "activeAccent": [{ type: i0.HostBinding, args: ['class.accent-active',] },],
        "disabledAccent": [{ type: i0.HostBinding, args: ['class.accent-disabled',] },],
        "hasOutline": [{ type: i0.HostBinding, args: ['class.outline',] },],
        "primaryOutline": [{ type: i0.HostBinding, args: ['class.outline-primary',] },],
        "infoOutline": [{ type: i0.HostBinding, args: ['class.outline-info',] },],
        "successOutline": [{ type: i0.HostBinding, args: ['class.outline-success',] },],
        "warningOutline": [{ type: i0.HostBinding, args: ['class.outline-warning',] },],
        "dangerOutline": [{ type: i0.HostBinding, args: ['class.outline-danger',] },],
        "activeOutline": [{ type: i0.HostBinding, args: ['class.outline-active',] },],
        "disabledOutline": [{ type: i0.HostBinding, args: ['class.outline-disabled',] },],
        "setSize": [{ type: i0.Input, args: ['size',] },],
        "setStatus": [{ type: i0.Input, args: ['status',] },],
        "setAccent": [{ type: i0.Input, args: ['accent',] },],
        "setOutline": [{ type: i0.Input, args: ['outline',] },],
        "close": [{ type: i0.Output },],
    };
    return NbAlertComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbAlertModule = /** @class */ (function () {
    function NbAlertModule() {
    }
    NbAlertModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: [
                        NbAlertComponent,
                    ],
                    exports: [
                        NbAlertComponent,
                    ],
                },] },
    ];
    return NbAlertModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Chat message component.
 *
 * Multiple message types are available through a `type` property, such as
 * - text - simple text message
 * - file - could be a file preview or a file icon
 * if multiple files are provided grouped files are shown
 * - quote - quotes a message with specific quote styles
 * - map - shows a google map picture by provided [latitude] and [longitude] properties
 *
 * @stacked-example(Available Types, chat/chat-message-types-showcase.component)
 *
 * Message with attached files:
 * ```html
 * <nb-chat-message
 *   type="file"
 *   [files]="[ { url: '...' } ]"
 *   message="Hello world!">
 * </nb-chat-message>
 * ```
 *
 * Map message:
 * ```html
 * <nb-chat-message
 *   type="map"
 *   [latitude]="53.914"
 *   [longitude]="27.59"
 *   message="Here I am">
 * </nb-chat-message>
 * ```
 *
 * @styles
 *
 * chat-message-fg:
 * chat-message-bg:
 * chat-message-reply-bg:
 * chat-message-reply-fg:
 * chat-message-avatar-bg:
 * chat-message-sender-fg:
 * chat-message-quote-fg:
 * chat-message-quote-bg:
 * chat-message-file-fg:
 * chat-message-file-bg:
 */
var NbChatMessageComponent = /** @class */ (function () {
    function NbChatMessageComponent(domSanitizer) {
        this.domSanitizer = domSanitizer;
        this.replyValue = false;
    }
    Object.defineProperty(NbChatMessageComponent.prototype, "flyInOut", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatMessageComponent.prototype, "notReply", {
        get: function () {
            return !this.replyValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatMessageComponent.prototype, "reply", {
        set: /**
           * Determines if a message is a reply
           */
        function (val) {
            this.replyValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatMessageComponent.prototype, "avatar", {
        set: /**
           * Message send avatar
           * @type {string}
           */
        function (value) {
            this.avatarStyle = value ? this.domSanitizer.bypassSecurityTrustStyle("url(" + value + ")") : null;
        },
        enumerable: true,
        configurable: true
    });
    NbChatMessageComponent.prototype.getInitials = function () {
        if (this.sender) {
            var names = this.sender.split(' ');
            return names.map(function (n) { return n.charAt(0); }).splice(0, 2).join('').toUpperCase();
        }
        return '';
    };
    NbChatMessageComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-chat-message',
                    template: "\n    <div class=\"avatar\" [style.background-image]=\"avatarStyle\" *ngIf=\"!replyValue\">\n      <ng-container *ngIf=\"!avatarStyle\">\n        {{ getInitials() }}\n      </ng-container>\n    </div>\n    <div class=\"message\">\n      <ng-container [ngSwitch]=\"type\">\n\n        <nb-chat-message-file *ngSwitchCase=\"'file'\"\n                              [sender]=\"sender\" [date]=\"date\" [message]=\"message\" [files]=\"files\">\n        </nb-chat-message-file>\n\n        <nb-chat-message-quote *ngSwitchCase=\"'quote'\"\n                              [sender]=\"sender\" [date]=\"date\" [message]=\"message\" [quote]=\"quote\">\n        </nb-chat-message-quote>\n\n        <nb-chat-message-map *ngSwitchCase=\"'map'\"\n                              [sender]=\"sender\" [date]=\"date\"\n                              [message]=\"message\" [latitude]=\"latitude\" [longitude]=\"longitude\">\n        </nb-chat-message-map>\n\n        <nb-chat-message-text *ngSwitchDefault\n                              [sender]=\"sender\" [date]=\"date\" [message]=\"message\">\n        </nb-chat-message-text>\n      </ng-container>\n    </div>\n  ",
                    animations: [
                        _angular_animations.trigger('flyInOut', [
                            _angular_animations.state('in', _angular_animations.style({ transform: 'translateX(0)' })),
                            _angular_animations.transition('void => *', [
                                _angular_animations.style({ transform: 'translateX(-100%)' }),
                                _angular_animations.animate(80),
                            ]),
                            _angular_animations.transition('* => void', [
                                _angular_animations.animate(80, _angular_animations.style({ transform: 'translateX(100%)' })),
                            ]),
                        ]),
                    ],
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageComponent.ctorParameters = function () { return [
        { type: _angular_platformBrowser.DomSanitizer, },
    ]; };
    NbChatMessageComponent.propDecorators = {
        "flyInOut": [{ type: i0.HostBinding, args: ['@flyInOut',] },],
        "replyValue": [{ type: i0.HostBinding, args: ['class.reply',] },],
        "notReply": [{ type: i0.HostBinding, args: ['class.not-reply',] },],
        "reply": [{ type: i0.Input },],
        "message": [{ type: i0.Input },],
        "sender": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
        "files": [{ type: i0.Input },],
        "quote": [{ type: i0.Input },],
        "latitude": [{ type: i0.Input },],
        "longitude": [{ type: i0.Input },],
        "avatar": [{ type: i0.Input },],
        "type": [{ type: i0.Input },],
    };
    return NbChatMessageComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Conversational UI collection - a set of components for chat-like UI construction.
 *
 * Main features:
 * - different message types support (text, image, file, file group, map, etc)
 * - drag & drop for images and files with preview
 * - different UI styles
 * - custom action buttons (coming soon)
 *
 * Here's a complete example build in a bot-like app. Type `help` to be able to receive different message types.
 * Enjoy the conversation and the beautiful UI.
 * @stacked-example(Showcase, chat/chat-showcase.component)
 *
 * Basic chat configuration and usage:
 * ```ts
 * <nb-chat title="Nebular Conversational UI">
 *       <nb-chat-message *ngFor="let msg of messages"
 *                        [type]="msg.type"
 *                        [message]="msg.text"
 *                        [reply]="msg.reply"
 *                        [sender]="msg.user.name"
 *                        [date]="msg.date"
 *                        [files]="msg.files"
 *                        [quote]="msg.quote"
 *                        [latitude]="msg.latitude"
 *                        [longitude]="msg.longitude"
 *                        [avatar]="msg.user.avatar">
 *   </nb-chat-message>
 *
 *   <nb-chat-form (send)="sendMessage($event)" [dropFiles]="true">
 *   </nb-chat-form>
 * </nb-chat>
 * ```
 * ### Installation
 *
 * Import `NbChatModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbChatModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 *
 * If you need to provide an API key for a `map` message type (which is required by Google Maps)
 * you may use `NbChatModule.forRoot({ ... })` call if this is a global app configuration
 * or `NbChatModule.forChild({ ... })` for a feature module configuration:
 *
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbChatModule.forRoot({ messageGoogleMapKey: 'MAP_KEY' }),
 *   ],
 * })
 * export class AppModule { }
 *
 * ### Usage
 *
 * There are three main components:
 * ```ts
 * <nb-chat>
 * </nb-chat> // chat container
 *
 * <nb-chat-form>
 * </nb-chat-form> // chat form with drag&drop files feature
 *
 * <nb-chat-message>
 * </nb-chat-message> // chat message, available multiple types
 * ```
 *
 * Two users conversation showcase:
 * @stacked-example(Conversation, chat/chat-conversation-showcase.component)
 *
 * Chat UI is also available in different colors by specifying a `[status]` input:
 *
 * @stacked-example(Colored Chat, chat/chat-colors.component)
 *
 * Also it is possible to configure sizes through `[size]` input:
 *
 * @stacked-example(Chat Sizes, chat/chat-sizes.component)
 *
 * @styles
 *
 * chat-font-size:
 * chat-fg:
 * chat-bg:
 * chat-border-radius:
 * chat-fg-text:
 * chat-height-xxsmall:
 * chat-height-xsmall:
 * chat-height-small:
 * chat-height-medium:
 * chat-height-large:
 * chat-height-xlarge:
 * chat-height-xxlarge:
 * chat-border:
 * chat-padding:
 * chat-shadow:
 * chat-separator:
 * chat-active-bg:
 * chat-disabled-bg:
 * chat-disabled-fg:
 * chat-primary-bg:
 * chat-info-bg:
 * chat-success-bg:
 * chat-warning-bg:
 * chat-danger-bg:
 */
var NbChatComponent = /** @class */ (function () {
    function NbChatComponent() {
    }
    Object.defineProperty(NbChatComponent.prototype, "xxsmall", {
        get: function () {
            return this.size === NbChatComponent.SIZE_XXSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbChatComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "small", {
        get: function () {
            return this.size === NbChatComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "medium", {
        get: function () {
            return this.size === NbChatComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "large", {
        get: function () {
            return this.size === NbChatComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "xlarge", {
        get: function () {
            return this.size === NbChatComponent.SIZE_XLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "xxlarge", {
        get: function () {
            return this.size === NbChatComponent.SIZE_XXLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "active", {
        get: function () {
            return this.status === NbChatComponent.STATUS_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "disabled", {
        get: function () {
            return this.status === NbChatComponent.STATUS_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "primary", {
        get: function () {
            return this.status === NbChatComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "info", {
        get: function () {
            return this.status === NbChatComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "success", {
        get: function () {
            return this.status === NbChatComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "warning", {
        get: function () {
            return this.status === NbChatComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "danger", {
        get: function () {
            return this.status === NbChatComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "hasAccent", {
        get: function () {
            return this.accent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "setSize", {
        set: /**
           * Chat size, available sizes:
           * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatComponent.prototype, "setStatus", {
        set: /**
           * Chat status color (adds specific styles):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    NbChatComponent.prototype.ngAfterViewChecked = function () {
        this.scrollable.nativeElement.scrollTop = this.scrollable.nativeElement.scrollHeight;
    };
    NbChatComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.messages.changes
            .subscribe(function (messages) { return _this.messages = messages; });
    };
    NbChatComponent.SIZE_XXSMALL = 'xxsmall';
    NbChatComponent.SIZE_XSMALL = 'xsmall';
    NbChatComponent.SIZE_SMALL = 'small';
    NbChatComponent.SIZE_MEDIUM = 'medium';
    NbChatComponent.SIZE_LARGE = 'large';
    NbChatComponent.SIZE_XLARGE = 'xlarge';
    NbChatComponent.SIZE_XXLARGE = 'xxlarge';
    NbChatComponent.STATUS_ACTIVE = 'active';
    NbChatComponent.STATUS_DISABLED = 'disabled';
    NbChatComponent.STATUS_PRIMARY = 'primary';
    NbChatComponent.STATUS_INFO = 'info';
    NbChatComponent.STATUS_SUCCESS = 'success';
    NbChatComponent.STATUS_WARNING = 'warning';
    NbChatComponent.STATUS_DANGER = 'danger';
    NbChatComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-chat',
                    styles: [":host{display:flex;flex-direction:column;position:relative;height:100%} "],
                    template: "\n    <div class=\"header\">{{ title }}</div>\n    <div class=\"scrollable\" #scrollable>\n      <div class=\"messages\">\n        <ng-content select=\"nb-chat-message\"></ng-content>\n        <p class=\"no-messages\" *ngIf=\"!messages?.length\">No messages yet.</p>\n      </div>\n    </div>\n    <div class=\"form\">\n      <ng-content select=\"nb-chat-form\"></ng-content>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbChatComponent.propDecorators = {
        "title": [{ type: i0.Input },],
        "xxsmall": [{ type: i0.HostBinding, args: ['class.xxsmall-chat',] },],
        "xsmall": [{ type: i0.HostBinding, args: ['class.xsmall-chat',] },],
        "small": [{ type: i0.HostBinding, args: ['class.small-chat',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium-chat',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large-chat',] },],
        "xlarge": [{ type: i0.HostBinding, args: ['class.xlarge-chat',] },],
        "xxlarge": [{ type: i0.HostBinding, args: ['class.xxlarge-chat',] },],
        "active": [{ type: i0.HostBinding, args: ['class.active-chat',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled-chat',] },],
        "primary": [{ type: i0.HostBinding, args: ['class.primary-chat',] },],
        "info": [{ type: i0.HostBinding, args: ['class.info-chat',] },],
        "success": [{ type: i0.HostBinding, args: ['class.success-chat',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.warning-chat',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.danger-chat',] },],
        "hasAccent": [{ type: i0.HostBinding, args: ['class.accent',] },],
        "setSize": [{ type: i0.Input, args: ['size',] },],
        "setStatus": [{ type: i0.Input, args: ['status',] },],
        "scrollable": [{ type: i0.ViewChild, args: ['scrollable',] },],
        "messages": [{ type: i0.ContentChildren, args: [NbChatMessageComponent,] },],
    };
    return NbChatComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbChatOptions = /** @class */ (function () {
    function NbChatOptions() {
    }
    return NbChatOptions;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageMapComponent = /** @class */ (function () {
    function NbChatMessageMapComponent(options) {
        this.mapKey = options.messageGoogleMapKey;
    }
    Object.defineProperty(NbChatMessageMapComponent.prototype, "file", {
        get: function () {
            return {
                // tslint:disable-next-line
                url: "https://maps.googleapis.com/maps/api/staticmap?center=" + this.latitude + "," + this.longitude + "&zoom=12&size=400x400&key=" + this.mapKey,
                type: 'image/png',
                icon: 'nb-location',
            };
        },
        enumerable: true,
        configurable: true
    });
    NbChatMessageMapComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-chat-message-map',
                    template: "\n    <nb-chat-message-file [files]=\"[file]\" [message]=\"message\" [sender]=\"sender\" [date]=\"date\"></nb-chat-message-file>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageMapComponent.ctorParameters = function () { return [
        { type: NbChatOptions, },
    ]; };
    NbChatMessageMapComponent.propDecorators = {
        "message": [{ type: i0.Input },],
        "sender": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
        "latitude": [{ type: i0.Input },],
        "longitude": [{ type: i0.Input },],
    };
    return NbChatMessageMapComponent;
}());

var __assign$1 = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageFileComponent = /** @class */ (function () {
    function NbChatMessageFileComponent(cd, domSanitizer) {
        this.cd = cd;
        this.domSanitizer = domSanitizer;
    }
    Object.defineProperty(NbChatMessageFileComponent.prototype, "files", {
        set: /**
           * Message file path
           * @type {Date}
           */
        function (files) {
            var _this = this;
            this.readyFiles = (files || []).map(function (file) {
                var isImage = _this.isImage(file);
                return __assign$1({}, file, { urlStyle: isImage && _this.domSanitizer.bypassSecurityTrustStyle("url(" + file.url + ")"), isImage: isImage });
            });
            this.cd.detectChanges();
        },
        enumerable: true,
        configurable: true
    });
    NbChatMessageFileComponent.prototype.isImage = function (file) {
        return ['image/png', 'image/jpeg', 'image/gif'].includes(file.type);
    };
    NbChatMessageFileComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-chat-message-file',
                    template: "\n    <nb-chat-message-text [sender]=\"sender\" [date]=\"date\" [message]=\"message\">\n      {{ message }}\n    </nb-chat-message-text>\n\n    <ng-container *ngIf=\"readyFiles?.length > 1\">\n      <div class=\"message-content-group\">\n        <a *ngFor=\"let file of readyFiles\" [href]=\"file.url\" target=\"_blank\">\n          <span [class]=\"file.icon\" *ngIf=\"!file.urlStyle\"></span>\n          <div *ngIf=\"file.isImage\" [style.background-image]=\"file.urlStyle\"></div>\n        </a>\n      </div>\n    </ng-container>\n\n    <ng-container *ngIf=\"readyFiles?.length === 1\">\n      <a [href]=\"readyFiles[0].url\" target=\"_blank\">\n        <span [class]=\"readyFiles[0].icon\"  *ngIf=\"!readyFiles[0].urlStyle\"></span>\n        <div *ngIf=\"readyFiles[0].isImage\" [style.background-image]=\"readyFiles[0].urlStyle\"></div>\n      </a>\n    </ng-container>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageFileComponent.ctorParameters = function () { return [
        { type: i0.ChangeDetectorRef, },
        { type: _angular_platformBrowser.DomSanitizer, },
    ]; };
    NbChatMessageFileComponent.propDecorators = {
        "message": [{ type: i0.Input },],
        "sender": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
        "files": [{ type: i0.Input },],
    };
    return NbChatMessageFileComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageQuoteComponent = /** @class */ (function () {
    function NbChatMessageQuoteComponent() {
    }
    NbChatMessageQuoteComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-chat-message-quote',
                    template: "\n    <p class=\"sender\" *ngIf=\"sender || date\">{{ sender }} <time>{{ date  | date:'shortTime' }}</time></p>\n    <p class=\"quote\">\n      {{ quote }}\n    </p>\n    <nb-chat-message-text [message]=\"message\">\n      {{ message }}\n    </nb-chat-message-text>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageQuoteComponent.propDecorators = {
        "message": [{ type: i0.Input },],
        "sender": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
        "quote": [{ type: i0.Input },],
    };
    return NbChatMessageQuoteComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageTextComponent = /** @class */ (function () {
    function NbChatMessageTextComponent() {
    }
    NbChatMessageTextComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-chat-message-text',
                    template: "\n    <p class=\"sender\" *ngIf=\"sender || date\">{{ sender }} <time>{{ date  | date:'shortTime' }}</time></p>\n    <p class=\"text\" *ngIf=\"message\">{{ message }}</p>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageTextComponent.propDecorators = {
        "sender": [{ type: i0.Input },],
        "message": [{ type: i0.Input },],
        "date": [{ type: i0.Input },],
    };
    return NbChatMessageTextComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Chat form component.
 *
 * Show a message form with a send message button.
 *
 * ```ts
 * <nb-chat-form showButton="true" buttonIcon="nb-send">
 * </nb-chat-form>
 * ```
 *
 * When `[dropFiles]="true"` handles files drag&drop with a file preview.
 *
 * Drag & drop available for files and images:
 * @stacked-example(Drag & Drop Chat, chat/chat-drop.component)
 *
 * New message could be tracked outside by using `(send)` output.
 *
 * ```ts
 * <nb-chat-form (send)="onNewMessage($event)">
 * </nb-chat-form>
 *
 * // ...
 *
 * onNewMessage({ message: string, files: any[] }) {
 *   this.service.sendToServer(message, files);
 * }
 * ```
 *
 * @styles
 *
 * chat-form-bg:
 * chat-form-fg:
 * chat-form-border:
 * chat-form-active-border:
 *
 */
var NbChatFormComponent = /** @class */ (function () {
    function NbChatFormComponent(cd, domSanitizer) {
        this.cd = cd;
        this.domSanitizer = domSanitizer;
        this.droppedFiles = [];
        this.imgDropTypes = ['image/png', 'image/jpeg', 'image/gif'];
        /**
           * Predefined message text
           * @type {string}
           */
        this.message = '';
        /**
           * Send button title
           * @type {string}
           */
        this.buttonTitle = '';
        /**
           * Send button icon, shown if `buttonTitle` is empty
           * @type {string}
           */
        this.buttonIcon = 'nb-paper-plane';
        /**
           * Show send button
           * @type {boolean}
           */
        this.showButton = true;
        /**
           * Show send button
           * @type {boolean}
           */
        this.dropFiles = false;
        /**
           *
           * @type {EventEmitter<{ message: string, files: File[] }>}
           */
        this.send = new i0.EventEmitter();
        this.fileOver = false;
    }
    NbChatFormComponent.prototype.onDrop = function (event) {
        var _this = this;
        if (this.dropFiles) {
            event.preventDefault();
            event.stopPropagation();
            this.fileOver = false;
            if (event.dataTransfer && event.dataTransfer.files) {
                var _loop_1 = function (file) {
                    var res = file;
                    if (this_1.imgDropTypes.includes(file.type)) {
                        var fr = new FileReader();
                        fr.onload = function (e) {
                            res.src = e.target.result;
                            res.urlStyle = _this.domSanitizer.bypassSecurityTrustStyle("url(" + res.src + ")");
                            _this.cd.detectChanges();
                        };
                        fr.readAsDataURL(file);
                    }
                    this_1.droppedFiles.push(res);
                };
                var this_1 = this;
                // tslint:disable-next-line
                for (var _i = 0, _a = event.dataTransfer.files; _i < _a.length; _i++) {
                    var file = _a[_i];
                    _loop_1(file);
                }
            }
        }
    };
    NbChatFormComponent.prototype.removeFile = function (file) {
        var index = this.droppedFiles.indexOf(file);
        if (index >= 0) {
            this.droppedFiles.splice(index, 1);
        }
    };
    NbChatFormComponent.prototype.onDragOver = function () {
        if (this.dropFiles) {
            this.fileOver = true;
        }
    };
    NbChatFormComponent.prototype.onDragLeave = function () {
        if (this.dropFiles) {
            this.fileOver = false;
        }
    };
    NbChatFormComponent.prototype.sendMessage = function () {
        if (this.droppedFiles.length || String(this.message).trim().length) {
            this.send.emit({ message: this.message, files: this.droppedFiles });
            this.message = '';
            this.droppedFiles = [];
        }
    };
    NbChatFormComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-chat-form',
                    template: "\n    <div class=\"dropped-files\" *ngIf=\"droppedFiles?.length\">\n      <ng-container *ngFor=\"let file of droppedFiles\">\n        <div *ngIf=\"file.urlStyle\" [style.background-image]=\"file.urlStyle\">\n          <span class=\"remove\" (click)=\"removeFile(file)\">&times;</span>\n        </div>\n        <div *ngIf=\"!file.urlStyle\" class=\"nb-compose\">\n          <span class=\"remove\" (click)=\"removeFile(file)\">&times;</span>\n        </div>\n      </ng-container>\n    </div>\n    <div class=\"message-row\">\n      <input [(ngModel)]=\"message\"\n             [class.with-button]=\"showButton\"\n             type=\"text\"\n             placeholder=\"{{ fileOver ? 'Drop file to send' : 'Type a message' }}\"\n             (keyup.enter)=\"sendMessage()\">\n      <button *ngIf=\"showButton\" class=\"btn\" [class.with-icon]=\"!buttonTitle\" (click)=\"sendMessage()\">\n        {{ buttonTitle }}<span *ngIf=\"!buttonTitle\" [class]=\"buttonIcon\"></span>\n      </button>\n    </div>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatFormComponent.ctorParameters = function () { return [
        { type: i0.ChangeDetectorRef, },
        { type: _angular_platformBrowser.DomSanitizer, },
    ]; };
    NbChatFormComponent.propDecorators = {
        "message": [{ type: i0.Input },],
        "buttonTitle": [{ type: i0.Input },],
        "buttonIcon": [{ type: i0.Input },],
        "showButton": [{ type: i0.Input },],
        "dropFiles": [{ type: i0.Input },],
        "send": [{ type: i0.Output },],
        "fileOver": [{ type: i0.HostBinding, args: ['class.file-over',] },],
        "onDrop": [{ type: i0.HostListener, args: ['drop', ['$event'],] },],
        "onDragOver": [{ type: i0.HostListener, args: ['dragover',] },],
        "onDragLeave": [{ type: i0.HostListener, args: ['dragleave',] },],
    };
    return NbChatFormComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_CHAT_COMPONENTS = [
    NbChatComponent,
    NbChatMessageComponent,
    NbChatFormComponent,
    NbChatMessageTextComponent,
    NbChatMessageFileComponent,
    NbChatMessageQuoteComponent,
    NbChatMessageMapComponent,
];
var NbChatModule = /** @class */ (function () {
    function NbChatModule() {
    }
    NbChatModule.forRoot = function (options) {
        return {
            ngModule: NbChatModule,
            providers: [
                { provide: NbChatOptions, useValue: options },
            ],
        };
    };
    NbChatModule.forChild = function (options) {
        return {
            ngModule: NbChatModule,
            providers: [
                { provide: NbChatOptions, useValue: options },
            ],
        };
    };
    NbChatModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: NB_CHAT_COMPONENTS.slice(),
                    exports: NB_CHAT_COMPONENTS.slice(),
                },] },
    ];
    return NbChatModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Styled spinner component
 */
var NbSpinnerComponent = /** @class */ (function () {
    function NbSpinnerComponent() {
        this.size = NbSpinnerComponent.SIZE_MEDIUM;
        this.status = NbSpinnerComponent.STATUS_ACTIVE;
        /**
           * Loading text that is shown near the icon
           * @type string
           */
        this.message = 'Loading...';
    }
    Object.defineProperty(NbSpinnerComponent.prototype, "setSize", {
        set: /**
           * Spiiner size, available sizes:
           * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "setStatus", {
        set: /**
           * Spiiner status (adds specific styles):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xxsmall", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XXSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "small", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "medium", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "large", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xlarge", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xxlarge", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XXLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "active", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "disabled", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "primary", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "info", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "success", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "warning", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "danger", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    NbSpinnerComponent.SIZE_XXSMALL = 'xxsmall';
    NbSpinnerComponent.SIZE_XSMALL = 'xsmall';
    NbSpinnerComponent.SIZE_SMALL = 'small';
    NbSpinnerComponent.SIZE_MEDIUM = 'medium';
    NbSpinnerComponent.SIZE_LARGE = 'large';
    NbSpinnerComponent.SIZE_XLARGE = 'xlarge';
    NbSpinnerComponent.SIZE_XXLARGE = 'xxlarge';
    NbSpinnerComponent.STATUS_ACTIVE = 'active';
    NbSpinnerComponent.STATUS_DISABLED = 'disabled';
    NbSpinnerComponent.STATUS_PRIMARY = 'primary';
    NbSpinnerComponent.STATUS_INFO = 'info';
    NbSpinnerComponent.STATUS_SUCCESS = 'success';
    NbSpinnerComponent.STATUS_WARNING = 'warning';
    NbSpinnerComponent.STATUS_DANGER = 'danger';
    NbSpinnerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-spinner',
                    template: "\n    <span class=\"spin-circle\"></span>\n    <span class=\"message\" *ngIf=\"message\">{{ message }}</span>\n  ",
                    styles: [":host{opacity:1;position:absolute;border-radius:inherit;top:0;right:0;left:0;bottom:0;overflow:hidden;z-index:9999;display:flex;justify-content:center;align-items:center;visibility:visible}:host .spin-circle{animation:spin 0.8s infinite linear;border-radius:50%;border-style:solid;border-width:0.125em;width:1em;height:1em}:host .message{margin-left:0.5rem;line-height:1rem;font-size:1rem} "],
                },] },
    ];
    /** @nocollapse */
    NbSpinnerComponent.propDecorators = {
        "message": [{ type: i0.Input },],
        "setSize": [{ type: i0.Input, args: ['size',] },],
        "setStatus": [{ type: i0.Input, args: ['status',] },],
        "xxsmall": [{ type: i0.HostBinding, args: ['class.xxsmall-spinner',] },],
        "xsmall": [{ type: i0.HostBinding, args: ['class.xsmall-spinner',] },],
        "small": [{ type: i0.HostBinding, args: ['class.small-spinner',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.medium-spinner',] },],
        "large": [{ type: i0.HostBinding, args: ['class.large-spinner',] },],
        "xlarge": [{ type: i0.HostBinding, args: ['class.xlarge-spinner',] },],
        "xxlarge": [{ type: i0.HostBinding, args: ['class.xxlarge-spinner',] },],
        "active": [{ type: i0.HostBinding, args: ['class.active-spinner',] },],
        "disabled": [{ type: i0.HostBinding, args: ['class.disabled-spinner',] },],
        "primary": [{ type: i0.HostBinding, args: ['class.primary-spinner',] },],
        "info": [{ type: i0.HostBinding, args: ['class.info-spinner',] },],
        "success": [{ type: i0.HostBinding, args: ['class.success-spinner',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.warning-spinner',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.danger-spinner',] },],
    };
    return NbSpinnerComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Styled spinner directive
 *
 * @stacked-example(Spinner Showcase, spinner/spinner-card.component)
 *
 *
 * ```ts
 * <nb-card [nbSpinner]="loading" nbSpinnerStatus="danger">
 *   <nb-card-body>Card Content</nb-card-body>
 * </nb-card>
 * ```
 *
 * ### Installation
 *
 * Import `NbSpinnerModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbSpinnerModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Could be colored using `status` property
 *
 * @stacked-example(Spinner Colors, spinner/spinner-colors.component)
 *
 * Available in different sizes with `size` property:
 *
 * @stacked-example(Spinner Sizes, spinner/spinner-sizes.component)
 *
 * It is also possible to place it into the button:
 * @stacked-example(Buttons with spinner, spinner/spinner-button.component)
 *
 * Or tabs:
 * @stacked-example(Spinner in tabs, spinner/spinner-tabs.component)
 */
var NbSpinnerDirective = /** @class */ (function () {
    function NbSpinnerDirective(directiveView, componentFactoryResolver, renderer, directiveElement) {
        this.directiveView = directiveView;
        this.componentFactoryResolver = componentFactoryResolver;
        this.renderer = renderer;
        this.directiveElement = directiveElement;
        this.isSpinnerExist = false;
        this.shouldShow = false;
    }
    Object.defineProperty(NbSpinnerDirective.prototype, "nbSpinner", {
        set: /**
           * Directive value - show or hide spinner
           * @param {boolean} val
           */
        function (val) {
            if (this.componentFactory) {
                if (val) {
                    this.show();
                }
                else {
                    this.hide();
                }
            }
            else {
                this.shouldShow = val;
            }
        },
        enumerable: true,
        configurable: true
    });
    NbSpinnerDirective.prototype.ngOnInit = function () {
        this.componentFactory = this.componentFactoryResolver.resolveComponentFactory(NbSpinnerComponent);
        if (this.shouldShow) {
            this.show();
        }
    };
    NbSpinnerDirective.prototype.hide = function () {
        if (this.isSpinnerExist) {
            this.directiveView.remove();
            this.isSpinnerExist = false;
        }
    };
    NbSpinnerDirective.prototype.show = function () {
        if (!this.isSpinnerExist) {
            this.spinner = this.directiveView.createComponent(this.componentFactory);
            this.setInstanceInputs(this.spinner.instance);
            this.spinner.changeDetectorRef.detectChanges();
            this.renderer.appendChild(this.directiveElement.nativeElement, this.spinner.location.nativeElement);
            this.isSpinnerExist = true;
        }
    };
    NbSpinnerDirective.prototype.setInstanceInputs = function (instance) {
        typeof this.spinnerMessage !== 'undefined' && (instance.message = this.spinnerMessage);
        typeof this.spinnerStatus !== 'undefined' && (instance.status = this.spinnerStatus);
        typeof this.spinnerSize !== 'undefined' && (instance.size = this.spinnerSize);
    };
    NbSpinnerDirective.decorators = [
        { type: i0.Directive, args: [{ selector: '[nbSpinner]' },] },
    ];
    /** @nocollapse */
    NbSpinnerDirective.ctorParameters = function () { return [
        { type: i0.ViewContainerRef, },
        { type: i0.ComponentFactoryResolver, },
        { type: i0.Renderer2, },
        { type: i0.ElementRef, },
    ]; };
    NbSpinnerDirective.propDecorators = {
        "isSpinnerExist": [{ type: i0.HostBinding, args: ['class.nb-spinner-container',] },],
        "spinnerMessage": [{ type: i0.Input, args: ['nbSpinnerMessage',] },],
        "spinnerStatus": [{ type: i0.Input, args: ['nbSpinnerStatus',] },],
        "spinnerSize": [{ type: i0.Input, args: ['nbSpinnerSize',] },],
        "nbSpinner": [{ type: i0.Input, args: ['nbSpinner',] },],
    };
    return NbSpinnerDirective;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbSpinnerModule = /** @class */ (function () {
    function NbSpinnerModule() {
    }
    NbSpinnerModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    exports: [NbSpinnerComponent, NbSpinnerDirective],
                    declarations: [NbSpinnerComponent, NbSpinnerDirective],
                    entryComponents: [NbSpinnerComponent],
                },] },
    ];
    return NbSpinnerModule;
}());

/**
 * Component intended to be used within  the `<nb-stepper>` component.
 * Container for a step
 */
var NbStepComponent = /** @class */ (function () {
    function NbStepComponent(stepper) {
        this.stepper = stepper;
        this.completedValue = false;
        this.interacted = false;
    }
    Object.defineProperty(NbStepComponent.prototype, "isLabelTemplate", {
        /**
         * Check that label is a TemplateRef.
         *
         * @return boolean
         * */
        get: /**
           * Check that label is a TemplateRef.
           *
           * @return boolean
           * */
        function () {
            return this.label instanceof i0.TemplateRef;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbStepComponent.prototype, "completed", {
        get: /**
           * Whether step is marked as completed.
           *
           * @type {boolean}
           */
        function () {
            return this.completedValue || this.isCompleted;
        },
        set: function (value) {
            this.completedValue = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbStepComponent.prototype, "isCompleted", {
        get: function () {
            return this.stepControl ? this.stepControl.valid && this.interacted : this.interacted;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Mark step as selected
     * */
    /**
       * Mark step as selected
       * */
    NbStepComponent.prototype.select = /**
       * Mark step as selected
       * */
    function () {
        this.stepper.selected = this;
    };
    /**
     * Reset step and stepControl state
     * */
    /**
       * Reset step and stepControl state
       * */
    NbStepComponent.prototype.reset = /**
       * Reset step and stepControl state
       * */
    function () {
        this.interacted = false;
        if (this.stepControl) {
            this.stepControl.reset();
        }
    };
    NbStepComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-step',
                    template: "\n    <ng-template>\n      <ng-content></ng-content>\n    </ng-template>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbStepComponent.ctorParameters = function () { return [
        { type: NbStepperComponent, decorators: [{ type: i0.Inject, args: [i0.forwardRef(function () { return NbStepperComponent; }),] },] },
    ]; };
    NbStepComponent.propDecorators = {
        "content": [{ type: i0.ViewChild, args: [i0.TemplateRef,] },],
        "stepControl": [{ type: i0.Input },],
        "label": [{ type: i0.Input },],
        "hidden": [{ type: i0.Input },],
        "completed": [{ type: i0.Input },],
    };
    return NbStepComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

(function (NbStepperOrientation) {
    NbStepperOrientation["VERTICAL"] = "vertical";
    NbStepperOrientation["HORIZONTAL"] = "horizontal";
})(exports.NbStepperOrientation || (exports.NbStepperOrientation = {}));
/**
 * Stepper component
 *
 * @stacked-example(Showcase, stepper/stepper-showcase.component)
 *
 * ### Installation
 *
 * Import `NbStepperModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbStepperModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * If step label is string you can pass it as `label` attribute. Otherwise ng-template should be used:
 * ```html
 * // ...
 * <nb-stepper orientation="horizontal">
 *   <nb-step label="step number one">
 *       // ... step content here
 *   <nb-step>
 *   <nb-step label="stepLabel">
 *       <ng-template #stepLabel>
 *           <div>
 *               step number two
 *           </div>
 *       </ng-template>
 *       // ... step content here
 *   <nb-step>
 * </nb-stepper>
 * ```
 * Specify `[stepControl]="form"` and user can navigates only if submit previous step's form.
 * ```html
 * // ...
 * <nb-stepper  orientation="horizontal">
 *   <nb-step label="step number one" [stepControl]="form">
 *     <form [formGroup]="form">
 *       // ...
 *     </form>
 *   <nb-step>
 *    // ...
 * </nb-stepper>
 * ```
 *
 * @stacked-example(Validation, stepper/stepper-validation.component)
 *
 * Stepper component has two layout options - `vertical` & `horizontal`
 * @stacked-example(Vertical, stepper/stepper-vertical.component)
 *
 * @styles
 *
 * stepper-index-size:
 * stepper-label-font-size:
 * stepper-label-font-weight:
 * stepper-accent-color:
 * stepper-completed-fg:
 * stepper-fg:
 * stepper-completed-icon-size:
 * stepper-completed-icon-weight:
 */
var NbStepperComponent = /** @class */ (function () {
    function NbStepperComponent() {
        /**
           * Stepper orientation - `horizontal`|`vertical`
           * @type {string}
           */
        this.orientation = exports.NbStepperOrientation.HORIZONTAL;
        this.index = 0;
    }
    Object.defineProperty(NbStepperComponent.prototype, "vertical", {
        get: function () {
            return this.orientation === exports.NbStepperOrientation.VERTICAL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbStepperComponent.prototype, "horizontal", {
        get: function () {
            return this.orientation === exports.NbStepperOrientation.HORIZONTAL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbStepperComponent.prototype, "selectedIndex", {
        get: /**
           * Selected step index
           *
           * @type {boolean}
           */
        function () {
            return this.index;
        },
        set: function (index) {
            if (this.steps) {
                if (this.index !== index && this.isStepValid(index)) {
                    this.index = index;
                }
            }
            else {
                this.index = index;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbStepperComponent.prototype, "selected", {
        get: /**
           * Selected step component
           *
           * @type {boolean}
           */
        function () {
            return this.steps ? this.steps.toArray()[this.selectedIndex] : undefined;
        },
        set: function (step) {
            this.selectedIndex = this.steps ? this.steps.toArray().indexOf(step) : -1;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Navigate to next step
     * */
    /**
       * Navigate to next step
       * */
    NbStepperComponent.prototype.next = /**
       * Navigate to next step
       * */
    function () {
        this.selectedIndex = Math.min(this.index + 1, this.steps.length - 1);
    };
    /**
     * Navigate to previous step
     * */
    /**
       * Navigate to previous step
       * */
    NbStepperComponent.prototype.previous = /**
       * Navigate to previous step
       * */
    function () {
        this.selectedIndex = Math.max(this.index - 1, 0);
    };
    /**
     * Reset stepper and stepControls to initial state
     * */
    /**
       * Reset stepper and stepControls to initial state
       * */
    NbStepperComponent.prototype.reset = /**
       * Reset stepper and stepControls to initial state
       * */
    function () {
        this.selectedIndex = 0;
        this.steps.forEach(function (step) { return step.reset(); });
    };
    NbStepperComponent.prototype.isStepSelected = function (step) {
        return this.index === this.steps.toArray().indexOf(step);
    };
    NbStepperComponent.prototype.isStepValid = function (index) {
        var steps = this.steps.toArray();
        steps[this.index].interacted = true;
        if (index >= this.index && index > 0) {
            var currentStep = steps[this.index];
            return currentStep.completed;
        }
        return true;
    };
    NbStepperComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-stepper',
                    styles: [":host.horizontal .header .step{flex-direction:column}:host.horizontal .header .connector{height:2px}:host.vertical{display:flex;height:100%}:host.vertical .header{flex-direction:column}:host.vertical .header .label{margin:0 10px}:host.vertical .header .connector{width:2px}.header{display:flex;justify-content:space-between;align-items:flex-start;margin-bottom:10px}.header .connector{flex:auto}.header .step{display:flex;align-items:center;cursor:pointer}.header .label-index{margin-bottom:10px;display:flex;justify-content:center;align-items:center}.header .label{width:max-content} "],
                    template: "<ng-template><ng-content select=\"nb-step\"></ng-content></ng-template> <div class=\"header\"> <ng-container *ngFor=\"let step of steps; let index = index; let first = first\"> <div *ngIf=\"!first && !step.hidden\" [class.connector-past]=\"index < selectedIndex\" class=\"connector\"></div> <div *ngIf=\"!step.hidden\" class=\"step\" [class.selected]=\"isStepSelected(step)\" [class.completed]=\"!isStepSelected(step) && step.completed\" (click)=\"step.select()\"> <div class=\"label-index\"> <span *ngIf=\"!step.completed || isStepSelected(step)\">{{ index + 1 }}</span> <i *ngIf=\"!isStepSelected(step) && step.completed\" class=\"icon nb-checkmark\"></i> </div> <div class=\"label\"> <ng-container *ngIf=\"step.isLabelTemplate\"> <ng-container *ngTemplateOutlet=\"step.label\"></ng-container> </ng-container> <span *ngIf=\"!step.isLabelTemplate\">{{ step.label }}</span> </div> </div> </ng-container> </div> <div class=\"step-content\"> <ng-container [ngTemplateOutlet]=\"selected?.content\"></ng-container> </div> ",
                },] },
    ];
    /** @nocollapse */
    NbStepperComponent.propDecorators = {
        "steps": [{ type: i0.ContentChildren, args: [NbStepComponent,] },],
        "vertical": [{ type: i0.HostBinding, args: ['class.vertical',] },],
        "horizontal": [{ type: i0.HostBinding, args: ['class.horizontal',] },],
        "selectedIndex": [{ type: i0.Input },],
        "selected": [{ type: i0.Input },],
        "orientation": [{ type: i0.Input },],
    };
    return NbStepperComponent;
}());

var NbStepperNextDirective = /** @class */ (function () {
    function NbStepperNextDirective(stepper) {
        this.stepper = stepper;
        this.type = 'submit';
    }
    NbStepperNextDirective.prototype.onClick = function () {
        this.stepper.next();
    };
    NbStepperNextDirective.decorators = [
        { type: i0.Directive, args: [{
                    selector: 'button[nbStepperNext]',
                },] },
    ];
    /** @nocollapse */
    NbStepperNextDirective.ctorParameters = function () { return [
        { type: NbStepperComponent, },
    ]; };
    NbStepperNextDirective.propDecorators = {
        "type": [{ type: i0.Input }, { type: i0.HostBinding, args: ['attr.type',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbStepperNextDirective;
}());
var NbStepperPreviousDirective = /** @class */ (function () {
    function NbStepperPreviousDirective(stepper) {
        this.stepper = stepper;
        this.type = 'button';
    }
    NbStepperPreviousDirective.prototype.onClick = function () {
        this.stepper.previous();
    };
    NbStepperPreviousDirective.decorators = [
        { type: i0.Directive, args: [{
                    selector: 'button[nbStepperPrevious]',
                },] },
    ];
    /** @nocollapse */
    NbStepperPreviousDirective.ctorParameters = function () { return [
        { type: NbStepperComponent, },
    ]; };
    NbStepperPreviousDirective.propDecorators = {
        "type": [{ type: i0.Input }, { type: i0.HostBinding, args: ['attr.type',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbStepperPreviousDirective;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbStepperModule = /** @class */ (function () {
    function NbStepperModule() {
    }
    NbStepperModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: [
                        NbStepperComponent,
                        NbStepComponent,
                        NbStepperNextDirective,
                        NbStepperPreviousDirective,
                    ],
                    exports: [
                        NbStepperComponent,
                        NbStepComponent,
                        NbStepperNextDirective,
                        NbStepperPreviousDirective,
                    ],
                },] },
    ];
    return NbStepperModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * An accordion allows to toggle the display of sections of content
 *
 * Basic example
 * @stacked-example(Showcase, accordion/accordion-showcase.component)
 *
 * ```ts
 * <nb-accordion>
 *  <nb-accordion-item>
 *   <nb-accordion-item-header>Product Details</nb-accordion-item-header>
 *   <nb-accordion-item-body>
 *     Item Content
 *   </nb-accordion-item-body>
 *  </nb-accordion-item>
 * </nb-accordion>
 * ```
 * ### Installation
 *
 * Import `NbAccordionModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbAccordionModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * With `multi` mode acordion can have multiple items expanded:
 * @stacked-example(Showcase, accordion/accordion-multi.component)
 *
 * `NbAccordionItemComponent` has several method, for example it is possible to trigger item click/toggle:
 * @stacked-example(Showcase, accordion/accordion-toggle.component)
 *
 * @styles
 *
 * accordion-padding:
 * accordion-separator:
 * accordion-header-font-family:
 * accordion-header-font-size:
 * accordion-header-font-weight:
 * accordion-header-fg-heading:
 * accordion-header-disabled-fg:
 * accordion-header-border-width:
 * accordion-header-border-type:
 * accordion-header-border-color:
 * accordion-item-bg:
 * accordion-item-font-size:
 * accordion-item-font-weight:
 * accordion-item-font-family:
 * accordion-item-fg-text:
 * accordion-item-shadow:
 */
var NbAccordionComponent = /** @class */ (function () {
    function NbAccordionComponent() {
        this.openCloseItems = new rxjs.Subject();
        this.multiValue = false;
    }
    Object.defineProperty(NbAccordionComponent.prototype, "multi", {
        get: /**
           *  Allow multiple items to be expanded at the same time.
           * @type {boolean}
           */
        function () {
            return this.multiValue;
        },
        set: function (val) {
            this.multiValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Opens all enabled accordion items.
     */
    /**
       * Opens all enabled accordion items.
       */
    NbAccordionComponent.prototype.openAll = /**
       * Opens all enabled accordion items.
       */
    function () {
        if (this.multi) {
            this.openCloseItems.next(false);
        }
    };
    /**
     * Closes all enabled accordion items.
     */
    /**
       * Closes all enabled accordion items.
       */
    NbAccordionComponent.prototype.closeAll = /**
       * Closes all enabled accordion items.
       */
    function () {
        this.openCloseItems.next(true);
    };
    NbAccordionComponent.STATUS_ACTIVE = 'active';
    NbAccordionComponent.STATUS_DISABLED = 'disabled';
    NbAccordionComponent.STATUS_PRIMARY = 'primary';
    NbAccordionComponent.STATUS_INFO = 'info';
    NbAccordionComponent.STATUS_SUCCESS = 'success';
    NbAccordionComponent.STATUS_WARNING = 'warning';
    NbAccordionComponent.STATUS_DANGER = 'danger';
    NbAccordionComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-accordion',
                    template: "\n    <ng-content select=\"nb-accordion-item\"></ng-content>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbAccordionComponent.propDecorators = {
        "multi": [{ type: i0.Input, args: ['multi',] },],
    };
    return NbAccordionComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Component intended to be used within `<nb-accordion>` component
 */
var NbAccordionItemComponent = /** @class */ (function () {
    function NbAccordionItemComponent(accordion, cd) {
        this.accordion = accordion;
        this.cd = cd;
        /**
           * Emits whenever the expanded state of the accordion changes.
           * Primarily used to facilitate two-way binding.
           */
        this.collapsedChange = new i0.EventEmitter();
        this.accordionItemInvalidate = new rxjs.Subject();
        this.collapsedValue = true;
        this.disabledValue = false;
        this.alive = true;
    }
    Object.defineProperty(NbAccordionItemComponent.prototype, "collapsed", {
        get: /**
           * Item is collapse (`true` by default)
           * @type {boolean}
           */
        function () {
            return this.collapsedValue;
        },
        set: function (val) {
            this.collapsedValue = convertToBoolProperty(val);
            this.collapsedChange.emit(this.collapsedValue);
            this.invalidate();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemComponent.prototype, "expanded", {
        get: /**
           * Item is expanded (`false` by default)
           * @type {boolean}
           */
        function () {
            return !this.collapsed;
        },
        set: function (val) {
            this.collapsedValue = !convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemComponent.prototype, "disabled", {
        get: /**
           * Item is disabled and cannot be opened.
           * @type {boolean}
           */
        function () {
            return this.disabledValue;
        },
        set: function (val) {
            this.disabledValue = convertToBoolProperty(val);
            this.invalidate();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Open/close the item
     */
    /**
       * Open/close the item
       */
    NbAccordionItemComponent.prototype.toggle = /**
       * Open/close the item
       */
    function () {
        if (!this.disabled) {
            // we need this temporary variable as `openCloseItems.next` will change current value we need to save
            var willSet = !this.collapsed;
            if (!this.accordion.multi) {
                this.accordion.openCloseItems.next(true);
            }
            this.collapsed = willSet;
        }
    };
    /**
     * Open the item.
     */
    /**
       * Open the item.
       */
    NbAccordionItemComponent.prototype.open = /**
       * Open the item.
       */
    function () {
        !this.disabled && (this.collapsed = false);
    };
    /**
     * Collapse the item.
     */
    /**
       * Collapse the item.
       */
    NbAccordionItemComponent.prototype.close = /**
       * Collapse the item.
       */
    function () {
        !this.disabled && (this.collapsed = true);
    };
    NbAccordionItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accordion.openCloseItems
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (collapsed) {
            !_this.disabled && (_this.collapsed = collapsed);
        });
    };
    NbAccordionItemComponent.prototype.ngOnChanges = function (changes) {
        this.accordionItemInvalidate.next(true);
    };
    NbAccordionItemComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
        this.accordionItemInvalidate.complete();
    };
    NbAccordionItemComponent.prototype.invalidate = function () {
        this.accordionItemInvalidate.next(true);
        this.cd.markForCheck();
    };
    NbAccordionItemComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-accordion-item',
                    styles: [":host{display:flex;flex-direction:column} "],
                    template: "\n    <ng-content select=\"nb-accordion-item-header\"></ng-content>\n    <ng-content select=\"nb-accordion-item-body\"></ng-content>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbAccordionItemComponent.ctorParameters = function () { return [
        { type: NbAccordionComponent, decorators: [{ type: i0.Host },] },
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbAccordionItemComponent.propDecorators = {
        "collapsed": [{ type: i0.Input, args: ['collapsed',] }, { type: i0.HostBinding, args: ['class.collapsed',] },],
        "expanded": [{ type: i0.Input, args: ['expanded',] }, { type: i0.HostBinding, args: ['class.expanded',] },],
        "disabled": [{ type: i0.Input, args: ['disabled',] }, { type: i0.HostBinding, args: ['class.disabled',] },],
        "collapsedChange": [{ type: i0.Output },],
    };
    return NbAccordionItemComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var accordionItemBodyTrigger = _angular_animations.trigger('accordionItemBody', [
    _angular_animations.state('collapsed', _angular_animations.style({
        overflow: 'hidden',
        visibility: 'hidden',
        height: 0,
    })),
    _angular_animations.state('expanded', _angular_animations.style({
        overflow: 'hidden',
        visibility: 'visible',
        height: '{{ contentHeight }}',
    }), { params: { contentHeight: '1rem' } }),
    _angular_animations.transition('collapsed => expanded', _angular_animations.animate('100ms ease-in')),
    _angular_animations.transition('expanded => collapsed', _angular_animations.animate('100ms ease-out')),
]);
/**
 * Component intended to be used within `<nb-accordion-item>` component
 */
var NbAccordionItemBodyComponent = /** @class */ (function () {
    function NbAccordionItemBodyComponent(accordionItem, el, cd) {
        this.accordionItem = accordionItem;
        this.el = el;
        this.cd = cd;
        this.alive = true;
    }
    Object.defineProperty(NbAccordionItemBodyComponent.prototype, "state", {
        get: function () {
            return this.accordionItem.collapsed ? 'collapsed' : 'expanded';
        },
        enumerable: true,
        configurable: true
    });
    NbAccordionItemBodyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.contentHeight = this.el.nativeElement.clientHeight + "px";
        this.accordionItem.accordionItemInvalidate
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.cd.markForCheck(); });
    };
    NbAccordionItemBodyComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbAccordionItemBodyComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-accordion-item-body',
                    template: "\n    <div [@accordionItemBody]=\"{ value: state, params: { contentHeight: contentHeight } }\">\n      <div class=\"item-body\">\n        <ng-content></ng-content>\n      </div>\n    </div>\n  ",
                    animations: [accordionItemBodyTrigger],
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbAccordionItemBodyComponent.ctorParameters = function () { return [
        { type: NbAccordionItemComponent, decorators: [{ type: i0.Host },] },
        { type: i0.ElementRef, },
        { type: i0.ChangeDetectorRef, },
    ]; };
    return NbAccordionItemBodyComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Component intended to be used within `<nb-accordion-item>` component
 */
var NbAccordionItemHeaderComponent = /** @class */ (function () {
    function NbAccordionItemHeaderComponent(accordionItem, cd) {
        this.accordionItem = accordionItem;
        this.cd = cd;
        this.alive = true;
    }
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "isCollapsed", {
        get: function () {
            return this.accordionItem.collapsed;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "expanded", {
        get: function () {
            return !this.accordionItem.collapsed;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "tabbable", {
        get: function () {
            return this.accordionItem.disabled ? '-1' : '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "disabled", {
        get: function () {
            return this.accordionItem.disabled;
        },
        enumerable: true,
        configurable: true
    });
    NbAccordionItemHeaderComponent.prototype.toggle = function () {
        this.accordionItem.toggle();
    };
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "state", {
        get: function () {
            if (this.isCollapsed) {
                return 'collapsed';
            }
            if (this.expanded) {
                return 'expanded';
            }
        },
        enumerable: true,
        configurable: true
    });
    NbAccordionItemHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accordionItem.accordionItemInvalidate
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.cd.markForCheck(); });
    };
    NbAccordionItemHeaderComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbAccordionItemHeaderComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-accordion-item-header',
                    styles: [":host{display:flex;align-items:center;cursor:pointer}:host:focus{outline:0} "],
                    template: "\n    <ng-content select=\"nb-accordion-item-title\"></ng-content>\n    <ng-content select=\"nb-accordion-item-description\"></ng-content>\n    <ng-content></ng-content>\n    <i [@expansionIndicator]=\"state\" *ngIf=\"!disabled\" class=\"nb-arrow-down\"></i>\n  ",
                    animations: [
                        _angular_animations.trigger('expansionIndicator', [
                            _angular_animations.state('expanded', _angular_animations.style({
                                transform: 'rotate(180deg)',
                            })),
                            _angular_animations.transition('collapsed => expanded', _angular_animations.animate('100ms ease-in')),
                            _angular_animations.transition('expanded => collapsed', _angular_animations.animate('100ms ease-out')),
                        ]),
                    ],
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbAccordionItemHeaderComponent.ctorParameters = function () { return [
        { type: NbAccordionItemComponent, decorators: [{ type: i0.Host },] },
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbAccordionItemHeaderComponent.propDecorators = {
        "isCollapsed": [{ type: i0.HostBinding, args: ['class.accordion-item-header-collapsed',] },],
        "expanded": [{ type: i0.HostBinding, args: ['class.accordion-item-header-expanded',] }, { type: i0.HostBinding, args: ['attr.aria-expanded',] },],
        "tabbable": [{ type: i0.HostBinding, args: ['attr.tabindex',] },],
        "disabled": [{ type: i0.HostBinding, args: ['attr.aria-disabled',] },],
        "toggle": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbAccordionItemHeaderComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_ACCORDION_COMPONENTS = [
    NbAccordionComponent,
    NbAccordionItemComponent,
    NbAccordionItemHeaderComponent,
    NbAccordionItemBodyComponent,
];
var NbAccordionModule = /** @class */ (function () {
    function NbAccordionModule() {
    }
    NbAccordionModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [i1.CommonModule],
                    exports: NB_ACCORDION_COMPONENTS.slice(),
                    declarations: NB_ACCORDION_COMPONENTS.slice(),
                    providers: [],
                },] },
    ];
    return NbAccordionModule;
}());

/**
 * List is a container component that wraps `nb-list-item` component.
 *
 * Basic example:
 * @stacked-example(Simple list, list/simple-list-showcase.component)
 *
 * `nb-list-item` accepts arbitrary content, so you can create a list of any components.
 *
 * ### Installation
 *
 * Import `NbListModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbListModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * List of users:
 * @stacked-example(Users list, list/users-list-showcase.component)
 *
 * @styles
 *
 * list-item-border-color:
 * list-item-padding:
 */
var NbListComponent = /** @class */ (function () {
    function NbListComponent() {
        /**
           * Role attribute value
           *
           * @type {string}
           */
        this.role = 'list';
    }
    NbListComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-list',
                    template: "<ng-content select=\"nb-list-item\"></ng-content>",
                    styles: [":host{display:flex;flex-direction:column;flex:1 1 auto;overflow:auto} "],
                },] },
    ];
    /** @nocollapse */
    NbListComponent.propDecorators = {
        "role": [{ type: i0.Input }, { type: i0.HostBinding, args: ['attr.role',] },],
    };
    return NbListComponent;
}());
/**
 * List item component is a grouping component that accepts arbitrary content.
 * It should be direct child of `nb-list` componet.
 */
var NbListItemComponent = /** @class */ (function () {
    function NbListItemComponent() {
        /**
           * Role attribute value
           *
           * @type {string}
           */
        this.role = 'listitem';
    }
    NbListItemComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-list-item',
                    template: "<ng-content></ng-content>",
                    styles: [":host{flex-shrink:0} "],
                },] },
    ];
    /** @nocollapse */
    NbListItemComponent.propDecorators = {
        "role": [{ type: i0.Input }, { type: i0.HostBinding, args: ['attr.role',] },],
    };
    return NbListItemComponent;
}());

/**
 * List pager directive
 *
 * Directive allows you to determine page of currently viewing items.
 *
 */
var NbListPageTrackerDirective = /** @class */ (function () {
    function NbListPageTrackerDirective() {
        var _this = this;
        this.alive = true;
        /**
           * Page to start counting with.
           */
        this.startPage = 1;
        /**
           * Emits when another page become visible.
           */
        this.pageChange = new i0.EventEmitter();
        this.observer = new IntersectionObserver(function (entries) { return _this.checkForPageChange(entries); }, { threshold: 0.5 });
    }
    NbListPageTrackerDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (this.listItems && this.listItems.length) {
            this.observeItems();
        }
        this.listItems.changes
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.observeItems(); });
    };
    NbListPageTrackerDirective.prototype.ngOnDestroy = function () {
        this.observer.disconnect && this.observer.disconnect();
    };
    NbListPageTrackerDirective.prototype.observeItems = function () {
        var _this = this;
        this.listItems.forEach(function (i) { return _this.observer.observe(i.nativeElement); });
    };
    NbListPageTrackerDirective.prototype.checkForPageChange = function (entries) {
        var mostVisiblePage = this.findMostVisiblePage(entries);
        if (mostVisiblePage && this.currentPage !== mostVisiblePage) {
            this.currentPage = mostVisiblePage;
            this.pageChange.emit(this.currentPage);
        }
    };
    NbListPageTrackerDirective.prototype.findMostVisiblePage = function (entries) {
        var intersectionRatioByPage = new Map();
        for (var _i = 0, entries_1 = entries; _i < entries_1.length; _i++) {
            var entry = entries_1[_i];
            if (entry.intersectionRatio < 0.5) {
                continue;
            }
            var elementIndex = this.elementIndex(entry.target);
            if (elementIndex === -1) {
                continue;
            }
            var page = this.startPage + Math.floor(elementIndex / this.pageSize);
            var ratio = entry.intersectionRatio;
            if (intersectionRatioByPage.has(page)) {
                ratio += intersectionRatioByPage.get(page);
            }
            intersectionRatioByPage.set(page, ratio);
        }
        var maxRatio = 0;
        var mostVisiblePage;
        intersectionRatioByPage.forEach(function (ratio, page) {
            if (ratio > maxRatio) {
                maxRatio = ratio;
                mostVisiblePage = page;
            }
        });
        return mostVisiblePage;
    };
    NbListPageTrackerDirective.prototype.elementIndex = function (element) {
        return element.parentElement && element.parentElement.children
            ? Array.from(element.parentElement.children).indexOf(element)
            : -1;
    };
    NbListPageTrackerDirective.decorators = [
        { type: i0.Directive, args: [{
                    selector: '[nbListPageTracker]',
                },] },
    ];
    /** @nocollapse */
    NbListPageTrackerDirective.ctorParameters = function () { return []; };
    NbListPageTrackerDirective.propDecorators = {
        "pageSize": [{ type: i0.Input },],
        "startPage": [{ type: i0.Input },],
        "pageChange": [{ type: i0.Output },],
        "listItems": [{ type: i0.ContentChildren, args: [NbListItemComponent, { read: i0.ElementRef },] },],
    };
    return NbListPageTrackerDirective;
}());

var NbScrollableContainerDimentions = /** @class */ (function () {
    function NbScrollableContainerDimentions() {
    }
    return NbScrollableContainerDimentions;
}());
/**
 * Infinite List Directive
 *
 * ```html
 *  <nb-list nbInfiniteList [threshold]="500" (bottomThreshold)="loadNext()">
 *    <nb-list-item *ngFor="let item of items"></nb-list-item>
 *  </nb-list>
 * ```
 *
 * @stacked-example(Simple infinite list, infinite-list/infinite-list-showcase.component)
 *
 * Directive will notify when list scrolled up or down to given a threshold.
 * By default it listen to scroll of list on which applied, but also can be set to listen to window scroll.
 *
 * @stacked-example(Scroll modes, infinite-list/infinite-list-scroll-modes.component)
 *
 * To improve UX of infinite lists, it's better to keep current page in url,
 * so user able to return to the last viewed page or to share a link to this page.
 * `nbListPageTracker` directive will help you to know, what page user currently viewing.
 * Just put it on a list, set page size and it will calculate page that currently in viewport.
 * You can [open the example](example/infinite-list/infinite-news-list.component)
 * in a new tab to check out this feature.
 *
 * @stacked-example(Infinite list with pager, infinite-list/infinite-news-list.component)
 *
 * @stacked-example(Infinite list with placeholders at the top, infinite-list/infinite-list-placeholders.component)
 *
 */
var NbInfiniteListDirective = /** @class */ (function () {
    function NbInfiniteListDirective(elementRef, scrollService, dimensionsService) {
        this.elementRef = elementRef;
        this.scrollService = scrollService;
        this.dimensionsService = dimensionsService;
        this.alive = true;
        this.windowScroll = false;
        /**
           * Emits when distance between list bottom and current scroll position is less than threshold.
           */
        this.bottomThreshold = new i0.EventEmitter(true);
        /**
           * Emits when distance between list top and current scroll position is less than threshold.
           */
        this.topThreshold = new i0.EventEmitter(true);
    }
    Object.defineProperty(NbInfiniteListDirective.prototype, "elementScroll", {
        get: function () {
            return !this.windowScroll;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInfiniteListDirective.prototype, "listenWindowScroll", {
        set: /**
           * By default component observes list scroll position.
           * If set to `true`, component will observe position of page scroll instead.
           */
        function (value) {
            this.windowScroll = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    NbInfiniteListDirective.prototype.onElementScroll = function () {
        if (this.elementScroll) {
            this.checkPosition(this.elementRef.nativeElement);
        }
    };
    NbInfiniteListDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.scrollService.onScroll()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), rxjs_operators.filter(function () { return _this.windowScroll; }), rxjs_operators.switchMap(function () { return _this.getContainerDimensions(); }))
            .subscribe(function (dimentions) { return _this.checkPosition(dimentions); });
        this.listItems.changes
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }), 
        // For some reason, changes are emitted before list item removed from dom,
        // so dimensions will be incorrect.
        // Check every 50ms for a second if dom and query are in sync.
        // Once they synchronized, we can get proper dimensions.
        rxjs_operators.switchMap(function () {
            return rxjs.interval(50).pipe(rxjs_operators.takeUntil(rxjs.timer(1000)), rxjs_operators.filter(function () { return _this.inSyncWithDom(); }), rxjs_operators.take(1));
        }), rxjs_operators.switchMap(function () { return _this.getContainerDimensions(); }))
            .subscribe(function (dimentions) { return _this.checkPosition(dimentions); });
        this.getContainerDimensions().subscribe(function (dimentions) { return _this.checkPosition(dimentions); });
    };
    NbInfiniteListDirective.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbInfiniteListDirective.prototype.checkPosition = function (_a) {
        var scrollHeight = _a.scrollHeight, scrollTop = _a.scrollTop, clientHeight = _a.clientHeight;
        var initialCheck = this.lastScrollPosition == null;
        var manualCheck = this.lastScrollPosition === scrollTop;
        var scrollUp = scrollTop < this.lastScrollPosition;
        var scrollDown = scrollTop > this.lastScrollPosition;
        var distanceToBottom = scrollHeight - scrollTop - clientHeight;
        if ((initialCheck || manualCheck || scrollDown) && distanceToBottom <= this.threshold) {
            this.bottomThreshold.emit();
        }
        if ((initialCheck || scrollUp) && scrollTop <= this.threshold) {
            this.topThreshold.emit();
        }
        this.lastScrollPosition = scrollTop;
    };
    NbInfiniteListDirective.prototype.getContainerDimensions = function () {
        if (this.elementScroll) {
            var _a = this.elementRef.nativeElement, scrollTop = _a.scrollTop, scrollHeight = _a.scrollHeight, clientHeight = _a.clientHeight;
            return rxjs.of({ scrollTop: scrollTop, scrollHeight: scrollHeight, clientHeight: clientHeight });
        }
        return rxjs.forkJoin(this.scrollService.getPosition(), this.dimensionsService.getDimensions())
            .pipe(rxjs_operators.map(function (_a) {
            var scrollPosition = _a[0], dimensions = _a[1];
            return ({
                scrollTop: scrollPosition.y,
                scrollHeight: dimensions.scrollHeight,
                clientHeight: dimensions.clientHeight,
            });
        }));
    };
    NbInfiniteListDirective.prototype.inSyncWithDom = function () {
        return this.elementRef.nativeElement.children.length === this.listItems.length;
    };
    NbInfiniteListDirective.decorators = [
        { type: i0.Directive, args: [{
                    selector: '[nbInfiniteList]',
                },] },
    ];
    /** @nocollapse */
    NbInfiniteListDirective.ctorParameters = function () { return [
        { type: i0.ElementRef, },
        { type: NbLayoutScrollService, },
        { type: NbLayoutRulerService, },
    ]; };
    NbInfiniteListDirective.propDecorators = {
        "threshold": [{ type: i0.Input },],
        "listenWindowScroll": [{ type: i0.Input },],
        "bottomThreshold": [{ type: i0.Output },],
        "topThreshold": [{ type: i0.Output },],
        "onElementScroll": [{ type: i0.HostListener, args: ['scroll',] },],
        "listItems": [{ type: i0.ContentChildren, args: [NbListItemComponent,] },],
    };
    return NbInfiniteListDirective;
}());

var components = [
    NbListComponent,
    NbListItemComponent,
    NbListPageTrackerDirective,
    NbInfiniteListDirective,
];
var NbListModule = /** @class */ (function () {
    function NbListModule() {
    }
    NbListModule.decorators = [
        { type: i0.NgModule, args: [{
                    declarations: components,
                    exports: components,
                },] },
    ];
    return NbListModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Basic input directive.
 *
 * ```html
 * <input nbInput></input>
 * ```
 *
 * ### Installation
 *
 * Import `NbInputModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbInputModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Default input size is `medium`:
 * @stacked-example(Showcase, input/input-showcase.component)
 *
 * Inputs are available in multiple colors using `status` property:
 * @stacked-example(Input Colors, input/input-colors.component)
 *
 * There are three input sizes:
 *
 * @stacked-example(Input Sizes, input/input-sizes.component)
 *
 * Inputs available in different shapes, which could be combined with the other properties:
 * @stacked-example(Input Shapes, input/input-shapes.component)
 *
 * `nbInput` could be applied to the following selectors - `input`, `textarea`:
 * @stacked-example(Input Elements, input/input-types.component)
 *
 * You can add `fullWidth` attribute to make element fill container:
 * @stacked-example(Full width inputs, input/input-full-width.component)
 *
 * @styles
 *
 * form-control-bg:
 * form-control-border-width:
 * form-control-border-type:
 * form-control-border-color:
 * form-control-text-primary-color:
 * form-control-focus-bg:
 * form-control-selected-border-color:
 * form-control-placeholder-font-size:
 * form-control-placeholder-color:
 * form-control-font-size:
 * form-control-padding:
 * form-control-font-size-sm:
 * form-control-padding-sm:
 * form-control-font-size-lg:
 * form-control-padding-lg:
 * form-control-border-radius:
 * form-control-semi-round-border-radius:
 * form-control-round-border-radius:
 * form-control-info-border-color:
 * form-control-success-border-color:
 * form-control-warning-border-color:
 * form-control-danger-border-color:
 */
var NbInputDirective = /** @class */ (function () {
    function NbInputDirective() {
        this.size = NbInputDirective.SIZE_MEDIUM;
        /**
           * Field shapes: `rectangle`, `round`, `semi-round`
           * @param {string} val
           */
        this.shape = NbInputDirective.SHAPE_RECTANGLE;
        this.fullWidth = false;
    }
    Object.defineProperty(NbInputDirective.prototype, "setSize", {
        set: /**
           * Field size, available sizes:
           * `small`, `medium`, `large`
           * @param {string} val
           */
        function (value) {
            this.size = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "setFullWidth", {
        set: /**
           * If set element will fill container
           * @param {string}
           */
        function (value) {
            this.fullWidth = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "small", {
        get: function () {
            return this.size === NbInputDirective.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "medium", {
        get: function () {
            return this.size === NbInputDirective.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "large", {
        get: function () {
            return this.size === NbInputDirective.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "info", {
        get: function () {
            return this.status === NbInputDirective.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "success", {
        get: function () {
            return this.status === NbInputDirective.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "warning", {
        get: function () {
            return this.status === NbInputDirective.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "danger", {
        get: function () {
            return this.status === NbInputDirective.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "rectangle", {
        get: function () {
            return this.shape === NbInputDirective.SHAPE_RECTANGLE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "semiRound", {
        get: function () {
            return this.shape === NbInputDirective.SHAPE_SEMI_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "round", {
        get: function () {
            return this.shape === NbInputDirective.SHAPE_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    NbInputDirective.SIZE_SMALL = 'small';
    NbInputDirective.SIZE_MEDIUM = 'medium';
    NbInputDirective.SIZE_LARGE = 'large';
    NbInputDirective.STATUS_INFO = 'info';
    NbInputDirective.STATUS_SUCCESS = 'success';
    NbInputDirective.STATUS_WARNING = 'warning';
    NbInputDirective.STATUS_DANGER = 'danger';
    NbInputDirective.SHAPE_RECTANGLE = 'rectangle';
    NbInputDirective.SHAPE_SEMI_ROUND = 'semi-round';
    NbInputDirective.SHAPE_ROUND = 'round';
    NbInputDirective.decorators = [
        { type: i0.Directive, args: [{
                    selector: 'input[nbInput],textarea[nbInput]',
                },] },
    ];
    /** @nocollapse */
    NbInputDirective.propDecorators = {
        "setSize": [{ type: i0.Input, args: ['fieldSize',] },],
        "status": [{ type: i0.Input, args: ['status',] },],
        "shape": [{ type: i0.Input, args: ['shape',] },],
        "setFullWidth": [{ type: i0.Input, args: ['fullWidth',] },],
        "fullWidth": [{ type: i0.HostBinding, args: ['class.input-full-width',] },],
        "small": [{ type: i0.HostBinding, args: ['class.input-sm',] },],
        "medium": [{ type: i0.HostBinding, args: ['class.input-md',] },],
        "large": [{ type: i0.HostBinding, args: ['class.input-lg',] },],
        "info": [{ type: i0.HostBinding, args: ['class.input-info',] },],
        "success": [{ type: i0.HostBinding, args: ['class.input-success',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.input-warning',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.input-danger',] },],
        "rectangle": [{ type: i0.HostBinding, args: ['class.input-rectangle',] },],
        "semiRound": [{ type: i0.HostBinding, args: ['class.input-semi-round',] },],
        "round": [{ type: i0.HostBinding, args: ['class.input-round',] },],
    };
    return NbInputDirective;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_INPUT_COMPONENTS = [
    NbInputDirective,
];
var NbInputModule = /** @class */ (function () {
    function NbInputModule() {
    }
    NbInputModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbSharedModule],
                    declarations: NB_INPUT_COMPONENTS,
                    exports: NB_INPUT_COMPONENTS,
                },] },
    ];
    return NbInputModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_DIALOG_CONFIG = new i0.InjectionToken('Default dialog options');
/**
 * Describes all available options that may be passed to the NbDialogService.
 * */
var NbDialogConfig = /** @class */ (function () {
    function NbDialogConfig(config) {
        /**
           * If true than overlay will render backdrop under a dialog.
           * */
        this.hasBackdrop = true;
        /**
           * Class that'll be assigned to the backdrop element.
           * */
        this.backdropClass = 'overlay-backdrop';
        /**
           * If true then mouse clicks by backdrop will close a dialog.
           * */
        this.closeOnBackdropClick = true;
        /**
           * If true then escape press will close a dialog.
           * */
        this.closeOnEsc = true;
        /**
           * Disables scroll on content under dialog if true and does nothing otherwise.
           * */
        this.hasScroll = false;
        /**
           * Focuses dialog automatically after open if true.
           * */
        this.autoFocus = true;
        Object.assign(this, config);
    }
    return NbDialogConfig;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbDialogRef` helps to manipulate dialog after it was created.
 * The dialog can be dismissed by using `close` method of the dialogRef.
 * You can access rendered component as `content` property of the dialogRef.
 * `onBackdropClick` streams click events on the backdrop of the dialog.
 * */
var NbDialogRef = /** @class */ (function () {
    function NbDialogRef(overlayRef) {
        this.overlayRef = overlayRef;
        this.onClose$ = new rxjs.Subject();
        this.onClose = this.onClose$.asObservable();
        this.onBackdropClick = this.overlayRef.backdropClick();
    }
    /**
     * Hides dialog.
     * */
    /**
       * Hides dialog.
       * */
    NbDialogRef.prototype.close = /**
       * Hides dialog.
       * */
    function (res) {
        this.overlayRef.detach();
        this.overlayRef.dispose();
        this.onClose$.next(res);
        this.onClose$.complete();
    };
    return NbDialogRef;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Container component for each dialog.
 * All the dialogs will be attached to it.
 * // TODO add animations
 * */
var NbDialogContainerComponent = /** @class */ (function () {
    function NbDialogContainerComponent(config, elementRef, focusTrapFactory) {
        this.config = config;
        this.elementRef = elementRef;
        this.focusTrapFactory = focusTrapFactory;
    }
    NbDialogContainerComponent.prototype.ngOnInit = function () {
        if (this.config.autoFocus) {
            this.focusTrap = this.focusTrapFactory.create(this.elementRef.nativeElement);
            this.focusTrap.blurPreviouslyFocusedElement();
            this.focusTrap.focusInitialElement();
        }
    };
    NbDialogContainerComponent.prototype.ngOnDestroy = function () {
        if (this.config.autoFocus && this.focusTrap) {
            this.focusTrap.restoreFocus();
        }
    };
    NbDialogContainerComponent.prototype.attachComponentPortal = function (portal) {
        return this.portalOutlet.attachComponentPortal(portal);
    };
    NbDialogContainerComponent.prototype.attachTemplatePortal = function (portal) {
        return this.portalOutlet.attachTemplatePortal(portal);
    };
    NbDialogContainerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-dialog-container',
                    template: '<ng-template nbPortalOutlet></ng-template>',
                },] },
    ];
    /** @nocollapse */
    NbDialogContainerComponent.ctorParameters = function () { return [
        { type: NbDialogConfig, },
        { type: i0.ElementRef, },
        { type: NbFocusTrapFactoryService, },
    ]; };
    NbDialogContainerComponent.propDecorators = {
        "portalOutlet": [{ type: i0.ViewChild, args: [NbPortalOutletDirective,] },],
    };
    return NbDialogContainerComponent;
}());

var __assign$2 = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbDialogService` helps to open dialogs.
 *
 * @stacked-example(Showcase, dialog/dialog-showcase.component)
 *
 * A new dialog is opened by calling the `open` method with a component to be loaded and an optional configuration.
 * `open` method will return `NbDialogRef` that can be used for the further manipulations.
 *
 * ### Installation
 *
 * Import `NbDialogModule.forRoot()` to your app module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbDialogModule.forRoot(config),
 *   ],
 * })
 * export class AppModule { }
 * ```
 *
 * If you are using it in a lazy loaded module than you have to install it with `NbDialogModule.forChild()`:
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbDialogModule.forChild(config),
 *   ],
 * })
 * export class LazyLoadedModule { }
 * ```
 *
 * ### Usage
 *
 * ```ts
 * const dialogRef = this.dialogService.open(MyDialogComponent, { ... });
 * ```
 *
 * `NbDialogRef` gives capability access reference to the rendered dialog component,
 * destroy dialog and some other options described below.
 *
 * Also, you can inject `NbDialogRef` in dialog component.
 *
 * ```ts
 * this.dialogService.open(MyDialogComponent, { ... });
 *
 * // my-dialog.component.ts
 * constructor(protected dialogRef: NbDialogRef) {
 * }
 *
 * close() {
 *   this.dialogRef.close();
 * }
 * ```
 *
 * Instead of component you can create dialog from TemplateRef:
 *
 * @stacked-example(Template ref, dialog/dialog-template.component)
 *
 * The dialog may return result through `NbDialogRef`. Calling component can receive this result with `onClose`
 * stream of `NbDialogRef`.
 *
 * @stacked-example(Result, dialog/dialog-result.component)
 *
 * ### Configuration
 *
 * As we mentioned above, `open` method of the `NbDialogService` may receive optional configuration options.
 * Also, you can provide global dialogs configuration through `NbDialogModule.forRoot({ ... })`.
 *
 * This config may contain the following:
 *
 * `context` - both, template and component may receive data through `config.context` property.
 * For components, this data will be assigned through inputs.
 * For templates, you can access it inside template as $implicit.
 *
 * ```ts
 * this.dialogService.open(template, { context: 'pass data in template' });
 * ```
 *
 * ```html
 * <ng-template let-some-additional-data>
 *   {{ some-additional-data }}
 * <ng-template/>
 * ```
 *
 * `hasBackdrop` - determines is service have to render backdrop under the dialog.
 * Default is true.
 * @stacked-example(Backdrop, dialog/dialog-has-backdrop.component)
 *
 * `closeOnBackdropClick` - close dialog on backdrop click if true.
 * Default is true.
 * @stacked-example(Backdrop click, dialog/dialog-backdrop-click.component)
 *
 * `closeOnEsc` - close dialog on escape button on the keyboard.
 * Default is true.
 * @stacked-example(Escape hit, dialog/dialog-esc.component)
 *
 * `hasScroll` - Disables scroll on content under dialog if true and does nothing otherwise.
 * Default is false.
 * Please, open dialogs in the separate window and try to scroll.
 * @stacked-example(Scroll, dialog/dialog-scroll.component)
 *
 * `autoFocus` - Focuses dialog automatically after open if true. It's useful to prevent misclicks on
 * trigger elements and opening multiple dialogs.
 * Default is true.
 *
 * As you can see, if you open dialog with auto focus dialog will focus first focusable element
 * or just blur previously focused automatically.
 * Otherwise, without auto focus, the focus will stay on the previously focused element.
 * Please, open dialogs in the separate window and try to click on the button without focus
 * and then hit space any times. Multiple same dialogs will be opened.
 * @stacked-example(Auto focus, dialog/dialog-auto-focus.component)
 * */
var NbDialogService = /** @class */ (function () {
    function NbDialogService(document, globalConfig, positionBuilder, overlay, injector, cfr) {
        this.document = document;
        this.globalConfig = globalConfig;
        this.positionBuilder = positionBuilder;
        this.overlay = overlay;
        this.injector = injector;
        this.cfr = cfr;
    }
    /**
     * Opens new instance of the dialog, may receive optional config.
     * */
    /**
       * Opens new instance of the dialog, may receive optional config.
       * */
    NbDialogService.prototype.open = /**
       * Opens new instance of the dialog, may receive optional config.
       * */
    function (content, userConfig) {
        if (userConfig === void 0) { userConfig = {}; }
        var config = new NbDialogConfig(__assign$2({}, this.globalConfig, userConfig));
        var overlayRef = this.createOverlay(config);
        var dialogRef = new NbDialogRef(overlayRef);
        var container = this.createContainer(config, overlayRef);
        this.createContent(config, content, container, dialogRef);
        this.registerCloseListeners(config, overlayRef, dialogRef);
        return dialogRef;
    };
    NbDialogService.prototype.createOverlay = function (config) {
        var positionStrategy = this.createPositionStrategy();
        var scrollStrategy = this.createScrollStrategy(config.hasScroll);
        return this.overlay.create({
            positionStrategy: positionStrategy,
            scrollStrategy: scrollStrategy,
            hasBackdrop: config.hasBackdrop,
            backdropClass: config.backdropClass,
        });
    };
    NbDialogService.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .global()
            .centerVertically()
            .centerHorizontally();
    };
    NbDialogService.prototype.createScrollStrategy = function (hasScroll) {
        if (hasScroll) {
            return this.overlay.scrollStrategies.noop();
        }
        else {
            return this.overlay.scrollStrategies.block();
        }
    };
    NbDialogService.prototype.createContainer = function (config, overlayRef) {
        var injector = new NbPortalInjector(this.createInjector(config), new WeakMap([[NbDialogConfig, config]]));
        var containerPortal = new NbComponentPortal(NbDialogContainerComponent, null, injector, this.cfr);
        var containerRef = overlayRef.attach(containerPortal);
        return containerRef.instance;
    };
    NbDialogService.prototype.createContent = function (config, content, container, dialogRef) {
        if (content instanceof i0.TemplateRef) {
            var portal = this.createTemplatePortal(config, content, dialogRef);
            container.attachTemplatePortal(portal);
        }
        else {
            var portal = this.createComponentPortal(config, content, dialogRef);
            dialogRef.componentRef = container.attachComponentPortal(portal);
            if (config.context) {
                Object.assign(dialogRef.componentRef.instance, __assign$2({}, config.context));
            }
        }
    };
    NbDialogService.prototype.createTemplatePortal = function (config, content, dialogRef) {
        return new NbTemplatePortal(content, null, { $implicit: config.context, dialogRef: dialogRef });
    };
    /**
     * We're creating portal with custom injector provided through config or using global injector.
     * This approach provides us capability inject `NbDialogRef` in dialog component.
     * */
    /**
       * We're creating portal with custom injector provided through config or using global injector.
       * This approach provides us capability inject `NbDialogRef` in dialog component.
       * */
    NbDialogService.prototype.createComponentPortal = /**
       * We're creating portal with custom injector provided through config or using global injector.
       * This approach provides us capability inject `NbDialogRef` in dialog component.
       * */
    function (config, content, dialogRef) {
        var injector = this.createInjector(config);
        var portalInjector = new NbPortalInjector(injector, new WeakMap([[NbDialogRef, dialogRef]]));
        return new NbComponentPortal(content, config.viewContainerRef, portalInjector);
    };
    NbDialogService.prototype.createInjector = function (config) {
        return config.viewContainerRef && config.viewContainerRef.injector || this.injector;
    };
    NbDialogService.prototype.registerCloseListeners = function (config, overlayRef, dialogRef) {
        if (config.closeOnBackdropClick) {
            overlayRef.backdropClick().subscribe(function () { return dialogRef.close(); });
        }
        if (config.closeOnEsc) {
            rxjs.fromEvent(this.document, 'keyup')
                .pipe(rxjs_operators.filter(function (event) { return event.keyCode === 27; }))
                .subscribe(function () { return dialogRef.close(); });
        }
    };
    NbDialogService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbDialogService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DIALOG_CONFIG,] },] },
        { type: NbPositionBuilderService, },
        { type: NbOverlayService, },
        { type: i0.Injector, },
        { type: i0.ComponentFactoryResolver, },
    ]; };
    return NbDialogService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbDialogModule = /** @class */ (function () {
    function NbDialogModule() {
    }
    NbDialogModule.forRoot = function (dialogConfig) {
        if (dialogConfig === void 0) { dialogConfig = {}; }
        return {
            ngModule: NbDialogModule,
            providers: [
                NbDialogService,
                { provide: NB_DIALOG_CONFIG, useValue: dialogConfig },
            ],
        };
    };
    NbDialogModule.forChild = function (dialogConfig) {
        if (dialogConfig === void 0) { dialogConfig = {}; }
        return {
            ngModule: NbDialogModule,
            providers: [
                NbDialogService,
                { provide: NB_DIALOG_CONFIG, useValue: dialogConfig },
            ],
        };
    };
    NbDialogModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbSharedModule, NbOverlayModule],
                    declarations: [NbDialogContainerComponent],
                    entryComponents: [NbDialogContainerComponent],
                },] },
    ];
    return NbDialogModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbToastStatus;
(function (NbToastStatus) {
    NbToastStatus["SUCCESS"] = "success";
    NbToastStatus["INFO"] = "info";
    NbToastStatus["WARNING"] = "warning";
    NbToastStatus["PRIMARY"] = "primary";
    NbToastStatus["DANGER"] = "danger";
    NbToastStatus["DEFAULT"] = "default";
})(NbToastStatus || (NbToastStatus = {}));

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbToastComponent` is responsible for rendering each toast with appropriate styles.
 *
 * @styles
 *
 * toastr-bg
 * toastr-padding
 * toastr-fg
 * toastr-border
 * toastr-border-radius
 * toastr-border-color
 * */
/**
 * TODO
 * Remove svg icons, include them in font.
 * */
var NbToastComponent = /** @class */ (function () {
    function NbToastComponent() {
        this.destroy = new i0.EventEmitter();
    }
    Object.defineProperty(NbToastComponent.prototype, "success", {
        get: function () {
            return this.toast.config.status === NbToastStatus.SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "info", {
        get: function () {
            return this.toast.config.status === NbToastStatus.INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "warning", {
        get: function () {
            return this.toast.config.status === NbToastStatus.WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "primary", {
        get: function () {
            return this.toast.config.status === NbToastStatus.PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "danger", {
        get: function () {
            return this.toast.config.status === NbToastStatus.DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "default", {
        get: function () {
            return this.toast.config.status === NbToastStatus.DEFAULT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "destroyByClick", {
        get: function () {
            return this.toast.config.destroyByClick;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "hasIcon", {
        get: function () {
            return this.toast.config.hasIcon && this.toast.config.status !== NbToastStatus.DEFAULT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "customIcon", {
        get: function () {
            return !!this.icon;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "icon", {
        get: function () {
            return this.toast.config.icon;
        },
        enumerable: true,
        configurable: true
    });
    NbToastComponent.prototype.onClick = function () {
        this.destroy.emit();
    };
    NbToastComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-toast',
                    styles: [":host{display:flex;align-items:center;width:25rem;margin:0.5rem;opacity:0.9}:host .title{font-weight:800;margin-right:0.25rem}:host>.content-container{line-height:1.25}:host>.content-container>.message{font-weight:300}:host.default .content-container,:host:not(.has-icon) .content-container{display:flex;flex-direction:row}:host.destroy-by-click{cursor:pointer}:host.destroy-by-click:hover{opacity:1}:host .icon{font-size:2.5rem}:host svg{width:2.5rem;height:2.5rem} "],
                    template: "<i class=\"icon {{ icon }}\" *ngIf=\"hasIcon\"></i> <div class=\"content-container\"> <span class=\"title\">{{ toast.title }}</span> <div class=\"message\">{{ toast.message }}</div> </div> ",
                },] },
    ];
    /** @nocollapse */
    NbToastComponent.propDecorators = {
        "toast": [{ type: i0.Input },],
        "destroy": [{ type: i0.Output },],
        "success": [{ type: i0.HostBinding, args: ['class.success',] },],
        "info": [{ type: i0.HostBinding, args: ['class.info',] },],
        "warning": [{ type: i0.HostBinding, args: ['class.warning',] },],
        "primary": [{ type: i0.HostBinding, args: ['class.primary',] },],
        "danger": [{ type: i0.HostBinding, args: ['class.danger',] },],
        "default": [{ type: i0.HostBinding, args: ['class.default',] },],
        "destroyByClick": [{ type: i0.HostBinding, args: ['class.destroy-by-click',] },],
        "hasIcon": [{ type: i0.HostBinding, args: ['class.has-icon',] },],
        "customIcon": [{ type: i0.HostBinding, args: ['class.custom-icon',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbToastComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var voidState = _angular_animations.style({
    transform: 'translateX({{ direction }}110%)',
    height: 0,
    margin: 0,
});
var defaultOptions = { params: { direction: '' } };
var NbToastrContainerComponent = /** @class */ (function () {
    function NbToastrContainerComponent(layoutDirection, positionHelper) {
        this.layoutDirection = layoutDirection;
        this.positionHelper = positionHelper;
        this.content = [];
        this.alive = true;
    }
    NbToastrContainerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.layoutDirection.onDirectionChange()
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.onDirectionChange(); });
    };
    NbToastrContainerComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbToastrContainerComponent.prototype.onDirectionChange = function () {
        var direction = this.positionHelper.isRightPosition(this.position) ? '' : '-';
        this.fadeIn = { value: '', params: { direction: direction } };
    };
    NbToastrContainerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-toastr-container',
                    template: "\n    <nb-toast [@fadeIn]=\"fadeIn\" *ngFor=\"let toast of content\" [toast]=\"toast\"></nb-toast>",
                    animations: [
                        _angular_animations.trigger('fadeIn', [
                            _angular_animations.transition(':enter', [voidState, _angular_animations.animate(100)], defaultOptions),
                            _angular_animations.transition(':leave', [_angular_animations.animate(100, voidState)], defaultOptions),
                        ]),
                    ],
                },] },
    ];
    /** @nocollapse */
    NbToastrContainerComponent.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
        { type: NbPositionHelper, },
    ]; };
    NbToastrContainerComponent.propDecorators = {
        "content": [{ type: i0.Input },],
        "context": [{ type: i0.Input },],
        "position": [{ type: i0.Input },],
        "toasts": [{ type: i0.ViewChildren, args: [NbToastComponent,] },],
    };
    return NbToastrContainerComponent;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NB_TOASTR_CONFIG = new i0.InjectionToken('Default toastr options');
/**
 * The `NbToastrConfig` class describes configuration of the `NbToastrService.show` and global toastr configuration.
 * */
var NbToastrConfig = /** @class */ (function () {
    function NbToastrConfig(config) {
        /**
           * Determines where on the screen toast have to be rendered.
           * */
        this.position = exports.NbGlobalLogicalPosition.TOP_END;
        /**
           * Status chooses color scheme for the toast.
           * */
        this.status = NbToastStatus.PRIMARY;
        /**
           * Duration is timeout between toast appears and disappears.
           * */
        this.duration = 3000;
        /**
           * Destroy by click means you can hide the toast by clicking it.
           * */
        this.destroyByClick = true;
        /**
           * If preventDuplicates is true then the next toast with the same title and message will not be rendered.
           * */
        this.preventDuplicates = false;
        /**
           * Determines render icon or not.
           * */
        this.hasIcon = true;
        /**
           * Icon class that can be provided to render custom icon.
           * */
        this.icon = 'nb-email';
        /**
           * Toast status icon-class mapping.
           * */
        this.icons = (_a = {},
            _a[NbToastStatus.DANGER] = 'nb-danger',
            _a[NbToastStatus.SUCCESS] = 'nb-checkmark-circle',
            _a[NbToastStatus.INFO] = 'nb-help',
            _a[NbToastStatus.WARNING] = 'nb-alert',
            _a[NbToastStatus.PRIMARY] = 'nb-email',
            _a);
        this.patchIcon(config);
        Object.assign(this, config);
        var _a;
    }
    NbToastrConfig.prototype.patchIcon = function (config) {
        if (!('icon' in config)) {
            config.icon = this.icons[config.status || NbToastStatus.PRIMARY];
        }
    };
    return NbToastrConfig;
}());

var __assign$3 = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbToastContainer = /** @class */ (function () {
    function NbToastContainer(position, containerRef, positionHelper) {
        this.position = position;
        this.containerRef = containerRef;
        this.positionHelper = positionHelper;
        this.toasts = [];
    }
    NbToastContainer.prototype.attach = function (toast) {
        if (toast.config.preventDuplicates && this.isDuplicate(toast)) {
            return;
        }
        var toastComponent = this.attachToast(toast);
        if (toast.config.destroyByClick) {
            this.subscribeOnClick(toastComponent, toast);
        }
        if (toast.config.duration) {
            this.setDestroyTimeout(toast);
        }
        this.prevToast = toast;
    };
    NbToastContainer.prototype.isDuplicate = function (toast) {
        return this.prevToast
            && this.prevToast.message === toast.message
            && this.prevToast.title === toast.title;
    };
    NbToastContainer.prototype.attachToast = function (toast) {
        if (this.positionHelper.isTopPosition(toast.config.position)) {
            return this.attachToTop(toast);
        }
        else {
            return this.attachToBottom(toast);
        }
    };
    NbToastContainer.prototype.attachToTop = function (toast) {
        this.toasts.unshift(toast);
        this.updateContainer();
        return this.containerRef.instance.toasts.first;
    };
    NbToastContainer.prototype.attachToBottom = function (toast) {
        this.toasts.push(toast);
        this.updateContainer();
        return this.containerRef.instance.toasts.last;
    };
    NbToastContainer.prototype.setDestroyTimeout = function (toast) {
        var _this = this;
        setTimeout(function () { return _this.destroy(toast); }, toast.config.duration);
    };
    NbToastContainer.prototype.subscribeOnClick = function (toastComponent, toast) {
        var _this = this;
        toastComponent.destroy.subscribe(function () { return _this.destroy(toast); });
    };
    NbToastContainer.prototype.destroy = function (toast) {
        this.toasts = this.toasts.filter(function (t) { return t !== toast; });
        this.updateContainer();
    };
    NbToastContainer.prototype.updateContainer = function () {
        patch(this.containerRef, { content: this.toasts, position: this.position });
    };
    return NbToastContainer;
}());
var NbToastrContainerRegistry = /** @class */ (function () {
    function NbToastrContainerRegistry(overlay, positionBuilder, positionHelper, cfr) {
        this.overlay = overlay;
        this.positionBuilder = positionBuilder;
        this.positionHelper = positionHelper;
        this.cfr = cfr;
        this.overlays = new Map();
    }
    NbToastrContainerRegistry.prototype.get = function (position) {
        var logicalPosition = this.positionHelper.toLogicalPosition(position);
        if (!this.overlays.has(logicalPosition)) {
            this.instantiateContainer(logicalPosition);
        }
        return this.overlays.get(logicalPosition);
    };
    NbToastrContainerRegistry.prototype.instantiateContainer = function (position) {
        var container = this.createContainer(position);
        this.overlays.set(position, container);
    };
    NbToastrContainerRegistry.prototype.createContainer = function (position) {
        var positionStrategy = this.positionBuilder.global().position(position);
        var ref = this.overlay.create({ positionStrategy: positionStrategy });
        var containerRef = ref.attach(new NbComponentPortal(NbToastrContainerComponent, null, null, this.cfr));
        return new NbToastContainer(position, containerRef, this.positionHelper);
    };
    NbToastrContainerRegistry.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbToastrContainerRegistry.ctorParameters = function () { return [
        { type: NbOverlayService, },
        { type: NbPositionBuilderService, },
        { type: NbPositionHelper, },
        { type: i0.ComponentFactoryResolver, },
    ]; };
    return NbToastrContainerRegistry;
}());
/**
 * The `NbToastrService` provides a capability to build toast notifications.
 *
 * @stacked-example(Showcase, toastr/toastr-showcase.component)
 *
 * `NbToastrService.show(message, title, config)` accepts three params, title and config are optional.
 *
 * ### Installation
 *
 * Import `NbToastrModule.forRoot()` to your app module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbToastrModule.forRoot(config),
 *   ],
 * })
 * export class AppModule { }
 * ```
 *
 * ### Usage
 *
 * Config accepts following options:
 *
 * `position` - determines where on the screen toast will be rendered.
 * Default is `top-end`.
 *
 * @stacked-example(Position, toastr/toastr-positions.component)
 *
 * `status` - coloring and icon of the toast.
 * Default is `primary`
 *
 * @stacked-example(Status, toastr/toastr-statuses.component)
 *
 * `duration` - the time after which the toast will be destroyed.
 * `0` means endless toast, that may be destroyed by click only.
 * Default is 3000 ms.
 *
 * @stacked-example(Duration, toastr/toastr-duration.component)
 *
 * `destroyByClick` - provides a capability to destroy toast by click.
 * Default is true.
 *
 * @stacked-example(Destroy by click, toastr/toastr-destroy-by-click.component)
 *
 * `preventDuplicates` - don't create new toast if it has the same title and the same message with previous one.
 * Default is false.
 *
 * @stacked-example(Prevent duplicates, toastr/toastr-prevent-duplicates.component)
 *
 * `hasIcon` - if true then render toast icon.
 * `icon` - you can pass icon class that will be applied into the toast.
 *
 * @stacked-example(Has icon, toastr/toastr-icon.component)
 * */
var NbToastrService = /** @class */ (function () {
    function NbToastrService(globalConfig, containerRegistry) {
        this.globalConfig = globalConfig;
        this.containerRegistry = containerRegistry;
    }
    /**
     * Shows toast with message, title and user config.
     * */
    /**
       * Shows toast with message, title and user config.
       * */
    NbToastrService.prototype.show = /**
       * Shows toast with message, title and user config.
       * */
    function (message, title, userConfig) {
        var config = new NbToastrConfig(__assign$3({}, this.globalConfig, userConfig));
        var container = this.containerRegistry.get(config.position);
        var toast = { message: message, title: title, config: config };
        container.attach(toast);
    };
    /**
     * Shows success toast with message, title and user config.
     * */
    /**
       * Shows success toast with message, title and user config.
       * */
    NbToastrService.prototype.success = /**
       * Shows success toast with message, title and user config.
       * */
    function (message, title, config) {
        return this.show(message, title, __assign$3({}, config, { status: NbToastStatus.SUCCESS }));
    };
    /**
     * Shows info toast with message, title and user config.
     * */
    /**
       * Shows info toast with message, title and user config.
       * */
    NbToastrService.prototype.info = /**
       * Shows info toast with message, title and user config.
       * */
    function (message, title, config) {
        return this.show(message, title, __assign$3({}, config, { status: NbToastStatus.INFO }));
    };
    /**
     * Shows warning toast with message, title and user config.
     * */
    /**
       * Shows warning toast with message, title and user config.
       * */
    NbToastrService.prototype.warning = /**
       * Shows warning toast with message, title and user config.
       * */
    function (message, title, config) {
        return this.show(message, title, __assign$3({}, config, { status: NbToastStatus.WARNING }));
    };
    /**
     * Shows primary toast with message, title and user config.
     * */
    /**
       * Shows primary toast with message, title and user config.
       * */
    NbToastrService.prototype.primary = /**
       * Shows primary toast with message, title and user config.
       * */
    function (message, title, config) {
        return this.show(message, title, __assign$3({}, config, { status: NbToastStatus.PRIMARY }));
    };
    /**
     * Shows danger toast with message, title and user config.
     * */
    /**
       * Shows danger toast with message, title and user config.
       * */
    NbToastrService.prototype.danger = /**
       * Shows danger toast with message, title and user config.
       * */
    function (message, title, config) {
        return this.show(message, title, __assign$3({}, config, { status: NbToastStatus.DANGER }));
    };
    /**
     * Shows default toast with message, title and user config.
     * */
    /**
       * Shows default toast with message, title and user config.
       * */
    NbToastrService.prototype.default = /**
       * Shows default toast with message, title and user config.
       * */
    function (message, title, config) {
        return this.show(message, title, __assign$3({}, config, { status: NbToastStatus.DEFAULT }));
    };
    NbToastrService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbToastrService.ctorParameters = function () { return [
        { type: NbToastrConfig, decorators: [{ type: i0.Inject, args: [NB_TOASTR_CONFIG,] },] },
        { type: NbToastrContainerRegistry, },
    ]; };
    return NbToastrService;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbToastrModule = /** @class */ (function () {
    function NbToastrModule() {
    }
    NbToastrModule.forRoot = function (toastrConfig) {
        if (toastrConfig === void 0) { toastrConfig = {}; }
        return {
            ngModule: NbToastrModule,
            providers: [
                NbToastrService,
                NbToastrContainerRegistry,
                { provide: NB_TOASTR_CONFIG, useValue: toastrConfig },
            ],
        };
    };
    NbToastrModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbSharedModule, NbOverlayModule],
                    declarations: [NbToastrContainerComponent, NbToastComponent],
                    entryComponents: [NbToastrContainerComponent, NbToastComponent],
                },] },
    ];
    return NbToastrModule;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * Tooltip container.
 * Renders provided tooltip inside.
 *
 * @styles
 *
 * tooltip-bg
 * tooltip-primary-bg
 * tooltip-info-bg
 * tooltip-success-bg
 * tooltip-warning-bg
 * tooltip-danger-bg
 * tooltip-fg
 * tooltip-shadow
 * tooltip-font-size
 *
 */
var NbTooltipComponent = /** @class */ (function () {
    function NbTooltipComponent() {
        /**
           * Popover position relatively host element.
           * */
        this.position = exports.NbPosition.TOP;
        this.context = {};
    }
    Object.defineProperty(NbTooltipComponent.prototype, "binding", {
        get: function () {
            return this.position + " " + this.context.status + "-tooltip";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbTooltipComponent.prototype, "show", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    NbTooltipComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-tooltip',
                    styles: [":host{z-index:10000;border-radius:5px}:host .content{padding:0.5rem 1.25rem;display:flex}:host.right .content{flex-direction:row-reverse}:host .arrow{position:absolute;width:0;height:0}:host .icon{font-size:1.25rem}:host span{line-height:1.25rem}:host .icon+span{margin-left:0.5rem}:host.right .icon+span{margin-right:0.5rem}:host .arrow{border-left:5px solid transparent;border-right:5px solid transparent}:host.bottom .arrow{top:-5px;left:calc(50% - 5px)}:host.left .arrow{right:-7px;top:calc(50% - 2px);transform:rotate(90deg)}:host.top .arrow{bottom:-5px;left:calc(50% - 5px);transform:rotate(180deg)}:host.right .arrow{left:-7px;top:calc(50% - 2px);transform:rotate(270deg)} "],
                    template: "\n    <span class=\"arrow\"></span>\n    <div class=\"content\">\n      <i *ngIf=\"context?.icon\" class=\"icon {{ context?.icon }}\"></i>\n      <span *ngIf=\"content\">{{ content }}</span>\n    </div>\n  ",
                    animations: [
                        _angular_animations.trigger('showTooltip', [
                            _angular_animations.state('in', _angular_animations.style({ opacity: 1 })),
                            _angular_animations.transition('void => *', [
                                _angular_animations.style({ opacity: 0 }),
                                _angular_animations.animate(100),
                            ]),
                            _angular_animations.transition('* => void', [
                                _angular_animations.animate(100, _angular_animations.style({ opacity: 0 })),
                            ]),
                        ]),
                    ],
                },] },
    ];
    /** @nocollapse */
    NbTooltipComponent.propDecorators = {
        "content": [{ type: i0.Input },],
        "position": [{ type: i0.Input },],
        "binding": [{ type: i0.HostBinding, args: ['class',] },],
        "show": [{ type: i0.HostBinding, args: ['@showTooltip',] },],
        "context": [{ type: i0.Input },],
    };
    return NbTooltipComponent;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 *
 * Tooltip directive for small text/icon hints.
 *
 * ### Installation
 *
 * Import `NbTooltipModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbTooltipModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * @stacked-example(Showcase, tooltip/tooltip-showcase.component)
 *
 * Tooltip can accept a hint text and/or an icon:
 * @stacked-example(With Icon, tooltip/tooltip-with-icon.component)
 *
 * Same way as Popover, tooltip can accept placement position with `nbTooltipPlacement` proprety:
 * @stacked-example(Placements, tooltip/tooltip-placements.component)
 *
 * It is also possible to specify tooltip color using `nbTooltipStatus` property:
 * @stacked-example(Colored Tooltips, tooltip/tooltip-colors.component)
 *
 */
var NbTooltipDirective = /** @class */ (function () {
    function NbTooltipDirective(document, hostRef, positionBuilder, overlay) {
        this.document = document;
        this.hostRef = hostRef;
        this.positionBuilder = positionBuilder;
        this.overlay = overlay;
        this.context = {};
        /**
           * Position will be calculated relatively host element based on the position.
           * Can be top, right, bottom, left, start or end.
           */
        this.position = exports.NbPosition.TOP;
        /**
           * Container position will be changes automatically based on this strategy if container can't fit view port.
           * Set this property to any falsy value if you want to disable automatically adjustment.
           * Available values: clockwise, counterclockwise.
           */
        this.adjustment = exports.NbAdjustment.CLOCKWISE;
        this.alive = true;
    }
    Object.defineProperty(NbTooltipDirective.prototype, "icon", {
        set: /**
           *
           * @param {string} icon
           */
        function (icon) {
            this.context = Object.assign(this.context, { icon: icon });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbTooltipDirective.prototype, "status", {
        set: /**
           *
           * @param {string} status
           */
        function (status) {
            this.context = Object.assign(this.context, { status: status });
        },
        enumerable: true,
        configurable: true
    });
    NbTooltipDirective.prototype.ngAfterViewInit = function () {
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({
            positionStrategy: this.positionStrategy,
            scrollStrategy: this.overlay.scrollStrategies.reposition(),
        });
        this.triggerStrategy = this.createTriggerStrategy();
        this.subscribeOnTriggers();
        this.subscribeOnPositionChange();
    };
    NbTooltipDirective.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbTooltipDirective.prototype.show = function () {
        this.container = createContainer(this.ref, NbTooltipComponent, {
            position: this.position,
            content: this.content,
            context: this.context,
        });
    };
    NbTooltipDirective.prototype.hide = function () {
        this.ref.detach();
    };
    NbTooltipDirective.prototype.toggle = function () {
        if (this.ref && this.ref.hasAttached()) {
            this.hide();
        }
        else {
            this.show();
        }
    };
    NbTooltipDirective.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(this.position)
            .adjustment(this.adjustment)
            .offset(8);
    };
    NbTooltipDirective.prototype.createTriggerStrategy = function () {
        var _this = this;
        return new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(exports.NbTrigger.HINT)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.container; })
            .build();
    };
    NbTooltipDirective.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return patch(_this.container, { position: position }); });
    };
    NbTooltipDirective.prototype.subscribeOnTriggers = function () {
        var _this = this;
        this.triggerStrategy.show$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.show(); });
        this.triggerStrategy.hide$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.hide(); });
    };
    NbTooltipDirective.decorators = [
        { type: i0.Directive, args: [{ selector: '[nbTooltip]' },] },
    ];
    /** @nocollapse */
    NbTooltipDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: i0.ElementRef, },
        { type: NbPositionBuilderService, },
        { type: NbOverlayService, },
    ]; };
    NbTooltipDirective.propDecorators = {
        "content": [{ type: i0.Input, args: ['nbTooltip',] },],
        "position": [{ type: i0.Input, args: ['nbTooltipPlacement',] },],
        "adjustment": [{ type: i0.Input, args: ['nbTooltipAdjustment',] },],
        "icon": [{ type: i0.Input, args: ['nbTooltipIcon',] },],
        "status": [{ type: i0.Input, args: ['nbTooltipStatus',] },],
    };
    return NbTooltipDirective;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbTooltipModule = /** @class */ (function () {
    function NbTooltipModule() {
    }
    NbTooltipModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbSharedModule, NbOverlayModule],
                    declarations: [NbTooltipComponent, NbTooltipDirective],
                    exports: [NbTooltipDirective],
                    entryComponents: [NbTooltipComponent],
                },] },
    ];
    return NbTooltipModule;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbOptionComponent = /** @class */ (function () {
    function NbOptionComponent(parent, elementRef, cd) {
        this.parent = parent;
        this.elementRef = elementRef;
        this.cd = cd;
        /**
           * Fires value on click.
           * */
        this.selectionChange = new i0.EventEmitter();
        this.selected = false;
        this.disabled = false;
    }
    Object.defineProperty(NbOptionComponent.prototype, "setDisabled", {
        set: function (disabled) {
            this.disabled = convertToBoolProperty(disabled);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "withCheckbox", {
        /**
         * Determines should we render checkbox.
         * */
        get: /**
           * Determines should we render checkbox.
           * */
        function () {
            return this.multiple && !!this.value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "content", {
        get: function () {
            return this.elementRef.nativeElement.textContent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "multiple", {
        get: function () {
            return this.parent.multiple;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "selectedClass", {
        get: function () {
            return this.selected;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "disabledClass", {
        get: function () {
            return this.disabled;
        },
        enumerable: true,
        configurable: true
    });
    NbOptionComponent.prototype.onClick = function () {
        this.selectionChange.emit(this);
    };
    NbOptionComponent.prototype.select = function () {
        this.selected = true;
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NbOptionComponent.prototype.deselect = function () {
        this.selected = false;
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NbOptionComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-option',
                    styles: ["/*! * @license * Copyright Akveo. All Rights Reserved. * Licensed under the MIT License. See License.txt in the project root for license information. */:host{display:block}:host.disabled{pointer-events:none}:host:hover{cursor:pointer}:host nb-checkbox{pointer-events:none} "],
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    template: "\n    <nb-checkbox *ngIf=\"withCheckbox\" [(ngModel)]=\"selected\">\n      <ng-container *ngTemplateOutlet=\"content\"></ng-container>\n    </nb-checkbox>\n\n    <ng-container *ngIf=\"!withCheckbox\">\n      <ng-container *ngTemplateOutlet=\"content\"></ng-container>\n    </ng-container>\n\n    <ng-template #content>\n      <ng-content></ng-content>\n    </ng-template>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbOptionComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [i0.forwardRef(function () { return NbSelectComponent; }),] },] },
        { type: i0.ElementRef, },
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbOptionComponent.propDecorators = {
        "value": [{ type: i0.Input },],
        "setDisabled": [{ type: i0.Input, args: ['disabled',] },],
        "selectionChange": [{ type: i0.Output },],
        "selectedClass": [{ type: i0.HostBinding, args: ['class.selected',] },],
        "disabledClass": [{ type: i0.HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: i0.HostListener, args: ['click',] },],
    };
    return NbOptionComponent;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbSelectLabelComponent = /** @class */ (function () {
    function NbSelectLabelComponent() {
    }
    NbSelectLabelComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-select-label',
                    template: '<ng-content></ng-content>',
                },] },
    ];
    return NbSelectLabelComponent;
}());
/**
 * The `NbSelectComponent` provides a capability to select one of the passed items.
 *
 * @stacked-example(Showcase, select/select-showcase.component)
 *
 * ### Installation
 *
 * Import `NbSelectModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbSelectModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * If you want to use it as the multi-select control you have to mark it as `multiple`.
 * In this case, `nb-select` will work only with arrays - accept arrays and propagate arrays.
 *
 * @stacked-example(Multiple, select/select-multiple.component)
 *
 * Items without values will clean selection.
 *
 * @stacked-example(Clean selection, select/select-clean.component)
 *
 * Select may be bounded using `selected` input:
 *
 * ```html
 * <nb-select [(selected)]="selected"></nb-selected>
 * ```
 *
 * Or you can bind control with form controls or ngModel:
 *
 * @stacked-example(Select form binding, select/select-form.component)
 *
 * Options in the select may be grouped using `nb-option-group` component.
 *
 * @stacked-example(Grouping, select/select-groups.component)
 *
 * Select may have a placeholder that will be shown when nothing selected:
 *
 * @stacked-example(Placeholder, select/select-placeholder.component)
 *
 * You can disable select, options and whole groups.
 *
 * @stacked-example(Disabled select, select/select-disabled.component)
 *
 * Also, the custom label may be provided in select.
 * This custom label will be used for instead placeholder when something selected.
 *
 * @stacked-example(Custom label, select/select-label.component)
 *
 * Default `nb-select` size is `medium` and status color is `primary`.
 * Select is available in multiple colors using `status` property:
 *
 * @stacked-example(Select statuses, select/select-status.component)
 *
 * There are three select sizes:
 *
 * @stacked-example(Select sizes, select/select-sizes.component)
 *
 * And two additional style types - `outline`:
 *
 * @stacked-example(Outline select, select/select-outline.component)
 *
 * and `hero`:
 *
 * @stacked-example(Select colors, select/select-hero.component)
 *
 * Select is available in different shapes, that could be combined with the other properties:
 *
 * @stacked-example(Select shapes, select/select-shapes.component)
 *
 *
 * @styles
 *
 * select-border-width:
 * select-max-height:
 * select-bg:
 * select-checkbox-color:
 * select-checkmark-color:
 * select-option-disabled-bg:
 * select-option-disabled-opacity:
 * select-option-padding:
 * */
var NbSelectComponent = /** @class */ (function () {
    function NbSelectComponent(document, overlay, hostRef, positionBuilder, cd) {
        var _this = this;
        this.document = document;
        this.overlay = overlay;
        this.hostRef = hostRef;
        this.positionBuilder = positionBuilder;
        this.cd = cd;
        /**
           * Select status (adds specific styles):
           * `primary`, `info`, `success`, `warning`, `danger`
           */
        this.status = 'primary';
        /**
           * Renders select placeholder if nothing selected.
           * */
        this.placeholder = '';
        /**
           * Will be emitted when selected value changes.
           * */
        this.selectedChange = new i0.EventEmitter();
        this.multiple = false;
        /**
           * List of selected options.
           * */
        this.selectionModel = [];
        /**
           * Current overlay position because of we have to toggle overlayPosition
           * in [ngClass] direction and this directive can use only string.
           */
        this.overlayPosition = '';
        /**
           * Stream of events that will fire when one of the options fire selectionChange event.
           * */
        this.selectionChange = rxjs.defer(function () {
            return rxjs.merge.apply(void 0, _this.options.map(function (it) { return it.selectionChange; }));
        });
        this.alive = true;
        /**
           * Function passed through control value accessor to propagate changes.
           * */
        this.onChange = function () { };
    }
    Object.defineProperty(NbSelectComponent.prototype, "setSelected", {
        set: /**
           * Accepts selected item or array of selected items.
           * */
        function (value) {
            this.writeValue(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "setMultiple", {
        set: /**
           * Gives capability just write `multiple` over the element.
           * */
        function (multiple) {
            this.multiple = convertToBoolProperty(multiple);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "isOpened", {
        /**
         * Determines is select opened.
         * */
        get: /**
           * Determines is select opened.
           * */
        function () {
            return this.ref && this.ref.hasAttached();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "isHidden", {
        /**
         * Determines is select hidden.
         * */
        get: /**
           * Determines is select hidden.
           * */
        function () {
            return !this.isOpened;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "hostWidth", {
        /**
         * Returns width of the select button.
         * */
        get: /**
           * Returns width of the select button.
           * */
        function () {
            return this.hostRef.nativeElement.getBoundingClientRect().width;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "selectionView", {
        /**
         * Content rendered in the label.
         * */
        get: /**
           * Content rendered in the label.
           * */
        function () {
            if (this.selectionModel.length > 1) {
                return this.selectionModel.map(function (option) { return option.content; }).join(', ');
            }
            return this.selectionModel[0].content;
        },
        enumerable: true,
        configurable: true
    });
    NbSelectComponent.prototype.ngOnInit = function () {
        this.createOverlay();
    };
    NbSelectComponent.prototype.ngAfterViewInit = function () {
        this.subscribeOnTriggers();
        this.subscribeOnPositionChange();
        this.subscribeOnSelectionChange();
    };
    NbSelectComponent.prototype.ngAfterContentInit = function () {
        if (this.queue) {
            this.writeValue(this.queue);
        }
    };
    NbSelectComponent.prototype.ngOnDestroy = function () {
        this.ref.dispose();
    };
    NbSelectComponent.prototype.show = function () {
        if (this.isHidden) {
            this.ref.attach(this.portal);
            this.cd.markForCheck();
        }
    };
    NbSelectComponent.prototype.hide = function () {
        if (this.isOpened) {
            this.ref.detach();
            this.cd.markForCheck();
        }
    };
    NbSelectComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    NbSelectComponent.prototype.registerOnTouched = function (fn) {
    };
    NbSelectComponent.prototype.setDisabledState = function (isDisabled) {
    };
    NbSelectComponent.prototype.writeValue = function (value) {
        if (!value) {
            return;
        }
        if (this.options) {
            this.setSelection(value);
        }
        else {
            this.queue = value;
        }
    };
    /**
     * Selects option or clear all selected options if value is null.
     * */
    /**
       * Selects option or clear all selected options if value is null.
       * */
    NbSelectComponent.prototype.handleSelect = /**
       * Selects option or clear all selected options if value is null.
       * */
    function (option) {
        if (option.value) {
            this.selectOption(option);
        }
        else {
            this.reset();
        }
        this.cd.detectChanges();
    };
    /**
     * Deselect all selected options.
     * */
    /**
       * Deselect all selected options.
       * */
    NbSelectComponent.prototype.reset = /**
       * Deselect all selected options.
       * */
    function () {
        this.selectionModel.forEach(function (option) { return option.deselect(); });
        this.selectionModel = [];
        this.hide();
        this.emitSelected(null);
    };
    /**
     * Determines how to select option as multiple or single.
     * */
    /**
       * Determines how to select option as multiple or single.
       * */
    NbSelectComponent.prototype.selectOption = /**
       * Determines how to select option as multiple or single.
       * */
    function (option) {
        if (this.multiple) {
            this.handleMultipleSelect(option);
        }
        else {
            this.handleSingleSelect(option);
        }
    };
    /**
     * Select single option.
     * */
    /**
       * Select single option.
       * */
    NbSelectComponent.prototype.handleSingleSelect = /**
       * Select single option.
       * */
    function (option) {
        var selected = this.selectionModel.pop();
        if (selected && selected !== option) {
            selected.deselect();
        }
        this.selectionModel = [option];
        option.select();
        this.hide();
        this.emitSelected(option.value);
    };
    /**
     * Select for multiple options.
     * */
    /**
       * Select for multiple options.
       * */
    NbSelectComponent.prototype.handleMultipleSelect = /**
       * Select for multiple options.
       * */
    function (option) {
        if (option.selected) {
            this.selectionModel = this.selectionModel.filter(function (s) { return s.value !== option.value; });
            option.deselect();
        }
        else {
            this.selectionModel.push(option);
            option.select();
        }
        this.emitSelected(this.selectionModel.map(function (opt) { return opt.value; }));
    };
    NbSelectComponent.prototype.createOverlay = function () {
        var scrollStrategy = this.createScrollStrategy();
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({ positionStrategy: this.positionStrategy, scrollStrategy: scrollStrategy });
    };
    NbSelectComponent.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(exports.NbPosition.BOTTOM)
            .offset(0)
            .adjustment(exports.NbAdjustment.VERTICAL);
    };
    NbSelectComponent.prototype.createScrollStrategy = function () {
        return this.overlay.scrollStrategies.block();
    };
    NbSelectComponent.prototype.subscribeOnTriggers = function () {
        var _this = this;
        var triggerStrategy = new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(exports.NbTrigger.CLICK)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.getContainer(); })
            .build();
        triggerStrategy.show$
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.show(); });
        triggerStrategy.hide$
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.hide(); });
    };
    NbSelectComponent.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return _this.overlayPosition = position; });
        this.positionStrategy.positionChange
            .pipe(rxjs_operators.take(1))
            .subscribe(function () { return _this.cd.detectChanges(); });
    };
    NbSelectComponent.prototype.subscribeOnSelectionChange = function () {
        var _this = this;
        this.selectionChange
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (option) { return _this.handleSelect(option); });
    };
    NbSelectComponent.prototype.getContainer = function () {
        return this.ref && this.ref.hasAttached() && {
            location: {
                nativeElement: this.ref.overlayElement,
            },
        };
    };
    /**
     * Propagate selected value.
     * */
    /**
       * Propagate selected value.
       * */
    NbSelectComponent.prototype.emitSelected = /**
       * Propagate selected value.
       * */
    function (selected) {
        this.onChange(selected);
        this.selectedChange.emit(selected);
    };
    /**
     * Set selected value in model.
     * */
    /**
       * Set selected value in model.
       * */
    NbSelectComponent.prototype.setSelection = /**
       * Set selected value in model.
       * */
    function (value) {
        var _this = this;
        var isArray = Array.isArray(value);
        if (this.multiple && !isArray) {
            throw new Error('Can\'t assign single value if select is marked as multiple');
        }
        if (!this.multiple && isArray) {
            throw new Error('Can\'t assign array if select is not marked as multiple');
        }
        this.cleanSelection();
        if (isArray) {
            value.forEach(function (option) { return _this.selectValue(option); });
        }
        else {
            this.selectValue(value);
        }
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NbSelectComponent.prototype.cleanSelection = function () {
        this.selectionModel.forEach(function (option) { return option.deselect(); });
        this.selectionModel = [];
    };
    /**
     * Selects value.
     * */
    /**
       * Selects value.
       * */
    NbSelectComponent.prototype.selectValue = /**
       * Selects value.
       * */
    function (value) {
        var corresponding = this.options.find(function (option) { return option.value === value; });
        if (corresponding) {
            corresponding.select();
            this.selectionModel.push(corresponding);
        }
    };
    NbSelectComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-select',
                    template: "<button nbButton [size]=\"size\" [status]=\"status\" [shape]=\"shape\" [hero]=\"hero\" [disabled]=\"disabled\" [fullWidth]=\"fullWidth\" [outline]=\"outline\" [class.opened]=\"isOpened\" [ngClass]=\"overlayPosition\"> <ng-container *ngIf=\"selectionModel?.length\"> <ng-container *ngIf=\"customLabel\"> <ng-content select=\"nb-select-label\"></ng-content> </ng-container> <ng-container *ngIf=\"!customLabel\">{{ selectionView }}</ng-container> </ng-container> <ng-container *ngIf=\"!selectionModel?.length\">{{ placeholder }}</ng-container> </button> <nb-card *nbPortal class=\"select\" [ngClass]=\"[status, overlayPosition]\" [style.width.px]=\"hostWidth\"> <nb-card-body> <ng-content select=\"nb-option, nb-option-group\"></ng-content> </nb-card-body> </nb-card> ",
                    styles: ["/*! * @license * Copyright Akveo. All Rights Reserved. * Licensed under the MIT License. See License.txt in the project root for license information. */:host{display:block}:host button{position:relative;width:100%;text-align:start;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;border:none}:host button::after{top:50%;right:0.75rem;position:absolute;display:inline-block;width:0;height:0;margin-left:0.255em;vertical-align:0.255em;content:'';border-top:0.3em solid;border-right:0.3em solid transparent;border-bottom:0;border-left:0.3em solid transparent} "],
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    providers: [
                        {
                            provide: _angular_forms.NG_VALUE_ACCESSOR,
                            useExisting: i0.forwardRef(function () { return NbSelectComponent; }),
                            multi: true,
                        },
                    ],
                },] },
    ];
    /** @nocollapse */
    NbSelectComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: NbOverlayService, },
        { type: i0.ElementRef, },
        { type: NbPositionBuilderService, },
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbSelectComponent.propDecorators = {
        "size": [{ type: i0.Input },],
        "status": [{ type: i0.Input },],
        "shape": [{ type: i0.Input },],
        "hero": [{ type: i0.Input },],
        "disabled": [{ type: i0.Input },],
        "fullWidth": [{ type: i0.Input },],
        "outline": [{ type: i0.Input },],
        "placeholder": [{ type: i0.Input },],
        "selectedChange": [{ type: i0.Output },],
        "setSelected": [{ type: i0.Input, args: ['selected',] },],
        "setMultiple": [{ type: i0.Input, args: ['multiple',] },],
        "options": [{ type: i0.ContentChildren, args: [NbOptionComponent, { descendants: true },] },],
        "customLabel": [{ type: i0.ContentChild, args: [NbSelectLabelComponent,] },],
        "portal": [{ type: i0.ViewChild, args: [NbPortalDirective,] },],
    };
    return NbSelectComponent;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbOptionGroupComponent = /** @class */ (function () {
    function NbOptionGroupComponent() {
        this.disabled = false;
    }
    Object.defineProperty(NbOptionGroupComponent.prototype, "setDisabled", {
        set: function (disabled) {
            this.disabled = convertToBoolProperty(disabled);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionGroupComponent.prototype, "disabledClass", {
        get: function () {
            return this.disabled;
        },
        enumerable: true,
        configurable: true
    });
    NbOptionGroupComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-option-group',
                    styles: [":host{display:block}:host span{padding:1.125rem 0.5rem;display:block}:host.disabled{pointer-events:none}:host /deep/ nb-option{padding:0.75rem 0.75rem 0.75rem 2.5rem} "],
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    template: "\n    <span>{{ title }}</span>\n    <ng-content select=\"nb-option, ng-container\"></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbOptionGroupComponent.propDecorators = {
        "title": [{ type: i0.Input },],
        "setDisabled": [{ type: i0.Input, args: ['disabled',] },],
        "disabledClass": [{ type: i0.HostBinding, args: ['class.disabled',] },],
    };
    return NbOptionGroupComponent;
}());

var NB_SELECT_COMPONENTS = [
    NbSelectComponent,
    NbOptionComponent,
    NbOptionGroupComponent,
    NbSelectLabelComponent,
];
var NbSelectModule = /** @class */ (function () {
    function NbSelectModule() {
    }
    NbSelectModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbSharedModule, NbOverlayModule, NbButtonModule, NbInputModule, NbCardModule, NbCheckboxModule],
                    exports: NB_SELECT_COMPONENTS.slice(),
                    declarations: NB_SELECT_COMPONENTS.slice(),
                },] },
    ];
    return NbSelectModule;
}());

(function (NbWindowState) {
    NbWindowState["MINIMIZED"] = "minimized";
    NbWindowState["MAXIMIZED"] = "maximized";
    NbWindowState["FULL_SCREEN"] = "full-screen";
})(exports.NbWindowState || (exports.NbWindowState = {}));
/**
 * Window configuration options.
 */
var NbWindowConfig = /** @class */ (function () {
    function NbWindowConfig() {
        var configs = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            configs[_i] = arguments[_i];
        }
        /**
           * Window title.
           */
        this.title = '';
        /**
           * Initial window state. Full screen by default.
           */
        this.initialState = exports.NbWindowState.FULL_SCREEN;
        /**
           * If true than backdrop will be rendered behind window.
           * By default set to true.
           */
        this.hasBackdrop = true;
        /**
           * If set to true mouse clicks on backdrop will close a window.
           * Default is true.
           */
        this.closeOnBackdropClick = true;
        /**
           * If true then escape press will close a window.
           * Default is true.
           */
        this.closeOnEsc = true;
        /**
           * Class to be applied to the window.
           */
        this.windowClass = '';
        /**
           * Both, template and component may receive data through `config.context` property.
           * For components, this data will be set as component properties.
           * For templates, you can access it inside template as $implicit.
           */
        this.context = {};
        /**
           * Where the attached component should live in Angular's *logical* component tree.
           * This affects what is available for injection and the change detection order for the
           * component instantiated inside of the window. This does not affect where the window
           * content will be rendered.
           */
        this.viewContainerRef = null;
        Object.assign.apply(Object, [this].concat(configs));
    }
    return NbWindowConfig;
}());
var NB_WINDOW_CONTENT = new i0.InjectionToken('Nebular Window Content');
var NB_WINDOW_CONFIG = new i0.InjectionToken('Nebular Window Config');
var NB_WINDOW_CONTEXT = new i0.InjectionToken('Nebular Window Context');

/**
 * The `NbWindowRef` helps to manipulate window after it was created.
 * The window can be dismissed by using `close` method of the windowRef.
 * You can access rendered component as `componentRef` property of the windowRef.
 */
var NbWindowRef = /** @class */ (function () {
    function NbWindowRef(config) {
        this.config = config;
        this.stateChange$ = new rxjs.ReplaySubject(1);
        this._closed = false;
        this.closed$ = new rxjs.Subject();
        this.state = config.initialState;
    }
    Object.defineProperty(NbWindowRef.prototype, "state", {
        /**
         * Current window state.
         */
        get: /**
           * Current window state.
           */
        function () {
            return this.stateValue;
        },
        set: function (newState) {
            if (newState && this.stateValue !== newState) {
                this.prevStateValue = this.state;
                this.stateValue = newState;
                this.stateChange$.next({ oldState: this.prevStateValue, newState: newState });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbWindowRef.prototype, "stateChange", {
        /**
         * Emits when window state change.
         */
        get: /**
           * Emits when window state change.
           */
        function () {
            return this.stateChange$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbWindowRef.prototype, "onClose", {
        /**
         * Emits when window was closed.
         */
        get: /**
           * Emits when window was closed.
           */
        function () {
            return this.closed$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Minimize window.
     */
    /**
       * Minimize window.
       */
    NbWindowRef.prototype.minimize = /**
       * Minimize window.
       */
    function () {
        this.state = exports.NbWindowState.MINIMIZED;
    };
    /**
     * Maximize window.
     */
    /**
       * Maximize window.
       */
    NbWindowRef.prototype.maximize = /**
       * Maximize window.
       */
    function () {
        this.state = exports.NbWindowState.MAXIMIZED;
    };
    /**
     * Set window on top.
     */
    /**
       * Set window on top.
       */
    NbWindowRef.prototype.fullScreen = /**
       * Set window on top.
       */
    function () {
        this.state = exports.NbWindowState.FULL_SCREEN;
    };
    NbWindowRef.prototype.toPreviousState = function () {
        this.state = this.prevStateValue;
    };
    /**
     * Closes window.
     * */
    /**
       * Closes window.
       * */
    NbWindowRef.prototype.close = /**
       * Closes window.
       * */
    function () {
        if (this._closed) {
            return;
        }
        this._closed = true;
        this.componentRef.destroy();
        this.stateChange$.complete();
        this.closed$.next();
        this.closed$.complete();
    };
    return NbWindowRef;
}());

var NbWindowsContainerComponent = /** @class */ (function () {
    function NbWindowsContainerComponent() {
    }
    NbWindowsContainerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-windows-container',
                    template: "<ng-container #viewContainerRef></ng-container>",
                    styles: [":host{display:flex;align-items:flex-end;overflow-x:auto}:host /deep/ nb-window:not(.full-screen){margin:0 2rem} "],
                },] },
    ];
    /** @nocollapse */
    NbWindowsContainerComponent.propDecorators = {
        "viewContainerRef": [{ type: i0.ViewChild, args: ['viewContainerRef', { read: i0.ViewContainerRef },] },],
    };
    return NbWindowsContainerComponent;
}());

var NbWindowComponent = /** @class */ (function () {
    function NbWindowComponent(content, context, windowRef, config, focusTrapFactory, elementRef, renderer) {
        this.content = content;
        this.context = context;
        this.windowRef = windowRef;
        this.config = config;
        this.focusTrapFactory = focusTrapFactory;
        this.elementRef = elementRef;
        this.renderer = renderer;
    }
    Object.defineProperty(NbWindowComponent.prototype, "isFullScreen", {
        get: function () {
            return this.windowRef.state === exports.NbWindowState.FULL_SCREEN;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbWindowComponent.prototype, "maximized", {
        get: function () {
            return this.windowRef.state === exports.NbWindowState.MAXIMIZED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbWindowComponent.prototype, "minimized", {
        get: function () {
            return this.windowRef.state === exports.NbWindowState.MINIMIZED;
        },
        enumerable: true,
        configurable: true
    });
    NbWindowComponent.prototype.ngOnInit = function () {
        this.focusTrap = this.focusTrapFactory.create(this.elementRef.nativeElement);
        this.focusTrap.blurPreviouslyFocusedElement();
        this.focusTrap.focusInitialElement();
        if (this.config.windowClass) {
            this.renderer.addClass(this.elementRef.nativeElement, this.config.windowClass);
        }
    };
    NbWindowComponent.prototype.ngAfterViewInit = function () {
        if (this.content instanceof i0.TemplateRef) {
            this.attachTemplate();
        }
        else {
            this.attachComponent();
        }
    };
    NbWindowComponent.prototype.ngOnDestroy = function () {
        if (this.focusTrap) {
            this.focusTrap.restoreFocus();
        }
        this.close();
    };
    NbWindowComponent.prototype.minimize = function () {
        if (this.windowRef.state === exports.NbWindowState.MINIMIZED) {
            this.windowRef.toPreviousState();
        }
        else {
            this.windowRef.minimize();
        }
    };
    NbWindowComponent.prototype.maximize = function () {
        this.windowRef.maximize();
    };
    NbWindowComponent.prototype.fullScreen = function () {
        this.windowRef.fullScreen();
    };
    NbWindowComponent.prototype.maximizeOrFullScreen = function () {
        if (this.windowRef.state === exports.NbWindowState.MINIMIZED) {
            this.maximize();
        }
        else {
            this.fullScreen();
        }
    };
    NbWindowComponent.prototype.close = function () {
        this.windowRef.close();
    };
    NbWindowComponent.prototype.attachTemplate = function () {
        this.overlayContainer.attachTemplatePortal(new NbTemplatePortal(this.content, null, {
            $implicit: this.context,
        }));
    };
    NbWindowComponent.prototype.attachComponent = function () {
        var portal = new NbComponentPortal(this.content, null, null, this.cfr);
        var ref = this.overlayContainer.attachComponentPortal(portal);
        Object.assign(ref.instance, this.context);
        ref.changeDetectorRef.detectChanges();
    };
    NbWindowComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-window',
                    template: "\n    <nb-card>\n      <nb-card-header>\n        <div cdkFocusInitial class=\"title\" tabindex=\"-1\">{{ config.title }}</div>\n\n        <div class=\"buttons\">\n          <button class=\"button\" (click)=\"minimize()\">\n            <i class=\"nb-fold\"></i>\n          </button>\n          <button class=\"button\" *ngIf=\"isFullScreen\" (click)=\"maximize()\">\n            <i class=\"nb-minimize\"></i>\n          </button>\n          <button class=\"button\" *ngIf=\"minimized || maximized\" (click)=\"maximizeOrFullScreen()\">\n            <i class=\"nb-maximize\"></i>\n          </button>\n          <button class=\"button\" (click)=\"close()\">\n            <i class=\"nb-close\"></i>\n          </button>\n        </div>\n      </nb-card-header>\n      <nb-card-body *ngIf=\"maximized || isFullScreen\">\n        <nb-overlay-container></nb-overlay-container>\n      </nb-card-body>\n    </nb-card>\n  ",
                    styles: [":host{flex:1 0 auto;min-width:20rem}:host nb-card{margin:0}:host nb-card-header{display:flex;justify-content:space-between;align-items:center;padding-right:0;overflow:hidden}:host .title{flex:1 0 auto;margin-right:3rem;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}:host .buttons{width:9.5rem;display:flex;justify-content:space-evenly}:host .buttons .button{background:none;border:none;flex:0 0 3rem;padding:0 0.8rem}:host(.full-screen){position:fixed;top:50%;left:50%;transform:translate(-50%, -50%)}:host(.maximized) nb-card{border-bottom-left-radius:0;border-bottom-right-radius:0}:host(.minimized) nb-card{border-bottom-left-radius:0;border-bottom-right-radius:0;height:auto}:host(.minimized) nb-card nb-card-header{border-bottom:none} "],
                },] },
    ];
    /** @nocollapse */
    NbWindowComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_WINDOW_CONTENT,] },] },
        { type: Object, decorators: [{ type: i0.Inject, args: [NB_WINDOW_CONTEXT,] },] },
        { type: NbWindowRef, },
        { type: NbWindowConfig, },
        { type: NbFocusTrapFactoryService, },
        { type: i0.ElementRef, },
        { type: i0.Renderer2, },
    ]; };
    NbWindowComponent.propDecorators = {
        "cfr": [{ type: i0.Input },],
        "isFullScreen": [{ type: i0.HostBinding, args: ['class.full-screen',] },],
        "maximized": [{ type: i0.HostBinding, args: ['class.maximized',] },],
        "minimized": [{ type: i0.HostBinding, args: ['class.minimized',] },],
        "overlayContainer": [{ type: i0.ViewChild, args: [NbOverlayContainerComponent,] },],
    };
    return NbWindowComponent;
}());

/**
 * The `NbWindowService` can be used to open windows.
 *
 * @stacked-example(Showcase, window/window-showcase.component)
 *
 * ### Installation
 *
 * Import `NbWindowModule` to your app module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbWindowModule.forRoot(config),
 *   ],
 * })
 * export class AppModule { }
 * ```
 *
 * If you are using it in a lazy loaded module than you have to install `NbWindowModule.forChild`:
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbWindowModule.forChild(config),
 *   ],
 * })
 * export class LazyLoadedModule { }
 * ```
 *
 * ### Usage
 *
 * A new window can be opened by calling the `open` method with a component or template to be loaded
 * and an optional configuration.
 * `open` method will return `NbWindowRef` that can be used for the further manipulations.
 *
 * ```ts
 * const windowRef = this.windowService.open(MyComponent, { ... });
 * ```
 *
 * `NbWindowRef` gives you ability manipulate opened window.
 * Also, you can inject `NbWindowRef` inside provided component which rendered in window.
 *
 * ```ts
 * this.windowService.open(MyWindowComponent, { ... });
 *
 * // my.component.ts
 * constructor(protected windowRef: NbWindowRef) {
 * }
 *
 * minimize() {
 *   this.windowRef.minimize();
 * }
 *
 * close() {
 *   this.windowRef.close();
 * }
 * ```
 *
 * Instead of component you can create window from TemplateRef. As usual you can access context provided via config
 * via `let-` variables. Also you can get reference to the `NbWindowRef` in context's `windowRef` property.
 *
 * @stacked-example(Window content from TemplateRef, window/template-window.component)
 *
 * ### Configuration
 *
 * As mentioned above, `open` method of the `NbWindowService` may receive optional configuration options.
 * Also, you can modify default windows configuration through `NbWindowModule.forRoot({ ... })`.
 * You can read about all available options on [API tab](docs/components/window/api#nbwindowconfig).
 *
 * @stacked-example(Configuration, window/windows-backdrop.component)
 */
var NbWindowService = /** @class */ (function () {
    function NbWindowService(componentFactoryResolver, overlayService, overlayPositionBuilder, blockScrollStrategy, defaultWindowsConfig, cfr) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.overlayService = overlayService;
        this.overlayPositionBuilder = overlayPositionBuilder;
        this.blockScrollStrategy = blockScrollStrategy;
        this.defaultWindowsConfig = defaultWindowsConfig;
        this.cfr = cfr;
        this.openWindows = [];
    }
    /**
     * Opens new window.
     * @param windowContent
     * @param windowConfig
     * */
    /**
       * Opens new window.
       * @param windowContent
       * @param windowConfig
       * */
    NbWindowService.prototype.open = /**
       * Opens new window.
       * @param windowContent
       * @param windowConfig
       * */
    function (windowContent, windowConfig) {
        if (windowConfig === void 0) { windowConfig = {}; }
        if (this.windowsContainerViewRef == null) {
            this.createWindowsContainer();
        }
        var config = new NbWindowConfig(this.defaultWindowsConfig, windowConfig);
        var windowRef = new NbWindowRef(config);
        windowRef.componentRef = this.appendWindow(windowContent, config, windowRef);
        this.openWindows.push(windowRef);
        this.subscribeToEvents(windowRef);
        return windowRef;
    };
    NbWindowService.prototype.createWindowsContainer = function () {
        this.overlayRef = this.overlayService.create({
            scrollStrategy: this.overlayService.scrollStrategies.noop(),
            positionStrategy: this.overlayPositionBuilder.global().bottom().right(),
            hasBackdrop: true,
        });
        var windowsContainerPortal = new NbComponentPortal(NbWindowsContainerComponent, null, null, this.cfr);
        var overlayRef = this.overlayRef.attach(windowsContainerPortal);
        this.windowsContainerViewRef = overlayRef.instance.viewContainerRef;
    };
    NbWindowService.prototype.appendWindow = function (content, config, windowRef) {
        var context = content instanceof i0.TemplateRef
            ? { $implicit: config.context, windowRef: windowRef }
            : config.context;
        var providers = [
            { provide: NB_WINDOW_CONTENT, useValue: content },
            { provide: NB_WINDOW_CONTEXT, useValue: context },
            { provide: NbWindowConfig, useValue: config },
            { provide: NbWindowRef, useValue: windowRef },
        ];
        var parentInjector = config.viewContainerRef
            ? config.viewContainerRef.injector
            : this.windowsContainerViewRef.injector;
        var injector = i0.Injector.create({ parent: parentInjector, providers: providers });
        var windowFactory = this.componentFactoryResolver.resolveComponentFactory(NbWindowComponent);
        var ref = this.windowsContainerViewRef.createComponent(windowFactory, null, injector);
        ref.instance.cfr = this.cfr;
        ref.changeDetectorRef.detectChanges();
        return ref;
    };
    NbWindowService.prototype.subscribeToEvents = function (windowRef) {
        var _this = this;
        if (windowRef.config.closeOnBackdropClick) {
            this.overlayRef.backdropClick().subscribe(function () { return windowRef.close(); });
        }
        if (windowRef.config.closeOnEsc) {
            this.overlayRef.keydownEvents()
                .pipe(rxjs_operators.filter(function (event) { return event.keyCode === 27; }))
                .subscribe(function () { return windowRef.close(); });
        }
        windowRef.stateChange.subscribe(function () { return _this.checkAndUpdateOverlay(); });
        windowRef.onClose.subscribe(function () {
            _this.openWindows.splice(_this.openWindows.indexOf(windowRef), 1);
            _this.checkAndUpdateOverlay();
        });
    };
    NbWindowService.prototype.checkAndUpdateOverlay = function () {
        var fullScreenWindows = this.openWindows.filter(function (w) { return w.state === exports.NbWindowState.FULL_SCREEN; });
        if (fullScreenWindows.length > 0) {
            this.blockScrollStrategy.enable();
        }
        else {
            this.blockScrollStrategy.disable();
        }
        if (fullScreenWindows.some(function (w) { return w.config.hasBackdrop; })) {
            this.overlayRef.backdropElement.removeAttribute('hidden');
        }
        else {
            this.overlayRef.backdropElement.setAttribute('hidden', '');
        }
    };
    NbWindowService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbWindowService.ctorParameters = function () { return [
        { type: i0.ComponentFactoryResolver, },
        { type: NbOverlayService, },
        { type: NbOverlayPositionBuilder, },
        { type: NbBlockScrollStrategyAdapter, },
        { type: NbWindowConfig, decorators: [{ type: i0.Inject, args: [NB_WINDOW_CONFIG,] },] },
        { type: i0.ComponentFactoryResolver, },
    ]; };
    return NbWindowService;
}());

var NbWindowModule = /** @class */ (function () {
    function NbWindowModule() {
    }
    NbWindowModule.forRoot = function (defaultConfig) {
        return {
            ngModule: NbWindowModule,
            providers: [
                NbWindowService,
                { provide: NB_WINDOW_CONFIG, useValue: defaultConfig },
            ],
        };
    };
    NbWindowModule.forChild = function (defaultConfig) {
        return {
            ngModule: NbWindowModule,
            providers: [
                NbWindowService,
                { provide: NB_WINDOW_CONFIG, useValue: defaultConfig },
            ],
        };
    };
    NbWindowModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [i1.CommonModule, NbOverlayModule, NbCardModule],
                    declarations: [
                        NbWindowsContainerComponent,
                        NbWindowComponent,
                    ],
                    entryComponents: [NbWindowsContainerComponent, NbWindowComponent],
                },] },
    ];
    return NbWindowModule;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbDatepickerAdapter` instances provide way how to parse, format and validate
 * different date types.
 * */
var NbDatepickerAdapter = /** @class */ (function () {
    function NbDatepickerAdapter() {
    }
    return NbDatepickerAdapter;
}());
/**
 * Datepicker is an control that can pick any values anyway.
 * It has to be bound to the datepicker directive through nbDatepicker input.
 * */
var NbDatepicker = /** @class */ (function () {
    function NbDatepicker() {
    }
    return NbDatepicker;
}());
var NB_DATE_ADAPTER = new i0.InjectionToken('Datepicker Adapter');
/**
 * The `NbDatepickerDirective` is form control that gives you ability to select dates and ranges. The datepicker
 * is shown when input receives a `focus` event.
 *
 * ```html
 * <input [nbDatepicker]="datepicker">
 * <nb-datepicker #datepicker></nb-datepicker>
 * ```
 *
 * @stacked-example(Showcase, datepicker/datepicker-showcase.component)
 *
 * ### Installation
 *
 * Import `NbDatepickerModule.forRoot()` to your root module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbDatepickerModule.forRoot(),
 *   ],
 * })
 * export class AppModule { }
 * ```
 * And `NbDatepickerModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbDatepickerModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * If you want to use range selection, you have to use `NbRangepickerComponent` instead:
 *
 * ```html
 * <input [nbDatepicker]="rangepicker">
 * <nb-rangepicker #rangepicker></nb-rangepicker>
 * ```
 *
 * Both range and date pickers support all parameters as calendar, so, check `NbCalendarComponent` for additional
 * info.
 *
 * @stacked-example(Range showcase, datepicker/rangepicker-showcase.component)
 *
 * Datepicker is the form control so it can be bound with angular forms through ngModel and form controls.
 *
 * @stacked-example(Forms, datepicker/datepicker-forms.component)
 *
 * `NbDatepickerDirective` may be validated using `min` and `max` dates passed to the datepicker.
 * And `filter` predicate that receives date object and has to return a boolean value.
 *
 * @stacked-example(Validation, datepicker/datepicker-validation.component)
 *
 * The `NbDatepickerComponent` supports date formatting:
 *
 * ```html
 * <input [nbDatepicker]="datepicker">
 * <nb-datepicker #datepicker format="MM\dd\yyyy"></nb-datepicker>
 * ```
 *
 * ## Formatting Issue
 *
 * By default, datepicker uses angulars `LOCALE_ID` token for localization and `DatePipe` for dates formatting.
 * And native `Date.parse(...)` for dates parsing. But native `Date.parse` function doesn't support formats.
 * To provide custom formatting you have to use one of the following packages:
 *
 * - `@nebular/moment` - provides moment date adapter that uses moment for date objects. This means datepicker than
 * will operate only moment date objects. If you want to use it you have to install it: `npm i @nebular/moment`, and
 * import `NbMomentDateModule` from this package.
 *
 * - `@nebular/date-fns` - adapter for popular date-fns library. This way is preferred if you need only date formatting.
 * Because date-fns is treeshakable, tiny and operates native date objects. If you want to use it you have to
 * install it: `npm i @nebular/date-fns`, and import `NbDateFnsDateModule` from this package.
 *
 * @styles
 *
 * datepicker-fg
 * datepicker-bg
 * datepicker-border
 * datepicker-border-radius
 * datepicker-shadow
 * datepicker-arrow-size
 * */
var NbDatepickerDirective = /** @class */ (function () {
    function NbDatepickerDirective(document, datepickerAdapters, hostRef, dateService) {
        var _this = this;
        this.document = document;
        this.datepickerAdapters = datepickerAdapters;
        this.hostRef = hostRef;
        this.dateService = dateService;
        this.alive = true;
        this.onChange = function () {
        };
        /**
           * Form control validators will be called in validators context, so, we need to bind them.
           * */
        this.validator = _angular_forms.Validators.compose([
            this.parseValidator,
            this.minValidator,
            this.maxValidator,
            this.filterValidator,
        ].map(function (fn) { return fn.bind(_this); }));
        this.subscribeOnInputChange();
    }
    Object.defineProperty(NbDatepickerDirective.prototype, "setPicker", {
        set: /**
           * Provides datepicker component.
           * */
        function (picker) {
            this.picker = picker;
            this.setupPicker();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerDirective.prototype, "input", {
        /**
         * Returns html input element.
         * */
        get: /**
           * Returns html input element.
           * */
        function () {
            return this.hostRef.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerDirective.prototype, "inputValue", {
        /**
         * Returns host input value.
         * */
        get: /**
           * Returns host input value.
           * */
        function () {
            return this.input.value;
        },
        enumerable: true,
        configurable: true
    });
    NbDatepickerDirective.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    /**
     * Writes value in picker and html input element.
     * */
    /**
       * Writes value in picker and html input element.
       * */
    NbDatepickerDirective.prototype.writeValue = /**
       * Writes value in picker and html input element.
       * */
    function (value) {
        this.writePicker(value);
        this.writeInput(value);
    };
    NbDatepickerDirective.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    NbDatepickerDirective.prototype.registerOnTouched = function (fn) {
    };
    NbDatepickerDirective.prototype.setDisabledState = function (isDisabled) {
    };
    /**
     * Form control validation based on picker validator config.
     * */
    /**
       * Form control validation based on picker validator config.
       * */
    NbDatepickerDirective.prototype.validate = /**
       * Form control validation based on picker validator config.
       * */
    function () {
        return this.validator(null);
    };
    /**
     * Hides picker, focuses the input
     */
    /**
       * Hides picker, focuses the input
       */
    NbDatepickerDirective.prototype.hidePicker = /**
       * Hides picker, focuses the input
       */
    function () {
        this.input.focus();
        this.picker.hide();
    };
    /**
     * Validates that we can parse value correctly.
     * */
    /**
       * Validates that we can parse value correctly.
       * */
    NbDatepickerDirective.prototype.parseValidator = /**
       * Validates that we can parse value correctly.
       * */
    function () {
        var isValid = this.datepickerAdapter.isValid(this.inputValue, this.picker.format);
        return isValid ? null : { nbDatepickerParse: { value: this.inputValue } };
    };
    /**
     * Validates passed value is greater than min.
     * */
    /**
       * Validates passed value is greater than min.
       * */
    NbDatepickerDirective.prototype.minValidator = /**
       * Validates passed value is greater than min.
       * */
    function () {
        var config = this.picker.getValidatorConfig();
        var date = this.datepickerAdapter.parse(this.inputValue, this.picker.format);
        return (!config.min || !date || this.dateService.compareDates(config.min, date) <= 0) ?
            null : { nbDatepickerMin: { min: config.min, actual: date } };
    };
    /**
     * Validates passed value is smaller than max.
     * */
    /**
       * Validates passed value is smaller than max.
       * */
    NbDatepickerDirective.prototype.maxValidator = /**
       * Validates passed value is smaller than max.
       * */
    function () {
        var config = this.picker.getValidatorConfig();
        var date = this.datepickerAdapter.parse(this.inputValue, this.picker.format);
        return (!config.max || !date || this.dateService.compareDates(config.max, date) >= 0) ?
            null : { nbDatepickerMax: { max: config.max, actual: date } };
    };
    /**
     * Validates passed value satisfy the filter.
     * */
    /**
       * Validates passed value satisfy the filter.
       * */
    NbDatepickerDirective.prototype.filterValidator = /**
       * Validates passed value satisfy the filter.
       * */
    function () {
        var config = this.picker.getValidatorConfig();
        var date = this.datepickerAdapter.parse(this.inputValue, this.picker.format);
        return (!config.filter || !date || config.filter(date)) ?
            null : { nbDatepickerFilter: true };
    };
    /**
     * Chooses datepicker adapter based on passed picker component.
     * */
    /**
       * Chooses datepicker adapter based on passed picker component.
       * */
    NbDatepickerDirective.prototype.chooseDatepickerAdapter = /**
       * Chooses datepicker adapter based on passed picker component.
       * */
    function () {
        var _this = this;
        this.datepickerAdapter = this.datepickerAdapters.find(function (_a) {
            var picker = _a.picker;
            return _this.picker instanceof picker;
        });
        if (this.noDatepickerAdapterProvided()) {
            throw new Error('No datepickerAdapter provided for picker');
        }
    };
    /**
     * Attaches picker to the host input element and subscribes on value changes.
     * */
    /**
       * Attaches picker to the host input element and subscribes on value changes.
       * */
    NbDatepickerDirective.prototype.setupPicker = /**
       * Attaches picker to the host input element and subscribes on value changes.
       * */
    function () {
        var _this = this;
        this.chooseDatepickerAdapter();
        this.picker.attach(this.hostRef);
        if (this.hostRef.nativeElement.value) {
            this.picker.value = this.datepickerAdapter.parse(this.hostRef.nativeElement.value, this.picker.format);
        }
        this.picker.valueChange
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (value) {
            _this.writePicker(value);
            _this.writeInput(value);
            _this.onChange(value);
            if (_this.picker.shouldHide()) {
                _this.hidePicker();
            }
        });
    };
    NbDatepickerDirective.prototype.writePicker = function (value) {
        this.picker.value = value;
    };
    NbDatepickerDirective.prototype.writeInput = function (value) {
        var stringRepresentation = this.datepickerAdapter.format(value, this.picker.format);
        this.hostRef.nativeElement.value = stringRepresentation;
    };
    /**
     * Validates if no datepicker adapter provided.
     * */
    /**
       * Validates if no datepicker adapter provided.
       * */
    NbDatepickerDirective.prototype.noDatepickerAdapterProvided = /**
       * Validates if no datepicker adapter provided.
       * */
    function () {
        return !this.datepickerAdapter || !(this.datepickerAdapter instanceof NbDatepickerAdapter);
    };
    NbDatepickerDirective.prototype.subscribeOnInputChange = function () {
        var _this = this;
        rxjs.fromEvent(this.input, 'input')
            .pipe(rxjs_operators.map(function () { return _this.inputValue; }), rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (value) { return _this.handleInputChange(value); });
    };
    /**
     * Parses input value and write if it isn't null.
     * */
    /**
       * Parses input value and write if it isn't null.
       * */
    NbDatepickerDirective.prototype.handleInputChange = /**
       * Parses input value and write if it isn't null.
       * */
    function (value) {
        var date = this.parseInputValue(value);
        this.onChange(date);
        this.writePicker(date);
    };
    NbDatepickerDirective.prototype.parseInputValue = function (value) {
        if (this.datepickerAdapter.isValid(value, this.picker.format)) {
            return this.datepickerAdapter.parse(value, this.picker.format);
        }
        return null;
    };
    NbDatepickerDirective.decorators = [
        { type: i0.Directive, args: [{
                    selector: 'input[nbDatepicker]',
                    providers: [
                        {
                            provide: _angular_forms.NG_VALUE_ACCESSOR,
                            useExisting: i0.forwardRef(function () { return NbDatepickerDirective; }),
                            multi: true,
                        },
                        {
                            provide: _angular_forms.NG_VALIDATORS,
                            useExisting: i0.forwardRef(function () { return NbDatepickerDirective; }),
                            multi: true,
                        },
                    ],
                },] },
    ];
    /** @nocollapse */
    NbDatepickerDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: Array, decorators: [{ type: i0.Inject, args: [NB_DATE_ADAPTER,] },] },
        { type: i0.ElementRef, },
        { type: NbDateService, },
    ]; };
    NbDatepickerDirective.propDecorators = {
        "setPicker": [{ type: i0.Input, args: ['nbDatepicker',] },],
    };
    return NbDatepickerDirective;
}());

var __extends$11 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbDatepickerContainerComponent = /** @class */ (function (_super) {
    __extends$11(NbDatepickerContainerComponent, _super);
    function NbDatepickerContainerComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbDatepickerContainerComponent.prototype.attach = function (portal) {
        return this.overlayContainer.attachComponentPortal(portal);
    };
    NbDatepickerContainerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-datepicker-container',
                    styles: [":host .arrow{position:absolute;width:0;height:0}:host /deep/ nb-overlay-container .primitive-overlay{padding:0.75rem 1rem} "],
                    template: "\n    <span class=\"arrow\"></span>\n    <nb-overlay-container></nb-overlay-container>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbDatepickerContainerComponent.propDecorators = {
        "overlayContainer": [{ type: i0.ViewChild, args: [NbOverlayContainerComponent,] },],
    };
    return NbDatepickerContainerComponent;
}(NbPositionedContainer));

var __extends$12 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbBasePicker` component concentrates overlay manipulation logic.
 * */
var NbBasePicker = /** @class */ (function (_super) {
    __extends$12(NbBasePicker, _super);
    function NbBasePicker(document, positionBuilder, overlay, cfr) {
        var _this = _super.call(this) || this;
        _this.document = document;
        _this.positionBuilder = positionBuilder;
        _this.overlay = overlay;
        _this.cfr = cfr;
        /**
           * Defines if we should render previous and next months
           * in the current month view.
           * */
        _this.boundingMonth = true;
        /**
           * Defines starting view for calendar.
           * */
        _this.startView = exports.NbCalendarViewMode.DATE;
        /**
           * Size of the calendar and entire components.
           * Can be 'medium' which is default or 'large'.
           * */
        _this.size = exports.NbCalendarSize.MEDIUM;
        /**
           * Hide picker when a date or a range is selected, `true` by default
           * @type {boolean}
           */
        _this.hideOnSelect = true;
        /**
           * Stream of picker changes. Required to be the subject because picker hides and shows and picker
           * change stream becomes recreated.
           * */
        _this.onChange$ = new rxjs.Subject();
        _this.alive = true;
        return _this;
    }
    Object.defineProperty(NbBasePicker.prototype, "picker", {
        /**
         * Returns picker instance.
         * */
        get: /**
           * Returns picker instance.
           * */
        function () {
            return this.pickerRef && this.pickerRef.instance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbBasePicker.prototype, "valueChange", {
        /**
         * Stream of picker value changes.
         * */
        get: /**
           * Stream of picker value changes.
           * */
        function () {
            return this.onChange$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    NbBasePicker.prototype.ngOnDestroy = function () {
        this.alive = false;
        this.hide();
        this.ref.dispose();
    };
    /**
     * Datepicker knows nothing about host html input element.
     * So, attach method attaches datepicker to the host input element.
     * */
    /**
       * Datepicker knows nothing about host html input element.
       * So, attach method attaches datepicker to the host input element.
       * */
    NbBasePicker.prototype.attach = /**
       * Datepicker knows nothing about host html input element.
       * So, attach method attaches datepicker to the host input element.
       * */
    function (hostRef) {
        this.hostRef = hostRef;
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({
            positionStrategy: this.positionStrategy,
            scrollStrategy: this.overlay.scrollStrategies.reposition(),
        });
        this.subscribeOnPositionChange();
        this.subscribeOnTriggers();
    };
    NbBasePicker.prototype.getValidatorConfig = function () {
        return { min: this.min, max: this.max, filter: this.filter };
    };
    NbBasePicker.prototype.show = function () {
        this.container = this.ref.attach(new NbComponentPortal(NbDatepickerContainerComponent, null, null, this.cfr));
        this.instantiatePicker();
        this.subscribeOnValueChange();
        this.writeQueue();
        this.patchWithInputs();
    };
    NbBasePicker.prototype.shouldHide = function () {
        return this.hideOnSelect && !!this.value;
    };
    NbBasePicker.prototype.hide = function () {
        this.ref.detach();
        // save current value if picker was rendered
        if (this.picker) {
            this.queue = this.value;
            this.pickerRef.destroy();
            this.pickerRef = null;
            this.container = null;
        }
    };
    NbBasePicker.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(exports.NbPosition.BOTTOM)
            .adjustment(exports.NbAdjustment.COUNTERCLOCKWISE);
    };
    NbBasePicker.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return patch(_this.container, { position: position }); });
    };
    NbBasePicker.prototype.createTriggerStrategy = function () {
        var _this = this;
        return new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(exports.NbTrigger.FOCUS)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.container; })
            .build();
    };
    NbBasePicker.prototype.subscribeOnTriggers = function () {
        var _this = this;
        var triggerStrategy = this.createTriggerStrategy();
        triggerStrategy.show$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.show(); });
        triggerStrategy.hide$.pipe(rxjs_operators.takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.hide(); });
    };
    NbBasePicker.prototype.instantiatePicker = function () {
        this.pickerRef = this.container.instance.attach(new NbComponentPortal(this.pickerClass, null, null, this.cfr));
    };
    /**
     * Subscribes on picker value changes and emit data through this.onChange$ subject.
     * */
    /**
       * Subscribes on picker value changes and emit data through this.onChange$ subject.
       * */
    NbBasePicker.prototype.subscribeOnValueChange = /**
       * Subscribes on picker value changes and emit data through this.onChange$ subject.
       * */
    function () {
        var _this = this;
        this.pickerValueChange.subscribe(function (date) {
            _this.onChange$.next(date);
        });
    };
    NbBasePicker.prototype.patchWithInputs = function () {
        this.picker.boundingMonth = this.boundingMonth;
        this.picker.startView = this.startView;
        this.picker.min = this.min;
        this.picker.max = this.max;
        this.picker.filter = this.filter;
        this.picker._cellComponent = this.dayCellComponent;
        this.picker.monthCellComponent = this.monthCellComponent;
        this.picker._yearCellComponent = this.yearCellComponent;
        this.picker.size = this.size;
        this.picker.visibleDate = this.visibleDate;
    };
    /** @nocollapse */
    NbBasePicker.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: [NB_DOCUMENT,] },] },
        { type: NbPositionBuilderService, },
        { type: NbOverlayService, },
        { type: i0.ComponentFactoryResolver, },
    ]; };
    NbBasePicker.propDecorators = {
        "format": [{ type: i0.Input },],
        "boundingMonth": [{ type: i0.Input },],
        "startView": [{ type: i0.Input },],
        "min": [{ type: i0.Input },],
        "max": [{ type: i0.Input },],
        "filter": [{ type: i0.Input },],
        "dayCellComponent": [{ type: i0.Input },],
        "monthCellComponent": [{ type: i0.Input },],
        "yearCellComponent": [{ type: i0.Input },],
        "size": [{ type: i0.Input },],
        "visibleDate": [{ type: i0.Input },],
        "hideOnSelect": [{ type: i0.Input },],
    };
    return NbBasePicker;
}(NbDatepicker));
/**
 * The DatePicker components itself.
 * Provides a proxy to `NbCalendar` options as well as custom picker options.
 */
var NbDatepickerComponent = /** @class */ (function (_super) {
    __extends$12(NbDatepickerComponent, _super);
    function NbDatepickerComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pickerClass = NbCalendarComponent;
        return _this;
    }
    Object.defineProperty(NbDatepickerComponent.prototype, "date", {
        set: /**
           * Date which will be rendered as selected.
           * */
        function (date) {
            this.value = date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerComponent.prototype, "dateChange", {
        get: /**
           * Emits date when selected.
           * */
        function () {
            return this.valueChange;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerComponent.prototype, "value", {
        get: function () {
            return this.picker.date;
        },
        set: function (date) {
            if (!this.picker) {
                this.queue = date;
                return;
            }
            if (date) {
                this.picker.visibleDate = date;
                this.picker.date = date;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerComponent.prototype, "pickerValueChange", {
        get: function () {
            return this.picker.dateChange;
        },
        enumerable: true,
        configurable: true
    });
    NbDatepickerComponent.prototype.writeQueue = function () {
        this.value = this.queue;
    };
    NbDatepickerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-datepicker',
                    template: '',
                },] },
    ];
    /** @nocollapse */
    NbDatepickerComponent.propDecorators = {
        "date": [{ type: i0.Input },],
        "dateChange": [{ type: i0.Output },],
    };
    return NbDatepickerComponent;
}(NbBasePicker));
/**
 * The RangeDatePicker components itself.
 * Provides a proxy to `NbCalendarRange` options as well as custom picker options.
 */
var NbRangepickerComponent = /** @class */ (function (_super) {
    __extends$12(NbRangepickerComponent, _super);
    function NbRangepickerComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pickerClass = NbCalendarRangeComponent;
        return _this;
    }
    Object.defineProperty(NbRangepickerComponent.prototype, "range", {
        set: /**
           * Range which will be rendered as selected.
           * */
        function (range) {
            this.value = range;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRangepickerComponent.prototype, "rangeChange", {
        get: /**
           * Emits range when start selected and emits again when end selected.
           * */
        function () {
            return this.valueChange;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRangepickerComponent.prototype, "value", {
        get: function () {
            return this.picker.range;
        },
        set: function (range) {
            if (!this.picker) {
                this.queue = range;
                return;
            }
            if (range) {
                this.picker.visibleDate = range && range.start;
                this.picker.range = range;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRangepickerComponent.prototype, "pickerValueChange", {
        get: function () {
            return this.picker.rangeChange;
        },
        enumerable: true,
        configurable: true
    });
    NbRangepickerComponent.prototype.shouldHide = function () {
        return _super.prototype.shouldHide.call(this) && !!(this.value.start && this.value.end);
    };
    NbRangepickerComponent.prototype.writeQueue = function () {
        this.value = this.queue;
    };
    NbRangepickerComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-rangepicker',
                    template: '',
                },] },
    ];
    /** @nocollapse */
    NbRangepickerComponent.propDecorators = {
        "range": [{ type: i0.Input },],
        "rangeChange": [{ type: i0.Output },],
    };
    return NbRangepickerComponent;
}(NbBasePicker));

var __extends$13 = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbDateAdapterService = /** @class */ (function (_super) {
    __extends$13(NbDateAdapterService, _super);
    function NbDateAdapterService(dateService) {
        var _this = _super.call(this) || this;
        _this.dateService = dateService;
        _this.picker = NbDatepickerComponent;
        return _this;
    }
    NbDateAdapterService.prototype.parse = function (date, format) {
        return this.dateService.parse(date, format);
    };
    NbDateAdapterService.prototype.format = function (date, format) {
        return this.dateService.format(date, format);
    };
    NbDateAdapterService.prototype.isValid = function (date, format) {
        return this.dateService.isValidDateString(date, format);
    };
    NbDateAdapterService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbDateAdapterService.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    return NbDateAdapterService;
}(NbDatepickerAdapter));
var NbRangeAdapterService = /** @class */ (function (_super) {
    __extends$13(NbRangeAdapterService, _super);
    function NbRangeAdapterService(dateService) {
        var _this = _super.call(this) || this;
        _this.dateService = dateService;
        _this.picker = NbRangepickerComponent;
        return _this;
    }
    NbRangeAdapterService.prototype.parse = function (range, format) {
        var _a = range.split('-').map(function (subDate) { return subDate.trim(); }), start = _a[0], end = _a[1];
        return {
            start: this.dateService.parse(start, format),
            end: this.dateService.parse(end, format),
        };
    };
    NbRangeAdapterService.prototype.format = function (range, format) {
        if (!range) {
            return '';
        }
        var start = this.dateService.format(range.start, format);
        var end = this.dateService.format(range.end, format);
        if (end) {
            return start + " - " + end;
        }
        else {
            return start;
        }
    };
    NbRangeAdapterService.prototype.isValid = function (range, format) {
        var _a = range.split('-').map(function (subDate) { return subDate.trim(); }), start = _a[0], end = _a[1];
        return this.dateService.isValidDateString(start, format) && this.dateService.isValidDateString(end, format);
    };
    NbRangeAdapterService.decorators = [
        { type: i0.Injectable },
    ];
    /** @nocollapse */
    NbRangeAdapterService.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    return NbRangeAdapterService;
}(NbDatepickerAdapter));

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbDatepickerModule = /** @class */ (function () {
    function NbDatepickerModule() {
    }
    NbDatepickerModule.forRoot = function () {
        return {
            ngModule: NbDatepickerModule,
            providers: [
                i1.DatePipe,
                {
                    provide: NB_DATE_ADAPTER,
                    multi: true,
                    useClass: NbDateAdapterService,
                },
                {
                    provide: NB_DATE_ADAPTER,
                    multi: true,
                    useClass: NbRangeAdapterService,
                },
            ],
        };
    };
    NbDatepickerModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [NbOverlayModule, NbCalendarModule, NbCalendarRangeModule],
                    exports: [NbDatepickerDirective, NbDatepickerComponent, NbRangepickerComponent],
                    declarations: [NbDatepickerDirective, NbDatepickerContainerComponent, NbDatepickerComponent, NbRangepickerComponent],
                    entryComponents: [NbCalendarComponent, NbCalendarRangeComponent, NbDatepickerContainerComponent],
                },] },
    ];
    return NbDatepickerModule;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbRadioComponent` provides the same functionality as native `<input type="radio">`
 * with Nebular styles and animations.
 *
 * @stacked-example(Showcase, radio/radio-showcase.component)
 *
 * ### Installation
 *
 * Import `NbRadioModule` to your feature module.
 *
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbRadioModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 *
 * ### Usage
 *
 * Radio buttons should be wrapped in `nb-radio-group` to provide form bindings.
 *
 * ```html
 * <nb-radio-group [(ngModule)]="selectedOption">
 *   <nb-radio>Option 1</nb-radio>
 *   <nb-radio>Option 2</nb-radio>
 *   <nb-radio>Option 3</nb-radio>
 * </nb-radio-group>
 * ```
 *
 * You can disable some radios in the group using a `disabled` attribute.
 *
 * @stacked-example(Disabled, radio/radio-disabled.component)
 *
 *
 * @styles
 *
 * radio-bg
 * radio-fg
 * radio-size
 * radio-border-size
 * radio-border-color
 * radio-checkmark
 * radio-checked-bg
 * radio-checked-size
 * radio-checked-border-size
 * radio-checked-border-color
 * radio-checked-checkmark
 * radio-disabled-bg
 * radio-disabled-size
 * radio-disabled-border-size
 * radio-disabled-border-color
 * radio-disabled-checkmark
 * */
var NbRadioComponent = /** @class */ (function () {
    function NbRadioComponent(cd) {
        this.cd = cd;
        this.valueChange = new i0.EventEmitter();
    }
    Object.defineProperty(NbRadioComponent.prototype, "setDisabled", {
        set: function (disabled) {
            this.disabled = convertToBoolProperty(disabled);
        },
        enumerable: true,
        configurable: true
    });
    NbRadioComponent.prototype.markForCheck = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NbRadioComponent.prototype.onChange = function (event) {
        event.stopPropagation();
        this.checked = true;
        this.valueChange.emit(this.value);
    };
    NbRadioComponent.prototype.onClick = function (event) {
        event.stopPropagation();
    };
    NbRadioComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-radio',
                    template: "\n    <label>\n      <input\n        type=\"radio\"\n        [name]=\"name\"\n        [value]=\"value\"\n        [checked]=\"checked\"\n        [disabled]=\"disabled\"\n        (change)=\"onChange($event)\"\n        (click)=\"onClick($event)\">\n      <span class=\"radio-indicator\"></span>\n      <span class=\"radio-description\">\n        <ng-content></ng-content>\n      </span>\n    </label>\n  ",
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                    styles: [":host{display:block}:host label{position:relative;display:inline-flex;margin:0;min-height:inherit;padding:0.375rem 1.5rem 0.375rem 0}:host input{position:absolute;opacity:0}:host input:disabled+.radio-indicator,:host input:disabled ~ .radio-description{opacity:0.5}:host .radio-indicator{border-radius:50%;flex-shrink:0;display:flex;justify-content:center;align-items:center}:host .radio-indicator::before{content:'';transition:all 0.1s;border-radius:50%}[dir=ltr] :host .radio-description{padding-left:.5rem}[dir=rtl] :host .radio-description{padding-right:.5rem} "],
                },] },
    ];
    /** @nocollapse */
    NbRadioComponent.ctorParameters = function () { return [
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbRadioComponent.propDecorators = {
        "name": [{ type: i0.Input },],
        "checked": [{ type: i0.Input },],
        "value": [{ type: i0.Input },],
        "setDisabled": [{ type: i0.Input, args: ['disabled',] },],
        "valueChange": [{ type: i0.Output },],
    };
    return NbRadioComponent;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
/**
 * The `NbRadioGroupComponent` is the wrapper for `nb-radio` button.
 * It provides form bindings:
 *
 * ```html
 * <nb-radio-group [(ngModel)]="selectedOption">
 *   <nb-radio>Option 1</nb-radio>
 *   <nb-radio>Option 2</nb-radio>
 *   <nb-radio>Option 3</nb-radio>
 * </nb-radio-group>
 * ```
 *
 * Also, you can use `value` and `valueChange` for binding without forms.
 *
 * ```html
 * <nb-radio-group [(value)]="selectedOption">
 *   <nb-radio>Option 1</nb-radio>
 *   <nb-radio>Option 2</nb-radio>
 *   <nb-radio>Option 3</nb-radio>
 * </nb-radio-group>
 * ```
 *
 * Radio items name has to be provided through `name` input property of the radio group.
 *
 * ```html
 * <nb-radio-group name="my-radio-group">
 *   ...
 * </nb-radio-group>
 * ```
 *
 * Also, you can disable the whole group using `disabled` attribute.
 *
 * ```html
 * <nb-radio-group disabled>
 *   ...
 * </nb-radio-group>
 * ```
 * */
var NbRadioGroupComponent = /** @class */ (function () {
    function NbRadioGroupComponent(cd) {
        this.cd = cd;
        this.valueChange = new i0.EventEmitter();
        this.alive = true;
        this.onChange = function (value) { };
    }
    Object.defineProperty(NbRadioGroupComponent.prototype, "setValue", {
        set: function (value) {
            this.value = value;
            this.updateValues();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRadioGroupComponent.prototype, "setName", {
        set: function (name) {
            this.name = name;
            this.updateNames();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRadioGroupComponent.prototype, "setDisabled", {
        set: function (disabled) {
            this.disabled = convertToBoolProperty(disabled);
            this.updateDisabled();
        },
        enumerable: true,
        configurable: true
    });
    NbRadioGroupComponent.prototype.ngAfterContentInit = function () {
        this.updateNames();
        this.updateValues();
        this.updateDisabled();
        this.subscribeOnRadiosValueChange();
    };
    NbRadioGroupComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbRadioGroupComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    NbRadioGroupComponent.prototype.registerOnTouched = function (fn) {
    };
    NbRadioGroupComponent.prototype.writeValue = function (value) {
        this.value = value;
        if (typeof value !== 'undefined') {
            this.updateValues();
        }
    };
    NbRadioGroupComponent.prototype.updateNames = function () {
        var _this = this;
        if (this.radios) {
            this.radios.forEach(function (radio) { return radio.name = _this.name; });
            this.markRadiosForCheck();
        }
    };
    NbRadioGroupComponent.prototype.updateValues = function () {
        var _this = this;
        if (this.radios && typeof this.value !== 'undefined') {
            this.radios.forEach(function (radio) { return radio.checked = radio.value === _this.value; });
            this.markRadiosForCheck();
        }
    };
    NbRadioGroupComponent.prototype.updateDisabled = function () {
        var _this = this;
        if (this.radios && typeof this.disabled !== 'undefined') {
            this.radios.forEach(function (radio) { return radio.setDisabled = _this.disabled; });
            this.markRadiosForCheck();
        }
    };
    NbRadioGroupComponent.prototype.subscribeOnRadiosValueChange = function () {
        var _this = this;
        rxjs.merge.apply(void 0, this.radios.map(function (radio) { return radio.valueChange; })).pipe(rxjs_operators.takeWhile(function () { return _this.alive; }))
            .subscribe(function (value) {
            _this.writeValue(value);
            _this.propagateValue(value);
        });
    };
    NbRadioGroupComponent.prototype.propagateValue = function (value) {
        this.valueChange.emit(value);
        this.onChange(value);
    };
    NbRadioGroupComponent.prototype.markRadiosForCheck = function () {
        this.radios.forEach(function (radio) { return radio.markForCheck(); });
    };
    NbRadioGroupComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'nb-radio-group',
                    template: "\n    <ng-content select=\"nb-radio\"></ng-content>",
                    providers: [
                        {
                            provide: _angular_forms.NG_VALUE_ACCESSOR,
                            useExisting: i0.forwardRef(function () { return NbRadioGroupComponent; }),
                            multi: true,
                        },
                    ],
                    changeDetection: i0.ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbRadioGroupComponent.ctorParameters = function () { return [
        { type: i0.ChangeDetectorRef, },
    ]; };
    NbRadioGroupComponent.propDecorators = {
        "radios": [{ type: i0.ContentChildren, args: [NbRadioComponent, { descendants: true },] },],
        "setValue": [{ type: i0.Input, args: ['value',] },],
        "setName": [{ type: i0.Input, args: ['name',] },],
        "setDisabled": [{ type: i0.Input, args: ['disabled',] },],
        "valueChange": [{ type: i0.Output },],
    };
    return NbRadioGroupComponent;
}());

/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
var NbRadioModule = /** @class */ (function () {
    function NbRadioModule() {
    }
    NbRadioModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [],
                    exports: [NbRadioComponent, NbRadioGroupComponent],
                    declarations: [NbRadioComponent, NbRadioGroupComponent],
                },] },
    ];
    return NbRadioModule;
}());

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

exports.NbMenuService = NbMenuService;
exports.NbMenuItem = NbMenuItem;
exports.NB_THEME_OPTIONS = NB_THEME_OPTIONS;
exports.NB_MEDIA_BREAKPOINTS = NB_MEDIA_BREAKPOINTS;
exports.NB_BUILT_IN_JS_THEMES = NB_BUILT_IN_JS_THEMES;
exports.NB_JS_THEMES = NB_JS_THEMES;
exports.NB_WINDOW = NB_WINDOW;
exports.NB_DOCUMENT = NB_DOCUMENT;
exports.nbWindowFactory = nbWindowFactory;
exports.NbThemeModule = NbThemeModule;
exports.NbThemeService = NbThemeService;
exports.NbSpinnerService = NbSpinnerService;
exports.DEFAULT_MEDIA_BREAKPOINTS = DEFAULT_MEDIA_BREAKPOINTS;
exports.NbMediaBreakpointsService = NbMediaBreakpointsService;
exports.NbColorHelper = NbColorHelper;
exports.NB_LAYOUT_DIRECTION = NB_LAYOUT_DIRECTION;
exports.NbLayoutDirectionService = NbLayoutDirectionService;
exports.NbLayoutScrollService = NbLayoutScrollService;
exports.NbLayoutRulerService = NbLayoutRulerService;
exports.NbCardModule = NbCardModule;
exports.NbCalendarModule = NbCalendarModule;
exports.NbCalendarComponent = NbCalendarComponent;
exports.NbCalendarRangeModule = NbCalendarRangeModule;
exports.NbCalendarRangeComponent = NbCalendarRangeComponent;
exports.NbCalendarHeaderComponent = NbCalendarHeaderComponent;
exports.NbCalendarDayCellComponent = NbCalendarDayCellComponent;
exports.NbCalendarYearPickerComponent = NbCalendarYearPickerComponent;
exports.NbCalendarMonthPickerComponent = NbCalendarMonthPickerComponent;
exports.NbCalendarDayPickerComponent = NbCalendarDayPickerComponent;
exports.NbCalendarNavigationComponent = NbCalendarNavigationComponent;
exports.NbCalendarPageableNavigationComponent = NbCalendarPageableNavigationComponent;
exports.NbCalendarDaysNamesComponent = NbCalendarDaysNamesComponent;
exports.NbCalendarMonthCellComponent = NbCalendarMonthCellComponent;
exports.NbCalendarYearCellComponent = NbCalendarYearCellComponent;
exports.NbCalendarPickerRowComponent = NbCalendarPickerRowComponent;
exports.NbCalendarPickerComponent = NbCalendarPickerComponent;
exports.NbCalendarDatePipe = NbCalendarDatePipe;
exports.NbCalendarMonthModelService = NbCalendarMonthModelService;
exports.NbNativeDateService = NbNativeDateService;
exports.NbDateService = NbDateService;
exports.NbCalendarKitModule = NbCalendarKitModule;
exports.NbLayoutModule = NbLayoutModule;
exports.NbRestoreScrollTopHelper = NbRestoreScrollTopHelper;
exports.NbMenuModule = NbMenuModule;
exports.NbRouteTabsetModule = NbRouteTabsetModule;
exports.NbSidebarModule = NbSidebarModule;
exports.NbSidebarService = NbSidebarService;
exports.NbTabsetModule = NbTabsetModule;
exports.NbUserModule = NbUserModule;
exports.NbActionsModule = NbActionsModule;
exports.NbSearchModule = NbSearchModule;
exports.NbSearchService = NbSearchService;
exports.NbCheckboxComponent = NbCheckboxComponent;
exports.NbCheckboxModule = NbCheckboxModule;
exports.NbBadgeComponent = NbBadgeComponent;
exports.NbBadgeModule = NbBadgeModule;
exports.NbPopoverDirective = NbPopoverDirective;
exports.NbPopoverModule = NbPopoverModule;
exports.NbContextMenuDirective = NbContextMenuDirective;
exports.NbContextMenuModule = NbContextMenuModule;
exports.NbProgressBarComponent = NbProgressBarComponent;
exports.NbProgressBarModule = NbProgressBarModule;
exports.NbAlertComponent = NbAlertComponent;
exports.NbAlertModule = NbAlertModule;
exports.NbChatComponent = NbChatComponent;
exports.NbChatMessageComponent = NbChatMessageComponent;
exports.NbChatMessageMapComponent = NbChatMessageMapComponent;
exports.NbChatMessageFileComponent = NbChatMessageFileComponent;
exports.NbChatMessageQuoteComponent = NbChatMessageQuoteComponent;
exports.NbChatMessageTextComponent = NbChatMessageTextComponent;
exports.NbChatFormComponent = NbChatFormComponent;
exports.NbChatModule = NbChatModule;
exports.NbSpinnerComponent = NbSpinnerComponent;
exports.NbSpinnerDirective = NbSpinnerDirective;
exports.NbSpinnerModule = NbSpinnerModule;
exports.NbStepperComponent = NbStepperComponent;
exports.NbStepperModule = NbStepperModule;
exports.NbAccordionComponent = NbAccordionComponent;
exports.NbAccordionItemComponent = NbAccordionItemComponent;
exports.NbAccordionItemBodyComponent = NbAccordionItemBodyComponent;
exports.NbAccordionItemHeaderComponent = NbAccordionItemHeaderComponent;
exports.NbAccordionModule = NbAccordionModule;
exports.NbButtonComponent = NbButtonComponent;
exports.NbButtonModule = NbButtonModule;
exports.NbListComponent = NbListComponent;
exports.NbListItemComponent = NbListItemComponent;
exports.NbListModule = NbListModule;
exports.NbListPageTrackerDirective = NbListPageTrackerDirective;
exports.NbScrollableContainerDimentions = NbScrollableContainerDimentions;
exports.NbInfiniteListDirective = NbInfiniteListDirective;
exports.NbInputDirective = NbInputDirective;
exports.NbInputModule = NbInputModule;
exports.NbOverlayModule = NbOverlayModule;
exports.patch = patch;
exports.createContainer = createContainer;
exports.NbOverlayService = NbOverlayService;
exports.NbAdjustableConnectedPositionStrategy = NbAdjustableConnectedPositionStrategy;
exports.NbGlobalPositionStrategy = NbGlobalPositionStrategy;
exports.NbPositionBuilderService = NbPositionBuilderService;
exports.NbPositionedContainer = NbPositionedContainer;
exports.NbOverlayContainerComponent = NbOverlayContainerComponent;
exports.NbTriggerStrategy = NbTriggerStrategy;
exports.NbClickTriggerStrategy = NbClickTriggerStrategy;
exports.NbHoverTriggerStrategy = NbHoverTriggerStrategy;
exports.NbHintTriggerStrategy = NbHintTriggerStrategy;
exports.NbFocusTriggerStrategy = NbFocusTriggerStrategy;
exports.NbTriggerStrategyBuilder = NbTriggerStrategyBuilder;
exports.NbPortalDirective = NbPortalDirective;
exports.NbPortalOutletDirective = NbPortalOutletDirective;
exports.NbComponentPortal = NbComponentPortal;
exports.NbDomPortalOutlet = NbDomPortalOutlet;
exports.NbOverlay = NbOverlay;
exports.NbPlatform = NbPlatform;
exports.NbOverlayPositionBuilder = NbOverlayPositionBuilder;
exports.NbTemplatePortal = NbTemplatePortal;
exports.NbOverlayContainer = NbOverlayContainer;
exports.NbFlexibleConnectedPositionStrategy = NbFlexibleConnectedPositionStrategy;
exports.NbPortalInjector = NbPortalInjector;
exports.NbCdkMappingModule = NbCdkMappingModule;
exports.NbPositionHelper = NbPositionHelper;
exports.NB_DIALOG_CONFIG = NB_DIALOG_CONFIG;
exports.NbDialogConfig = NbDialogConfig;
exports.NbDialogRef = NbDialogRef;
exports.NbDialogService = NbDialogService;
exports.NbDialogModule = NbDialogModule;
exports.NbToastrModule = NbToastrModule;
exports.NbToastContainer = NbToastContainer;
exports.NbToastrContainerRegistry = NbToastrContainerRegistry;
exports.NbToastrService = NbToastrService;
exports.NbTooltipModule = NbTooltipModule;
exports.NbTooltipDirective = NbTooltipDirective;
exports.NbSelectModule = NbSelectModule;
exports.NbWindowConfig = NbWindowConfig;
exports.NB_WINDOW_CONFIG = NB_WINDOW_CONFIG;
exports.NbWindowModule = NbWindowModule;
exports.NbWindowService = NbWindowService;
exports.NbWindowRef = NbWindowRef;
exports.NbDatepickerModule = NbDatepickerModule;
exports.NbDatepickerAdapter = NbDatepickerAdapter;
exports.NbDatepicker = NbDatepicker;
exports.NB_DATE_ADAPTER = NB_DATE_ADAPTER;
exports.NbDatepickerDirective = NbDatepickerDirective;
exports.NbRadioModule = NbRadioModule;

Object.defineProperty(exports, '__esModule', { value: true });

})));

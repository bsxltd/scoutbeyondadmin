/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { NbRadioComponent } from './radio.component';
import { NbRadioGroupComponent } from './radio-group.component';
var NbRadioModule = /** @class */ (function () {
    function NbRadioModule() {
    }
    NbRadioModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    exports: [NbRadioComponent, NbRadioGroupComponent],
                    declarations: [NbRadioComponent, NbRadioGroupComponent],
                },] },
    ];
    return NbRadioModule;
}());
export { NbRadioModule };
//# sourceMappingURL=radio.module.js.map
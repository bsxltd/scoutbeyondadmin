import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, } from '@angular/core';
import { NbDateService } from '../calendar-kit';
var NbCalendarRangeDayCellComponent = /** @class */ (function () {
    function NbCalendarRangeDayCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new EventEmitter(true);
    }
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "inRange", {
        get: function () {
            return this.date && this.selectedValue
                && (this.selectedValue.start && this.dateService.compareDates(this.date, this.selectedValue.start) >= 0)
                && (this.selectedValue.end && this.dateService.compareDates(this.date, this.selectedValue.end) <= 0);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "start", {
        get: function () {
            return this.date && this.selectedValue && this.selectedValue.end
                && (this.selectedValue.start && this.dateService.isSameDay(this.date, this.selectedValue.start));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "end", {
        get: function () {
            return this.date && this.selectedValue &&
                (this.selectedValue.end && this.dateService.isSameDay(this.date, this.selectedValue.end));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "today", {
        get: function () {
            return this.date && this.dateService.isSameDay(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "boundingMonth", {
        get: function () {
            return !this.dateService.isSameMonthSafe(this.date, this.visibleDate);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "selected", {
        get: function () {
            return this.date && this.selectedValue
                && (this.selectedValue.start && this.dateService.isSameDay(this.date, this.selectedValue.start)) || this.end;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "empty", {
        get: function () {
            return !this.date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax() || this.dontFitFilter();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeDayCellComponent.prototype, "day", {
        get: function () {
            return this.date && this.dateService.getDate(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeDayCellComponent.prototype.onClick = function () {
        if (this.disabled || this.empty) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarRangeDayCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.date, this.min) < 0;
    };
    NbCalendarRangeDayCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.date, this.max) > 0;
    };
    NbCalendarRangeDayCellComponent.prototype.dontFitFilter = function () {
        return this.date && this.filter && !this.filter(this.date);
    };
    NbCalendarRangeDayCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-range-day-cell',
                    template: "\n    <div\n      class=\"day-cell\"\n      [class.today]=\"today\"\n      [class.selected]=\"selected\"\n      [class.bounding-month]=\"boundingMonth\"\n      [class.start]=\"start\"\n      [class.end]=\"end\"\n      [class.in-range]=\"inRange\"\n      [class.disabled]=\"disabled\">\n      {{ day }}\n    </div>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    host: { '(click)': 'onClick()', 'class': 'range-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarRangeDayCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarRangeDayCellComponent.propDecorators = {
        "date": [{ type: Input },],
        "selectedValue": [{ type: Input },],
        "visibleDate": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "filter": [{ type: Input },],
        "select": [{ type: Output },],
        "inRange": [{ type: HostBinding, args: ['class.in-range',] },],
        "start": [{ type: HostBinding, args: ['class.start',] },],
        "end": [{ type: HostBinding, args: ['class.end',] },],
    };
    return NbCalendarRangeDayCellComponent;
}());
export { NbCalendarRangeDayCellComponent };
var NbCalendarRangeYearCellComponent = /** @class */ (function () {
    function NbCalendarRangeYearCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new EventEmitter(true);
    }
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "selected", {
        get: function () {
            return this.selectedValue && this.dateService.isSameYear(this.date, this.selectedValue.start);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "today", {
        get: function () {
            return this.date && this.dateService.isSameYear(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarRangeYearCellComponent.prototype, "year", {
        get: function () {
            return this.dateService.getYear(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarRangeYearCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarRangeYearCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.yearEnd(), this.min) < 0;
    };
    NbCalendarRangeYearCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.yearStart(), this.max) > 0;
    };
    NbCalendarRangeYearCellComponent.prototype.yearStart = function () {
        return this.dateService.getYearStart(this.date);
    };
    NbCalendarRangeYearCellComponent.prototype.yearEnd = function () {
        return this.dateService.getYearEnd(this.date);
    };
    NbCalendarRangeYearCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-range-year-cell',
                    template: "{{ year }}",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'year-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarRangeYearCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarRangeYearCellComponent.propDecorators = {
        "date": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "selectedValue": [{ type: Input },],
        "select": [{ type: Output },],
        "selected": [{ type: HostBinding, args: ['class.selected',] },],
        "today": [{ type: HostBinding, args: ['class.today',] },],
        "disabled": [{ type: HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbCalendarRangeYearCellComponent;
}());
export { NbCalendarRangeYearCellComponent };
//# sourceMappingURL=calendar-range-cells.js.map
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbCalendarKitModule } from '../calendar-kit';
import { NbCardModule } from '../card/card.module';
import { NbBaseCalendarComponent } from './base-calendar.component';
var NbBaseCalendarModule = /** @class */ (function () {
    function NbBaseCalendarModule() {
    }
    NbBaseCalendarModule.decorators = [
        { type: NgModule, args: [{
                    imports: [NbCalendarKitModule, NbSharedModule, NbCardModule],
                    exports: [NbBaseCalendarComponent],
                    declarations: [NbBaseCalendarComponent],
                },] },
    ];
    return NbBaseCalendarModule;
}());
export { NbBaseCalendarModule };
//# sourceMappingURL=base-calendar.module.js.map
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { NbCalendarComponent } from './calendar.component';
import { NbBaseCalendarModule } from './base-calendar.module';
var NbCalendarModule = /** @class */ (function () {
    function NbCalendarModule() {
    }
    NbCalendarModule.decorators = [
        { type: NgModule, args: [{
                    imports: [NbBaseCalendarModule],
                    exports: [NbCalendarComponent],
                    declarations: [NbCalendarComponent],
                },] },
    ];
    return NbCalendarModule;
}());
export { NbCalendarModule };
//# sourceMappingURL=calendar.module.js.map
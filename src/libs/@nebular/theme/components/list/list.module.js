import { NgModule } from '@angular/core';
import { NbListComponent, NbListItemComponent } from './list.component';
import { NbListPageTrackerDirective } from './list-page-tracker.directive';
import { NbInfiniteListDirective } from './infinite-list.directive';
var components = [
    NbListComponent,
    NbListItemComponent,
    NbListPageTrackerDirective,
    NbInfiniteListDirective,
];
var NbListModule = /** @class */ (function () {
    function NbListModule() {
    }
    NbListModule.decorators = [
        { type: NgModule, args: [{
                    declarations: components,
                    exports: components,
                },] },
    ];
    return NbListModule;
}());
export { NbListModule };
//# sourceMappingURL=list.module.js.map
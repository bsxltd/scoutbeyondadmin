import { Component, Input, HostBinding } from '@angular/core';
/**
 * List is a container component that wraps `nb-list-item` component.
 *
 * Basic example:
 * @stacked-example(Simple list, list/simple-list-showcase.component)
 *
 * `nb-list-item` accepts arbitrary content, so you can create a list of any components.
 *
 * ### Installation
 *
 * Import `NbListModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbListModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * List of users:
 * @stacked-example(Users list, list/users-list-showcase.component)
 *
 * @styles
 *
 * list-item-border-color:
 * list-item-padding:
 */
var NbListComponent = /** @class */ (function () {
    function NbListComponent() {
        /**
           * Role attribute value
           *
           * @type {string}
           */
        this.role = 'list';
    }
    NbListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-list',
                    template: "<ng-content select=\"nb-list-item\"></ng-content>",
                    styles: [":host{display:flex;flex-direction:column;flex:1 1 auto;overflow:auto} "],
                },] },
    ];
    /** @nocollapse */
    NbListComponent.propDecorators = {
        "role": [{ type: Input }, { type: HostBinding, args: ['attr.role',] },],
    };
    return NbListComponent;
}());
export { NbListComponent };
/**
 * List item component is a grouping component that accepts arbitrary content.
 * It should be direct child of `nb-list` componet.
 */
var NbListItemComponent = /** @class */ (function () {
    function NbListItemComponent() {
        /**
           * Role attribute value
           *
           * @type {string}
           */
        this.role = 'listitem';
    }
    NbListItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-list-item',
                    template: "<ng-content></ng-content>",
                    styles: [":host{flex-shrink:0} "],
                },] },
    ];
    /** @nocollapse */
    NbListItemComponent.propDecorators = {
        "role": [{ type: Input }, { type: HostBinding, args: ['attr.role',] },],
    };
    return NbListItemComponent;
}());
export { NbListItemComponent };
//# sourceMappingURL=list.component.js.map
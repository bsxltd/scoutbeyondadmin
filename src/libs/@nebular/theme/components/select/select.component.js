/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChild, ContentChildren, ElementRef, EventEmitter, forwardRef, Inject, Input, Output, QueryList, ViewChild, } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { take, takeWhile } from 'rxjs/operators';
import { defer, merge } from 'rxjs';
import { NbAdjustment, NbOverlayService, NbPortalDirective, NbPosition, NbPositionBuilderService, NbTrigger, NbTriggerStrategyBuilder, } from '../cdk';
import { NbOptionComponent } from './option.component';
import { NB_DOCUMENT } from '../../theme.options';
import { convertToBoolProperty } from '../helpers';
var NbSelectLabelComponent = /** @class */ (function () {
    function NbSelectLabelComponent() {
    }
    NbSelectLabelComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-select-label',
                    template: '<ng-content></ng-content>',
                },] },
    ];
    return NbSelectLabelComponent;
}());
export { NbSelectLabelComponent };
/**
 * The `NbSelectComponent` provides a capability to select one of the passed items.
 *
 * @stacked-example(Showcase, select/select-showcase.component)
 *
 * ### Installation
 *
 * Import `NbSelectModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbSelectModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * If you want to use it as the multi-select control you have to mark it as `multiple`.
 * In this case, `nb-select` will work only with arrays - accept arrays and propagate arrays.
 *
 * @stacked-example(Multiple, select/select-multiple.component)
 *
 * Items without values will clean selection.
 *
 * @stacked-example(Clean selection, select/select-clean.component)
 *
 * Select may be bounded using `selected` input:
 *
 * ```html
 * <nb-select [(selected)]="selected"></nb-selected>
 * ```
 *
 * Or you can bind control with form controls or ngModel:
 *
 * @stacked-example(Select form binding, select/select-form.component)
 *
 * Options in the select may be grouped using `nb-option-group` component.
 *
 * @stacked-example(Grouping, select/select-groups.component)
 *
 * Select may have a placeholder that will be shown when nothing selected:
 *
 * @stacked-example(Placeholder, select/select-placeholder.component)
 *
 * You can disable select, options and whole groups.
 *
 * @stacked-example(Disabled select, select/select-disabled.component)
 *
 * Also, the custom label may be provided in select.
 * This custom label will be used for instead placeholder when something selected.
 *
 * @stacked-example(Custom label, select/select-label.component)
 *
 * Default `nb-select` size is `medium` and status color is `primary`.
 * Select is available in multiple colors using `status` property:
 *
 * @stacked-example(Select statuses, select/select-status.component)
 *
 * There are three select sizes:
 *
 * @stacked-example(Select sizes, select/select-sizes.component)
 *
 * And two additional style types - `outline`:
 *
 * @stacked-example(Outline select, select/select-outline.component)
 *
 * and `hero`:
 *
 * @stacked-example(Select colors, select/select-hero.component)
 *
 * Select is available in different shapes, that could be combined with the other properties:
 *
 * @stacked-example(Select shapes, select/select-shapes.component)
 *
 *
 * @styles
 *
 * select-border-width:
 * select-max-height:
 * select-bg:
 * select-checkbox-color:
 * select-checkmark-color:
 * select-option-disabled-bg:
 * select-option-disabled-opacity:
 * select-option-padding:
 * */
var NbSelectComponent = /** @class */ (function () {
    function NbSelectComponent(document, overlay, hostRef, positionBuilder, cd) {
        var _this = this;
        this.document = document;
        this.overlay = overlay;
        this.hostRef = hostRef;
        this.positionBuilder = positionBuilder;
        this.cd = cd;
        /**
           * Select status (adds specific styles):
           * `primary`, `info`, `success`, `warning`, `danger`
           */
        this.status = 'primary';
        /**
           * Renders select placeholder if nothing selected.
           * */
        this.placeholder = '';
        /**
           * Will be emitted when selected value changes.
           * */
        this.selectedChange = new EventEmitter();
        this.multiple = false;
        /**
           * List of selected options.
           * */
        this.selectionModel = [];
        /**
           * Current overlay position because of we have to toggle overlayPosition
           * in [ngClass] direction and this directive can use only string.
           */
        this.overlayPosition = '';
        /**
           * Stream of events that will fire when one of the options fire selectionChange event.
           * */
        this.selectionChange = defer(function () {
            return merge.apply(void 0, _this.options.map(function (it) { return it.selectionChange; }));
        });
        this.alive = true;
        /**
           * Function passed through control value accessor to propagate changes.
           * */
        this.onChange = function () { };
    }
    Object.defineProperty(NbSelectComponent.prototype, "setSelected", {
        set: /**
           * Accepts selected item or array of selected items.
           * */
        function (value) {
            this.writeValue(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "setMultiple", {
        set: /**
           * Gives capability just write `multiple` over the element.
           * */
        function (multiple) {
            this.multiple = convertToBoolProperty(multiple);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "isOpened", {
        /**
         * Determines is select opened.
         * */
        get: /**
           * Determines is select opened.
           * */
        function () {
            return this.ref && this.ref.hasAttached();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "isHidden", {
        /**
         * Determines is select hidden.
         * */
        get: /**
           * Determines is select hidden.
           * */
        function () {
            return !this.isOpened;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "hostWidth", {
        /**
         * Returns width of the select button.
         * */
        get: /**
           * Returns width of the select button.
           * */
        function () {
            return this.hostRef.nativeElement.getBoundingClientRect().width;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSelectComponent.prototype, "selectionView", {
        /**
         * Content rendered in the label.
         * */
        get: /**
           * Content rendered in the label.
           * */
        function () {
            if (this.selectionModel.length > 1) {
                return this.selectionModel.map(function (option) { return option.content; }).join(', ');
            }
            return this.selectionModel[0].content;
        },
        enumerable: true,
        configurable: true
    });
    NbSelectComponent.prototype.ngOnInit = function () {
        this.createOverlay();
    };
    NbSelectComponent.prototype.ngAfterViewInit = function () {
        this.subscribeOnTriggers();
        this.subscribeOnPositionChange();
        this.subscribeOnSelectionChange();
    };
    NbSelectComponent.prototype.ngAfterContentInit = function () {
        if (this.queue) {
            this.writeValue(this.queue);
        }
    };
    NbSelectComponent.prototype.ngOnDestroy = function () {
        this.ref.dispose();
    };
    NbSelectComponent.prototype.show = function () {
        if (this.isHidden) {
            this.ref.attach(this.portal);
            this.cd.markForCheck();
        }
    };
    NbSelectComponent.prototype.hide = function () {
        if (this.isOpened) {
            this.ref.detach();
            this.cd.markForCheck();
        }
    };
    NbSelectComponent.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    NbSelectComponent.prototype.registerOnTouched = function (fn) {
    };
    NbSelectComponent.prototype.setDisabledState = function (isDisabled) {
    };
    NbSelectComponent.prototype.writeValue = function (value) {
        if (!value) {
            return;
        }
        if (this.options) {
            this.setSelection(value);
        }
        else {
            this.queue = value;
        }
    };
    /**
     * Selects option or clear all selected options if value is null.
     * */
    /**
       * Selects option or clear all selected options if value is null.
       * */
    NbSelectComponent.prototype.handleSelect = /**
       * Selects option or clear all selected options if value is null.
       * */
    function (option) {
        if (option.value) {
            this.selectOption(option);
        }
        else {
            this.reset();
        }
        this.cd.detectChanges();
    };
    /**
     * Deselect all selected options.
     * */
    /**
       * Deselect all selected options.
       * */
    NbSelectComponent.prototype.reset = /**
       * Deselect all selected options.
       * */
    function () {
        this.selectionModel.forEach(function (option) { return option.deselect(); });
        this.selectionModel = [];
        this.hide();
        this.emitSelected(null);
    };
    /**
     * Determines how to select option as multiple or single.
     * */
    /**
       * Determines how to select option as multiple or single.
       * */
    NbSelectComponent.prototype.selectOption = /**
       * Determines how to select option as multiple or single.
       * */
    function (option) {
        if (this.multiple) {
            this.handleMultipleSelect(option);
        }
        else {
            this.handleSingleSelect(option);
        }
    };
    /**
     * Select single option.
     * */
    /**
       * Select single option.
       * */
    NbSelectComponent.prototype.handleSingleSelect = /**
       * Select single option.
       * */
    function (option) {
        var selected = this.selectionModel.pop();
        if (selected && selected !== option) {
            selected.deselect();
        }
        this.selectionModel = [option];
        option.select();
        this.hide();
        this.emitSelected(option.value);
    };
    /**
     * Select for multiple options.
     * */
    /**
       * Select for multiple options.
       * */
    NbSelectComponent.prototype.handleMultipleSelect = /**
       * Select for multiple options.
       * */
    function (option) {
        if (option.selected) {
            this.selectionModel = this.selectionModel.filter(function (s) { return s.value !== option.value; });
            option.deselect();
        }
        else {
            this.selectionModel.push(option);
            option.select();
        }
        this.emitSelected(this.selectionModel.map(function (opt) { return opt.value; }));
    };
    NbSelectComponent.prototype.createOverlay = function () {
        var scrollStrategy = this.createScrollStrategy();
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({ positionStrategy: this.positionStrategy, scrollStrategy: scrollStrategy });
    };
    NbSelectComponent.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(NbPosition.BOTTOM)
            .offset(0)
            .adjustment(NbAdjustment.VERTICAL);
    };
    NbSelectComponent.prototype.createScrollStrategy = function () {
        return this.overlay.scrollStrategies.block();
    };
    NbSelectComponent.prototype.subscribeOnTriggers = function () {
        var _this = this;
        var triggerStrategy = new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(NbTrigger.CLICK)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.getContainer(); })
            .build();
        triggerStrategy.show$
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.show(); });
        triggerStrategy.hide$
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.hide(); });
    };
    NbSelectComponent.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return _this.overlayPosition = position; });
        this.positionStrategy.positionChange
            .pipe(take(1))
            .subscribe(function () { return _this.cd.detectChanges(); });
    };
    NbSelectComponent.prototype.subscribeOnSelectionChange = function () {
        var _this = this;
        this.selectionChange
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function (option) { return _this.handleSelect(option); });
    };
    NbSelectComponent.prototype.getContainer = function () {
        return this.ref && this.ref.hasAttached() && {
            location: {
                nativeElement: this.ref.overlayElement,
            },
        };
    };
    /**
     * Propagate selected value.
     * */
    /**
       * Propagate selected value.
       * */
    NbSelectComponent.prototype.emitSelected = /**
       * Propagate selected value.
       * */
    function (selected) {
        this.onChange(selected);
        this.selectedChange.emit(selected);
    };
    /**
     * Set selected value in model.
     * */
    /**
       * Set selected value in model.
       * */
    NbSelectComponent.prototype.setSelection = /**
       * Set selected value in model.
       * */
    function (value) {
        var _this = this;
        var isArray = Array.isArray(value);
        if (this.multiple && !isArray) {
            throw new Error('Can\'t assign single value if select is marked as multiple');
        }
        if (!this.multiple && isArray) {
            throw new Error('Can\'t assign array if select is not marked as multiple');
        }
        this.cleanSelection();
        if (isArray) {
            value.forEach(function (option) { return _this.selectValue(option); });
        }
        else {
            this.selectValue(value);
        }
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NbSelectComponent.prototype.cleanSelection = function () {
        this.selectionModel.forEach(function (option) { return option.deselect(); });
        this.selectionModel = [];
    };
    /**
     * Selects value.
     * */
    /**
       * Selects value.
       * */
    NbSelectComponent.prototype.selectValue = /**
       * Selects value.
       * */
    function (value) {
        var corresponding = this.options.find(function (option) { return option.value === value; });
        if (corresponding) {
            corresponding.select();
            this.selectionModel.push(corresponding);
        }
    };
    NbSelectComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-select',
                    template: "<button nbButton [size]=\"size\" [status]=\"status\" [shape]=\"shape\" [hero]=\"hero\" [disabled]=\"disabled\" [fullWidth]=\"fullWidth\" [outline]=\"outline\" [class.opened]=\"isOpened\" [ngClass]=\"overlayPosition\"> <ng-container *ngIf=\"selectionModel?.length\"> <ng-container *ngIf=\"customLabel\"> <ng-content select=\"nb-select-label\"></ng-content> </ng-container> <ng-container *ngIf=\"!customLabel\">{{ selectionView }}</ng-container> </ng-container> <ng-container *ngIf=\"!selectionModel?.length\">{{ placeholder }}</ng-container> </button> <nb-card *nbPortal class=\"select\" [ngClass]=\"[status, overlayPosition]\" [style.width.px]=\"hostWidth\"> <nb-card-body> <ng-content select=\"nb-option, nb-option-group\"></ng-content> </nb-card-body> </nb-card> ",
                    styles: ["/*! * @license * Copyright Akveo. All Rights Reserved. * Licensed under the MIT License. See License.txt in the project root for license information. */:host{display:block}:host button{position:relative;width:100%;text-align:start;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;border:none}:host button::after{top:50%;right:0.75rem;position:absolute;display:inline-block;width:0;height:0;margin-left:0.255em;vertical-align:0.255em;content:'';border-top:0.3em solid;border-right:0.3em solid transparent;border-bottom:0;border-left:0.3em solid transparent} "],
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return NbSelectComponent; }),
                            multi: true,
                        },
                    ],
                },] },
    ];
    /** @nocollapse */
    NbSelectComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NB_DOCUMENT,] },] },
        { type: NbOverlayService, },
        { type: ElementRef, },
        { type: NbPositionBuilderService, },
        { type: ChangeDetectorRef, },
    ]; };
    NbSelectComponent.propDecorators = {
        "size": [{ type: Input },],
        "status": [{ type: Input },],
        "shape": [{ type: Input },],
        "hero": [{ type: Input },],
        "disabled": [{ type: Input },],
        "fullWidth": [{ type: Input },],
        "outline": [{ type: Input },],
        "placeholder": [{ type: Input },],
        "selectedChange": [{ type: Output },],
        "setSelected": [{ type: Input, args: ['selected',] },],
        "setMultiple": [{ type: Input, args: ['multiple',] },],
        "options": [{ type: ContentChildren, args: [NbOptionComponent, { descendants: true },] },],
        "customLabel": [{ type: ContentChild, args: [NbSelectLabelComponent,] },],
        "portal": [{ type: ViewChild, args: [NbPortalDirective,] },],
    };
    return NbSelectComponent;
}());
export { NbSelectComponent };
//# sourceMappingURL=select.component.js.map
/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { convertToBoolProperty } from '../helpers';
var NbOptionGroupComponent = /** @class */ (function () {
    function NbOptionGroupComponent() {
        this.disabled = false;
    }
    Object.defineProperty(NbOptionGroupComponent.prototype, "setDisabled", {
        set: function (disabled) {
            this.disabled = convertToBoolProperty(disabled);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionGroupComponent.prototype, "disabledClass", {
        get: function () {
            return this.disabled;
        },
        enumerable: true,
        configurable: true
    });
    NbOptionGroupComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-option-group',
                    styles: [":host{display:block}:host span{padding:1.125rem 0.5rem;display:block}:host.disabled{pointer-events:none}:host /deep/ nb-option{padding:0.75rem 0.75rem 0.75rem 2.5rem} "],
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    template: "\n    <span>{{ title }}</span>\n    <ng-content select=\"nb-option, ng-container\"></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbOptionGroupComponent.propDecorators = {
        "title": [{ type: Input },],
        "setDisabled": [{ type: Input, args: ['disabled',] },],
        "disabledClass": [{ type: HostBinding, args: ['class.disabled',] },],
    };
    return NbOptionGroupComponent;
}());
export { NbOptionGroupComponent };
//# sourceMappingURL=option-group.component.js.map
/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, forwardRef, HostBinding, HostListener, Inject, Input, Output, } from '@angular/core';
import { convertToBoolProperty } from '../helpers';
import { NbSelectComponent } from './select.component';
var NbOptionComponent = /** @class */ (function () {
    function NbOptionComponent(parent, elementRef, cd) {
        this.parent = parent;
        this.elementRef = elementRef;
        this.cd = cd;
        /**
           * Fires value on click.
           * */
        this.selectionChange = new EventEmitter();
        this.selected = false;
        this.disabled = false;
    }
    Object.defineProperty(NbOptionComponent.prototype, "setDisabled", {
        set: function (disabled) {
            this.disabled = convertToBoolProperty(disabled);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "withCheckbox", {
        /**
         * Determines should we render checkbox.
         * */
        get: /**
           * Determines should we render checkbox.
           * */
        function () {
            return this.multiple && !!this.value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "content", {
        get: function () {
            return this.elementRef.nativeElement.textContent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "multiple", {
        get: function () {
            return this.parent.multiple;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "selectedClass", {
        get: function () {
            return this.selected;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbOptionComponent.prototype, "disabledClass", {
        get: function () {
            return this.disabled;
        },
        enumerable: true,
        configurable: true
    });
    NbOptionComponent.prototype.onClick = function () {
        this.selectionChange.emit(this);
    };
    NbOptionComponent.prototype.select = function () {
        this.selected = true;
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NbOptionComponent.prototype.deselect = function () {
        this.selected = false;
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NbOptionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-option',
                    styles: ["/*! * @license * Copyright Akveo. All Rights Reserved. * Licensed under the MIT License. See License.txt in the project root for license information. */:host{display:block}:host.disabled{pointer-events:none}:host:hover{cursor:pointer}:host nb-checkbox{pointer-events:none} "],
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    template: "\n    <nb-checkbox *ngIf=\"withCheckbox\" [(ngModel)]=\"selected\">\n      <ng-container *ngTemplateOutlet=\"content\"></ng-container>\n    </nb-checkbox>\n\n    <ng-container *ngIf=\"!withCheckbox\">\n      <ng-container *ngTemplateOutlet=\"content\"></ng-container>\n    </ng-container>\n\n    <ng-template #content>\n      <ng-content></ng-content>\n    </ng-template>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbOptionComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [forwardRef(function () { return NbSelectComponent; }),] },] },
        { type: ElementRef, },
        { type: ChangeDetectorRef, },
    ]; };
    NbOptionComponent.propDecorators = {
        "value": [{ type: Input },],
        "setDisabled": [{ type: Input, args: ['disabled',] },],
        "selectionChange": [{ type: Output },],
        "selectedClass": [{ type: HostBinding, args: ['class.selected',] },],
        "disabledClass": [{ type: HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbOptionComponent;
}());
export { NbOptionComponent };
//# sourceMappingURL=option.component.js.map
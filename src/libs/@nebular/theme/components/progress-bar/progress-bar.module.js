/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbProgressBarComponent } from './progress-bar.component';
var NbProgressBarModule = /** @class */ (function () {
    function NbProgressBarModule() {
    }
    NbProgressBarModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: [NbProgressBarComponent],
                    exports: [NbProgressBarComponent],
                },] },
    ];
    return NbProgressBarModule;
}());
export { NbProgressBarModule };
//# sourceMappingURL=progress-bar.module.js.map
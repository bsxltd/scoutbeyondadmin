/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Input, HostBinding, HostListener } from '@angular/core';
import { convertToBoolProperty } from '../helpers';
/**
 * Basic button component.
 *
 * Default button size is `medium` and status color is `primary`:
 * @stacked-example(Button Showcase, button/button-showcase.component)
 *
 * ```html
 * <button nbButton></button>
 * ```
 * ### Installation
 *
 * Import `NbButtonModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbButtonModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Buttons are available in multiple colors using `status` property:
 * @stacked-example(Button Colors, button/button-colors.component.html)
 *
 * There are three button sizes:
 *
 * @stacked-example(Button Sizes, button/button-sizes.component.html)
 *
 * And two additional style types - `outline`:
 *
 * @stacked-example(Outline Buttons, button/button-outline.component.html)
 *
 * and `hero`:
 *
 * @stacked-example(Button Colors, button/button-hero.component.html)
 *
 * Buttons available in different shapes, which could be combined with the other properties:
 * @stacked-example(Button Shapes, button/button-shapes.component)
 *
 * `nbButton` could be applied to the following selectors - `button`, `input[type="button"]`, `input[type="submit"]`
 * and `a`:
 * @stacked-example(Button Elements, button/button-types.component.html)
 *
 * Button can be made `fullWidth`:
 * @stacked-example(Full Width Button, button/button-full-width.component.html)
 *
 * @styles
 *
 * btn-fg:
 * btn-font-family:
 * btn-line-height:
 * btn-disabled-opacity:
 * btn-cursor:
 * btn-primary-bg:
 * btn-secondary-bg:
 * btn-info-bg:
 * btn-success-bg:
 * btn-warning-bg:
 * btn-danger-bg:
 * btn-secondary-border:
 * btn-secondary-border-width:
 * btn-padding-y-lg:
 * btn-padding-x-lg:
 * btn-font-size-lg:
 * btn-padding-y-md:
 * btn-padding-x-md:
 * btn-font-size-md:
 * btn-padding-y-sm:
 * btn-padding-x-sm:
 * btn-font-size-sm:
 * btn-padding-y-xs:
 * btn-padding-x-xs:
 * btn-font-size-xs:
 * btn-border-radius:
 * btn-rectangle-border-radius:
 * btn-semi-round-border-radius:
 * btn-round-border-radius:
 * btn-hero-shadow:
 * btn-hero-text-shadow:
 * btn-hero-bevel-size:
 * btn-hero-glow-size:
 * btn-hero-primary-glow-size:
 * btn-hero-success-glow-size:
 * btn-hero-warning-glow-size:
 * btn-hero-info-glow-size:
 * btn-hero-danger-glow-size:
 * btn-hero-secondary-glow-size:
 * btn-hero-degree:
 * btn-hero-primary-degree:
 * btn-hero-success-degree:
 * btn-hero-warning-degree:
 * btn-hero-info-degree:
 * btn-hero-danger-degree:
 * btn-hero-secondary-degree:
 * btn-hero-border-radius:
 * btn-outline-fg:
 * btn-outline-hover-fg:
 * btn-outline-focus-fg:
 */
var NbButtonComponent = /** @class */ (function () {
    function NbButtonComponent() {
        this.fullWidth = false;
    }
    Object.defineProperty(NbButtonComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "small", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "medium", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "large", {
        get: function () {
            return this.size === NbButtonComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "primary", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "info", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "success", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "warning", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "danger", {
        get: function () {
            return this.status === NbButtonComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "rectangle", {
        get: function () {
            return this.shape === NbButtonComponent.SHAPE_RECTANGLE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "round", {
        get: function () {
            return this.shape === NbButtonComponent.SHAPE_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "semiRound", {
        get: function () {
            return this.shape === NbButtonComponent.SHAPE_SEMI_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "tabbable", {
        get: function () {
            return this.disabled ? '-1' : '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setSize", {
        set: /**
           * Button size, available sizes:
           * `xxsmall`, `xsmall`, `small`, `medium`, `large`
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setStatus", {
        set: /**
           * Button status (adds specific styles):
           * `primary`, `info`, `success`, `warning`, `danger`
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setShape", {
        set: /**
           * Button shapes: `rectangle`, `round`, `semi-round`
           * @param {string} val
           */
        function (val) {
            this.shape = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setHero", {
        set: /**
           * Adds `hero` styles
           * @param {boolean} val
           */
        function (val) {
            this.hero = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setDisabled", {
        set: /**
           * Disables the button
           * @param {boolean} val
           */
        function (val) {
            this.disabled = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setFullWidth", {
        set: /**
           * If set element will fill its container
           * @param {boolean}
           */
        function (value) {
            this.fullWidth = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbButtonComponent.prototype, "setOutline", {
        set: /**
           * Adds `outline` styles
           * @param {boolean} val
           */
        function (val) {
            this.outline = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    NbButtonComponent.prototype.onClick = function (event) {
        if (this.disabled) {
            event.preventDefault();
            event.stopImmediatePropagation();
        }
    };
    NbButtonComponent.SIZE_XSMALL = 'xsmall';
    NbButtonComponent.SIZE_SMALL = 'small';
    NbButtonComponent.SIZE_MEDIUM = 'medium';
    NbButtonComponent.SIZE_LARGE = 'large';
    NbButtonComponent.STATUS_PRIMARY = 'primary';
    NbButtonComponent.STATUS_INFO = 'info';
    NbButtonComponent.STATUS_SUCCESS = 'success';
    NbButtonComponent.STATUS_WARNING = 'warning';
    NbButtonComponent.STATUS_DANGER = 'danger';
    NbButtonComponent.SHAPE_RECTANGLE = 'rectangle';
    NbButtonComponent.SHAPE_ROUND = 'round';
    NbButtonComponent.SHAPE_SEMI_ROUND = 'semi-round';
    NbButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'button[nbButton],a[nbButton],input[type="button"][nbButton],input[type="submit"][nbButton]',
                    styles: [":host{text-transform:uppercase;letter-spacing:0.4px;border:2px solid transparent;transition:none;cursor:pointer;-webkit-appearance:none;-moz-appearance:none;text-align:center;text-decoration:none;display:inline-block;white-space:nowrap;vertical-align:middle;user-select:none}:host:hover,:host:focus{text-decoration:none}:host.btn-full-width{width:100%} "],
                    template: "\n    <ng-content></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbButtonComponent.propDecorators = {
        "xsmall": [{ type: HostBinding, args: ['class.btn-xsmall',] },],
        "small": [{ type: HostBinding, args: ['class.btn-small',] },],
        "medium": [{ type: HostBinding, args: ['class.btn-medium',] },],
        "large": [{ type: HostBinding, args: ['class.btn-large',] },],
        "primary": [{ type: HostBinding, args: ['class.btn-primary',] },],
        "info": [{ type: HostBinding, args: ['class.btn-info',] },],
        "success": [{ type: HostBinding, args: ['class.btn-success',] },],
        "warning": [{ type: HostBinding, args: ['class.btn-warning',] },],
        "danger": [{ type: HostBinding, args: ['class.btn-danger',] },],
        "rectangle": [{ type: HostBinding, args: ['class.btn-rectangle',] },],
        "round": [{ type: HostBinding, args: ['class.btn-round',] },],
        "semiRound": [{ type: HostBinding, args: ['class.btn-semi-round',] },],
        "hero": [{ type: HostBinding, args: ['class.btn-hero',] },],
        "outline": [{ type: HostBinding, args: ['class.btn-outline',] },],
        "disabled": [{ type: HostBinding, args: ['attr.aria-disabled',] }, { type: HostBinding, args: ['class.btn-disabled',] },],
        "tabbable": [{ type: HostBinding, args: ['attr.tabindex',] },],
        "fullWidth": [{ type: HostBinding, args: ['class.btn-full-width',] },],
        "setSize": [{ type: Input, args: ['size',] },],
        "setStatus": [{ type: Input, args: ['status',] },],
        "setShape": [{ type: Input, args: ['shape',] },],
        "setHero": [{ type: Input, args: ['hero',] },],
        "setDisabled": [{ type: Input, args: ['disabled',] },],
        "setFullWidth": [{ type: Input, args: ['fullWidth',] },],
        "setOutline": [{ type: Input, args: ['outline',] },],
        "onClick": [{ type: HostListener, args: ['click', ['$event'],] },],
    };
    return NbButtonComponent;
}());
export { NbButtonComponent };
//# sourceMappingURL=button.component.js.map
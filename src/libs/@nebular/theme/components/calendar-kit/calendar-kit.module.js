/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { NbSharedModule } from '../shared/shared.module';
import { NbButtonModule } from '../button/button.module';
import { NbCalendarMonthModelService, NbDateService } from './services';
import { NbCalendarDatePipe, NbCalendarDayCellComponent, NbCalendarDayPickerComponent, NbCalendarDaysNamesComponent, NbCalendarHeaderComponent, NbCalendarMonthCellComponent, NbCalendarMonthPickerComponent, NbCalendarNavigationComponent, NbCalendarPageableNavigationComponent, NbCalendarPickerComponent, NbCalendarPickerRowComponent, NbCalendarYearCellComponent, NbCalendarYearPickerComponent, } from './components';
import { NbNativeDateService } from './services/native-date.service';
var SERVICES = [
    { provide: NbDateService, useClass: NbNativeDateService },
    DatePipe,
    NbCalendarMonthModelService,
];
var COMPONENTS = [
    NbCalendarHeaderComponent,
    NbCalendarNavigationComponent,
    NbCalendarPageableNavigationComponent,
    NbCalendarDaysNamesComponent,
    NbCalendarYearPickerComponent,
    NbCalendarMonthPickerComponent,
    NbCalendarDayPickerComponent,
    NbCalendarDayCellComponent,
    NbCalendarMonthCellComponent,
    NbCalendarYearCellComponent,
    NbCalendarPickerRowComponent,
    NbCalendarPickerComponent,
];
var PIPES = [
    NbCalendarDatePipe,
];
/**
 * `NbCalendarKitModule` is a module that contains multiple useful components for building custom calendars.
 * So if you think our calendars is not enough powerful for you just use calendar-kit and build your own calendar!
 *
 * Available components:
 * - `NbCalendarDayPicker`
 * - `NbCalendarDayCell`
 * - `NbCalendarMonthPicker`
 * - `NbCalendarMonthCell`
 * - `NbCalendarYearPicker`
 * - `NbCalendarYearCell`
 * - `NbCalendarHeader`
 * - `NbCalendarNavigation`
 * - `NbCalendarPageableNavigation`
 *
 * For example you can easily build full calendar:
 * @stacked-example(Full calendar, calendar-kit/calendar-kit-full-calendar.component)
 * */
var NbCalendarKitModule = /** @class */ (function () {
    function NbCalendarKitModule() {
    }
    NbCalendarKitModule.decorators = [
        { type: NgModule, args: [{
                    imports: [NbSharedModule, NbButtonModule],
                    exports: COMPONENTS.concat(PIPES),
                    declarations: COMPONENTS.concat(PIPES),
                    providers: SERVICES.slice(),
                    entryComponents: [
                        NbCalendarDayCellComponent,
                        NbCalendarMonthCellComponent,
                        NbCalendarYearCellComponent,
                    ],
                },] },
    ];
    return NbCalendarKitModule;
}());
export { NbCalendarKitModule };
//# sourceMappingURL=calendar-kit.module.js.map
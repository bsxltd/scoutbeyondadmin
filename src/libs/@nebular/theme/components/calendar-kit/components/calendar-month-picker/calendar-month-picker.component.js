/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output, Type, } from '@angular/core';
import { batch, range } from '../../helpers';
import { NbCalendarSize } from '../../model';
import { NbCalendarMonthCellComponent } from './calendar-month-cell.component';
import { NbDateService } from '../../services';
var NbCalendarMonthPickerComponent = /** @class */ (function () {
    function NbCalendarMonthPickerComponent(dateService) {
        this.dateService = dateService;
        this.size = NbCalendarSize.MEDIUM;
        this.monthChange = new EventEmitter();
        this.cellComponent = NbCalendarMonthCellComponent;
    }
    Object.defineProperty(NbCalendarMonthPickerComponent.prototype, "_cellComponent", {
        set: function (cellComponent) {
            if (cellComponent) {
                this.cellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthPickerComponent.prototype, "medium", {
        get: function () {
            return this.size === NbCalendarSize.MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthPickerComponent.prototype, "large", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarMonthPickerComponent.prototype.ngOnInit = function () {
        this.initMonths();
    };
    NbCalendarMonthPickerComponent.prototype.initMonths = function () {
        var _this = this;
        var months = range(12).map(function (i) { return _this.createMonthDateByIndex(i); });
        this.months = batch(months, 4);
    };
    NbCalendarMonthPickerComponent.prototype.onSelect = function (month) {
        this.monthChange.emit(month);
    };
    NbCalendarMonthPickerComponent.prototype.createMonthDateByIndex = function (i) {
        return this.dateService.createDate(this.dateService.getYear(this.month), i, this.dateService.getDate(this.month));
    };
    NbCalendarMonthPickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-month-picker',
                    template: "\n    <nb-calendar-picker\n      [data]=\"months\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      [selectedValue]=\"month\"\n      [cellComponent]=\"cellComponent\"\n      (select)=\"onSelect($event)\">\n    </nb-calendar-picker>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarMonthPickerComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarMonthPickerComponent.propDecorators = {
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "filter": [{ type: Input },],
        "size": [{ type: Input },],
        "month": [{ type: Input },],
        "monthChange": [{ type: Output },],
        "_cellComponent": [{ type: Input, args: ['cellComponent',] },],
        "medium": [{ type: HostBinding, args: ['class.medium',] },],
        "large": [{ type: HostBinding, args: ['class.large',] },],
    };
    return NbCalendarMonthPickerComponent;
}());
export { NbCalendarMonthPickerComponent };
//# sourceMappingURL=calendar-month-picker.component.js.map
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, } from '@angular/core';
import { NbDateService } from '../../services';
var NbCalendarMonthCellComponent = /** @class */ (function () {
    function NbCalendarMonthCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new EventEmitter(true);
    }
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "selected", {
        get: function () {
            return this.dateService.isSameMonthSafe(this.date, this.selectedValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameMonthSafe(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarMonthCellComponent.prototype, "month", {
        get: function () {
            return this.dateService.getMonthName(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarMonthCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarMonthCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.monthEnd(), this.min) < 0;
    };
    NbCalendarMonthCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.monthStart(), this.max) > 0;
    };
    NbCalendarMonthCellComponent.prototype.monthStart = function () {
        return this.dateService.getMonthStart(this.date);
    };
    NbCalendarMonthCellComponent.prototype.monthEnd = function () {
        return this.dateService.getMonthEnd(this.date);
    };
    NbCalendarMonthCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-month-cell',
                    template: "{{ month }}",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'month-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarMonthCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarMonthCellComponent.propDecorators = {
        "date": [{ type: Input },],
        "selectedValue": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "select": [{ type: Output },],
        "selected": [{ type: HostBinding, args: ['class.selected',] },],
        "today": [{ type: HostBinding, args: ['class.today',] },],
        "disabled": [{ type: HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbCalendarMonthCellComponent;
}());
export { NbCalendarMonthCellComponent };
//# sourceMappingURL=calendar-month-cell.component.js.map
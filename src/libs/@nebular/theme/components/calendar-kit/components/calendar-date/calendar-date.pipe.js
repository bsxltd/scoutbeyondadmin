/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Pipe } from '@angular/core';
import { NbDateService } from '../../services';
var NbCalendarDatePipe = /** @class */ (function () {
    function NbCalendarDatePipe(dateService) {
        this.dateService = dateService;
    }
    NbCalendarDatePipe.prototype.transform = function (date) {
        return date ? this.dateService.getMonthName(date) + " " + this.dateService.getYear(date) : '';
    };
    NbCalendarDatePipe.decorators = [
        { type: Pipe, args: [{ name: 'nbCalendarDate' },] },
    ];
    /** @nocollapse */
    NbCalendarDatePipe.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    return NbCalendarDatePipe;
}());
export { NbCalendarDatePipe };
//# sourceMappingURL=calendar-date.pipe.js.map
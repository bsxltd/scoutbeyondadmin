/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { PipeTransform } from '@angular/core';
import { NbDateService } from '../../services';
export declare class NbCalendarDatePipe<D> implements PipeTransform {
    protected dateService: NbDateService<D>;
    constructor(dateService: NbDateService<D>);
    transform(date: D): string;
}

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, } from '@angular/core';
import { NbDateService } from '../../services';
var NbCalendarYearCellComponent = /** @class */ (function () {
    function NbCalendarYearCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new EventEmitter(true);
    }
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "selected", {
        get: function () {
            return this.dateService.isSameYearSafe(this.date, this.selectedValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameYearSafe(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarYearCellComponent.prototype, "year", {
        get: function () {
            return this.dateService.getYear(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarYearCellComponent.prototype.onClick = function () {
        if (this.disabled) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarYearCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.yearEnd(), this.min) < 0;
    };
    NbCalendarYearCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.yearStart(), this.max) > 0;
    };
    NbCalendarYearCellComponent.prototype.yearStart = function () {
        return this.dateService.getYearStart(this.date);
    };
    NbCalendarYearCellComponent.prototype.yearEnd = function () {
        return this.dateService.getYearEnd(this.date);
    };
    NbCalendarYearCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-year-cell',
                    template: "{{ year }}",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'year-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarYearCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarYearCellComponent.propDecorators = {
        "date": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "selectedValue": [{ type: Input },],
        "select": [{ type: Output },],
        "selected": [{ type: HostBinding, args: ['class.selected',] },],
        "today": [{ type: HostBinding, args: ['class.today',] },],
        "disabled": [{ type: HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbCalendarYearCellComponent;
}());
export { NbCalendarYearCellComponent };
//# sourceMappingURL=calendar-year-cell.component.js.map
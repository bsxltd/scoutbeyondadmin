/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbLayoutDirectionService } from '../../../../services/direction.service';
var NbCalendarPageableNavigationComponent = /** @class */ (function () {
    function NbCalendarPageableNavigationComponent(directionService) {
        this.directionService = directionService;
        this.changeMode = new EventEmitter();
        this.next = new EventEmitter();
        this.prev = new EventEmitter();
    }
    Object.defineProperty(NbCalendarPageableNavigationComponent.prototype, "isRtl", {
        get: function () {
            return this.directionService.isRtl();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarPageableNavigationComponent.prototype, "isLtr", {
        get: function () {
            return this.directionService.isLtr();
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarPageableNavigationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-pageable-navigation',
                    styles: [":host{display:flex;align-items:center;justify-content:space-between}:host i{font-size:1.5rem;cursor:pointer} "],
                    template: "\n    <i [ngClass]=\"{'nb-arrow-left': isLtr, 'nb-arrow-right': isRtl }\" (click)=\"prev.emit()\"></i>\n    <nb-calendar-navigation [date]=\"date\" (changeMode)=\"changeMode.emit()\"></nb-calendar-navigation>\n    <i [ngClass]=\"{'nb-arrow-right': isLtr, 'nb-arrow-left': isRtl }\" (click)=\"next.emit()\"></i>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbCalendarPageableNavigationComponent.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
    ]; };
    NbCalendarPageableNavigationComponent.propDecorators = {
        "date": [{ type: Input },],
        "changeMode": [{ type: Output },],
        "next": [{ type: Output },],
        "prev": [{ type: Output },],
    };
    return NbCalendarPageableNavigationComponent;
}());
export { NbCalendarPageableNavigationComponent };
//# sourceMappingURL=calendar-pageable-navigation.component.js.map
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
var NbCalendarNavigationComponent = /** @class */ (function () {
    function NbCalendarNavigationComponent() {
        this.changeMode = new EventEmitter(true);
    }
    NbCalendarNavigationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-navigation',
                    styles: ["\n    :host {\n      display: flex;\n      justify-content: center;\n    }\n\n    :host button {\n      height: 3.125rem;\n    }\n  "],
                    template: "\n    <button nbButton (click)=\"changeMode.emit()\">\n      {{ date | nbCalendarDate }}\n    </button>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarNavigationComponent.propDecorators = {
        "date": [{ type: Input },],
        "changeMode": [{ type: Output },],
    };
    return NbCalendarNavigationComponent;
}());
export { NbCalendarNavigationComponent };
//# sourceMappingURL=calendar-navigation.component.js.map
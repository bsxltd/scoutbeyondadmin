/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output, Type, } from '@angular/core';
import { NbCalendarMonthModelService } from '../../services';
import { NbCalendarDayCellComponent } from './calendar-day-cell.component';
import { NbCalendarSize } from '../../model';
/**
 * Provides capability pick days.
 * */
var NbCalendarDayPickerComponent = /** @class */ (function () {
    function NbCalendarDayPickerComponent(monthModel) {
        this.monthModel = monthModel;
        /**
           * Defines if we should render previous and next months
           * in the current month view.
           * */
        this.boundingMonths = true;
        this.cellComponent = NbCalendarDayCellComponent;
        /**
           * Size of the component.
           * Can be 'medium' which is default or 'large'.
           * */
        this.size = NbCalendarSize.MEDIUM;
        /**
           * Fires newly selected date.
           * */
        this.dateChange = new EventEmitter();
    }
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "setCellComponent", {
        set: /**
           * Custom day cell component. Have to implement `NbCalendarCell` interface.
           * */
        function (cellComponent) {
            if (cellComponent) {
                this.cellComponent = cellComponent;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "medium", {
        get: function () {
            return this.size === NbCalendarSize.MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayPickerComponent.prototype, "large", {
        get: function () {
            return this.size === NbCalendarSize.LARGE;
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarDayPickerComponent.prototype.ngOnChanges = function (_a) {
        var visibleDate = _a.visibleDate;
        if (visibleDate) {
            this.weeks = this.monthModel.createDaysGrid(this.visibleDate, this.boundingMonths);
        }
    };
    NbCalendarDayPickerComponent.prototype.onSelect = function (day) {
        this.dateChange.emit(day);
    };
    NbCalendarDayPickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-day-picker',
                    styles: [" :host { display: block; } "],
                    template: "\n    <nb-calendar-days-names></nb-calendar-days-names>\n    <nb-calendar-picker\n      [data]=\"weeks\"\n      [visibleDate]=\"visibleDate\"\n      [selectedValue]=\"date\"\n      [cellComponent]=\"cellComponent\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      (select)=\"onSelect($event)\">\n    </nb-calendar-picker>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarDayPickerComponent.ctorParameters = function () { return [
        { type: NbCalendarMonthModelService, },
    ]; };
    NbCalendarDayPickerComponent.propDecorators = {
        "visibleDate": [{ type: Input },],
        "boundingMonths": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "filter": [{ type: Input },],
        "setCellComponent": [{ type: Input, args: ['cellComponent',] },],
        "size": [{ type: Input },],
        "date": [{ type: Input },],
        "dateChange": [{ type: Output },],
        "medium": [{ type: HostBinding, args: ['class.medium',] },],
        "large": [{ type: HostBinding, args: ['class.large',] },],
    };
    return NbCalendarDayPickerComponent;
}());
export { NbCalendarDayPickerComponent };
//# sourceMappingURL=calendar-day-picker.component.js.map
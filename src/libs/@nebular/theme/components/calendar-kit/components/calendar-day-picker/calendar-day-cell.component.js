/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, } from '@angular/core';
import { NbDateService } from '../../services';
var NbCalendarDayCellComponent = /** @class */ (function () {
    function NbCalendarDayCellComponent(dateService) {
        this.dateService = dateService;
        this.select = new EventEmitter(true);
    }
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "today", {
        get: function () {
            return this.dateService.isSameDaySafe(this.date, this.dateService.today());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "boundingMonth", {
        get: function () {
            return !this.dateService.isSameMonthSafe(this.date, this.visibleDate);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "selected", {
        get: function () {
            return this.dateService.isSameDaySafe(this.date, this.selectedValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "empty", {
        get: function () {
            return !this.date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "disabled", {
        get: function () {
            return this.smallerThanMin() || this.greaterThanMax() || this.dontFitFilter();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarDayCellComponent.prototype, "day", {
        get: function () {
            return this.date && this.dateService.getDate(this.date);
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarDayCellComponent.prototype.onClick = function () {
        if (this.disabled || this.empty) {
            return;
        }
        this.select.emit(this.date);
    };
    NbCalendarDayCellComponent.prototype.smallerThanMin = function () {
        return this.date && this.min && this.dateService.compareDates(this.date, this.min) < 0;
    };
    NbCalendarDayCellComponent.prototype.greaterThanMax = function () {
        return this.date && this.max && this.dateService.compareDates(this.date, this.max) > 0;
    };
    NbCalendarDayCellComponent.prototype.dontFitFilter = function () {
        return this.date && this.filter && !this.filter(this.date);
    };
    NbCalendarDayCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-day-cell',
                    template: '{{ day }}',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    host: { 'class': 'day-cell' },
                },] },
    ];
    /** @nocollapse */
    NbCalendarDayCellComponent.ctorParameters = function () { return [
        { type: NbDateService, },
    ]; };
    NbCalendarDayCellComponent.propDecorators = {
        "date": [{ type: Input },],
        "selectedValue": [{ type: Input },],
        "visibleDate": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "filter": [{ type: Input },],
        "select": [{ type: Output },],
        "today": [{ type: HostBinding, args: ['class.today',] },],
        "boundingMonth": [{ type: HostBinding, args: ['class.bounding-month',] },],
        "selected": [{ type: HostBinding, args: ['class.selected',] },],
        "empty": [{ type: HostBinding, args: ['class.empty',] },],
        "disabled": [{ type: HostBinding, args: ['class.disabled',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbCalendarDayCellComponent;
}());
export { NbCalendarDayCellComponent };
//# sourceMappingURL=calendar-day-cell.component.js.map
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, ComponentFactoryResolver, EventEmitter, Input, Output, TemplateRef, Type, ViewChild, ViewContainerRef, } from '@angular/core';
var NbCalendarPickerRowComponent = /** @class */ (function () {
    function NbCalendarPickerRowComponent(cfr) {
        this.cfr = cfr;
        this.select = new EventEmitter();
    }
    NbCalendarPickerRowComponent.prototype.ngOnChanges = function () {
        var _this = this;
        var factory = this.cfr.resolveComponentFactory(this.component);
        this.containerRef.clear();
        this.row.forEach(function (date) {
            var component = _this.containerRef.createComponent(factory);
            _this.patchWithContext(component.instance, date);
            component.changeDetectorRef.detectChanges();
        });
    };
    NbCalendarPickerRowComponent.prototype.patchWithContext = function (component, date) {
        component.visibleDate = this.visibleDate;
        component.selectedValue = this.selectedValue;
        component.date = date;
        component.min = this.min;
        component.max = this.max;
        component.filter = this.filter;
        component.select.subscribe(this.select.emit.bind(this.select));
    };
    NbCalendarPickerRowComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-picker-row',
                    styles: ["\n    :host {\n      display: flex;\n      justify-content: space-between;\n    }\n  "],
                    template: '<ng-template></ng-template>',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarPickerRowComponent.ctorParameters = function () { return [
        { type: ComponentFactoryResolver, },
    ]; };
    NbCalendarPickerRowComponent.propDecorators = {
        "row": [{ type: Input },],
        "selectedValue": [{ type: Input },],
        "visibleDate": [{ type: Input },],
        "component": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "filter": [{ type: Input },],
        "select": [{ type: Output },],
        "containerRef": [{ type: ViewChild, args: [TemplateRef, { read: ViewContainerRef },] },],
    };
    return NbCalendarPickerRowComponent;
}());
export { NbCalendarPickerRowComponent };
//# sourceMappingURL=calendar-picker-row.component.js.map
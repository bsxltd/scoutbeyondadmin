/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, Type } from '@angular/core';
var NbCalendarPickerComponent = /** @class */ (function () {
    function NbCalendarPickerComponent() {
        this.select = new EventEmitter();
    }
    NbCalendarPickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-picker',
                    template: "\n    <nb-calendar-picker-row\n      *ngFor=\"let row of data\"\n      [row]=\"row\"\n      [visibleDate]=\"visibleDate\"\n      [selectedValue]=\"selectedValue\"\n      [component]=\"cellComponent\"\n      [min]=\"min\"\n      [max]=\"max\"\n      [filter]=\"filter\"\n      (select)=\"select.emit($event)\">\n    </nb-calendar-picker-row>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbCalendarPickerComponent.propDecorators = {
        "data": [{ type: Input },],
        "visibleDate": [{ type: Input },],
        "selectedValue": [{ type: Input },],
        "cellComponent": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "filter": [{ type: Input },],
        "select": [{ type: Output },],
    };
    return NbCalendarPickerComponent;
}());
export { NbCalendarPickerComponent };
//# sourceMappingURL=calendar-picker.component.js.map
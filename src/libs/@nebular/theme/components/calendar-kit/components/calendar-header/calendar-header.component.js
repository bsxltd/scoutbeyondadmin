/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NbLayoutDirectionService } from '../../../../services/direction.service';
import { NbDateService } from '../../services';
var NbCalendarHeaderComponent = /** @class */ (function () {
    function NbCalendarHeaderComponent(directionService, dateService) {
        this.directionService = directionService;
        this.dateService = dateService;
        this.navigateToday = new EventEmitter();
        this.date = this.dateService.today();
    }
    Object.defineProperty(NbCalendarHeaderComponent.prototype, "isRtl", {
        get: function () {
            return this.directionService.isRtl();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbCalendarHeaderComponent.prototype, "isLtr", {
        get: function () {
            return this.directionService.isLtr();
        },
        enumerable: true,
        configurable: true
    });
    NbCalendarHeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-calendar-header',
                    template: "\n    <div class=\"header\">\n      <span class=\"title\" (click)=\"navigateToday.emit()\">\n        {{ date | nbCalendarDate }}\n        <i [ngClass]=\"{ 'nb-arrow-dropright': isLtr, 'nb-arrow-dropleft': isRtl }\"></i>\n      </span>\n      <span class=\"sub-title\">Today</span>\n    </div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbCalendarHeaderComponent.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
        { type: NbDateService, },
    ]; };
    NbCalendarHeaderComponent.propDecorators = {
        "date": [{ type: Input },],
        "navigateToday": [{ type: Output },],
    };
    return NbCalendarHeaderComponent;
}());
export { NbCalendarHeaderComponent };
//# sourceMappingURL=calendar-header.component.js.map
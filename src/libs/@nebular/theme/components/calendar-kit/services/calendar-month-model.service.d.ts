import { NbDateService } from './date.service';
export declare class NbCalendarMonthModelService<D> {
    protected dateService: NbDateService<D>;
    constructor(dateService: NbDateService<D>);
    createDaysGrid(activeMonth: D, boundingMonth?: boolean): D[][];
    private createDates(activeMonth);
    private withBoundingMonths(weeks, activeMonth, boundingMonth);
    private addPrevBoundingMonth(weeks, activeMonth, boundingMonth);
    private addNextBoundingMonth(weeks, activeMonth, boundingMonth);
    private createPrevBoundingDays(activeMonth, boundingMonth, requiredItems);
    private createNextBoundingDays(activeMonth, boundingMonth, requiredItems);
    private getStartOfWeekDayDiff(date);
    private getWeekStartDiff(date);
    private isShouldAddPrevBoundingMonth(weeks);
    private isShouldAddNextBoundingMonth(weeks);
    private createDateRangeForMonth(date);
}

import { NgModule } from '@angular/core';
import { NbFocusTrapFactoryService } from './focus-trap';
var NbA11yModule = /** @class */ (function () {
    function NbA11yModule() {
    }
    NbA11yModule.forRoot = function () {
        return {
            ngModule: NbA11yModule,
            providers: [NbFocusTrapFactoryService],
        };
    };
    NbA11yModule.decorators = [
        { type: NgModule, args: [{},] },
    ];
    return NbA11yModule;
}());
export { NbA11yModule };
//# sourceMappingURL=a11y.module.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Inject, Injectable, NgZone } from '@angular/core';
import { FocusTrap, FocusTrapFactory, InteractivityChecker } from '@angular/cdk/a11y';
import { NB_DOCUMENT } from '../../../theme.options';
/**
 * Overrides angular cdk focus trap to keep restore functionality inside trap.
 * */
var /**
 * Overrides angular cdk focus trap to keep restore functionality inside trap.
 * */
NbFocusTrap = /** @class */ (function (_super) {
    __extends(NbFocusTrap, _super);
    function NbFocusTrap(element, checker, ngZone, document, deferAnchors) {
        var _this = _super.call(this, element, checker, ngZone, document, deferAnchors) || this;
        _this.element = element;
        _this.checker = checker;
        _this.ngZone = ngZone;
        _this.document = document;
        _this.savePreviouslyFocusedElement();
        return _this;
    }
    NbFocusTrap.prototype.restoreFocus = function () {
        this.previouslyFocusedElement.focus();
        this.destroy();
    };
    NbFocusTrap.prototype.blurPreviouslyFocusedElement = function () {
        this.previouslyFocusedElement.blur();
    };
    NbFocusTrap.prototype.savePreviouslyFocusedElement = function () {
        this.previouslyFocusedElement = this.document.activeElement;
    };
    return NbFocusTrap;
}(FocusTrap));
/**
 * Overrides angular cdk focus trap to keep restore functionality inside trap.
 * */
export { NbFocusTrap };
var NbFocusTrapFactoryService = /** @class */ (function (_super) {
    __extends(NbFocusTrapFactoryService, _super);
    function NbFocusTrapFactoryService(checker, ngZone, document) {
        var _this = _super.call(this, checker, ngZone, document) || this;
        _this.checker = checker;
        _this.ngZone = ngZone;
        _this.document = document;
        return _this;
    }
    NbFocusTrapFactoryService.prototype.create = function (element, deferCaptureElements) {
        return new NbFocusTrap(element, this.checker, this.ngZone, this.document, deferCaptureElements);
    };
    NbFocusTrapFactoryService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    NbFocusTrapFactoryService.ctorParameters = function () { return [
        { type: InteractivityChecker, },
        { type: NgZone, },
        { type: undefined, decorators: [{ type: Inject, args: [NB_DOCUMENT,] },] },
    ]; };
    return NbFocusTrapFactoryService;
}(FocusTrapFactory));
export { NbFocusTrapFactoryService };
//# sourceMappingURL=focus-trap.js.map
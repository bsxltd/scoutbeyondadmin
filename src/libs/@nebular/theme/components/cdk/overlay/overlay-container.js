import { Component, ComponentFactoryResolver, HostBinding, Injector, Input, ViewContainerRef, } from '@angular/core';
import { NbPosition } from './overlay-position';
import { NbPortalInjector } from './mapping';
var NbPositionedContainer = /** @class */ (function () {
    function NbPositionedContainer() {
    }
    Object.defineProperty(NbPositionedContainer.prototype, "top", {
        get: function () {
            return this.position === NbPosition.TOP;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbPositionedContainer.prototype, "right", {
        get: function () {
            return this.position === NbPosition.RIGHT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbPositionedContainer.prototype, "bottom", {
        get: function () {
            return this.position === NbPosition.BOTTOM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbPositionedContainer.prototype, "left", {
        get: function () {
            return this.position === NbPosition.LEFT;
        },
        enumerable: true,
        configurable: true
    });
    NbPositionedContainer.propDecorators = {
        "position": [{ type: Input },],
        "top": [{ type: HostBinding, args: ['class.nb-overlay-top',] },],
        "right": [{ type: HostBinding, args: ['class.nb-overlay-right',] },],
        "bottom": [{ type: HostBinding, args: ['class.nb-overlay-bottom',] },],
        "left": [{ type: HostBinding, args: ['class.nb-overlay-left',] },],
    };
    return NbPositionedContainer;
}());
export { NbPositionedContainer };
var NbOverlayContainerComponent = /** @class */ (function () {
    function NbOverlayContainerComponent(vcr, injector) {
        this.vcr = vcr;
        this.injector = injector;
    }
    Object.defineProperty(NbOverlayContainerComponent.prototype, "isStringContent", {
        get: function () {
            return !!this.content;
        },
        enumerable: true,
        configurable: true
    });
    NbOverlayContainerComponent.prototype.attachComponentPortal = function (portal) {
        var factory = portal.cfr.resolveComponentFactory(portal.component);
        var injector = this.createChildInjector(portal.cfr);
        return this.vcr.createComponent(factory, null, injector);
    };
    NbOverlayContainerComponent.prototype.attachTemplatePortal = function (portal) {
        return this.vcr.createEmbeddedView(portal.templateRef, portal.context);
    };
    NbOverlayContainerComponent.prototype.attachStringContent = function (content) {
        this.content = content;
    };
    NbOverlayContainerComponent.prototype.createChildInjector = function (cfr) {
        return new NbPortalInjector(this.injector, new WeakMap([
            [ComponentFactoryResolver, cfr],
        ]));
    };
    NbOverlayContainerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-overlay-container',
                    template: "\n    <div *ngIf=\"isStringContent\" class=\"primitive-overlay\">{{ content }}</div>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbOverlayContainerComponent.ctorParameters = function () { return [
        { type: ViewContainerRef, },
        { type: Injector, },
    ]; };
    return NbOverlayContainerComponent;
}());
export { NbOverlayContainerComponent };
//# sourceMappingURL=overlay-container.js.map
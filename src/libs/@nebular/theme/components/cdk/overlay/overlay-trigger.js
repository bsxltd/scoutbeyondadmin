var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { fromEvent as observableFromEvent, merge as observableMerge, Subject } from 'rxjs';
import { debounceTime, delay, filter, repeat, switchMap, takeUntil, takeWhile } from 'rxjs/operators';
export var NbTrigger;
(function (NbTrigger) {
    NbTrigger["CLICK"] = "click";
    NbTrigger["HOVER"] = "hover";
    NbTrigger["HINT"] = "hint";
    NbTrigger["FOCUS"] = "focus";
})(NbTrigger || (NbTrigger = {}));
/**
 * Provides entity with two event stream: show and hide.
 * Each stream provides different events depends on implementation.
 * We have three main trigger strategies: click, hint and hover.
 * */
/**
 * TODO maybe we have to use renderer.listen instead of observableFromEvent?
 * Renderer provides capability use it in service worker, ssr and so on.
 * */
var /**
 * Provides entity with two event stream: show and hide.
 * Each stream provides different events depends on implementation.
 * We have three main trigger strategies: click, hint and hover.
 * */
/**
 * TODO maybe we have to use renderer.listen instead of observableFromEvent?
 * Renderer provides capability use it in service worker, ssr and so on.
 * */
NbTriggerStrategy = /** @class */ (function () {
    function NbTriggerStrategy(document, host, container) {
        this.document = document;
        this.host = host;
        this.container = container;
    }
    return NbTriggerStrategy;
}());
/**
 * Provides entity with two event stream: show and hide.
 * Each stream provides different events depends on implementation.
 * We have three main trigger strategies: click, hint and hover.
 * */
/**
 * TODO maybe we have to use renderer.listen instead of observableFromEvent?
 * Renderer provides capability use it in service worker, ssr and so on.
 * */
export { NbTriggerStrategy };
/**
 * Creates show and hide event streams.
 * Fires toggle event when the click was performed on the host element.
 * Fires close event when the click was performed on the document but
 * not on the host or container.
 * */
var /**
 * Creates show and hide event streams.
 * Fires toggle event when the click was performed on the host element.
 * Fires close event when the click was performed on the document but
 * not on the host or container.
 * */
NbClickTriggerStrategy = /** @class */ (function (_super) {
    __extends(NbClickTriggerStrategy, _super);
    function NbClickTriggerStrategy(document, host, container) {
        var _this = _super.call(this, document, host, container) || this;
        _this.document = document;
        _this.host = host;
        _this.container = container;
        _this.show = new Subject();
        _this.show$ = _this.show.asObservable();
        _this.hide = new Subject();
        _this.hide$ = observableMerge(_this.hide.asObservable(), observableFromEvent(_this.document, 'click')
            .pipe(filter(function (event) { return _this.isNotHostOrContainer(event); })));
        _this.subscribeOnHostClick();
        return _this;
    }
    NbClickTriggerStrategy.prototype.subscribeOnHostClick = function () {
        var _this = this;
        observableFromEvent(this.host, 'click')
            .subscribe(function (event) {
            if (_this.isContainerExists()) {
                _this.hide.next(event);
            }
            else {
                _this.show.next(event);
            }
        });
    };
    NbClickTriggerStrategy.prototype.isContainerExists = function () {
        return !!this.container();
    };
    NbClickTriggerStrategy.prototype.isNotHostOrContainer = function (event) {
        return !this.host.contains(event.target)
            && this.isContainerExists()
            && !this.container().location.nativeElement.contains(event.target);
    };
    return NbClickTriggerStrategy;
}(NbTriggerStrategy));
/**
 * Creates show and hide event streams.
 * Fires toggle event when the click was performed on the host element.
 * Fires close event when the click was performed on the document but
 * not on the host or container.
 * */
export { NbClickTriggerStrategy };
/**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element and stops out of the host and popover container.
 * */
var /**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element and stops out of the host and popover container.
 * */
NbHoverTriggerStrategy = /** @class */ (function (_super) {
    __extends(NbHoverTriggerStrategy, _super);
    function NbHoverTriggerStrategy() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.show$ = observableFromEvent(_this.host, 'mouseenter')
            .pipe(delay(100), takeUntil(observableFromEvent(_this.host, 'mouseleave')), repeat());
        _this.hide$ = observableFromEvent(_this.host, 'mouseleave')
            .pipe(switchMap(function () {
            return observableFromEvent(_this.document, 'mousemove')
                .pipe(debounceTime(100), takeWhile(function () { return !!_this.container(); }), filter(function (event) {
                return !_this.host.contains(event.target)
                    && !_this.container().location.nativeElement.contains(event.target);
            }));
        }));
        return _this;
    }
    return NbHoverTriggerStrategy;
}(NbTriggerStrategy));
/**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element and stops out of the host and popover container.
 * */
export { NbHoverTriggerStrategy };
/**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element.
 * */
var /**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element.
 * */
NbHintTriggerStrategy = /** @class */ (function (_super) {
    __extends(NbHintTriggerStrategy, _super);
    function NbHintTriggerStrategy() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.show$ = observableFromEvent(_this.host, 'mouseenter')
            .pipe(delay(100), takeUntil(observableFromEvent(_this.host, 'mouseleave')), repeat());
        _this.hide$ = observableFromEvent(_this.host, 'mouseleave');
        return _this;
    }
    return NbHintTriggerStrategy;
}(NbTriggerStrategy));
/**
 * Creates show and hide event streams.
 * Fires open event when a mouse hovers over the host element and stay over at least 100 milliseconds.
 * Fires close event when the mouse leaves the host element.
 * */
export { NbHintTriggerStrategy };
/**
 * Creates show and hide event streams.
 * Fires open event when a focus is on the host element and stay over at least 100 milliseconds.
 * Fires close event when the focus leaves the host element.
 * */
var /**
 * Creates show and hide event streams.
 * Fires open event when a focus is on the host element and stay over at least 100 milliseconds.
 * Fires close event when the focus leaves the host element.
 * */
NbFocusTriggerStrategy = /** @class */ (function (_super) {
    __extends(NbFocusTriggerStrategy, _super);
    function NbFocusTriggerStrategy() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.focusOut$ = observableFromEvent(_this.host, 'focusout')
            .pipe(switchMap(function () {
            return observableFromEvent(_this.document, 'focusin')
                .pipe(takeWhile(function () { return !!_this.container(); }), filter(function (event) { return _this.isNotOnHostOrContainer(event); }));
        }));
        _this.clickIn$ = observableFromEvent(_this.host, 'click')
            .pipe(filter(function () { return !_this.container(); }));
        _this.clickOut$ = observableFromEvent(_this.document, 'click')
            .pipe(filter(function () { return !!_this.container(); }), filter(function (event) { return _this.isNotOnHostOrContainer(event); }));
        _this.tabKeyPress$ = observableFromEvent(_this.document, 'keydown')
            .pipe(filter(function (event) { return event.keyCode === 9; }), filter(function () { return !!_this.container(); }));
        _this.show$ = observableMerge(observableFromEvent(_this.host, 'focusin'), _this.clickIn$)
            .pipe(filter(function () { return !_this.container(); }), debounceTime(100), takeUntil(observableFromEvent(_this.host, 'focusout')), repeat());
        _this.hide$ = observableMerge(_this.focusOut$, _this.tabKeyPress$, _this.clickOut$);
        return _this;
    }
    NbFocusTriggerStrategy.prototype.isNotOnHostOrContainer = function (event) {
        return !this.isOnHost(event) && !this.isOnContainer(event);
    };
    NbFocusTriggerStrategy.prototype.isOnHost = function (_a) {
        var target = _a.target;
        return this.host.contains(target);
    };
    NbFocusTriggerStrategy.prototype.isOnContainer = function (_a) {
        var target = _a.target;
        return this.container() && this.container().location.nativeElement.contains(target);
    };
    return NbFocusTriggerStrategy;
}(NbTriggerStrategy));
/**
 * Creates show and hide event streams.
 * Fires open event when a focus is on the host element and stay over at least 100 milliseconds.
 * Fires close event when the focus leaves the host element.
 * */
export { NbFocusTriggerStrategy };
var NbTriggerStrategyBuilder = /** @class */ (function () {
    function NbTriggerStrategyBuilder() {
    }
    NbTriggerStrategyBuilder.prototype.document = function (document) {
        this._document = document;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.trigger = function (trigger) {
        this._trigger = trigger;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.host = function (host) {
        this._host = host;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.container = function (container) {
        this._container = container;
        return this;
    };
    NbTriggerStrategyBuilder.prototype.build = function () {
        switch (this._trigger) {
            case NbTrigger.CLICK:
                return new NbClickTriggerStrategy(this._document, this._host, this._container);
            case NbTrigger.HINT:
                return new NbHintTriggerStrategy(this._document, this._host, this._container);
            case NbTrigger.HOVER:
                return new NbHoverTriggerStrategy(this._document, this._host, this._container);
            case NbTrigger.FOCUS:
                return new NbFocusTriggerStrategy(this._document, this._host, this._container);
            default:
                throw new Error('Trigger have to be provided');
        }
    };
    return NbTriggerStrategyBuilder;
}());
export { NbTriggerStrategyBuilder };
//# sourceMappingURL=overlay-trigger.js.map
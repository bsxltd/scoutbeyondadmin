import { NgModule } from '@angular/core';
import { NbSharedModule } from '../../shared/shared.module';
import { NbA11yModule } from '../a11y/a11y.module';
import { NbCdkMappingModule } from './mapping';
import { NbPositionBuilderService } from './overlay-position';
import { NbOverlayContainerComponent } from './overlay-container';
import { NbOverlayService } from './overlay';
import { NbCdkAdapterModule } from '../adapter/adapter.module';
import { NbPositionHelper } from './position-helper';
var NbOverlayModule = /** @class */ (function () {
    function NbOverlayModule() {
    }
    NbOverlayModule.forRoot = function () {
        return {
            ngModule: NbOverlayModule,
            providers: [
                NbPositionBuilderService,
                NbOverlayService,
                NbPositionHelper
            ].concat(NbCdkMappingModule.forRoot().providers, NbCdkAdapterModule.forRoot().providers, NbA11yModule.forRoot().providers),
        };
    };
    NbOverlayModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        NbCdkMappingModule,
                        NbSharedModule,
                    ],
                    declarations: [NbOverlayContainerComponent],
                    exports: [
                        NbCdkMappingModule,
                        NbCdkAdapterModule,
                        NbOverlayContainerComponent,
                    ],
                },] },
    ];
    return NbOverlayModule;
}());
export { NbOverlayModule };
//# sourceMappingURL=overlay.module.js.map
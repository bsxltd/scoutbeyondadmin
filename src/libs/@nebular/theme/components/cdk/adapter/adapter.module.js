import { NgModule } from '@angular/core';
import { OverlayContainer, ScrollDispatcher } from '@angular/cdk/overlay';
import { NbOverlayContainerAdapter } from './overlay-container-adapter';
import { NbScrollDispatcherAdapter } from './scroll-dispatcher-adapter';
import { NbViewportRulerAdapter } from './viewport-ruler-adapter';
import { NbBlockScrollStrategyAdapter } from './block-scroll-strategy-adapter';
var NbCdkAdapterModule = /** @class */ (function () {
    function NbCdkAdapterModule() {
    }
    NbCdkAdapterModule.forRoot = function () {
        return {
            ngModule: NbCdkAdapterModule,
            providers: [
                NbViewportRulerAdapter,
                NbOverlayContainerAdapter,
                NbBlockScrollStrategyAdapter,
                { provide: OverlayContainer, useExisting: NbOverlayContainerAdapter },
                { provide: ScrollDispatcher, useClass: NbScrollDispatcherAdapter },
            ],
        };
    };
    NbCdkAdapterModule.decorators = [
        { type: NgModule, args: [{},] },
    ];
    return NbCdkAdapterModule;
}());
export { NbCdkAdapterModule };
//# sourceMappingURL=adapter.module.js.map
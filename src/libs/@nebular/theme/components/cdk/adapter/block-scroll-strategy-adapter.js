var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, Inject } from '@angular/core';
import { BlockScrollStrategy } from '@angular/cdk/overlay';
import { NB_DOCUMENT } from '../../../theme.options';
import { NbViewportRulerAdapter } from './viewport-ruler-adapter';
var NbBlockScrollStrategyAdapter = /** @class */ (function (_super) {
    __extends(NbBlockScrollStrategyAdapter, _super);
    function NbBlockScrollStrategyAdapter(ruler, document) {
        return _super.call(this, ruler, document) || this;
    }
    NbBlockScrollStrategyAdapter.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    NbBlockScrollStrategyAdapter.ctorParameters = function () { return [
        { type: NbViewportRulerAdapter, },
        { type: undefined, decorators: [{ type: Inject, args: [NB_DOCUMENT,] },] },
    ]; };
    return NbBlockScrollStrategyAdapter;
}(BlockScrollStrategy));
export { NbBlockScrollStrategyAdapter };
//# sourceMappingURL=block-scroll-strategy-adapter.js.map
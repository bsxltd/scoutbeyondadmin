var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Injectable, NgZone } from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { NbPlatform } from '../overlay/mapping';
import { NbLayoutScrollService } from '../../../services/scroll.service';
var NbScrollDispatcherAdapter = /** @class */ (function (_super) {
    __extends(NbScrollDispatcherAdapter, _super);
    function NbScrollDispatcherAdapter(ngZone, platform, scrollService) {
        var _this = _super.call(this, ngZone, platform) || this;
        _this.scrollService = scrollService;
        return _this;
    }
    NbScrollDispatcherAdapter.prototype.scrolled = function (auditTimeInMs) {
        return this.scrollService.onScroll();
    };
    NbScrollDispatcherAdapter.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    NbScrollDispatcherAdapter.ctorParameters = function () { return [
        { type: NgZone, },
        { type: NbPlatform, },
        { type: NbLayoutScrollService, },
    ]; };
    return NbScrollDispatcherAdapter;
}(ScrollDispatcher));
export { NbScrollDispatcherAdapter };
//# sourceMappingURL=scroll-dispatcher-adapter.js.map
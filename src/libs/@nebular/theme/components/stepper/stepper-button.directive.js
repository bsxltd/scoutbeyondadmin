import { NbStepperComponent } from './stepper.component';
import { Directive, HostBinding, HostListener, Input } from '@angular/core';
var NbStepperNextDirective = /** @class */ (function () {
    function NbStepperNextDirective(stepper) {
        this.stepper = stepper;
        this.type = 'submit';
    }
    NbStepperNextDirective.prototype.onClick = function () {
        this.stepper.next();
    };
    NbStepperNextDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'button[nbStepperNext]',
                },] },
    ];
    /** @nocollapse */
    NbStepperNextDirective.ctorParameters = function () { return [
        { type: NbStepperComponent, },
    ]; };
    NbStepperNextDirective.propDecorators = {
        "type": [{ type: Input }, { type: HostBinding, args: ['attr.type',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbStepperNextDirective;
}());
export { NbStepperNextDirective };
var NbStepperPreviousDirective = /** @class */ (function () {
    function NbStepperPreviousDirective(stepper) {
        this.stepper = stepper;
        this.type = 'button';
    }
    NbStepperPreviousDirective.prototype.onClick = function () {
        this.stepper.previous();
    };
    NbStepperPreviousDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'button[nbStepperPrevious]',
                },] },
    ];
    /** @nocollapse */
    NbStepperPreviousDirective.ctorParameters = function () { return [
        { type: NbStepperComponent, },
    ]; };
    NbStepperPreviousDirective.propDecorators = {
        "type": [{ type: Input }, { type: HostBinding, args: ['attr.type',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbStepperPreviousDirective;
}());
export { NbStepperPreviousDirective };
//# sourceMappingURL=stepper-button.directive.js.map
import { Component, forwardRef, Inject, Input, TemplateRef, ViewChild, } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { NbStepperComponent } from './stepper.component';
import { convertToBoolProperty } from '../helpers';
/**
 * Component intended to be used within  the `<nb-stepper>` component.
 * Container for a step
 */
var NbStepComponent = /** @class */ (function () {
    function NbStepComponent(stepper) {
        this.stepper = stepper;
        this.completedValue = false;
        this.interacted = false;
    }
    Object.defineProperty(NbStepComponent.prototype, "isLabelTemplate", {
        /**
         * Check that label is a TemplateRef.
         *
         * @return boolean
         * */
        get: /**
           * Check that label is a TemplateRef.
           *
           * @return boolean
           * */
        function () {
            return this.label instanceof TemplateRef;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbStepComponent.prototype, "completed", {
        get: /**
           * Whether step is marked as completed.
           *
           * @type {boolean}
           */
        function () {
            return this.completedValue || this.isCompleted;
        },
        set: function (value) {
            this.completedValue = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbStepComponent.prototype, "isCompleted", {
        get: function () {
            return this.stepControl ? this.stepControl.valid && this.interacted : this.interacted;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Mark step as selected
     * */
    /**
       * Mark step as selected
       * */
    NbStepComponent.prototype.select = /**
       * Mark step as selected
       * */
    function () {
        this.stepper.selected = this;
    };
    /**
     * Reset step and stepControl state
     * */
    /**
       * Reset step and stepControl state
       * */
    NbStepComponent.prototype.reset = /**
       * Reset step and stepControl state
       * */
    function () {
        this.interacted = false;
        if (this.stepControl) {
            this.stepControl.reset();
        }
    };
    NbStepComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-step',
                    template: "\n    <ng-template>\n      <ng-content></ng-content>\n    </ng-template>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbStepComponent.ctorParameters = function () { return [
        { type: NbStepperComponent, decorators: [{ type: Inject, args: [forwardRef(function () { return NbStepperComponent; }),] },] },
    ]; };
    NbStepComponent.propDecorators = {
        "content": [{ type: ViewChild, args: [TemplateRef,] },],
        "stepControl": [{ type: Input },],
        "label": [{ type: Input },],
        "hidden": [{ type: Input },],
        "completed": [{ type: Input },],
    };
    return NbStepComponent;
}());
export { NbStepComponent };
//# sourceMappingURL=step.component.js.map
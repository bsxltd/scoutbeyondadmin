/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { EventEmitter } from '@angular/core';
/**
 * Alert component.
 *
 * Basic alert example:
 * @stacked-example(Showcase, alert/alert-showcase.component)
 *
 * Alert configuration:
 *
 * ```html
 * <nb-alert status="success">
 *   You have been successfully authenticated!
 * </nb-alert>
 * ```
 * ### Installation
 *
 * Import `NbButtonModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbAlertModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Alert could additionally have a `close` button when `closable` property is set:
 * ```html
 * <nb-alert status="success" closable (close)="onClose()">
 *   You have been successfully authenticated!
 * </nb-alert>
 * ```
 *
 * Colored alerts could be simply configured by providing a `status` property:
 * @stacked-example(Colored Alert, alert/alert-colors.component)
 *
 * It is also possible to assign an `accent` property for a slight alert highlight
 * as well as combine it with `status`:
 * @stacked-example(Accent Alert, alert/alert-accents.component)
 *
 * And `outline` property:
 * @stacked-example(Outline Alert, alert/alert-outline.component)
 *
 * @additional-example(Multiple Sizes, alert/alert-sizes.component)
 *
 * @styles
 *
 * alert-font-size:
 * alert-line-height:
 * alert-font-weight:
 * alert-fg:
 * alert-outline-fg:
 * alert-bg:
 * alert-active-bg:
 * alert-disabled-bg:
 * alert-disabled-fg:
 * alert-primary-bg:
 * alert-info-bg:
 * alert-success-bg:
 * alert-warning-bg:
 * alert-danger-bg:
 * alert-height-xxsmall:
 * alert-height-xsmall:
 * alert-height-small:
 * alert-height-medium:
 * alert-height-large:
 * alert-height-xlarge:
 * alert-height-xxlarge:
 * alert-shadow:
 * alert-border-radius:
 * alert-padding:
 * alert-closable-padding:
 * alert-button-padding:
 * alert-margin:
 */
export declare class NbAlertComponent {
    static readonly SIZE_XXSMALL: string;
    static readonly SIZE_XSMALL: string;
    static readonly SIZE_SMALL: string;
    static readonly SIZE_MEDIUM: string;
    static readonly SIZE_LARGE: string;
    static readonly SIZE_XLARGE: string;
    static readonly SIZE_XXLARGE: string;
    static readonly STATUS_ACTIVE: string;
    static readonly STATUS_DISABLED: string;
    static readonly STATUS_PRIMARY: string;
    static readonly STATUS_INFO: string;
    static readonly STATUS_SUCCESS: string;
    static readonly STATUS_WARNING: string;
    static readonly STATUS_DANGER: string;
    static readonly ACCENT_ACTIVE: string;
    static readonly ACCENT_DISABLED: string;
    static readonly ACCENT_PRIMARY: string;
    static readonly ACCENT_INFO: string;
    static readonly ACCENT_SUCCESS: string;
    static readonly ACCENT_WARNING: string;
    static readonly ACCENT_DANGER: string;
    static readonly OUTLINE_ACTIVE: string;
    static readonly OUTLINE_DISABLED: string;
    static readonly OUTLINE_PRIMARY: string;
    static readonly OUTLINE_INFO: string;
    static readonly OUTLINE_SUCCESS: string;
    static readonly OUTLINE_WARNING: string;
    static readonly OUTLINE_DANGER: string;
    size: string;
    status: string;
    accent: string;
    outline: string;
    closableValue: boolean;
    /**
     * Shows `close` icon
     */
    closable: boolean;
    readonly xxsmall: boolean;
    readonly xsmall: boolean;
    readonly small: boolean;
    readonly medium: boolean;
    readonly large: boolean;
    readonly xlarge: boolean;
    readonly xxlarge: boolean;
    readonly active: boolean;
    readonly disabled: boolean;
    readonly primary: boolean;
    readonly info: boolean;
    readonly success: boolean;
    readonly warning: boolean;
    readonly danger: boolean;
    readonly hasAccent: string;
    readonly hasStatus: string;
    readonly primaryAccent: boolean;
    readonly infoAccent: boolean;
    readonly successAccent: boolean;
    readonly warningAccent: boolean;
    readonly dangerAccent: boolean;
    readonly activeAccent: boolean;
    readonly disabledAccent: boolean;
    readonly hasOutline: string;
    readonly primaryOutline: boolean;
    readonly infoOutline: boolean;
    readonly successOutline: boolean;
    readonly warningOutline: boolean;
    readonly dangerOutline: boolean;
    readonly activeOutline: boolean;
    readonly disabledOutline: boolean;
    /**
     * Alert size, available sizes:
     * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
     * @param {string} val
     */
    private setSize;
    /**
     * Alert status (adds specific styles):
     * active, disabled, primary, info, success, warning, danger
     * @param {string} val
     */
    private setStatus;
    /**
     * Alert accent (color of the top border):
     * active, disabled, primary, info, success, warning, danger
     * @param {string} val
     */
    private setAccent;
    /**
     * Alert outline (color of the border):
     * active, disabled, primary, info, success, warning, danger
     * @param {string} val
     */
    private setOutline;
    /**
     * Emits when chip is removed
     * @type EventEmitter<any>
     */
    close: EventEmitter<{}>;
    /**
     * Emits the removed chip event
     */
    onClose(): void;
}

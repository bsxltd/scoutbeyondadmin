/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { convertToBoolProperty } from '../helpers';
/**
 * Alert component.
 *
 * Basic alert example:
 * @stacked-example(Showcase, alert/alert-showcase.component)
 *
 * Alert configuration:
 *
 * ```html
 * <nb-alert status="success">
 *   You have been successfully authenticated!
 * </nb-alert>
 * ```
 * ### Installation
 *
 * Import `NbButtonModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbAlertModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Alert could additionally have a `close` button when `closable` property is set:
 * ```html
 * <nb-alert status="success" closable (close)="onClose()">
 *   You have been successfully authenticated!
 * </nb-alert>
 * ```
 *
 * Colored alerts could be simply configured by providing a `status` property:
 * @stacked-example(Colored Alert, alert/alert-colors.component)
 *
 * It is also possible to assign an `accent` property for a slight alert highlight
 * as well as combine it with `status`:
 * @stacked-example(Accent Alert, alert/alert-accents.component)
 *
 * And `outline` property:
 * @stacked-example(Outline Alert, alert/alert-outline.component)
 *
 * @additional-example(Multiple Sizes, alert/alert-sizes.component)
 *
 * @styles
 *
 * alert-font-size:
 * alert-line-height:
 * alert-font-weight:
 * alert-fg:
 * alert-outline-fg:
 * alert-bg:
 * alert-active-bg:
 * alert-disabled-bg:
 * alert-disabled-fg:
 * alert-primary-bg:
 * alert-info-bg:
 * alert-success-bg:
 * alert-warning-bg:
 * alert-danger-bg:
 * alert-height-xxsmall:
 * alert-height-xsmall:
 * alert-height-small:
 * alert-height-medium:
 * alert-height-large:
 * alert-height-xlarge:
 * alert-height-xxlarge:
 * alert-shadow:
 * alert-border-radius:
 * alert-padding:
 * alert-closable-padding:
 * alert-button-padding:
 * alert-margin:
 */
var NbAlertComponent = /** @class */ (function () {
    function NbAlertComponent() {
        this.closableValue = false;
        /**
           * Emits when chip is removed
           * @type EventEmitter<any>
           */
        this.close = new EventEmitter();
    }
    Object.defineProperty(NbAlertComponent.prototype, "closable", {
        set: /**
           * Shows `close` icon
           */
        function (val) {
            this.closableValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xxsmall", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XXSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "small", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "medium", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "large", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xlarge", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "xxlarge", {
        get: function () {
            return this.size === NbAlertComponent.SIZE_XXLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "active", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "disabled", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "primary", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "info", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "success", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "warning", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "danger", {
        get: function () {
            return this.status === NbAlertComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "hasAccent", {
        get: function () {
            return this.accent;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "hasStatus", {
        get: function () {
            return this.status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "primaryAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "infoAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "successAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "warningAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "dangerAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "activeAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "disabledAccent", {
        get: function () {
            return this.accent === NbAlertComponent.ACCENT_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "hasOutline", {
        get: function () {
            return this.outline;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "primaryOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "infoOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "successOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "warningOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "dangerOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "activeOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "disabledOutline", {
        get: function () {
            return this.outline === NbAlertComponent.OUTLINE_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setSize", {
        set: /**
           * Alert size, available sizes:
           * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setStatus", {
        set: /**
           * Alert status (adds specific styles):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setAccent", {
        set: /**
           * Alert accent (color of the top border):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.accent = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAlertComponent.prototype, "setOutline", {
        set: /**
           * Alert outline (color of the border):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.outline = val;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Emits the removed chip event
     */
    /**
       * Emits the removed chip event
       */
    NbAlertComponent.prototype.onClose = /**
       * Emits the removed chip event
       */
    function () {
        this.close.emit();
    };
    NbAlertComponent.SIZE_XXSMALL = 'xxsmall';
    NbAlertComponent.SIZE_XSMALL = 'xsmall';
    NbAlertComponent.SIZE_SMALL = 'small';
    NbAlertComponent.SIZE_MEDIUM = 'medium';
    NbAlertComponent.SIZE_LARGE = 'large';
    NbAlertComponent.SIZE_XLARGE = 'xlarge';
    NbAlertComponent.SIZE_XXLARGE = 'xxlarge';
    NbAlertComponent.STATUS_ACTIVE = 'active';
    NbAlertComponent.STATUS_DISABLED = 'disabled';
    NbAlertComponent.STATUS_PRIMARY = 'primary';
    NbAlertComponent.STATUS_INFO = 'info';
    NbAlertComponent.STATUS_SUCCESS = 'success';
    NbAlertComponent.STATUS_WARNING = 'warning';
    NbAlertComponent.STATUS_DANGER = 'danger';
    NbAlertComponent.ACCENT_ACTIVE = 'active';
    NbAlertComponent.ACCENT_DISABLED = 'disabled';
    NbAlertComponent.ACCENT_PRIMARY = 'primary';
    NbAlertComponent.ACCENT_INFO = 'info';
    NbAlertComponent.ACCENT_SUCCESS = 'success';
    NbAlertComponent.ACCENT_WARNING = 'warning';
    NbAlertComponent.ACCENT_DANGER = 'danger';
    NbAlertComponent.OUTLINE_ACTIVE = 'active';
    NbAlertComponent.OUTLINE_DISABLED = 'disabled';
    NbAlertComponent.OUTLINE_PRIMARY = 'primary';
    NbAlertComponent.OUTLINE_INFO = 'info';
    NbAlertComponent.OUTLINE_SUCCESS = 'success';
    NbAlertComponent.OUTLINE_WARNING = 'warning';
    NbAlertComponent.OUTLINE_DANGER = 'danger';
    NbAlertComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-alert',
                    styles: [":host{display:flex;flex-direction:column;position:relative}.close{position:absolute;top:0;right:0;color:inherit;background-color:transparent;border:0;-webkit-appearance:none} "],
                    template: "\n    <button *ngIf=\"closableValue\" type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"onClose()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n    <ng-content></ng-content>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbAlertComponent.propDecorators = {
        "closableValue": [{ type: HostBinding, args: ['class.closable',] },],
        "closable": [{ type: Input },],
        "xxsmall": [{ type: HostBinding, args: ['class.xxsmall-alert',] },],
        "xsmall": [{ type: HostBinding, args: ['class.xsmall-alert',] },],
        "small": [{ type: HostBinding, args: ['class.small-alert',] },],
        "medium": [{ type: HostBinding, args: ['class.medium-alert',] },],
        "large": [{ type: HostBinding, args: ['class.large-alert',] },],
        "xlarge": [{ type: HostBinding, args: ['class.xlarge-alert',] },],
        "xxlarge": [{ type: HostBinding, args: ['class.xxlarge-alert',] },],
        "active": [{ type: HostBinding, args: ['class.active-alert',] },],
        "disabled": [{ type: HostBinding, args: ['class.disabled-alert',] },],
        "primary": [{ type: HostBinding, args: ['class.primary-alert',] },],
        "info": [{ type: HostBinding, args: ['class.info-alert',] },],
        "success": [{ type: HostBinding, args: ['class.success-alert',] },],
        "warning": [{ type: HostBinding, args: ['class.warning-alert',] },],
        "danger": [{ type: HostBinding, args: ['class.danger-alert',] },],
        "hasAccent": [{ type: HostBinding, args: ['class.accent',] },],
        "hasStatus": [{ type: HostBinding, args: ['class.status',] },],
        "primaryAccent": [{ type: HostBinding, args: ['class.accent-primary',] },],
        "infoAccent": [{ type: HostBinding, args: ['class.accent-info',] },],
        "successAccent": [{ type: HostBinding, args: ['class.accent-success',] },],
        "warningAccent": [{ type: HostBinding, args: ['class.accent-warning',] },],
        "dangerAccent": [{ type: HostBinding, args: ['class.accent-danger',] },],
        "activeAccent": [{ type: HostBinding, args: ['class.accent-active',] },],
        "disabledAccent": [{ type: HostBinding, args: ['class.accent-disabled',] },],
        "hasOutline": [{ type: HostBinding, args: ['class.outline',] },],
        "primaryOutline": [{ type: HostBinding, args: ['class.outline-primary',] },],
        "infoOutline": [{ type: HostBinding, args: ['class.outline-info',] },],
        "successOutline": [{ type: HostBinding, args: ['class.outline-success',] },],
        "warningOutline": [{ type: HostBinding, args: ['class.outline-warning',] },],
        "dangerOutline": [{ type: HostBinding, args: ['class.outline-danger',] },],
        "activeOutline": [{ type: HostBinding, args: ['class.outline-active',] },],
        "disabledOutline": [{ type: HostBinding, args: ['class.outline-disabled',] },],
        "setSize": [{ type: Input, args: ['size',] },],
        "setStatus": [{ type: Input, args: ['status',] },],
        "setAccent": [{ type: Input, args: ['accent',] },],
        "setOutline": [{ type: Input, args: ['outline',] },],
        "close": [{ type: Output },],
    };
    return NbAlertComponent;
}());
export { NbAlertComponent };
//# sourceMappingURL=alert.component.js.map
/**
 * Styled spinner component
 */
export declare class NbSpinnerComponent {
    static readonly SIZE_XXSMALL: string;
    static readonly SIZE_XSMALL: string;
    static readonly SIZE_SMALL: string;
    static readonly SIZE_MEDIUM: string;
    static readonly SIZE_LARGE: string;
    static readonly SIZE_XLARGE: string;
    static readonly SIZE_XXLARGE: string;
    static readonly STATUS_ACTIVE: string;
    static readonly STATUS_DISABLED: string;
    static readonly STATUS_PRIMARY: string;
    static readonly STATUS_INFO: string;
    static readonly STATUS_SUCCESS: string;
    static readonly STATUS_WARNING: string;
    static readonly STATUS_DANGER: string;
    size: string;
    status: string;
    /**
     * Loading text that is shown near the icon
     * @type string
     */
    message: string;
    /**
     * Spiiner size, available sizes:
     * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
     * @param {string} val
     */
    private setSize;
    /**
     * Spiiner status (adds specific styles):
     * active, disabled, primary, info, success, warning, danger
     * @param {string} val
     */
    private setStatus;
    readonly xxsmall: boolean;
    readonly xsmall: boolean;
    readonly small: boolean;
    readonly medium: boolean;
    readonly large: boolean;
    readonly xlarge: boolean;
    readonly xxlarge: boolean;
    readonly active: boolean;
    readonly disabled: boolean;
    readonly primary: boolean;
    readonly info: boolean;
    readonly success: boolean;
    readonly warning: boolean;
    readonly danger: boolean;
}

/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, HostBinding, Input } from '@angular/core';
/**
 * Styled spinner component
 */
var NbSpinnerComponent = /** @class */ (function () {
    function NbSpinnerComponent() {
        this.size = NbSpinnerComponent.SIZE_MEDIUM;
        this.status = NbSpinnerComponent.STATUS_ACTIVE;
        /**
           * Loading text that is shown near the icon
           * @type string
           */
        this.message = 'Loading...';
    }
    Object.defineProperty(NbSpinnerComponent.prototype, "setSize", {
        set: /**
           * Spiiner size, available sizes:
           * xxsmall, xsmall, small, medium, large, xlarge, xxlarge
           * @param {string} val
           */
        function (val) {
            this.size = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "setStatus", {
        set: /**
           * Spiiner status (adds specific styles):
           * active, disabled, primary, info, success, warning, danger
           * @param {string} val
           */
        function (val) {
            this.status = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xxsmall", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XXSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xsmall", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XSMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "small", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "medium", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "large", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xlarge", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "xxlarge", {
        get: function () {
            return this.size === NbSpinnerComponent.SIZE_XXLARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "active", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_ACTIVE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "disabled", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_DISABLED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "primary", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "info", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "success", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "warning", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbSpinnerComponent.prototype, "danger", {
        get: function () {
            return this.status === NbSpinnerComponent.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    NbSpinnerComponent.SIZE_XXSMALL = 'xxsmall';
    NbSpinnerComponent.SIZE_XSMALL = 'xsmall';
    NbSpinnerComponent.SIZE_SMALL = 'small';
    NbSpinnerComponent.SIZE_MEDIUM = 'medium';
    NbSpinnerComponent.SIZE_LARGE = 'large';
    NbSpinnerComponent.SIZE_XLARGE = 'xlarge';
    NbSpinnerComponent.SIZE_XXLARGE = 'xxlarge';
    NbSpinnerComponent.STATUS_ACTIVE = 'active';
    NbSpinnerComponent.STATUS_DISABLED = 'disabled';
    NbSpinnerComponent.STATUS_PRIMARY = 'primary';
    NbSpinnerComponent.STATUS_INFO = 'info';
    NbSpinnerComponent.STATUS_SUCCESS = 'success';
    NbSpinnerComponent.STATUS_WARNING = 'warning';
    NbSpinnerComponent.STATUS_DANGER = 'danger';
    NbSpinnerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-spinner',
                    template: "\n    <span class=\"spin-circle\"></span>\n    <span class=\"message\" *ngIf=\"message\">{{ message }}</span>\n  ",
                    styles: [":host{opacity:1;position:absolute;border-radius:inherit;top:0;right:0;left:0;bottom:0;overflow:hidden;z-index:9999;display:flex;justify-content:center;align-items:center;visibility:visible}:host .spin-circle{animation:spin 0.8s infinite linear;border-radius:50%;border-style:solid;border-width:0.125em;width:1em;height:1em}:host .message{margin-left:0.5rem;line-height:1rem;font-size:1rem} "],
                },] },
    ];
    /** @nocollapse */
    NbSpinnerComponent.propDecorators = {
        "message": [{ type: Input },],
        "setSize": [{ type: Input, args: ['size',] },],
        "setStatus": [{ type: Input, args: ['status',] },],
        "xxsmall": [{ type: HostBinding, args: ['class.xxsmall-spinner',] },],
        "xsmall": [{ type: HostBinding, args: ['class.xsmall-spinner',] },],
        "small": [{ type: HostBinding, args: ['class.small-spinner',] },],
        "medium": [{ type: HostBinding, args: ['class.medium-spinner',] },],
        "large": [{ type: HostBinding, args: ['class.large-spinner',] },],
        "xlarge": [{ type: HostBinding, args: ['class.xlarge-spinner',] },],
        "xxlarge": [{ type: HostBinding, args: ['class.xxlarge-spinner',] },],
        "active": [{ type: HostBinding, args: ['class.active-spinner',] },],
        "disabled": [{ type: HostBinding, args: ['class.disabled-spinner',] },],
        "primary": [{ type: HostBinding, args: ['class.primary-spinner',] },],
        "info": [{ type: HostBinding, args: ['class.info-spinner',] },],
        "success": [{ type: HostBinding, args: ['class.success-spinner',] },],
        "warning": [{ type: HostBinding, args: ['class.warning-spinner',] },],
        "danger": [{ type: HostBinding, args: ['class.danger-spinner',] },],
    };
    return NbSpinnerComponent;
}());
export { NbSpinnerComponent };
//# sourceMappingURL=spinner.component.js.map
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, ChangeDetectionStrategy, Host, ElementRef, ChangeDetectorRef, } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { takeWhile } from 'rxjs/operators';
import { NbAccordionItemComponent } from './accordion-item.component';
var accordionItemBodyTrigger = trigger('accordionItemBody', [
    state('collapsed', style({
        overflow: 'hidden',
        visibility: 'hidden',
        height: 0,
    })),
    state('expanded', style({
        overflow: 'hidden',
        visibility: 'visible',
        height: '{{ contentHeight }}',
    }), { params: { contentHeight: '1rem' } }),
    transition('collapsed => expanded', animate('100ms ease-in')),
    transition('expanded => collapsed', animate('100ms ease-out')),
]);
/**
 * Component intended to be used within `<nb-accordion-item>` component
 */
var NbAccordionItemBodyComponent = /** @class */ (function () {
    function NbAccordionItemBodyComponent(accordionItem, el, cd) {
        this.accordionItem = accordionItem;
        this.el = el;
        this.cd = cd;
        this.alive = true;
    }
    Object.defineProperty(NbAccordionItemBodyComponent.prototype, "state", {
        get: function () {
            return this.accordionItem.collapsed ? 'collapsed' : 'expanded';
        },
        enumerable: true,
        configurable: true
    });
    NbAccordionItemBodyComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.contentHeight = this.el.nativeElement.clientHeight + "px";
        this.accordionItem.accordionItemInvalidate
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.cd.markForCheck(); });
    };
    NbAccordionItemBodyComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbAccordionItemBodyComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-accordion-item-body',
                    template: "\n    <div [@accordionItemBody]=\"{ value: state, params: { contentHeight: contentHeight } }\">\n      <div class=\"item-body\">\n        <ng-content></ng-content>\n      </div>\n    </div>\n  ",
                    animations: [accordionItemBodyTrigger],
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbAccordionItemBodyComponent.ctorParameters = function () { return [
        { type: NbAccordionItemComponent, decorators: [{ type: Host },] },
        { type: ElementRef, },
        { type: ChangeDetectorRef, },
    ]; };
    return NbAccordionItemBodyComponent;
}());
export { NbAccordionItemBodyComponent };
//# sourceMappingURL=accordion-item-body.component.js.map
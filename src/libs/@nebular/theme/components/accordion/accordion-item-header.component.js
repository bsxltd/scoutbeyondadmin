/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, ChangeDetectionStrategy, Host, HostBinding, HostListener, ChangeDetectorRef, } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { takeWhile } from 'rxjs/operators';
import { NbAccordionItemComponent } from './accordion-item.component';
/**
 * Component intended to be used within `<nb-accordion-item>` component
 */
var NbAccordionItemHeaderComponent = /** @class */ (function () {
    function NbAccordionItemHeaderComponent(accordionItem, cd) {
        this.accordionItem = accordionItem;
        this.cd = cd;
        this.alive = true;
    }
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "isCollapsed", {
        get: function () {
            return this.accordionItem.collapsed;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "expanded", {
        get: function () {
            return !this.accordionItem.collapsed;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "tabbable", {
        get: function () {
            return this.accordionItem.disabled ? '-1' : '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "disabled", {
        get: function () {
            return this.accordionItem.disabled;
        },
        enumerable: true,
        configurable: true
    });
    NbAccordionItemHeaderComponent.prototype.toggle = function () {
        this.accordionItem.toggle();
    };
    Object.defineProperty(NbAccordionItemHeaderComponent.prototype, "state", {
        get: function () {
            if (this.isCollapsed) {
                return 'collapsed';
            }
            if (this.expanded) {
                return 'expanded';
            }
        },
        enumerable: true,
        configurable: true
    });
    NbAccordionItemHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accordionItem.accordionItemInvalidate
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.cd.markForCheck(); });
    };
    NbAccordionItemHeaderComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbAccordionItemHeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-accordion-item-header',
                    styles: [":host{display:flex;align-items:center;cursor:pointer}:host:focus{outline:0} "],
                    template: "\n    <ng-content select=\"nb-accordion-item-title\"></ng-content>\n    <ng-content select=\"nb-accordion-item-description\"></ng-content>\n    <ng-content></ng-content>\n    <i [@expansionIndicator]=\"state\" *ngIf=\"!disabled\" class=\"nb-arrow-down\"></i>\n  ",
                    animations: [
                        trigger('expansionIndicator', [
                            state('expanded', style({
                                transform: 'rotate(180deg)',
                            })),
                            transition('collapsed => expanded', animate('100ms ease-in')),
                            transition('expanded => collapsed', animate('100ms ease-out')),
                        ]),
                    ],
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbAccordionItemHeaderComponent.ctorParameters = function () { return [
        { type: NbAccordionItemComponent, decorators: [{ type: Host },] },
        { type: ChangeDetectorRef, },
    ]; };
    NbAccordionItemHeaderComponent.propDecorators = {
        "isCollapsed": [{ type: HostBinding, args: ['class.accordion-item-header-collapsed',] },],
        "expanded": [{ type: HostBinding, args: ['class.accordion-item-header-expanded',] }, { type: HostBinding, args: ['attr.aria-expanded',] },],
        "tabbable": [{ type: HostBinding, args: ['attr.tabindex',] },],
        "disabled": [{ type: HostBinding, args: ['attr.aria-disabled',] },],
        "toggle": [{ type: HostListener, args: ['click',] },],
    };
    return NbAccordionItemHeaderComponent;
}());
export { NbAccordionItemHeaderComponent };
//# sourceMappingURL=accordion-item-header.component.js.map
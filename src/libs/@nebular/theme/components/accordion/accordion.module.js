/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAccordionComponent } from './accordion.component';
import { NbAccordionItemComponent } from './accordion-item.component';
import { NbAccordionItemHeaderComponent } from './accordion-item-header.component';
import { NbAccordionItemBodyComponent } from './accordion-item-body.component';
var NB_ACCORDION_COMPONENTS = [
    NbAccordionComponent,
    NbAccordionItemComponent,
    NbAccordionItemHeaderComponent,
    NbAccordionItemBodyComponent,
];
var NbAccordionModule = /** @class */ (function () {
    function NbAccordionModule() {
    }
    NbAccordionModule.decorators = [
        { type: NgModule, args: [{
                    imports: [CommonModule],
                    exports: NB_ACCORDION_COMPONENTS.slice(),
                    declarations: NB_ACCORDION_COMPONENTS.slice(),
                    providers: [],
                },] },
    ];
    return NbAccordionModule;
}());
export { NbAccordionModule };
//# sourceMappingURL=accordion.module.js.map
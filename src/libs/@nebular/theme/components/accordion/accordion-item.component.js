/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, ChangeDetectionStrategy, ChangeDetectorRef, Input, Output, EventEmitter, HostBinding, Host, } from '@angular/core';
import { Subject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { NbAccordionComponent } from './accordion.component';
import { convertToBoolProperty } from '../helpers';
/**
 * Component intended to be used within `<nb-accordion>` component
 */
var NbAccordionItemComponent = /** @class */ (function () {
    function NbAccordionItemComponent(accordion, cd) {
        this.accordion = accordion;
        this.cd = cd;
        /**
           * Emits whenever the expanded state of the accordion changes.
           * Primarily used to facilitate two-way binding.
           */
        this.collapsedChange = new EventEmitter();
        this.accordionItemInvalidate = new Subject();
        this.collapsedValue = true;
        this.disabledValue = false;
        this.alive = true;
    }
    Object.defineProperty(NbAccordionItemComponent.prototype, "collapsed", {
        get: /**
           * Item is collapse (`true` by default)
           * @type {boolean}
           */
        function () {
            return this.collapsedValue;
        },
        set: function (val) {
            this.collapsedValue = convertToBoolProperty(val);
            this.collapsedChange.emit(this.collapsedValue);
            this.invalidate();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemComponent.prototype, "expanded", {
        get: /**
           * Item is expanded (`false` by default)
           * @type {boolean}
           */
        function () {
            return !this.collapsed;
        },
        set: function (val) {
            this.collapsedValue = !convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbAccordionItemComponent.prototype, "disabled", {
        get: /**
           * Item is disabled and cannot be opened.
           * @type {boolean}
           */
        function () {
            return this.disabledValue;
        },
        set: function (val) {
            this.disabledValue = convertToBoolProperty(val);
            this.invalidate();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Open/close the item
     */
    /**
       * Open/close the item
       */
    NbAccordionItemComponent.prototype.toggle = /**
       * Open/close the item
       */
    function () {
        if (!this.disabled) {
            // we need this temporary variable as `openCloseItems.next` will change current value we need to save
            var willSet = !this.collapsed;
            if (!this.accordion.multi) {
                this.accordion.openCloseItems.next(true);
            }
            this.collapsed = willSet;
        }
    };
    /**
     * Open the item.
     */
    /**
       * Open the item.
       */
    NbAccordionItemComponent.prototype.open = /**
       * Open the item.
       */
    function () {
        !this.disabled && (this.collapsed = false);
    };
    /**
     * Collapse the item.
     */
    /**
       * Collapse the item.
       */
    NbAccordionItemComponent.prototype.close = /**
       * Collapse the item.
       */
    function () {
        !this.disabled && (this.collapsed = true);
    };
    NbAccordionItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accordion.openCloseItems
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function (collapsed) {
            !_this.disabled && (_this.collapsed = collapsed);
        });
    };
    NbAccordionItemComponent.prototype.ngOnChanges = function (changes) {
        this.accordionItemInvalidate.next(true);
    };
    NbAccordionItemComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
        this.accordionItemInvalidate.complete();
    };
    NbAccordionItemComponent.prototype.invalidate = function () {
        this.accordionItemInvalidate.next(true);
        this.cd.markForCheck();
    };
    NbAccordionItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-accordion-item',
                    styles: [":host{display:flex;flex-direction:column} "],
                    template: "\n    <ng-content select=\"nb-accordion-item-header\"></ng-content>\n    <ng-content select=\"nb-accordion-item-body\"></ng-content>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbAccordionItemComponent.ctorParameters = function () { return [
        { type: NbAccordionComponent, decorators: [{ type: Host },] },
        { type: ChangeDetectorRef, },
    ]; };
    NbAccordionItemComponent.propDecorators = {
        "collapsed": [{ type: Input, args: ['collapsed',] }, { type: HostBinding, args: ['class.collapsed',] },],
        "expanded": [{ type: Input, args: ['expanded',] }, { type: HostBinding, args: ['class.expanded',] },],
        "disabled": [{ type: Input, args: ['disabled',] }, { type: HostBinding, args: ['class.disabled',] },],
        "collapsedChange": [{ type: Output },],
    };
    return NbAccordionItemComponent;
}());
export { NbAccordionItemComponent };
//# sourceMappingURL=accordion-item.component.js.map
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';
import { NbToast, NbToastStatus } from './model';
/**
 * The `NbToastComponent` is responsible for rendering each toast with appropriate styles.
 *
 * @styles
 *
 * toastr-bg
 * toastr-padding
 * toastr-fg
 * toastr-border
 * toastr-border-radius
 * toastr-border-color
 * */
/**
 * TODO
 * Remove svg icons, include them in font.
 * */
var NbToastComponent = /** @class */ (function () {
    function NbToastComponent() {
        this.destroy = new EventEmitter();
    }
    Object.defineProperty(NbToastComponent.prototype, "success", {
        get: function () {
            return this.toast.config.status === NbToastStatus.SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "info", {
        get: function () {
            return this.toast.config.status === NbToastStatus.INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "warning", {
        get: function () {
            return this.toast.config.status === NbToastStatus.WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "primary", {
        get: function () {
            return this.toast.config.status === NbToastStatus.PRIMARY;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "danger", {
        get: function () {
            return this.toast.config.status === NbToastStatus.DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "default", {
        get: function () {
            return this.toast.config.status === NbToastStatus.DEFAULT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "destroyByClick", {
        get: function () {
            return this.toast.config.destroyByClick;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "hasIcon", {
        get: function () {
            return this.toast.config.hasIcon && this.toast.config.status !== NbToastStatus.DEFAULT;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "customIcon", {
        get: function () {
            return !!this.icon;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbToastComponent.prototype, "icon", {
        get: function () {
            return this.toast.config.icon;
        },
        enumerable: true,
        configurable: true
    });
    NbToastComponent.prototype.onClick = function () {
        this.destroy.emit();
    };
    NbToastComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-toast',
                    styles: [":host{display:flex;align-items:center;width:25rem;margin:0.5rem;opacity:0.9}:host .title{font-weight:800;margin-right:0.25rem}:host>.content-container{line-height:1.25}:host>.content-container>.message{font-weight:300}:host.default .content-container,:host:not(.has-icon) .content-container{display:flex;flex-direction:row}:host.destroy-by-click{cursor:pointer}:host.destroy-by-click:hover{opacity:1}:host .icon{font-size:2.5rem}:host svg{width:2.5rem;height:2.5rem} "],
                    template: "<i class=\"icon {{ icon }}\" *ngIf=\"hasIcon\"></i> <div class=\"content-container\"> <span class=\"title\">{{ toast.title }}</span> <div class=\"message\">{{ toast.message }}</div> </div> ",
                },] },
    ];
    /** @nocollapse */
    NbToastComponent.propDecorators = {
        "toast": [{ type: Input },],
        "destroy": [{ type: Output },],
        "success": [{ type: HostBinding, args: ['class.success',] },],
        "info": [{ type: HostBinding, args: ['class.info',] },],
        "warning": [{ type: HostBinding, args: ['class.warning',] },],
        "primary": [{ type: HostBinding, args: ['class.primary',] },],
        "danger": [{ type: HostBinding, args: ['class.danger',] },],
        "default": [{ type: HostBinding, args: ['class.default',] },],
        "destroyByClick": [{ type: HostBinding, args: ['class.destroy-by-click',] },],
        "hasIcon": [{ type: HostBinding, args: ['class.has-icon',] },],
        "customIcon": [{ type: HostBinding, args: ['class.custom-icon',] },],
        "onClick": [{ type: HostListener, args: ['click',] },],
    };
    return NbToastComponent;
}());
export { NbToastComponent };
//# sourceMappingURL=toast.component.js.map
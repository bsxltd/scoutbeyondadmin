/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Input, QueryList, ViewChildren } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { NbToastComponent } from './toast.component';
import { NbLayoutDirectionService } from '../../services/direction.service';
import { NbPositionHelper } from '../cdk';
import { takeWhile } from 'rxjs/operators';
var voidState = style({
    transform: 'translateX({{ direction }}110%)',
    height: 0,
    margin: 0,
});
var defaultOptions = { params: { direction: '' } };
var NbToastrContainerComponent = /** @class */ (function () {
    function NbToastrContainerComponent(layoutDirection, positionHelper) {
        this.layoutDirection = layoutDirection;
        this.positionHelper = positionHelper;
        this.content = [];
        this.alive = true;
    }
    NbToastrContainerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.layoutDirection.onDirectionChange()
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function () { return _this.onDirectionChange(); });
    };
    NbToastrContainerComponent.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbToastrContainerComponent.prototype.onDirectionChange = function () {
        var direction = this.positionHelper.isRightPosition(this.position) ? '' : '-';
        this.fadeIn = { value: '', params: { direction: direction } };
    };
    NbToastrContainerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-toastr-container',
                    template: "\n    <nb-toast [@fadeIn]=\"fadeIn\" *ngFor=\"let toast of content\" [toast]=\"toast\"></nb-toast>",
                    animations: [
                        trigger('fadeIn', [
                            transition(':enter', [voidState, animate(100)], defaultOptions),
                            transition(':leave', [animate(100, voidState)], defaultOptions),
                        ]),
                    ],
                },] },
    ];
    /** @nocollapse */
    NbToastrContainerComponent.ctorParameters = function () { return [
        { type: NbLayoutDirectionService, },
        { type: NbPositionHelper, },
    ]; };
    NbToastrContainerComponent.propDecorators = {
        "content": [{ type: Input },],
        "context": [{ type: Input },],
        "position": [{ type: Input },],
        "toasts": [{ type: ViewChildren, args: [NbToastComponent,] },],
    };
    return NbToastrContainerComponent;
}());
export { NbToastrContainerComponent };
//# sourceMappingURL=toastr-container.component.js.map
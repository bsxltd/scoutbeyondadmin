/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Directive, ElementRef, forwardRef, Inject, InjectionToken, Input } from '@angular/core';
import { NG_VALIDATORS, NG_VALUE_ACCESSOR, Validators, } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';
import { NB_DOCUMENT } from '../../theme.options';
import { NbDateService } from '../calendar-kit';
/**
 * The `NbDatepickerAdapter` instances provide way how to parse, format and validate
 * different date types.
 * */
var /**
 * The `NbDatepickerAdapter` instances provide way how to parse, format and validate
 * different date types.
 * */
NbDatepickerAdapter = /** @class */ (function () {
    function NbDatepickerAdapter() {
    }
    return NbDatepickerAdapter;
}());
/**
 * The `NbDatepickerAdapter` instances provide way how to parse, format and validate
 * different date types.
 * */
export { NbDatepickerAdapter };
/**
 * Datepicker is an control that can pick any values anyway.
 * It has to be bound to the datepicker directive through nbDatepicker input.
 * */
var /**
 * Datepicker is an control that can pick any values anyway.
 * It has to be bound to the datepicker directive through nbDatepicker input.
 * */
NbDatepicker = /** @class */ (function () {
    function NbDatepicker() {
    }
    return NbDatepicker;
}());
/**
 * Datepicker is an control that can pick any values anyway.
 * It has to be bound to the datepicker directive through nbDatepicker input.
 * */
export { NbDatepicker };
export var NB_DATE_ADAPTER = new InjectionToken('Datepicker Adapter');
/**
 * The `NbDatepickerDirective` is form control that gives you ability to select dates and ranges. The datepicker
 * is shown when input receives a `focus` event.
 *
 * ```html
 * <input [nbDatepicker]="datepicker">
 * <nb-datepicker #datepicker></nb-datepicker>
 * ```
 *
 * @stacked-example(Showcase, datepicker/datepicker-showcase.component)
 *
 * ### Installation
 *
 * Import `NbDatepickerModule.forRoot()` to your root module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbDatepickerModule.forRoot(),
 *   ],
 * })
 * export class AppModule { }
 * ```
 * And `NbDatepickerModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbDatepickerModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * If you want to use range selection, you have to use `NbRangepickerComponent` instead:
 *
 * ```html
 * <input [nbDatepicker]="rangepicker">
 * <nb-rangepicker #rangepicker></nb-rangepicker>
 * ```
 *
 * Both range and date pickers support all parameters as calendar, so, check `NbCalendarComponent` for additional
 * info.
 *
 * @stacked-example(Range showcase, datepicker/rangepicker-showcase.component)
 *
 * Datepicker is the form control so it can be bound with angular forms through ngModel and form controls.
 *
 * @stacked-example(Forms, datepicker/datepicker-forms.component)
 *
 * `NbDatepickerDirective` may be validated using `min` and `max` dates passed to the datepicker.
 * And `filter` predicate that receives date object and has to return a boolean value.
 *
 * @stacked-example(Validation, datepicker/datepicker-validation.component)
 *
 * The `NbDatepickerComponent` supports date formatting:
 *
 * ```html
 * <input [nbDatepicker]="datepicker">
 * <nb-datepicker #datepicker format="MM\dd\yyyy"></nb-datepicker>
 * ```
 *
 * ## Formatting Issue
 *
 * By default, datepicker uses angulars `LOCALE_ID` token for localization and `DatePipe` for dates formatting.
 * And native `Date.parse(...)` for dates parsing. But native `Date.parse` function doesn't support formats.
 * To provide custom formatting you have to use one of the following packages:
 *
 * - `@nebular/moment` - provides moment date adapter that uses moment for date objects. This means datepicker than
 * will operate only moment date objects. If you want to use it you have to install it: `npm i @nebular/moment`, and
 * import `NbMomentDateModule` from this package.
 *
 * - `@nebular/date-fns` - adapter for popular date-fns library. This way is preferred if you need only date formatting.
 * Because date-fns is treeshakable, tiny and operates native date objects. If you want to use it you have to
 * install it: `npm i @nebular/date-fns`, and import `NbDateFnsDateModule` from this package.
 *
 * @styles
 *
 * datepicker-fg
 * datepicker-bg
 * datepicker-border
 * datepicker-border-radius
 * datepicker-shadow
 * datepicker-arrow-size
 * */
var NbDatepickerDirective = /** @class */ (function () {
    function NbDatepickerDirective(document, datepickerAdapters, hostRef, dateService) {
        var _this = this;
        this.document = document;
        this.datepickerAdapters = datepickerAdapters;
        this.hostRef = hostRef;
        this.dateService = dateService;
        this.alive = true;
        this.onChange = function () {
        };
        /**
           * Form control validators will be called in validators context, so, we need to bind them.
           * */
        this.validator = Validators.compose([
            this.parseValidator,
            this.minValidator,
            this.maxValidator,
            this.filterValidator,
        ].map(function (fn) { return fn.bind(_this); }));
        this.subscribeOnInputChange();
    }
    Object.defineProperty(NbDatepickerDirective.prototype, "setPicker", {
        set: /**
           * Provides datepicker component.
           * */
        function (picker) {
            this.picker = picker;
            this.setupPicker();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerDirective.prototype, "input", {
        /**
         * Returns html input element.
         * */
        get: /**
           * Returns html input element.
           * */
        function () {
            return this.hostRef.nativeElement;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerDirective.prototype, "inputValue", {
        /**
         * Returns host input value.
         * */
        get: /**
           * Returns host input value.
           * */
        function () {
            return this.input.value;
        },
        enumerable: true,
        configurable: true
    });
    NbDatepickerDirective.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    /**
     * Writes value in picker and html input element.
     * */
    /**
       * Writes value in picker and html input element.
       * */
    NbDatepickerDirective.prototype.writeValue = /**
       * Writes value in picker and html input element.
       * */
    function (value) {
        this.writePicker(value);
        this.writeInput(value);
    };
    NbDatepickerDirective.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    NbDatepickerDirective.prototype.registerOnTouched = function (fn) {
    };
    NbDatepickerDirective.prototype.setDisabledState = function (isDisabled) {
    };
    /**
     * Form control validation based on picker validator config.
     * */
    /**
       * Form control validation based on picker validator config.
       * */
    NbDatepickerDirective.prototype.validate = /**
       * Form control validation based on picker validator config.
       * */
    function () {
        return this.validator(null);
    };
    /**
     * Hides picker, focuses the input
     */
    /**
       * Hides picker, focuses the input
       */
    NbDatepickerDirective.prototype.hidePicker = /**
       * Hides picker, focuses the input
       */
    function () {
        this.input.focus();
        this.picker.hide();
    };
    /**
     * Validates that we can parse value correctly.
     * */
    /**
       * Validates that we can parse value correctly.
       * */
    NbDatepickerDirective.prototype.parseValidator = /**
       * Validates that we can parse value correctly.
       * */
    function () {
        var isValid = this.datepickerAdapter.isValid(this.inputValue, this.picker.format);
        return isValid ? null : { nbDatepickerParse: { value: this.inputValue } };
    };
    /**
     * Validates passed value is greater than min.
     * */
    /**
       * Validates passed value is greater than min.
       * */
    NbDatepickerDirective.prototype.minValidator = /**
       * Validates passed value is greater than min.
       * */
    function () {
        var config = this.picker.getValidatorConfig();
        var date = this.datepickerAdapter.parse(this.inputValue, this.picker.format);
        return (!config.min || !date || this.dateService.compareDates(config.min, date) <= 0) ?
            null : { nbDatepickerMin: { min: config.min, actual: date } };
    };
    /**
     * Validates passed value is smaller than max.
     * */
    /**
       * Validates passed value is smaller than max.
       * */
    NbDatepickerDirective.prototype.maxValidator = /**
       * Validates passed value is smaller than max.
       * */
    function () {
        var config = this.picker.getValidatorConfig();
        var date = this.datepickerAdapter.parse(this.inputValue, this.picker.format);
        return (!config.max || !date || this.dateService.compareDates(config.max, date) >= 0) ?
            null : { nbDatepickerMax: { max: config.max, actual: date } };
    };
    /**
     * Validates passed value satisfy the filter.
     * */
    /**
       * Validates passed value satisfy the filter.
       * */
    NbDatepickerDirective.prototype.filterValidator = /**
       * Validates passed value satisfy the filter.
       * */
    function () {
        var config = this.picker.getValidatorConfig();
        var date = this.datepickerAdapter.parse(this.inputValue, this.picker.format);
        return (!config.filter || !date || config.filter(date)) ?
            null : { nbDatepickerFilter: true };
    };
    /**
     * Chooses datepicker adapter based on passed picker component.
     * */
    /**
       * Chooses datepicker adapter based on passed picker component.
       * */
    NbDatepickerDirective.prototype.chooseDatepickerAdapter = /**
       * Chooses datepicker adapter based on passed picker component.
       * */
    function () {
        var _this = this;
        this.datepickerAdapter = this.datepickerAdapters.find(function (_a) {
            var picker = _a.picker;
            return _this.picker instanceof picker;
        });
        if (this.noDatepickerAdapterProvided()) {
            throw new Error('No datepickerAdapter provided for picker');
        }
    };
    /**
     * Attaches picker to the host input element and subscribes on value changes.
     * */
    /**
       * Attaches picker to the host input element and subscribes on value changes.
       * */
    NbDatepickerDirective.prototype.setupPicker = /**
       * Attaches picker to the host input element and subscribes on value changes.
       * */
    function () {
        var _this = this;
        this.chooseDatepickerAdapter();
        this.picker.attach(this.hostRef);
        if (this.hostRef.nativeElement.value) {
            this.picker.value = this.datepickerAdapter.parse(this.hostRef.nativeElement.value, this.picker.format);
        }
        this.picker.valueChange
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function (value) {
            _this.writePicker(value);
            _this.writeInput(value);
            _this.onChange(value);
            if (_this.picker.shouldHide()) {
                _this.hidePicker();
            }
        });
    };
    NbDatepickerDirective.prototype.writePicker = function (value) {
        this.picker.value = value;
    };
    NbDatepickerDirective.prototype.writeInput = function (value) {
        var stringRepresentation = this.datepickerAdapter.format(value, this.picker.format);
        this.hostRef.nativeElement.value = stringRepresentation;
    };
    /**
     * Validates if no datepicker adapter provided.
     * */
    /**
       * Validates if no datepicker adapter provided.
       * */
    NbDatepickerDirective.prototype.noDatepickerAdapterProvided = /**
       * Validates if no datepicker adapter provided.
       * */
    function () {
        return !this.datepickerAdapter || !(this.datepickerAdapter instanceof NbDatepickerAdapter);
    };
    NbDatepickerDirective.prototype.subscribeOnInputChange = function () {
        var _this = this;
        fromEvent(this.input, 'input')
            .pipe(map(function () { return _this.inputValue; }), takeWhile(function () { return _this.alive; }))
            .subscribe(function (value) { return _this.handleInputChange(value); });
    };
    /**
     * Parses input value and write if it isn't null.
     * */
    /**
       * Parses input value and write if it isn't null.
       * */
    NbDatepickerDirective.prototype.handleInputChange = /**
       * Parses input value and write if it isn't null.
       * */
    function (value) {
        var date = this.parseInputValue(value);
        this.onChange(date);
        this.writePicker(date);
    };
    NbDatepickerDirective.prototype.parseInputValue = function (value) {
        if (this.datepickerAdapter.isValid(value, this.picker.format)) {
            return this.datepickerAdapter.parse(value, this.picker.format);
        }
        return null;
    };
    NbDatepickerDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'input[nbDatepicker]',
                    providers: [
                        {
                            provide: NG_VALUE_ACCESSOR,
                            useExisting: forwardRef(function () { return NbDatepickerDirective; }),
                            multi: true,
                        },
                        {
                            provide: NG_VALIDATORS,
                            useExisting: forwardRef(function () { return NbDatepickerDirective; }),
                            multi: true,
                        },
                    ],
                },] },
    ];
    /** @nocollapse */
    NbDatepickerDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NB_DOCUMENT,] },] },
        { type: Array, decorators: [{ type: Inject, args: [NB_DATE_ADAPTER,] },] },
        { type: ElementRef, },
        { type: NbDateService, },
    ]; };
    NbDatepickerDirective.propDecorators = {
        "setPicker": [{ type: Input, args: ['nbDatepicker',] },],
    };
    return NbDatepickerDirective;
}());
export { NbDatepickerDirective };
//# sourceMappingURL=datepicker.directive.js.map
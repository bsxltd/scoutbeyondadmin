var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, ComponentFactoryResolver, EventEmitter, Inject, Input, Output, Type, } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NbAdjustment, NbComponentPortal, NbOverlayService, NbPosition, NbPositionBuilderService, NbTrigger, NbTriggerStrategyBuilder, patch, } from '../cdk';
import { NbDatepickerContainerComponent } from './datepicker-container.component';
import { NB_DOCUMENT } from '../../theme.options';
import { NbCalendarRangeComponent } from '../calendar/calendar-range.component';
import { NbCalendarComponent } from '../calendar/calendar.component';
import { NbCalendarSize, NbCalendarViewMode } from '../calendar-kit';
import { NbDatepicker } from './datepicker.directive';
/**
 * The `NbBasePicker` component concentrates overlay manipulation logic.
 * */
var NbBasePicker = /** @class */ (function (_super) {
    __extends(NbBasePicker, _super);
    function NbBasePicker(document, positionBuilder, overlay, cfr) {
        var _this = _super.call(this) || this;
        _this.document = document;
        _this.positionBuilder = positionBuilder;
        _this.overlay = overlay;
        _this.cfr = cfr;
        /**
           * Defines if we should render previous and next months
           * in the current month view.
           * */
        _this.boundingMonth = true;
        /**
           * Defines starting view for calendar.
           * */
        _this.startView = NbCalendarViewMode.DATE;
        /**
           * Size of the calendar and entire components.
           * Can be 'medium' which is default or 'large'.
           * */
        _this.size = NbCalendarSize.MEDIUM;
        /**
           * Hide picker when a date or a range is selected, `true` by default
           * @type {boolean}
           */
        _this.hideOnSelect = true;
        /**
           * Stream of picker changes. Required to be the subject because picker hides and shows and picker
           * change stream becomes recreated.
           * */
        _this.onChange$ = new Subject();
        _this.alive = true;
        return _this;
    }
    Object.defineProperty(NbBasePicker.prototype, "picker", {
        /**
         * Returns picker instance.
         * */
        get: /**
           * Returns picker instance.
           * */
        function () {
            return this.pickerRef && this.pickerRef.instance;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbBasePicker.prototype, "valueChange", {
        /**
         * Stream of picker value changes.
         * */
        get: /**
           * Stream of picker value changes.
           * */
        function () {
            return this.onChange$.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    NbBasePicker.prototype.ngOnDestroy = function () {
        this.alive = false;
        this.hide();
        this.ref.dispose();
    };
    /**
     * Datepicker knows nothing about host html input element.
     * So, attach method attaches datepicker to the host input element.
     * */
    /**
       * Datepicker knows nothing about host html input element.
       * So, attach method attaches datepicker to the host input element.
       * */
    NbBasePicker.prototype.attach = /**
       * Datepicker knows nothing about host html input element.
       * So, attach method attaches datepicker to the host input element.
       * */
    function (hostRef) {
        this.hostRef = hostRef;
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({
            positionStrategy: this.positionStrategy,
            scrollStrategy: this.overlay.scrollStrategies.reposition(),
        });
        this.subscribeOnPositionChange();
        this.subscribeOnTriggers();
    };
    NbBasePicker.prototype.getValidatorConfig = function () {
        return { min: this.min, max: this.max, filter: this.filter };
    };
    NbBasePicker.prototype.show = function () {
        this.container = this.ref.attach(new NbComponentPortal(NbDatepickerContainerComponent, null, null, this.cfr));
        this.instantiatePicker();
        this.subscribeOnValueChange();
        this.writeQueue();
        this.patchWithInputs();
    };
    NbBasePicker.prototype.shouldHide = function () {
        return this.hideOnSelect && !!this.value;
    };
    NbBasePicker.prototype.hide = function () {
        this.ref.detach();
        // save current value if picker was rendered
        if (this.picker) {
            this.queue = this.value;
            this.pickerRef.destroy();
            this.pickerRef = null;
            this.container = null;
        }
    };
    NbBasePicker.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(NbPosition.BOTTOM)
            .adjustment(NbAdjustment.COUNTERCLOCKWISE);
    };
    NbBasePicker.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return patch(_this.container, { position: position }); });
    };
    NbBasePicker.prototype.createTriggerStrategy = function () {
        var _this = this;
        return new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(NbTrigger.FOCUS)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.container; })
            .build();
    };
    NbBasePicker.prototype.subscribeOnTriggers = function () {
        var _this = this;
        var triggerStrategy = this.createTriggerStrategy();
        triggerStrategy.show$.pipe(takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.show(); });
        triggerStrategy.hide$.pipe(takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.hide(); });
    };
    NbBasePicker.prototype.instantiatePicker = function () {
        this.pickerRef = this.container.instance.attach(new NbComponentPortal(this.pickerClass, null, null, this.cfr));
    };
    /**
     * Subscribes on picker value changes and emit data through this.onChange$ subject.
     * */
    /**
       * Subscribes on picker value changes and emit data through this.onChange$ subject.
       * */
    NbBasePicker.prototype.subscribeOnValueChange = /**
       * Subscribes on picker value changes and emit data through this.onChange$ subject.
       * */
    function () {
        var _this = this;
        this.pickerValueChange.subscribe(function (date) {
            _this.onChange$.next(date);
        });
    };
    NbBasePicker.prototype.patchWithInputs = function () {
        this.picker.boundingMonth = this.boundingMonth;
        this.picker.startView = this.startView;
        this.picker.min = this.min;
        this.picker.max = this.max;
        this.picker.filter = this.filter;
        this.picker._cellComponent = this.dayCellComponent;
        this.picker.monthCellComponent = this.monthCellComponent;
        this.picker._yearCellComponent = this.yearCellComponent;
        this.picker.size = this.size;
        this.picker.visibleDate = this.visibleDate;
    };
    /** @nocollapse */
    NbBasePicker.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NB_DOCUMENT,] },] },
        { type: NbPositionBuilderService, },
        { type: NbOverlayService, },
        { type: ComponentFactoryResolver, },
    ]; };
    NbBasePicker.propDecorators = {
        "format": [{ type: Input },],
        "boundingMonth": [{ type: Input },],
        "startView": [{ type: Input },],
        "min": [{ type: Input },],
        "max": [{ type: Input },],
        "filter": [{ type: Input },],
        "dayCellComponent": [{ type: Input },],
        "monthCellComponent": [{ type: Input },],
        "yearCellComponent": [{ type: Input },],
        "size": [{ type: Input },],
        "visibleDate": [{ type: Input },],
        "hideOnSelect": [{ type: Input },],
    };
    return NbBasePicker;
}(NbDatepicker));
export { NbBasePicker };
/**
 * The DatePicker components itself.
 * Provides a proxy to `NbCalendar` options as well as custom picker options.
 */
var NbDatepickerComponent = /** @class */ (function (_super) {
    __extends(NbDatepickerComponent, _super);
    function NbDatepickerComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pickerClass = NbCalendarComponent;
        return _this;
    }
    Object.defineProperty(NbDatepickerComponent.prototype, "date", {
        set: /**
           * Date which will be rendered as selected.
           * */
        function (date) {
            this.value = date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerComponent.prototype, "dateChange", {
        get: /**
           * Emits date when selected.
           * */
        function () {
            return this.valueChange;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerComponent.prototype, "value", {
        get: function () {
            return this.picker.date;
        },
        set: function (date) {
            if (!this.picker) {
                this.queue = date;
                return;
            }
            if (date) {
                this.picker.visibleDate = date;
                this.picker.date = date;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbDatepickerComponent.prototype, "pickerValueChange", {
        get: function () {
            return this.picker.dateChange;
        },
        enumerable: true,
        configurable: true
    });
    NbDatepickerComponent.prototype.writeQueue = function () {
        this.value = this.queue;
    };
    NbDatepickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-datepicker',
                    template: '',
                },] },
    ];
    /** @nocollapse */
    NbDatepickerComponent.propDecorators = {
        "date": [{ type: Input },],
        "dateChange": [{ type: Output },],
    };
    return NbDatepickerComponent;
}(NbBasePicker));
export { NbDatepickerComponent };
/**
 * The RangeDatePicker components itself.
 * Provides a proxy to `NbCalendarRange` options as well as custom picker options.
 */
var NbRangepickerComponent = /** @class */ (function (_super) {
    __extends(NbRangepickerComponent, _super);
    function NbRangepickerComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pickerClass = NbCalendarRangeComponent;
        return _this;
    }
    Object.defineProperty(NbRangepickerComponent.prototype, "range", {
        set: /**
           * Range which will be rendered as selected.
           * */
        function (range) {
            this.value = range;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRangepickerComponent.prototype, "rangeChange", {
        get: /**
           * Emits range when start selected and emits again when end selected.
           * */
        function () {
            return this.valueChange;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRangepickerComponent.prototype, "value", {
        get: function () {
            return this.picker.range;
        },
        set: function (range) {
            if (!this.picker) {
                this.queue = range;
                return;
            }
            if (range) {
                this.picker.visibleDate = range && range.start;
                this.picker.range = range;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbRangepickerComponent.prototype, "pickerValueChange", {
        get: function () {
            return this.picker.rangeChange;
        },
        enumerable: true,
        configurable: true
    });
    NbRangepickerComponent.prototype.shouldHide = function () {
        return _super.prototype.shouldHide.call(this) && !!(this.value.start && this.value.end);
    };
    NbRangepickerComponent.prototype.writeQueue = function () {
        this.value = this.queue;
    };
    NbRangepickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-rangepicker',
                    template: '',
                },] },
    ];
    /** @nocollapse */
    NbRangepickerComponent.propDecorators = {
        "range": [{ type: Input },],
        "rangeChange": [{ type: Output },],
    };
    return NbRangepickerComponent;
}(NbBasePicker));
export { NbRangepickerComponent };
//# sourceMappingURL=datepicker.component.js.map
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, ViewChild } from '@angular/core';
import { NbOverlayContainerComponent, NbPositionedContainer } from '../cdk';
var NbDatepickerContainerComponent = /** @class */ (function (_super) {
    __extends(NbDatepickerContainerComponent, _super);
    function NbDatepickerContainerComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NbDatepickerContainerComponent.prototype.attach = function (portal) {
        return this.overlayContainer.attachComponentPortal(portal);
    };
    NbDatepickerContainerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-datepicker-container',
                    styles: [":host .arrow{position:absolute;width:0;height:0}:host /deep/ nb-overlay-container .primitive-overlay{padding:0.75rem 1rem} "],
                    template: "\n    <span class=\"arrow\"></span>\n    <nb-overlay-container></nb-overlay-container>\n  ",
                },] },
    ];
    /** @nocollapse */
    NbDatepickerContainerComponent.propDecorators = {
        "overlayContainer": [{ type: ViewChild, args: [NbOverlayContainerComponent,] },],
    };
    return NbDatepickerContainerComponent;
}(NbPositionedContainer));
export { NbDatepickerContainerComponent };
//# sourceMappingURL=datepicker-container.component.js.map
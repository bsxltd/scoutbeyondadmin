import { Component, ViewContainerRef, ViewChild } from '@angular/core';
var NbWindowsContainerComponent = /** @class */ (function () {
    function NbWindowsContainerComponent() {
    }
    NbWindowsContainerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-windows-container',
                    template: "<ng-container #viewContainerRef></ng-container>",
                    styles: [":host{display:flex;align-items:flex-end;overflow-x:auto}:host /deep/ nb-window:not(.full-screen){margin:0 2rem} "],
                },] },
    ];
    /** @nocollapse */
    NbWindowsContainerComponent.propDecorators = {
        "viewContainerRef": [{ type: ViewChild, args: ['viewContainerRef', { read: ViewContainerRef },] },],
    };
    return NbWindowsContainerComponent;
}());
export { NbWindowsContainerComponent };
//# sourceMappingURL=windows-container.component.js.map
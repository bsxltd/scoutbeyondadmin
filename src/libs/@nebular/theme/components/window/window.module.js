import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbOverlayModule } from '../cdk/overlay';
import { NbCardModule } from '../card/card.module';
import { NbWindowService } from './window.service';
import { NbWindowsContainerComponent } from './windows-container.component';
import { NbWindowComponent } from './window.component';
import { NB_WINDOW_CONFIG } from './window.options';
var NbWindowModule = /** @class */ (function () {
    function NbWindowModule() {
    }
    NbWindowModule.forRoot = function (defaultConfig) {
        return {
            ngModule: NbWindowModule,
            providers: [
                NbWindowService,
                { provide: NB_WINDOW_CONFIG, useValue: defaultConfig },
            ],
        };
    };
    NbWindowModule.forChild = function (defaultConfig) {
        return {
            ngModule: NbWindowModule,
            providers: [
                NbWindowService,
                { provide: NB_WINDOW_CONFIG, useValue: defaultConfig },
            ],
        };
    };
    NbWindowModule.decorators = [
        { type: NgModule, args: [{
                    imports: [CommonModule, NbOverlayModule, NbCardModule],
                    declarations: [
                        NbWindowsContainerComponent,
                        NbWindowComponent,
                    ],
                    entryComponents: [NbWindowsContainerComponent, NbWindowComponent],
                },] },
    ];
    return NbWindowModule;
}());
export { NbWindowModule };
//# sourceMappingURL=window.module.js.map
import { Component, ElementRef, HostBinding, Inject, TemplateRef, Renderer2, ViewChild, ComponentFactoryResolver, Input, } from '@angular/core';
import { NbComponentPortal, NbFocusTrapFactoryService, NbOverlayContainerComponent, NbTemplatePortal, } from '../cdk';
import { NB_WINDOW_CONTENT, NbWindowConfig, NbWindowState, NB_WINDOW_CONTEXT } from './window.options';
import { NbWindowRef } from './window-ref';
var NbWindowComponent = /** @class */ (function () {
    function NbWindowComponent(content, context, windowRef, config, focusTrapFactory, elementRef, renderer) {
        this.content = content;
        this.context = context;
        this.windowRef = windowRef;
        this.config = config;
        this.focusTrapFactory = focusTrapFactory;
        this.elementRef = elementRef;
        this.renderer = renderer;
    }
    Object.defineProperty(NbWindowComponent.prototype, "isFullScreen", {
        get: function () {
            return this.windowRef.state === NbWindowState.FULL_SCREEN;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbWindowComponent.prototype, "maximized", {
        get: function () {
            return this.windowRef.state === NbWindowState.MAXIMIZED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbWindowComponent.prototype, "minimized", {
        get: function () {
            return this.windowRef.state === NbWindowState.MINIMIZED;
        },
        enumerable: true,
        configurable: true
    });
    NbWindowComponent.prototype.ngOnInit = function () {
        this.focusTrap = this.focusTrapFactory.create(this.elementRef.nativeElement);
        this.focusTrap.blurPreviouslyFocusedElement();
        this.focusTrap.focusInitialElement();
        if (this.config.windowClass) {
            this.renderer.addClass(this.elementRef.nativeElement, this.config.windowClass);
        }
    };
    NbWindowComponent.prototype.ngAfterViewInit = function () {
        if (this.content instanceof TemplateRef) {
            this.attachTemplate();
        }
        else {
            this.attachComponent();
        }
    };
    NbWindowComponent.prototype.ngOnDestroy = function () {
        if (this.focusTrap) {
            this.focusTrap.restoreFocus();
        }
        this.close();
    };
    NbWindowComponent.prototype.minimize = function () {
        if (this.windowRef.state === NbWindowState.MINIMIZED) {
            this.windowRef.toPreviousState();
        }
        else {
            this.windowRef.minimize();
        }
    };
    NbWindowComponent.prototype.maximize = function () {
        this.windowRef.maximize();
    };
    NbWindowComponent.prototype.fullScreen = function () {
        this.windowRef.fullScreen();
    };
    NbWindowComponent.prototype.maximizeOrFullScreen = function () {
        if (this.windowRef.state === NbWindowState.MINIMIZED) {
            this.maximize();
        }
        else {
            this.fullScreen();
        }
    };
    NbWindowComponent.prototype.close = function () {
        this.windowRef.close();
    };
    NbWindowComponent.prototype.attachTemplate = function () {
        this.overlayContainer.attachTemplatePortal(new NbTemplatePortal(this.content, null, {
            $implicit: this.context,
        }));
    };
    NbWindowComponent.prototype.attachComponent = function () {
        var portal = new NbComponentPortal(this.content, null, null, this.cfr);
        var ref = this.overlayContainer.attachComponentPortal(portal);
        Object.assign(ref.instance, this.context);
        ref.changeDetectorRef.detectChanges();
    };
    NbWindowComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-window',
                    template: "\n    <nb-card>\n      <nb-card-header>\n        <div cdkFocusInitial class=\"title\" tabindex=\"-1\">{{ config.title }}</div>\n\n        <div class=\"buttons\">\n          <button class=\"button\" (click)=\"minimize()\">\n            <i class=\"nb-fold\"></i>\n          </button>\n          <button class=\"button\" *ngIf=\"isFullScreen\" (click)=\"maximize()\">\n            <i class=\"nb-minimize\"></i>\n          </button>\n          <button class=\"button\" *ngIf=\"minimized || maximized\" (click)=\"maximizeOrFullScreen()\">\n            <i class=\"nb-maximize\"></i>\n          </button>\n          <button class=\"button\" (click)=\"close()\">\n            <i class=\"nb-close\"></i>\n          </button>\n        </div>\n      </nb-card-header>\n      <nb-card-body *ngIf=\"maximized || isFullScreen\">\n        <nb-overlay-container></nb-overlay-container>\n      </nb-card-body>\n    </nb-card>\n  ",
                    styles: [":host{flex:1 0 auto;min-width:20rem}:host nb-card{margin:0}:host nb-card-header{display:flex;justify-content:space-between;align-items:center;padding-right:0;overflow:hidden}:host .title{flex:1 0 auto;margin-right:3rem;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}:host .buttons{width:9.5rem;display:flex;justify-content:space-evenly}:host .buttons .button{background:none;border:none;flex:0 0 3rem;padding:0 0.8rem}:host(.full-screen){position:fixed;top:50%;left:50%;transform:translate(-50%, -50%)}:host(.maximized) nb-card{border-bottom-left-radius:0;border-bottom-right-radius:0}:host(.minimized) nb-card{border-bottom-left-radius:0;border-bottom-right-radius:0;height:auto}:host(.minimized) nb-card nb-card-header{border-bottom:none} "],
                },] },
    ];
    /** @nocollapse */
    NbWindowComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NB_WINDOW_CONTENT,] },] },
        { type: Object, decorators: [{ type: Inject, args: [NB_WINDOW_CONTEXT,] },] },
        { type: NbWindowRef, },
        { type: NbWindowConfig, },
        { type: NbFocusTrapFactoryService, },
        { type: ElementRef, },
        { type: Renderer2, },
    ]; };
    NbWindowComponent.propDecorators = {
        "cfr": [{ type: Input },],
        "isFullScreen": [{ type: HostBinding, args: ['class.full-screen',] },],
        "maximized": [{ type: HostBinding, args: ['class.maximized',] },],
        "minimized": [{ type: HostBinding, args: ['class.minimized',] },],
        "overlayContainer": [{ type: ViewChild, args: [NbOverlayContainerComponent,] },],
    };
    return NbWindowComponent;
}());
export { NbWindowComponent };
//# sourceMappingURL=window.component.js.map
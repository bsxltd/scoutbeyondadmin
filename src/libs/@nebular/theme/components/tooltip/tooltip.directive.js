/*
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Directive, ElementRef, Inject, Input } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { createContainer, NbAdjustment, NbOverlayService, NbPosition, NbPositionBuilderService, NbTrigger, NbTriggerStrategyBuilder, patch, } from '../cdk';
import { NB_DOCUMENT } from '../../theme.options';
import { NbTooltipComponent } from './tooltip.component';
/**
 *
 * Tooltip directive for small text/icon hints.
 *
 * ### Installation
 *
 * Import `NbTooltipModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbTooltipModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * @stacked-example(Showcase, tooltip/tooltip-showcase.component)
 *
 * Tooltip can accept a hint text and/or an icon:
 * @stacked-example(With Icon, tooltip/tooltip-with-icon.component)
 *
 * Same way as Popover, tooltip can accept placement position with `nbTooltipPlacement` proprety:
 * @stacked-example(Placements, tooltip/tooltip-placements.component)
 *
 * It is also possible to specify tooltip color using `nbTooltipStatus` property:
 * @stacked-example(Colored Tooltips, tooltip/tooltip-colors.component)
 *
 */
var NbTooltipDirective = /** @class */ (function () {
    function NbTooltipDirective(document, hostRef, positionBuilder, overlay) {
        this.document = document;
        this.hostRef = hostRef;
        this.positionBuilder = positionBuilder;
        this.overlay = overlay;
        this.context = {};
        /**
           * Position will be calculated relatively host element based on the position.
           * Can be top, right, bottom, left, start or end.
           */
        this.position = NbPosition.TOP;
        /**
           * Container position will be changes automatically based on this strategy if container can't fit view port.
           * Set this property to any falsy value if you want to disable automatically adjustment.
           * Available values: clockwise, counterclockwise.
           */
        this.adjustment = NbAdjustment.CLOCKWISE;
        this.alive = true;
    }
    Object.defineProperty(NbTooltipDirective.prototype, "icon", {
        set: /**
           *
           * @param {string} icon
           */
        function (icon) {
            this.context = Object.assign(this.context, { icon: icon });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbTooltipDirective.prototype, "status", {
        set: /**
           *
           * @param {string} status
           */
        function (status) {
            this.context = Object.assign(this.context, { status: status });
        },
        enumerable: true,
        configurable: true
    });
    NbTooltipDirective.prototype.ngAfterViewInit = function () {
        this.positionStrategy = this.createPositionStrategy();
        this.ref = this.overlay.create({
            positionStrategy: this.positionStrategy,
            scrollStrategy: this.overlay.scrollStrategies.reposition(),
        });
        this.triggerStrategy = this.createTriggerStrategy();
        this.subscribeOnTriggers();
        this.subscribeOnPositionChange();
    };
    NbTooltipDirective.prototype.ngOnDestroy = function () {
        this.alive = false;
    };
    NbTooltipDirective.prototype.show = function () {
        this.container = createContainer(this.ref, NbTooltipComponent, {
            position: this.position,
            content: this.content,
            context: this.context,
        });
    };
    NbTooltipDirective.prototype.hide = function () {
        this.ref.detach();
    };
    NbTooltipDirective.prototype.toggle = function () {
        if (this.ref && this.ref.hasAttached()) {
            this.hide();
        }
        else {
            this.show();
        }
    };
    NbTooltipDirective.prototype.createPositionStrategy = function () {
        return this.positionBuilder
            .connectedTo(this.hostRef)
            .position(this.position)
            .adjustment(this.adjustment)
            .offset(8);
    };
    NbTooltipDirective.prototype.createTriggerStrategy = function () {
        var _this = this;
        return new NbTriggerStrategyBuilder()
            .document(this.document)
            .trigger(NbTrigger.HINT)
            .host(this.hostRef.nativeElement)
            .container(function () { return _this.container; })
            .build();
    };
    NbTooltipDirective.prototype.subscribeOnPositionChange = function () {
        var _this = this;
        this.positionStrategy.positionChange
            .pipe(takeWhile(function () { return _this.alive; }))
            .subscribe(function (position) { return patch(_this.container, { position: position }); });
    };
    NbTooltipDirective.prototype.subscribeOnTriggers = function () {
        var _this = this;
        this.triggerStrategy.show$.pipe(takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.show(); });
        this.triggerStrategy.hide$.pipe(takeWhile(function () { return _this.alive; })).subscribe(function () { return _this.hide(); });
    };
    NbTooltipDirective.decorators = [
        { type: Directive, args: [{ selector: '[nbTooltip]' },] },
    ];
    /** @nocollapse */
    NbTooltipDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NB_DOCUMENT,] },] },
        { type: ElementRef, },
        { type: NbPositionBuilderService, },
        { type: NbOverlayService, },
    ]; };
    NbTooltipDirective.propDecorators = {
        "content": [{ type: Input, args: ['nbTooltip',] },],
        "position": [{ type: Input, args: ['nbTooltipPlacement',] },],
        "adjustment": [{ type: Input, args: ['nbTooltipAdjustment',] },],
        "icon": [{ type: Input, args: ['nbTooltipIcon',] },],
        "status": [{ type: Input, args: ['nbTooltipStatus',] },],
    };
    return NbTooltipDirective;
}());
export { NbTooltipDirective };
//# sourceMappingURL=tooltip.directive.js.map
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageFileComponent = /** @class */ (function () {
    function NbChatMessageFileComponent(cd, domSanitizer) {
        this.cd = cd;
        this.domSanitizer = domSanitizer;
    }
    Object.defineProperty(NbChatMessageFileComponent.prototype, "files", {
        set: /**
           * Message file path
           * @type {Date}
           */
        function (files) {
            var _this = this;
            this.readyFiles = (files || []).map(function (file) {
                var isImage = _this.isImage(file);
                return __assign({}, file, { urlStyle: isImage && _this.domSanitizer.bypassSecurityTrustStyle("url(" + file.url + ")"), isImage: isImage });
            });
            this.cd.detectChanges();
        },
        enumerable: true,
        configurable: true
    });
    NbChatMessageFileComponent.prototype.isImage = function (file) {
        return ['image/png', 'image/jpeg', 'image/gif'].includes(file.type);
    };
    NbChatMessageFileComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-chat-message-file',
                    template: "\n    <nb-chat-message-text [sender]=\"sender\" [date]=\"date\" [message]=\"message\">\n      {{ message }}\n    </nb-chat-message-text>\n\n    <ng-container *ngIf=\"readyFiles?.length > 1\">\n      <div class=\"message-content-group\">\n        <a *ngFor=\"let file of readyFiles\" [href]=\"file.url\" target=\"_blank\">\n          <span [class]=\"file.icon\" *ngIf=\"!file.urlStyle\"></span>\n          <div *ngIf=\"file.isImage\" [style.background-image]=\"file.urlStyle\"></div>\n        </a>\n      </div>\n    </ng-container>\n\n    <ng-container *ngIf=\"readyFiles?.length === 1\">\n      <a [href]=\"readyFiles[0].url\" target=\"_blank\">\n        <span [class]=\"readyFiles[0].icon\"  *ngIf=\"!readyFiles[0].urlStyle\"></span>\n        <div *ngIf=\"readyFiles[0].isImage\" [style.background-image]=\"readyFiles[0].urlStyle\"></div>\n      </a>\n    </ng-container>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageFileComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef, },
        { type: DomSanitizer, },
    ]; };
    NbChatMessageFileComponent.propDecorators = {
        "message": [{ type: Input },],
        "sender": [{ type: Input },],
        "date": [{ type: Input },],
        "files": [{ type: Input },],
    };
    return NbChatMessageFileComponent;
}());
export { NbChatMessageFileComponent };
//# sourceMappingURL=chat-message-file.component.js.map
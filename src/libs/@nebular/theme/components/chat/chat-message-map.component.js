/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NbChatOptions } from './chat.options';
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageMapComponent = /** @class */ (function () {
    function NbChatMessageMapComponent(options) {
        this.mapKey = options.messageGoogleMapKey;
    }
    Object.defineProperty(NbChatMessageMapComponent.prototype, "file", {
        get: function () {
            return {
                // tslint:disable-next-line
                url: "https://maps.googleapis.com/maps/api/staticmap?center=" + this.latitude + "," + this.longitude + "&zoom=12&size=400x400&key=" + this.mapKey,
                type: 'image/png',
                icon: 'nb-location',
            };
        },
        enumerable: true,
        configurable: true
    });
    NbChatMessageMapComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-chat-message-map',
                    template: "\n    <nb-chat-message-file [files]=\"[file]\" [message]=\"message\" [sender]=\"sender\" [date]=\"date\"></nb-chat-message-file>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageMapComponent.ctorParameters = function () { return [
        { type: NbChatOptions, },
    ]; };
    NbChatMessageMapComponent.propDecorators = {
        "message": [{ type: Input },],
        "sender": [{ type: Input },],
        "date": [{ type: Input },],
        "latitude": [{ type: Input },],
        "longitude": [{ type: Input },],
    };
    return NbChatMessageMapComponent;
}());
export { NbChatMessageMapComponent };
//# sourceMappingURL=chat-message-map.component.js.map
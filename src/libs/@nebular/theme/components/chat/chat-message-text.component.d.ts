/**
 * Chat message component.
 *
 * @styles
 *
 */
export declare class NbChatMessageTextComponent {
    /**
     * Message sender
     * @type {string}
     */
    sender: string;
    /**
     * Message sender
     * @type {string}
     */
    message: string;
    /**
     * Message send date
     * @type {Date}
     */
    date: Date;
}

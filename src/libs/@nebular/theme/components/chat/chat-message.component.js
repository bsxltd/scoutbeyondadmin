/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { convertToBoolProperty } from '../helpers';
import { DomSanitizer } from '@angular/platform-browser';
import { animate, state, style, transition, trigger } from '@angular/animations';
/**
 * Chat message component.
 *
 * Multiple message types are available through a `type` property, such as
 * - text - simple text message
 * - file - could be a file preview or a file icon
 * if multiple files are provided grouped files are shown
 * - quote - quotes a message with specific quote styles
 * - map - shows a google map picture by provided [latitude] and [longitude] properties
 *
 * @stacked-example(Available Types, chat/chat-message-types-showcase.component)
 *
 * Message with attached files:
 * ```html
 * <nb-chat-message
 *   type="file"
 *   [files]="[ { url: '...' } ]"
 *   message="Hello world!">
 * </nb-chat-message>
 * ```
 *
 * Map message:
 * ```html
 * <nb-chat-message
 *   type="map"
 *   [latitude]="53.914"
 *   [longitude]="27.59"
 *   message="Here I am">
 * </nb-chat-message>
 * ```
 *
 * @styles
 *
 * chat-message-fg:
 * chat-message-bg:
 * chat-message-reply-bg:
 * chat-message-reply-fg:
 * chat-message-avatar-bg:
 * chat-message-sender-fg:
 * chat-message-quote-fg:
 * chat-message-quote-bg:
 * chat-message-file-fg:
 * chat-message-file-bg:
 */
var NbChatMessageComponent = /** @class */ (function () {
    function NbChatMessageComponent(domSanitizer) {
        this.domSanitizer = domSanitizer;
        this.replyValue = false;
    }
    Object.defineProperty(NbChatMessageComponent.prototype, "flyInOut", {
        get: function () {
            return true;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatMessageComponent.prototype, "notReply", {
        get: function () {
            return !this.replyValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatMessageComponent.prototype, "reply", {
        set: /**
           * Determines if a message is a reply
           */
        function (val) {
            this.replyValue = convertToBoolProperty(val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbChatMessageComponent.prototype, "avatar", {
        set: /**
           * Message send avatar
           * @type {string}
           */
        function (value) {
            this.avatarStyle = value ? this.domSanitizer.bypassSecurityTrustStyle("url(" + value + ")") : null;
        },
        enumerable: true,
        configurable: true
    });
    NbChatMessageComponent.prototype.getInitials = function () {
        if (this.sender) {
            var names = this.sender.split(' ');
            return names.map(function (n) { return n.charAt(0); }).splice(0, 2).join('').toUpperCase();
        }
        return '';
    };
    NbChatMessageComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-chat-message',
                    template: "\n    <div class=\"avatar\" [style.background-image]=\"avatarStyle\" *ngIf=\"!replyValue\">\n      <ng-container *ngIf=\"!avatarStyle\">\n        {{ getInitials() }}\n      </ng-container>\n    </div>\n    <div class=\"message\">\n      <ng-container [ngSwitch]=\"type\">\n\n        <nb-chat-message-file *ngSwitchCase=\"'file'\"\n                              [sender]=\"sender\" [date]=\"date\" [message]=\"message\" [files]=\"files\">\n        </nb-chat-message-file>\n\n        <nb-chat-message-quote *ngSwitchCase=\"'quote'\"\n                              [sender]=\"sender\" [date]=\"date\" [message]=\"message\" [quote]=\"quote\">\n        </nb-chat-message-quote>\n\n        <nb-chat-message-map *ngSwitchCase=\"'map'\"\n                              [sender]=\"sender\" [date]=\"date\"\n                              [message]=\"message\" [latitude]=\"latitude\" [longitude]=\"longitude\">\n        </nb-chat-message-map>\n\n        <nb-chat-message-text *ngSwitchDefault\n                              [sender]=\"sender\" [date]=\"date\" [message]=\"message\">\n        </nb-chat-message-text>\n      </ng-container>\n    </div>\n  ",
                    animations: [
                        trigger('flyInOut', [
                            state('in', style({ transform: 'translateX(0)' })),
                            transition('void => *', [
                                style({ transform: 'translateX(-100%)' }),
                                animate(80),
                            ]),
                            transition('* => void', [
                                animate(80, style({ transform: 'translateX(100%)' })),
                            ]),
                        ]),
                    ],
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageComponent.ctorParameters = function () { return [
        { type: DomSanitizer, },
    ]; };
    NbChatMessageComponent.propDecorators = {
        "flyInOut": [{ type: HostBinding, args: ['@flyInOut',] },],
        "replyValue": [{ type: HostBinding, args: ['class.reply',] },],
        "notReply": [{ type: HostBinding, args: ['class.not-reply',] },],
        "reply": [{ type: Input },],
        "message": [{ type: Input },],
        "sender": [{ type: Input },],
        "date": [{ type: Input },],
        "files": [{ type: Input },],
        "quote": [{ type: Input },],
        "latitude": [{ type: Input },],
        "longitude": [{ type: Input },],
        "avatar": [{ type: Input },],
        "type": [{ type: Input },],
    };
    return NbChatMessageComponent;
}());
export { NbChatMessageComponent };
//# sourceMappingURL=chat-message.component.js.map
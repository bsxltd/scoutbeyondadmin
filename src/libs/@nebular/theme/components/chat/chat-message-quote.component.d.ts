/**
 * Chat message component.
 *
 * @styles
 *
 */
export declare class NbChatMessageQuoteComponent {
    /**
     * Message sender
     * @type {string}
     */
    message: string;
    /**
     * Message sender
     * @type {string}
     */
    sender: string;
    /**
     * Message send date
     * @type {Date}
     */
    date: Date;
    /**
     * Quoted message
     * @type {Date}
     */
    quote: string;
}

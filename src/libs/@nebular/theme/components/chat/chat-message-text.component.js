/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageTextComponent = /** @class */ (function () {
    function NbChatMessageTextComponent() {
    }
    NbChatMessageTextComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-chat-message-text',
                    template: "\n    <p class=\"sender\" *ngIf=\"sender || date\">{{ sender }} <time>{{ date  | date:'shortTime' }}</time></p>\n    <p class=\"text\" *ngIf=\"message\">{{ message }}</p>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageTextComponent.propDecorators = {
        "sender": [{ type: Input },],
        "message": [{ type: Input },],
        "date": [{ type: Input },],
    };
    return NbChatMessageTextComponent;
}());
export { NbChatMessageTextComponent };
//# sourceMappingURL=chat-message-text.component.js.map
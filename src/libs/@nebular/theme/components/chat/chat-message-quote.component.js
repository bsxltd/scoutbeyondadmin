/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
/**
 * Chat message component.
 *
 * @styles
 *
 */
var NbChatMessageQuoteComponent = /** @class */ (function () {
    function NbChatMessageQuoteComponent() {
    }
    NbChatMessageQuoteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'nb-chat-message-quote',
                    template: "\n    <p class=\"sender\" *ngIf=\"sender || date\">{{ sender }} <time>{{ date  | date:'shortTime' }}</time></p>\n    <p class=\"quote\">\n      {{ quote }}\n    </p>\n    <nb-chat-message-text [message]=\"message\">\n      {{ message }}\n    </nb-chat-message-text>\n  ",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                },] },
    ];
    /** @nocollapse */
    NbChatMessageQuoteComponent.propDecorators = {
        "message": [{ type: Input },],
        "sender": [{ type: Input },],
        "date": [{ type: Input },],
        "quote": [{ type: Input },],
    };
    return NbChatMessageQuoteComponent;
}());
export { NbChatMessageQuoteComponent };
//# sourceMappingURL=chat-message-quote.component.js.map
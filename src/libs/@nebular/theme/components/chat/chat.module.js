/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { NgModule } from '@angular/core';
import { NbSharedModule } from '../shared/shared.module';
import { NbChatComponent } from './chat.component';
import { NbChatMessageComponent } from './chat-message.component';
import { NbChatFormComponent } from './chat-form.component';
import { NbChatMessageTextComponent } from './chat-message-text.component';
import { NbChatMessageFileComponent } from './chat-message-file.component';
import { NbChatMessageQuoteComponent } from './chat-message-quote.component';
import { NbChatMessageMapComponent } from './chat-message-map.component';
import { NbChatOptions } from './chat.options';
var NB_CHAT_COMPONENTS = [
    NbChatComponent,
    NbChatMessageComponent,
    NbChatFormComponent,
    NbChatMessageTextComponent,
    NbChatMessageFileComponent,
    NbChatMessageQuoteComponent,
    NbChatMessageMapComponent,
];
var NbChatModule = /** @class */ (function () {
    function NbChatModule() {
    }
    NbChatModule.forRoot = function (options) {
        return {
            ngModule: NbChatModule,
            providers: [
                { provide: NbChatOptions, useValue: options },
            ],
        };
    };
    NbChatModule.forChild = function (options) {
        return {
            ngModule: NbChatModule,
            providers: [
                { provide: NbChatOptions, useValue: options },
            ],
        };
    };
    NbChatModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        NbSharedModule,
                    ],
                    declarations: NB_CHAT_COMPONENTS.slice(),
                    exports: NB_CHAT_COMPONENTS.slice(),
                },] },
    ];
    return NbChatModule;
}());
export { NbChatModule };
//# sourceMappingURL=chat.module.js.map
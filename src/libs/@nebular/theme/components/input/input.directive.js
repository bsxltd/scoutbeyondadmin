/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Directive, Input, HostBinding } from '@angular/core';
import { convertToBoolProperty } from '../helpers';
/**
 * Basic input directive.
 *
 * ```html
 * <input nbInput></input>
 * ```
 *
 * ### Installation
 *
 * Import `NbInputModule` to your feature module.
 * ```ts
 * @NgModule({
 *   imports: [
 *   	// ...
 *     NbInputModule,
 *   ],
 * })
 * export class PageModule { }
 * ```
 * ### Usage
 *
 * Default input size is `medium`:
 * @stacked-example(Showcase, input/input-showcase.component)
 *
 * Inputs are available in multiple colors using `status` property:
 * @stacked-example(Input Colors, input/input-colors.component)
 *
 * There are three input sizes:
 *
 * @stacked-example(Input Sizes, input/input-sizes.component)
 *
 * Inputs available in different shapes, which could be combined with the other properties:
 * @stacked-example(Input Shapes, input/input-shapes.component)
 *
 * `nbInput` could be applied to the following selectors - `input`, `textarea`:
 * @stacked-example(Input Elements, input/input-types.component)
 *
 * You can add `fullWidth` attribute to make element fill container:
 * @stacked-example(Full width inputs, input/input-full-width.component)
 *
 * @styles
 *
 * form-control-bg:
 * form-control-border-width:
 * form-control-border-type:
 * form-control-border-color:
 * form-control-text-primary-color:
 * form-control-focus-bg:
 * form-control-selected-border-color:
 * form-control-placeholder-font-size:
 * form-control-placeholder-color:
 * form-control-font-size:
 * form-control-padding:
 * form-control-font-size-sm:
 * form-control-padding-sm:
 * form-control-font-size-lg:
 * form-control-padding-lg:
 * form-control-border-radius:
 * form-control-semi-round-border-radius:
 * form-control-round-border-radius:
 * form-control-info-border-color:
 * form-control-success-border-color:
 * form-control-warning-border-color:
 * form-control-danger-border-color:
 */
var NbInputDirective = /** @class */ (function () {
    function NbInputDirective() {
        this.size = NbInputDirective.SIZE_MEDIUM;
        /**
           * Field shapes: `rectangle`, `round`, `semi-round`
           * @param {string} val
           */
        this.shape = NbInputDirective.SHAPE_RECTANGLE;
        this.fullWidth = false;
    }
    Object.defineProperty(NbInputDirective.prototype, "setSize", {
        set: /**
           * Field size, available sizes:
           * `small`, `medium`, `large`
           * @param {string} val
           */
        function (value) {
            this.size = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "setFullWidth", {
        set: /**
           * If set element will fill container
           * @param {string}
           */
        function (value) {
            this.fullWidth = convertToBoolProperty(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "small", {
        get: function () {
            return this.size === NbInputDirective.SIZE_SMALL;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "medium", {
        get: function () {
            return this.size === NbInputDirective.SIZE_MEDIUM;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "large", {
        get: function () {
            return this.size === NbInputDirective.SIZE_LARGE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "info", {
        get: function () {
            return this.status === NbInputDirective.STATUS_INFO;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "success", {
        get: function () {
            return this.status === NbInputDirective.STATUS_SUCCESS;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "warning", {
        get: function () {
            return this.status === NbInputDirective.STATUS_WARNING;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "danger", {
        get: function () {
            return this.status === NbInputDirective.STATUS_DANGER;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "rectangle", {
        get: function () {
            return this.shape === NbInputDirective.SHAPE_RECTANGLE;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "semiRound", {
        get: function () {
            return this.shape === NbInputDirective.SHAPE_SEMI_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NbInputDirective.prototype, "round", {
        get: function () {
            return this.shape === NbInputDirective.SHAPE_ROUND;
        },
        enumerable: true,
        configurable: true
    });
    NbInputDirective.SIZE_SMALL = 'small';
    NbInputDirective.SIZE_MEDIUM = 'medium';
    NbInputDirective.SIZE_LARGE = 'large';
    NbInputDirective.STATUS_INFO = 'info';
    NbInputDirective.STATUS_SUCCESS = 'success';
    NbInputDirective.STATUS_WARNING = 'warning';
    NbInputDirective.STATUS_DANGER = 'danger';
    NbInputDirective.SHAPE_RECTANGLE = 'rectangle';
    NbInputDirective.SHAPE_SEMI_ROUND = 'semi-round';
    NbInputDirective.SHAPE_ROUND = 'round';
    NbInputDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'input[nbInput],textarea[nbInput]',
                },] },
    ];
    /** @nocollapse */
    NbInputDirective.propDecorators = {
        "setSize": [{ type: Input, args: ['fieldSize',] },],
        "status": [{ type: Input, args: ['status',] },],
        "shape": [{ type: Input, args: ['shape',] },],
        "setFullWidth": [{ type: Input, args: ['fullWidth',] },],
        "fullWidth": [{ type: HostBinding, args: ['class.input-full-width',] },],
        "small": [{ type: HostBinding, args: ['class.input-sm',] },],
        "medium": [{ type: HostBinding, args: ['class.input-md',] },],
        "large": [{ type: HostBinding, args: ['class.input-lg',] },],
        "info": [{ type: HostBinding, args: ['class.input-info',] },],
        "success": [{ type: HostBinding, args: ['class.input-success',] },],
        "warning": [{ type: HostBinding, args: ['class.input-warning',] },],
        "danger": [{ type: HostBinding, args: ['class.input-danger',] },],
        "rectangle": [{ type: HostBinding, args: ['class.input-rectangle',] },],
        "semiRound": [{ type: HostBinding, args: ['class.input-semi-round',] },],
        "round": [{ type: HostBinding, args: ['class.input-round',] },],
    };
    return NbInputDirective;
}());
export { NbInputDirective };
//# sourceMappingURL=input.directive.js.map
import {Injectable} from '@angular/core';
import {RequestService} from './request.service';
import {map} from "rxjs/operators";
import {Observable} from "rxjs/Observable";


@Injectable()
export class PageEditingService {


  constructor(private request: RequestService) {
  }

  updateText(data) {
    const url = 'dynamic-text/update';
    return this.request.post(url, data);
  }

  getText(data: Object = null) {
    const url = 'dynamic-texts';
    return this.request.get(url, data);
  }

  getPrice(data: Object = null) {
    const url = 'payment';
    return this.request.get(url, data);
  }

  updatePrice(data) {
    const url = 'payment/update';
    return this.request.post(url, data);
  }

  updateSection(id, data) {
    const url = 'about/' + id;
    return this.request.put(url, data);
  }

  getMenu(data: Object = null) {
    const url = 'menu';
    const menu = [];
    return this.request.get(url, data).pipe(
      map(res => {
        res['success'].forEach(item => {
          let temp = {};
          temp['key'] = item.name;
          item['menu'].forEach(text => {
            temp['location'] = text.location;
            text.language_id === 1 ? temp['eng'] = text['name'] : temp['heb'] = text['name'];
            text.language_id === 1 ? temp['eng_id'] = text['id'] : temp['heb_id'] = text['id'];
          });
          menu.push(temp);
        });
        return menu;
      })
    )
  }

  createMenu(data) {
    const url = 'menu';
    return this.request.post(url, data);
  }

  updateMenu(id, data) {
    const url = `menu/${id}`;
    return this.request.post(url, data);
  }

  deleteMenu(id) {
    const url = `menu/${id}`;
    return this.request.remove(url)
  }

  getSubjects(data: Object = null) {
    const url = 'subject';
    let subjects = [];
    return this.request.get(url, data).pipe(
      map(res => {
        res['success'].forEach(item => {
          let temp = {};
          temp['key'] = item.name;
          temp['key_id'] = item.id;
          item.subject.forEach(subject => {
            subject.language_id === 1 ? temp['eng'] = subject.name : temp['heb'] = subject.name;
            subject.language_id === 1 ? temp['eng_id'] = subject.id : temp['heb_id'] = subject.id;
          });
          subjects.push(temp);
        });
        return subjects;
      })
    )
  }

  createSubject(data) {
    const url = 'subject';
    return this.request.post(url, data);
  }

  updateSubject(data, id) {
    const url = `subject/${id}`;
    return this.request.put(url, data);
  }

  deleteSubject(id) {
    const url = `subject/${id}`;
    return this.request.remove(url);
  }

  getBanners(data: Object = null) {
    const url = 'banners';
    return this.request.get(url, data);
  }

  updateBanners(id, data) {
    const url = `banner/${id}`;
    return this.request.put(url, data);
  }

  getLanguages(data: Object = null) {
    const url = 'language-key';
    let textArr = [];
    return this.request.get(url, data).pipe(
      map(res => {
        res['success'].forEach(item => {
          let temp = {};
          temp['key'] = item.name;
          item.languages.forEach(button => {
            button.language_id === 1 ? temp['eng'] = button.name : temp['heb'] = button.name;
            button.language_id === 1 ? temp['eng_id'] = button.id : temp['heb_id'] = button.id;
          });
          textArr.push(temp);
        });
        return textArr;
      })
    )
  }

  updateLanguages(data, id) {
    const url = `language/${id}`;
    return this.request.put(url, data);
  }

  deleteLanguages(id) {
    const url = `language/${id}`;
    return this.request.remove(url);
  }

  createLanguages(data) {
    const url = 'language';
    return this.request.post(url, data);
  }

  getStaticText() {
    let textArr = [];
    return this.request.get('profile-button')
      .pipe(map(res => {
        res['data'].forEach(item => {
          let temp = {};
          temp['key'] = item.name;
          item.buttons.forEach(button => {
            button.language_id === 1 ? temp['eng'] = button.name : temp['heb'] = button.name;
            button.language_id === 1 ? temp['eng_id'] = button.id : temp['heb_id'] = button.id;
          });
          textArr.push(temp);
        });
        return textArr;
      }))
  }

  updateStaticText(id, name) {
    return this.request.post(`profile-button/${id}`, name);
  }

  updatePosition(id, position){
    return this.request.put(`update-position/${id}`, position);
  }

  getAboutUs(){
    return this.request.get('about-key');
  }

  updateAboutUs(id, data){
    return  this.request.put(`about/${id}`, data);
  }

  getGenders() {
    let genderArr = [];
    return this.request.get('gender')
      .pipe(map((res: any) => {
        res.success.forEach(item => {
          let temp = {};
          temp['key'] = item.name;
          item.gender.forEach(button => {
            button.language_id === 1 ? temp['eng'] = button.name : temp['heb'] = button.name;
            button.language_id === 1 ? temp['eng_id'] = button.id : temp['heb_id'] = button.id;
          });
          genderArr.push(temp);
        });
        return genderArr;
      }))
  }

  updateGender(id: number, data: any) {
      return this.request.put(`gender/${id}`, data);
  }

  getAllImages(): Observable<any> | any {
    return this.request.get('photo');
  }

  uploadImage(data: any): Observable<any> | any {
    return this.request.post('photo-create', data);
  }

  updateImage(data: any, id: number): Observable<any> | any {
    return this.request.put(`photo/${id}`, data);
  }

  deleteImage(id: number): Observable<any> | any {
    return this.request.remove(`photo/${id}`);
  }
}

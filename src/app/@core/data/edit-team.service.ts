import {Injectable} from '@angular/core';
import {RequestService} from './request.service';

@Injectable()
export class EditTeamService {

  constructor(private request: RequestService) {
  }

  getTeams() {
    const url = 'team';
    return this.request.get(url);
  }

  addTeam (data) {
    const url = 'team';

    return this.request.post(url, data);
  }

  updateTeam(data, id){
    const url = `team/${id}`;
    return this.request.put(url,data);
  }

  deleteTeam (data) {
    const url = `team/${data.id}`;
    return this.request.remove(url);
  }

}

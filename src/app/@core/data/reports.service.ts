import {Injectable} from '@angular/core';
import {RequestService} from './request.service';
import {NbAuthJWTToken, NbAuthService} from "@nebular/auth";
import {ToastrManager} from "ng6-toastr-notifications";


@Injectable()
export class ReportsService {

  report: any = {};

  state: any;

  right: boolean;

  constructor(private request: RequestService,
              public toastr: ToastrManager) {
  }

  getAll(data: Object = null) {
    const url = 'report/all';
    return this.request.get(url, data);
  }

  getIndex(id, data: Object = null) {
    const url = 'report/' + id;
    return this.request.get(url, data)
  }

  deleteReports(data) {
    const url = 'report/delete';
    return this.request.post(url, data);
  }

  getMy(data: Object = null) {
    const url = 'myreport';
    return this.request.get(url, data);
  }

  addVideo(data: any) {
    const url = 'report/addvideo';
    return this.request.post(url, data);
  }

  deleteVideo(data) {
    const url = 'video/delete';
    return this.request.post(url, data);
  }

  sizeCheck(sections, number) {
    number = Number(number);
    let len = sections[`section[${number}]`].length;

    if (number === 1 && len === 1) {
      this.toastr.errorToastr('גודל מקסימלי');
      return false;
    }
    if (number !== 1 && len === 3) {
      this.toastr.errorToastr('גודל מקסימלי');
      return false;
    }

    return true;
  }

  updateVideo(data) {
    const url = 'video/update';
    return this.request.post(url, data);
  }

  updateComment(data) {
    const url = 'comment';
    return this.request.post(url, data)
  }

  checkComment(data) {
    if (!data.comment1) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    if (!data.comment2) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    if (!data.comment3) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    if (!data.comment4) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    return true
  }

  checkRating(data) {
    if (!data.rating1) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    if (!data.rating2) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    if (!data.rating3) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    if (!data.rating4) {
      this.toastr.errorToastr('לא ניתן לשלוח את הדו״ח מכיוון שלא כל השדות מולאו');
      return false;
    }
    return true
  }

  updateRating(data) {
    const url = 'rating';
    return this.request.post(url, data)
  }

  setReport(data, state, right) {
    this.report = data;
    this.state = state;
    this.right = right;
  }

  setOnlyReport(data) {
    this.report = data;
  }

  getReport() {
    return this.report;
  }

  getState() {
    return this.state;
  }

  getRight() {
    return this.right;
  }
}

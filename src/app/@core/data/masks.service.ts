import {Injectable} from '@angular/core';
import {RequestService} from './request.service';

@Injectable()
export class MasksService {

  constructor(private request: RequestService) {
  }

  getMasks() {
    const url = 'masks';
    return this.request.get(url);
  }

  createMask(data) {
    const url = 'mask/create';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }

  updateMask(data) {
    const url = 'mask/update';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }

  deleteMask (data) {
    const url = 'mask/delete';
    return this.request.post(url, data);
  }

}

import {Injectable} from "@angular/core";
import {HttpBackend, HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable()

export class VimeoService {

  api = 'https://api.vimeo.com/me/videos';
  accessToken = 'dffa3ce792ab14f09e5d4f27d0e2a057';

  constructor(private http: HttpClient,
              public handler: HttpBackend) {
    this.http = new HttpClient(handler);
  }

  createVideo(file: File) {
    const body = {
      name: file.name,
      upload: {
        approach: 'post',
        size: file.size,
      }
    };
    console.log(file);
    const header: HttpHeaders = new HttpHeaders()
      .set('Authorization', 'bearer ' + this.accessToken)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/vnd.vimeo.*+json;version=3.4');
    return this.http.post(this.api, body, {
      headers: header,
      observe: 'response'
    });
  }

  notCallbackVimeo(url) {
    const header: HttpHeaders = new HttpHeaders()
      .set('Authorization', 'bearer ' + this.accessToken)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/vnd.vimeo.*+json;version=3.4');
    return this.http.get(`https://api.vimeo.com${url}?fields=uri,upload.status,transcode.status`, {
      headers: header,
      observe: 'response'
    });
  }

  getLink(link){
    const header: HttpHeaders = new HttpHeaders()
      .set('Authorization', 'bearer ' + this.accessToken)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/vnd.vimeo.*+json;version=3.4');
    return this.http.get(`https://api.vimeo.com${link}`, {
      headers: header,
      observe: 'response'
    });
  }

  deleteVideo(id){
    const header: HttpHeaders = new HttpHeaders()
      .set('Authorization', 'bearer ' + this.accessToken)
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/vnd.vimeo.*+json;version=3.4');
    return this.http.delete(`https://api.vimeo.com/videos/${id}`, {
      headers: header,
      observe: 'response'
    });
  }


}

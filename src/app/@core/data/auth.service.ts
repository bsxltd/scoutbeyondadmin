import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import {NbAuthJWTToken, NbAuthService} from "@nebular/auth";



@Injectable()
export class AuthService   {

  user = {};

  constructor(private authService: NbAuthService,
              private request: RequestService) {
    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {
        if (token.isValid()) {
          this._init();
        }
      });
  }

  _init() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user !== undefined) {
      this.user = this._parseRole(user);
    }
  }

  getUser() {
    return this.user;
  }


  _parseRole(user): any {
    user.role = user['role_id'] === 3 ? 'admin' : 'coach';
    return user;
  }
}

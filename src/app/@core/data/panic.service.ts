import {Injectable} from '@angular/core';
import {RequestService} from './request.service';


@Injectable()
export class PanicService {


  constructor(private request: RequestService) {
  }

  get() {
    const url = 'emergency-logs';

    return this.request.get(url);
  }

}

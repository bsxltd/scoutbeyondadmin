import {Injectable} from '@angular/core';
import {RequestService} from './request.service';

@Injectable()
export class ServerListService {

  constructor(private request: RequestService) {
  }

  getServerList() {
    const url = 'servers';
    return this.request.get(url);
  }

  createServer(data) {
    const url = 'server/create';
    return this.request.post(url, data);
  }

  updateServer(data) {
    const url = 'server/update';
    return this.request.post(url, data);
  }

}

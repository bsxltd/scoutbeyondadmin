import { Injectable } from '@angular/core';
import {RequestService} from "./request.service";

@Injectable()
export class EditMailService {

  constructor(public request: RequestService) { }

  getEmail(){
    return this.request.get('mails');
  }

  updateEmail(id, data){
    return this.request.put(`mail/${id}`, data);
  }
}

import {NgModule, ModuleWithProviders} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserService} from './users.service';
import {StateService} from './state.service';
import {LayoutService} from './layout.service';
import {AuthGuard} from './auth-guard.service';
import {PageEditingService} from './page-editing.service';
import {RequestService} from './request.service';
import {ContentEditingService} from './content-editing.service';
import {AuthService} from './auth.service';
import {ScheduleService} from './schedule.service';
import {PanicService} from './panic.service';
import {MasksService} from './masks.service';
import {ServerListService} from './server-list.service';
import {RoleGuardService} from "./role-guard.service";
import {CoachService} from "./coach.service";
import {SlidesService} from "./slides.service";
import {EditTeamService} from "./edit-team.service";
import {AdminVideoService} from "./admin-video.service";
import {VimeoService} from "./vimeo.service";
import {HowItWorkService} from "./how-it-work.service";
import {EditMailService} from "./edit-mail.service";

const SERVICES = [
  UserService,
  StateService,
  LayoutService,
  AuthGuard,
  PageEditingService,
  RequestService,
  ContentEditingService,
  AuthService,
  ScheduleService,
  PanicService,
  MasksService,
  ServerListService,
  RoleGuardService,
  CoachService,
  SlidesService,
  EditTeamService,
  AdminVideoService,
  VimeoService,
  HowItWorkService,
  EditMailService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}

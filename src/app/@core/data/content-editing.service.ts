
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import {map} from "rxjs/operators";



@Injectable()
export class ContentEditingService   {


  constructor (private request: RequestService) {
  }

  get() {
    const url = 'list-contents';

    return this.request.get(url);
  }
  getLiveContent () {
    const url = 'live-contents';

    return this.request.get(url);
  }

  create(data: any) {
    const url = 'create-static-information';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }

  update (data: any) {
    const url = 'update-content';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }

  delete (data: any) {
    const url = 'delete-content';

    return this.request.post(url, data);
  }

  getPosition(){
    let textArr = [];
    const url = 'position-key';
    return this.request.get(url).pipe(
      map(res => {
        res['success'].forEach(item => {
          let temp = {};
          temp['key'] = item.name;
          item.position.forEach(position => {
            position.language_id === 1 ? temp['eng'] = position.position : temp['heb'] = position.position;
            position.language_id === 1 ? temp['eng_id'] = position.id : temp['heb_id'] = position.id;
          });
          textArr.push(temp);
        });
        return textArr;
      })
    )
  }
}

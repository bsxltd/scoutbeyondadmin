import {Injectable} from '@angular/core';
import {RequestService} from './request.service';
import {ToastrManager} from "ng6-toastr-notifications";

@Injectable()
export class UserService {

  user: any;

  constructor(private request: RequestService,
              public toastr: ToastrManager) {
  }

  getUsers() {
    const url = 'users';
    return this.request.get(url);
  }

  deleteUsers(data){
    const url = 'players/delete';
    return this.request.post(url,data);
  }

  getConsultants() {
    const url = 'list-consultants';
    return this.request.get(url);
  }

  getKids() {
    const url = 'kids';
    return this.request.get(url);
  }

  getParents() {
    const url = 'parents';
    return this.request.get(url);
  }

  updateConsultant(data) {
    const url = 'consultant/update';

    return this.request.post(url, data);
  }

  createConsultant(data) {
    const url = 'create-consultant';

    return this.request.post(url, data);
  }
  deleteConsultant(data) {
    const url = 'consultant';

    return this.request.remove(url, data);
  }

  setUser(data){
    this.user = data;
  }

  getUser(){
    return this.user;
  }

  getIndex(id, data: Object = null){
    const url = `user/${id}`;
    return this.request.get(url,data);
  }

  getContact(id: number){
    const url = `user/update-by-admin/${id}`;
    return this.request.get(url);
  }

  updateUser(data: any){
    const url = 'admin/updatePlayer';
    return this.request.post(url,data);
  };

  updateContact(data: any, id: number){
    const url = `user/update-by-admin/${id}`;
    return this.request.post(url, data);
  }

  changePassword(data: any){
    const url = 'admin/updatePassword';
    return this.request.post(url,data);
  }

  getPositions(data: Object = null){
    const url = 'position';
    return this.request.get(url,data);
  }

  getCities(data: Object = null){
    const url = 'city';
    return this.request.get(url,data);
  }

  getLanguage(data: Object = null){
    const url = 'language-key';
    return this.request.get(url,data);
  }

  getCountry(data: Object = null){
    const url = 'country';
    return this.request.get(url,data)
  }

  resetPassword(data: any){
    const url = 'password/reset';
    return this.request.post(url,data);
  }

  getDeletedReports(){
    const url = 'report/deleted';
    return this.request.get(url);
  }

  restoreReport(data: any){
    const url = 'report/restore';
    return this.request.post(url,data);
  }

  getDeletedUsers(){
    const url = 'users/deleted';
    return this.request.get(url);
  }

  getDeletedCoaches(){
    const url = 'coach/deleted';
    return this.request.get(url);
  }

  restoreUser(data: any){
    const url = 'user/restore';
    return this.request.post(url,data);
  }

  credentialCheck(data: any){
    console.log(data);
    if(!data.firstname){
      this.toastr.errorToastr('יש למלא את השם הפרטי');
      return false
    }
    if(!data.lastname){
      this.toastr.errorToastr('יש למלא את השם משפחה');
      return false
    }
    if(data.has_contract && data.contract_date === '//' || data.has_contract && data.contract_date.length < 10){
      this.toastr.errorToastr('יש למלא את תאריך סיום החוזה');
      return false
    }
    return true
  }
}

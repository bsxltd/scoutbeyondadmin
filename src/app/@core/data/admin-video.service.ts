import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import {NbAuthJWTToken, NbAuthService} from "@nebular/auth";
import {ToastrManager} from "ng6-toastr-notifications";



@Injectable()
export class AdminVideoService   {

  constructor(private request: RequestService,
              public toastr: ToastrManager) {
  }


  getAll(){
    const url = 'report-video-key';
    return this.request.get(url)
  }

  updateDescription(data){
    const url = 'admin/video/updatetext';
    return this.request.post(url,data);
  }

  updateVideo(data){
    const url = 'admin/video/update';
    return this.request.post(url,data);
  }

  deleteVideo(data){
    const url = 'adminvideo/delete';
    return this.request.post(url,data);
  }

  createVideo(data){
    const url = 'admin/video';
    return this.request.post(url,data)
  }

  credentialCheck(file,data,selected){
    if(!file){
      this.toastr.errorToastr('Choose a file');
      return false;
    }
    if(!data.description){
      this.toastr.errorToastr('Enter description');
      return false;
    }
    if(!selected){
      this.toastr.errorToastr('Choose a section');
      return false;
    }
    return true;
  }


  sizeCheck(sections, number){
    if(+number === 1 && sections[`section[${number}]`].length === 1){
      this.toastr.errorToastr('Max size');
      return false;
    }
    if(+number !== 1 && sections[`section[${number}]`].length === 2){
      this.toastr.errorToastr('Max size');
      return false;
    }
    return true;
  }


}

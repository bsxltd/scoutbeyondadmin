import {Injectable} from '@angular/core';
import {RequestService} from './request.service';

@Injectable()
export class SlidesService {

  constructor(private request: RequestService) {
  }

  getSlides() {
    const url = 'slider-key';
    return this.request.get(url);
  }

  addSlide (data) {
    const url = 'slider';

    return this.request.post(url, data);
  }

  updateSlide(data, id){
    const url = `slider/${id}`;
    return this.request.put(url,data);
  }

  deleteSlide (data) {
    const url = `slider/${data.id}`;
    return this.request.remove(url);
  }

}

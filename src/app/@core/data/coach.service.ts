import {Injectable} from '@angular/core';
import {RequestService} from './request.service';
import {map} from "rxjs/operators";

@Injectable()
export class CoachService {

  constructor(private request: RequestService) {
  }

  getCoaches() {
    const url = 'user/coach';
    return this.request.get(url);
  }

  appointCoach(data){
    const url = 'report/coach';
    return this.request.post(url,data)
  }


  addCoach(data) {
    const url = 'user/coach';

    return this.request.post(url, data);
  }


  editCoach(data){
    const url = 'coach/update';

    return this.request.post(url,data);
  }

  deleteCoach (data) {
    const url = 'coach/delete';
    return this.request.post(url, data);
  }

  addScout(data){
    const url = 'register-scout';
    return this.request.post(url, data);
  }

  getScouts(){
    const url = 'scouts-list';
    return this.request.get(url);
  }

  deleteScout(id){
    const url = 'scout-delete';
    return this.request.post(url, id)
  }

  getDeletedScout(){
    const url = 'scout/deleted';
    return this.request.get(url);
  }

}

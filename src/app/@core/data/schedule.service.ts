import {of as observableOf, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {RequestService} from './request.service';


@Injectable()
export class ScheduleService {


  constructor(private request: RequestService) {
  }

  get() {
    const url = 'schedules';

    return this.request.get(url);
  }
  getTwoWeeks() {
    const url = 'schedules/all/weeks';

    return this.request.get(url);
  }

  approve(data: any) {
    const url = 'schedule/approve';

    return this.request.post(url, data);
  }
}

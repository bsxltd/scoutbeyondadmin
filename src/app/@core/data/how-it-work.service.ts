import { Injectable } from '@angular/core';
import {RequestService} from "./request.service";
import {map} from "rxjs/operators";

@Injectable()
export class HowItWorkService {
  url: string = 'how-it-work';

  constructor(public request: RequestService) { }

  getAll(){
    return this.request.get(`${this.url}s`);
  }

  addItem(data){
    return  this.request.post(this.url, data)
  }

  update(id, data){
    return this.request.put(`${this.url}/${id}`, data);
  }

  deleteItem(id){
    return this.request.remove(`${this.url}/${id}`);
  }
}

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from "rxjs/Rx";
import {AuthService} from "./auth.service";

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(private authService: AuthService, public router: Router) {
  }


  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const user = this.authService.getUser();

    if (user['role'] === next.data.role) {
      return true;
    }

    // navigate to not found page
    this.router.navigate(['/404']);
    return false;
  }
}

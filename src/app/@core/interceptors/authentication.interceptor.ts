import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders} from '@angular/common/http';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = JSON.parse(localStorage.getItem('auth_app_token'));

    if (token && typeof token.value !== 'undefined') {
      const headers = new HttpHeaders()
        .set('Authorization', 'Bearer ' + token.value);
      req = req.clone({headers});

    }
    return next.handle(req);
  }
}

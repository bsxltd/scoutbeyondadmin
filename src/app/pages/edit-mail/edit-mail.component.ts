import { Component, OnInit } from '@angular/core';
import {EditMailService} from "../../@core/data/edit-mail.service";
import {ToastrManager} from "ng6-toastr-notifications";

@Component({
  selector: 'ngx-edit-mail',
  templateUrl: './edit-mail.component.html',
  styleUrls: ['./edit-mail.component.scss']
})
export class EditMailComponent implements OnInit {

  emailList: any;
  constructor(public emailService: EditMailService, public toast: ToastrManager) { }

  ngOnInit() {
    this.getEmails();
  }

  getEmails(){
    this.emailService.getEmail()
      .subscribe(res => {
        console.log(res);
        this.emailList = res['data'];
      })
  }

  updateEmail(email){
    this.emailService.updateEmail(email.id, email)
      .subscribe(() => {
        this.toast.successToastr('Successfuly updated');
      }, () => {
        this.toast.errorToastr('Error');
      })
  }

}

import {Component, OnInit} from '@angular/core';
import {PageEditingService} from "../../@core/data/page-editing.service";

@Component({
  selector: 'ngx-gender',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.scss']
})
export class GenderComponent implements OnInit {

  source: any = [];
  settings = {
    attr: {
      class: 'change-table'
    },
    actions: {
      add: false,
      delete: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-check"></i>',
      cancelButtonContent: '<i class="fas fa-times"></i>'
    },
    columns: {
      key: {
        title: 'key',
        type: 'string',
        editable: false
      },
      heb: {
        title: 'heb',
        type: 'string',
      },
      eng: {
        title: 'eng',
        type: 'string',
      },
    },
  };

  constructor(private pageService: PageEditingService) {
  }

  ngOnInit() {
    this.getGender();
  }

  getGender() {
    this.pageService.getGenders()
      .subscribe((res: any) => {
        this.source = res;
        console.log(this.source);
      })
  }

  onEditConfirm(event) {
    console.log(event);
    event.confirm.resolve();
    for (let name in event.newData) {
      if (event.newData.hasOwnProperty(name)) {
        if (event.newData[name] !== event.data[name]) {
          this.updateGender(event.newData[`${name}_id`], {name: event.newData[name]});
        }
      }
    }
    event.confirm.reject();
  }

  updateGender(id: number, data: any){
    this.pageService.updateGender(id, data)
      .subscribe((res: any) => {
        console.log(res);
      })
  }
}

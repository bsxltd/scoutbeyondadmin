import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComfirmationPromptComponent } from './comfirmation-prompt.component';

describe('ComfirmationPromptComponent', () => {
  let component: ComfirmationPromptComponent;
  let fixture: ComponentFixture<ComfirmationPromptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComfirmationPromptComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComfirmationPromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

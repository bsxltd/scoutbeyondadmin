import {Component, Inject, OnInit} from '@angular/core';
import {ContentDialogComponent} from '../../content-editing/content-dialog/content-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'ngx-comfirmation-prompt',
  templateUrl: './comfirmation-prompt.component.html',
  styleUrls: ['./comfirmation-prompt.component.scss'],
})
export class ComfirmationPromptComponent implements OnInit {
  public state = '';
  constructor(public dialogRef: MatDialogRef<ContentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data) {
    this.state = this.data.state;
  }

  ngOnInit() {
  }

  save() {
    this.dialogRef.close(true);
  }

  close() {
    this.dialogRef.close(false);
  }
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PanicLogsComponent} from './panic-logs.component';


const routes: Routes = [{
  path: '',
  component: PanicLogsComponent,
  children: [{
    path: 'panic-logs',
    component: PanicLogsComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PanicLogsRoutingModule {
}

export const routedComponents = [
  PanicLogsComponent,
];

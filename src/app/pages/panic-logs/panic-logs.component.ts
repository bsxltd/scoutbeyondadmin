import { Component, OnInit } from '@angular/core';
import {PanicService} from '../../@core/data/panic.service';

@Component({
  selector: 'ngx-panic-logs',
  templateUrl: './panic-logs.component.html',
  styleUrls: ['./panic-logs.component.scss'],
})
export class PanicLogsComponent implements OnInit {
  public panicLogs: any;
constructor(public panicService: PanicService) {
    this.panicService.get().subscribe(res => {
      this.panicLogs = res['data'];
    });
  }

  ngOnInit() {
  }

}

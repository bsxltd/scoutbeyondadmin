import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import { routedComponents, PanicLogsRoutingModule } from './panic-logs.routing';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule, MatCardModule } from '@angular/material';
import { NbCardModule } from '@nebular/theme';

@NgModule({
  imports: [
    ThemeModule,
    EditorModule,
    ToasterModule.forRoot(),
    PanicLogsRoutingModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    NbCardModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
  entryComponents: [

  ],
})

export class PanicLogsModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanicLogsComponent } from './panic-logs.component';

describe('PanicLogsComponent', () => {
  let component: PanicLogsComponent;
  let fixture: ComponentFixture<PanicLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanicLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanicLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {AdminVideoService} from "../../@core/data/admin-video.service";
import {ToastrManager} from "ng6-toastr-notifications";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'ngx-admin-video',
  templateUrl: './admin-video.component.html',
  styleUrls: ['./admin-video.component.scss']
})
export class AdminVideoComponent implements OnInit {

  loading: boolean = false;

  video: any = {
    id: null,
    type: null,
    description: ''
  };
  file: any;

  selectedSection: any = '';

  sections: any = {
    'section[1]': [],
    'section[2]': [],
    'section[3]': [],
    'section[4]': [],
    'section[5]': []
  };

  ratings: any = [1,2,3,4,5];

  currentIndex: any;

  constructor(public adminVideoService: AdminVideoService,
              public toastr: ToastrManager,
              public sanitizer: DomSanitizer
              ) {
  }

  ngOnInit() {
    this.getVideo('1')
  }

  getVideo(sectionNumber) {
    this.currentIndex = sectionNumber;
    this[`sections`] = {
      'section[1]': [],
      'section[2]': [],
      'section[3]': [],
      'section[4]': [],
      'section[5]': []
    };
    this.adminVideoService.getAll()
      .subscribe(res => {
        res['data'].forEach(item => {
          item['admin_video'].forEach(video => {
            video.path = this.sanitizer.bypassSecurityTrustResourceUrl(video.path.replace("https://vimeo.com/", "https://player.vimeo.com/video/"));
            if (video.type === sectionNumber) {
              this[`sections`][`section[${video.section}]`].push(video);
            }
          })
        })
      })
  }

  updateDescription(video){
    this.adminVideoService.updateDescription({
      id: video.id,
      type: video.type,
      description: video.description,
      language_id: video.language_id
    })
      .subscribe(res =>{
        this.getVideo(this.currentIndex);
      })
  }

  updateVideo(event, id) {
    this.loading = true;
    console.log(id);
    let fd = new FormData();
    let file = event.target.files[0];
    fd.append('video_id', id);
    fd.append('file', file);
    this.adminVideoService.updateVideo(fd)
      .subscribe(res => {
        this.getVideo(this.currentIndex);
        this.loading = false;
      })
  }

  selectFile(event){
    this.file = event.target.files[0];
  }

  deleteVideo(id){
    this.loading = true;
    this.adminVideoService.deleteVideo({ids: [id]})
      .subscribe(res => {
        this.getVideo(this.currentIndex);
        this.loading = false;
      })

  }

  createVideo(){
    this.loading = true;
    if(this.adminVideoService.credentialCheck(this.file,this.video,this.selectedSection)){
      if(this.adminVideoService.sizeCheck(this.sections,this.selectedSection)){
        let fd = new FormData();
        console.log(`section[${this.selectedSection}][]`);
        fd.append(`section[${this.selectedSection}][]`,this.file);
        this.adminVideoService.createVideo(fd)
          .subscribe(res =>{
            this.adminVideoService.getAll()
              .subscribe(res =>{
                this.video.id = res['success'][res['success'].length-1].id;
                this.video.type = +this.currentIndex;
                this.adminVideoService.updateDescription(this.video)
                  .subscribe(res =>{
                    this.loading = false;
                    this.toastr.successToastr('Video has been created','Success');
                    this.getVideo(this.currentIndex);
                  },err =>{
                    this.loading = false;
                  })
              },err =>{
                this.loading = false;
              })
          },err=>{
            this.loading = false;
          })
      }else {
        this.loading = false;
      }
    }else {
      this.loading = false;
    }

  }

}

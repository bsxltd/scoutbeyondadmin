import {NgModule} from '@angular/core';

import {ThemeModule} from '../../@theme/theme.module';
import {ToasterModule} from 'angular2-toaster';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  imports: [
    ThemeModule,
    ToasterModule.forRoot(),
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
  ],
  declarations: [
  ],
  providers: [],
})
export class AdminVideoModule {
}

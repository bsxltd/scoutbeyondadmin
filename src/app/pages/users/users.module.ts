import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import {MatButtonModule, MatCardModule, MatDialogModule, MatSliderModule} from '@angular/material';
import {NbBadgeModule} from '@nebular/theme';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {MatMenuModule} from "@angular/material/menu";
import {UserInfoComponent} from "../user-info/user-info.component";

@NgModule({
  imports: [
    ThemeModule,
    EditorModule,
    ToasterModule.forRoot(),
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    Ng2SmartTableModule,
    MatMenuModule,
    MatInputModule,
    NbBadgeModule,
    MatSliderModule,
  ],
  declarations: [
  ],
  providers: [
  ],
  exports: [
    Ng2SmartTableModule
  ],
  entryComponents: [
  ],
})

export class UsersModule { }

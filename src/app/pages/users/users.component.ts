import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular2-toaster";
import {MatDialog} from "@angular/material";
import {UserService} from "../../@core/data/users.service";
import {LocalDataSource} from "ng2-smart-table";
import {Router} from "@angular/router";
import {ToastrManager} from "ng6-toastr-notifications";

@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public users: any = [];
  config: ToasterConfig;
  source: LocalDataSource = new LocalDataSource();

  passBlock: boolean = false;

  newPassword: any;

  settings = {
    actions: {
      add: false,
      edit: false,
      // custom: [{
      //   title: '<i class="fas fa-key"></i>'
      // }],
      position: 'right',
      columnTitle: 'פעולות'
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true,
      name: 'פעולות'
    },
    columns: {
      id: {
        title: 'מזהה משתמש',
        type: 'number',
      },
      firstname: {
        title: 'שם פרטי',
        type: 'string',
      },
      lastname: {
        title: 'שם משפחה',
        type: 'string',
      },
      email: {
        title: 'דוא"ל',
        type: 'string',
      },
      created_at: {
        title: 'תאריך הצטרפות',
        type: 'string',
      }
    },
  };

  userId: any;
  userEmail: any;
  userName: any;

  constructor(private userService: UserService,
              public dialog: MatDialog,
              public toasterService: ToasterService,
              public toastr: ToastrManager,
              public router: Router) { }

  ngOnInit() {
    this.getUsers()
  }

  onUserRowSelect(ev){
    this.userService.setUser(ev['data']);
    this.router.navigate(['/pages/user-info']);
  }

  changePassword(){
    this.userService.changePassword({user_id: this.userId, password: this.newPassword})
      .subscribe(res =>{
        if(res['success']){
          this.toastr.successToastr('הפרטים נשמרו');
          this.passBlock = false;
          this.newPassword = '';
        }else {
          this.toastr.errorToastr('הסיסמא לא נשלחה')
        }
      },err =>{
        this.toastr.errorToastr('הסיסמא לא נשלחה')
      })
  }

  onDeleteConfirm(event):void{
    if (window.confirm('אתה בטוח שאתה רוצה למחוק?')) {
      event.confirm.resolve();
      this.userService.deleteUsers({ids: [event['data']['id']]})
        .subscribe(res =>{
          this.toastr.successToastr('המשתמש נמחק.');
        })
    } else {
      event.confirm.reject();
    }
  }

  getData(ev){
    this.userId = ev.data.id;
    this.userEmail = ev.data.email;
    this.userName = ev.data.firstname;
    this.passBlock = true;
  }

  resetPassword(){
    this.userService.resetPassword({email: this.userEmail})
      .subscribe(res =>{
        if(res['success']){
          this.toastr.successToastr('סיסמא זמנית נשלחה לכתובת האימייל שהוזנה');
          this.passBlock = false;
          this.newPassword = '';
        }else {
          this.toastr.errorToastr('הסיסמא לא נשלחה')
        }
      },err =>{
        this.toastr.errorToastr('הסיסמא לא נשלחה')
      })
  }

  getUsers(){
    this.userService.getUsers()
      .subscribe(res => {
        this.users = res['data'];
        this.source.load(this.users);
      })
  }

}

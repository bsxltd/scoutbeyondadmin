import { Component, OnInit } from '@angular/core';
import {ToastrManager} from "ng6-toastr-notifications";
import {PageEditingService} from "../../@core/data/page-editing.service";

@Component({
  selector: 'ngx-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  contactUsText: any = [];
  banner: any = {
    title: "",
    photo: "",
    name: "contact_us",
    language_id: '2'
  };

  bannerEng: any = {
    title: "",
    photo: "",
    name: "contact_us",
    language_id: '1'
  };

  bannersId: any = {
    eng: '',
    heb: ''
  };
  constructor(public toasterService: ToastrManager,
              public pageEditingService: PageEditingService) { }

  ngOnInit() {
    this.getBanners();
    this.getContactText();
  }

  getContactText() {
    this.pageEditingService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          if (item.title === 'CONTACT-US'){
            item.dynamic_text_id = item.id;
            this.contactUsText.push(item);
          }
        })
      })
  }

  updateText(data) {
    console.log(data);
    this.pageEditingService.updateText(data)
      .subscribe(() => {
        this.toasterService.successToastr('Successfuly updated');
      }, () => {
        this.toasterService.errorToastr('Error updating')
      })
  }

  changePhoto(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = () => {
          this.banner.photo = reader.result;
          this.bannerEng.photo = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  updateBanner(id, banner) {
    let obj = {};
    Object.assign(obj, banner);
    if (banner.photo.startsWith('https://')){
      delete obj['photo'];
    }
    this.pageEditingService.updateBanners(id, obj)
      .subscribe(res => {
        this.toasterService.successToastr(res['message']);
      })
  }

  getBanners() {
    this.pageEditingService.getBanners()
      .subscribe(res => {
        res['success'].forEach(item => {
          if (item.name === 'contact_us') {
            item.language_id === 2 ? this.banner.title = item.title : this.bannerEng.title = item.title;
            item.language_id === 2 ? this.banner.photo = item.photo : this.bannerEng.photo = item.photo;
            item.language_id === 2 ? this.bannersId.heb = item.id : this.bannersId.eng = item.id;
          }
        })
      })
  }
}

import {NbMenuItem} from '@nebular/theme';

export class NbMenuItemWithPermissions extends NbMenuItem {
  role?: string;
}

export const MENU_ITEMS: NbMenuItemWithPermissions[] = [
  {
    title: 'שחקנים',
    icon: 'fas fa-address-book',
    link: '/pages/users-list',
    role: 'admin'
  },
  {
    title: 'הדוחות שלי',
    icon: 'fas fa-paste',
    link: '/pages/coaches-reports',
    role: 'coach',
    children: [
      {
        title: 'דוחות חדשים',
        link: '/pages/coaches-reports/coach-new-reports',
      },
      {
        title: 'דוחות בתהליך',
        link: '/pages/coaches-reports/coach-reports-on-check',
      },
      {
        title: 'דוחות שהוגשו',
        link: '/pages/coaches-reports/coach-finished-reports',
      }
    ]
  },
  {
    title: 'עריכת תוכן דינאמי',
    icon: 'far fa-edit',
    link: '/pages/page-editing',
    role: 'admin',
    children: [
      {
        title: 'דף הבית',
        link: '/pages/page-editing/home-page',
      },
      {
        title: 'דף דוחות',
        link: '/pages/page-editing/reports-page',
      },
      {
        title: 'דף הצוות שלנו',
        link: '/pages/page-editing/our-team-page',
      },
      {
        title: ' פופ-אפ מתחת לגיל 12',
        link: '/pages/page-editing/profile-page',
      },
      {
        title: 'אודות הדף',
        link: '/pages/page-editing/about-us-page',
      },
      {
        title: 'צור קשר',
        link: '/pages/page-editing/contact-us-page',
      },
      {
        title: 'באנרים בעמוד הבית',
        link: '/pages/page-editing/edit-slider',
      },
      // {
      //   title: 'עריכת צוות',
      //   link: '/pages/page-editing/edit-team',
      // },
      {
        title: 'עריכת תפריט',
        link: '/pages/page-editing/menu-edit',
      },
      {
        title: 'נושא\n',
        link: '/pages/page-editing/subjects',
      },
      {
        title: 'תנאי שימוש\n',
        link: '/pages/page-editing/terms',
      },
      {
        title: 'מדיניות פרטיות',
        link: '/pages/page-editing/privacy',
      },
      {
        title: 'כותרות דו״ח מאמן',
        link: '/pages/page-editing/types',
      },
      {
        title: 'שפות\n',
        link: '/pages/page-editing/languages',
      },
      {
        title: 'איך זה עובד',
        link: '/pages/page-editing/how-it-work',
      },
      {
        title: 'static text',
        link: '/pages/page-editing/static-text',
      },
      {
        title: 'email',
        link: '/pages/page-editing/edit-mail',
      },
      {
        title: 'images',
        link: '/pages/page-editing/images'
      },
      {
        title: 'position',
        link: '/pages/page-editing/dropdown'
      },
      {
        title: 'gender',
        link: '/pages/page-editing/genders'
      }
    ],
  },
  {
    title: 'דוחות מאמן',
    icon: 'fas fa-clipboard-list',
    link: '/pages/reports',
    role: 'admin',
    children: [
      {
        title: 'דוחות חדשים',
        link: '/pages/reports/new-reports',
      },
      {
        title: 'דוחות בתהליך',
        link: '/pages/reports/reports-on-check',
      },
      {
        title: 'דוחות שהוגשו',
        link: '/pages/reports/finished-reports',
      }
    ]},
  {
    title: 'הדרכות בדו״ח מאמן',
    icon: 'fas fa-video',
    link: '/pages/admin-video',
    role: 'admin',
  },
  {
    title: 'מאמנים',
    icon: 'fas fa-user-tie',
    link: '/pages/coaches',
    role: 'admin',
  },
  {
    title: 'צופים / סוכנים',
    icon: 'fas fa-user-secret',
    link: '/pages/scouts',
    role: 'admin',
  },
  {
    title: 'משתמשים מחוקים',
    icon: 'fas fa-user-times',
    link: '/pages/deleted-users',
    role: 'admin',
  },{
    title: 'דוחות מחוקים',
    icon: 'fas fa-calendar-times',
    link: '/pages/deleted-reports',
    role: 'admin',
  },{
    title: 'מאמנים מחוקים',
    icon: 'fas fa-user-slash',
    link: '/pages/deleted-coaches',
    role: 'admin',
  },
];

import {Component, OnInit} from '@angular/core';
import {PageEditingService} from "../../@core/data/page-editing.service";

@Component({
  selector: 'ngx-static-text',
  templateUrl: './static-text.component.html',
  styleUrls: ['./static-text.component.scss']
})
export class StaticTextComponent implements OnInit {

  source: any = [];
  settings = {
    attr: {
      class: 'change-table'
    },
    actions: {
      add: false,
      delete: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-check"></i>',
      cancelButtonContent: '<i class="fas fa-times"></i>'
    },
    columns: {
      key: {
        title: 'key',
        type: 'string',
        editable: false
      },
      heb: {
        title: 'heb',
        type: 'string',
      },
      eng: {
        title: 'eng',
        type: 'string',
      },
    },
  };

  constructor(public staticTextService: PageEditingService) {
  }

  ngOnInit() {
    this.getStaticText();
  }

  getStaticText() {
    this.staticTextService.getStaticText()
      .subscribe(res => {
          this.source = res;
          console.log(this.source);
        }
      )
  }

  onEditConfirm(ev){
    console.log(ev);
    ev.confirm.resolve();
    for (let name in ev.newData){
      if (ev.newData.hasOwnProperty(name)){
        if (ev.newData[name] !== ev.data[name]){
          this.updateStaticText(ev.newData[`${name}_id`], {name: ev.newData[name]});
        }
      }
    }
    ev.confirm.reject();
  }

  updateStaticText(id, text){
    this.staticTextService.updateStaticText(id, text)
      .subscribe(res => {
        console.log(res);
      })
  }

}

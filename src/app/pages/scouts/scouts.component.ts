import { Component, OnInit } from '@angular/core';
import {CoachService} from "../../@core/data/coach.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LocalDataSource} from "ng2-smart-table";
import {ToastrManager} from "ng6-toastr-notifications";
import {UserService} from "../../@core/data/users.service";

@Component({
  selector: 'ngx-scouts',
  templateUrl: './scouts.component.html',
  styleUrls: ['./scouts.component.scss']
})
export class ScoutsComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();
  deletedList: LocalDataSource = new LocalDataSource();
  settings = {
    actions: {
      add: false,
      edit: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    delete: {
      deleteButtonContent: '<i class="far fa-trash-alt"></i>',
      confirmDelete: true,
      name: 'פעולות'
    },
    columns: {
      firstname: {
        title: 'first name',
        type: 'string',
      },
      lastname: {
        title: 'last name',
        type: 'string',
      },
      email: {
        title: 'email',
        type: 'string',
      },
      // password: {
      //   title: 'password',
      //   type: 'string'
      // }
    },
  };
  settings2: any = {
    actions: {
      add: false,
      edit: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-trash-restore"></i>',
      confirmDelete: true,
      name: 'פעולות'
    },
    columns: {
      firstname: {
        title: 'first name',
        type: 'string',
      },
      lastname: {
        title: 'last name',
        type: 'string',
      },
      email: {
        title: 'email',
        type: 'string',
      },
      // password: {
      //   title: 'password',
      //   type: 'string'
      // }
    },
  };
  scoutData: FormGroup;
  constructor(public coachService: CoachService,
              public userService: UserService,
              public fb: FormBuilder,
              public toasterService: ToastrManager) { }

  ngOnInit() {
    this.getScouts();
    this.getDeletedScouts();
    this.initForm();
  }

  initForm(){
    this.scoutData = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    })
  }

  getScouts(){
    this.coachService.getScouts()
      .subscribe(res => {
        if (res['success']){
          this.source.load(res['data']);
        }
      })
  }

  addScout(){
    if (this.scoutData.valid){
      this.coachService.addScout(this.scoutData.value)
        .subscribe(res => {
          if (res['success']){
            this.toasterService.successToastr('הצופה נוצר בהצלחה');
            this.source.append(res['data']);
            this.initForm();
          }
        }, err => {
          this.toasterService.errorToastr(err.error['data']);
        })
    }
  }

  deleteScout(ev){
    ev.confirm.resolve();
    this.coachService.deleteScout({ids: [ev.data.id]})
      .subscribe(res => {
        if (res['success']){
          this.toasterService.successToastr('הסקאוט נמחק');
          this.source.remove(ev.data);
          this.getDeletedScouts();
        }
      });
    ev.confirm.reject();
  }

  getDeletedScouts(){
    this.coachService.getDeletedScout()
      .subscribe(res => {
        this.deletedList.load(res['data'])
      })
  }

  restoreScout(ev){
    this.userService.restoreUser({user_id: ev.data.id})
      .subscribe(() => {
        this.toasterService.successToastr('צופה שוחזר');
        this.deletedList.remove(ev.data);
        this.getScouts();
      })
  }
}

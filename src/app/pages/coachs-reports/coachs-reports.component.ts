import {Component, OnInit} from '@angular/core';
import {LocalDataSource} from "ng2-smart-table";
import {ReportsService} from "../../@core/data/reports.service";
import {Router} from "@angular/router";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'ngx-coachs-reports',
  templateUrl: './coachs-reports.component.html',
  styleUrls: ['./coachs-reports.component.scss']
})
export class CoachsReportsComponent implements OnInit {

  public tabs: any[] = [
    {
      title: 'דוחות חדשים',
      route: '/pages/coaches-reports/coach-new-reports',
    },
    {
      title: 'דווח על המחאה',
      route: '/pages/coaches-reports/coach-reports-on-check',
    },
    {
      title: 'דוחות שהסתיימו',
      route: '/pages/coaches-reports/coach-finished-reports',
    },
  ];

  constructor(public reportService: ReportsService,
              public router: Router) {
  }

  ngOnInit() {
  }


}

@Component({
  selector: 'coaches-new',
  template: `
    <nb-card>
      <nb-card-header>
        דוחות חדשים
      </nb-card-header>
      <nb-card-body>

        <ng2-smart-table [settings]="settings" [source]="source" (userRowSelect)="onUserRowSelect($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./coachs-reports.component.scss']
})
export class CoachNewReportsComponent extends CoachsReportsComponent {

  reports: any = [];
  selectedItem: any;

  source: LocalDataSource = new LocalDataSource();
  settings = {
    actions: false,
    columns: {
      id: {
        title: 'מספר דו״ח מאמן',
        type: 'number',
      },
      'player.firstname': {
        title: 'שם שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.firstname;
        },
      },
      player: {
        title: 'מספר מזהה',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.id;
        },
      },
      player_type: {
        title: 'סוג השחקן',
        type: 'string',
        valuePrepareFunction: (value) => {
          if (+value === 1) {
            return 'שוער'
          }else {
            return 'שחקן שדה'
          }
        },
      },
      created_at: {
        title: 'תאריך יצירת הדו״ח',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      due_date: {
        title: 'תאריך להגשה',
        type: 'html',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return '<p class="due-date" style="color: #ff0000!important;">' + new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm') + '</p>';
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
    },
  };

  constructor(public reportsService: ReportsService,
              public router: Router) {
    super(reportsService,router);
    console.log('coaches-new');
  }

  ngOnInit() {
    this.getReports();
  }

  onUserRowSelect(event) {
    console.log(event);
    this.reportService.setReport(event.data,'edit',true);
    this.router.navigate(['/pages/edit-report'])
  }

  getReports() {
    this.reports = [];
    this.reportService.getMy()
      .subscribe(res => {
        res['data'].filter(item => {
          if (item.status === 1 && item.comment === null) {
            this.reports.push(item);
          }
        });
        this.reportService.setReport(this.reports,'edit',true);
        this.source.load(this.reports);
      })
  }
}

@Component({
  selector: 'coaches-on-check',
  template: `
    <nb-card>
      <nb-card-header>
        דווח על המחאה
      </nb-card-header>
      <nb-card-body>

        <ng2-smart-table [settings]="settings" [source]="source" (userRowSelect)="onUserRowSelect($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./coachs-reports.component.scss']

})
export class CoachReportsOnCheckComponent extends CoachsReportsComponent {

  public reports: any = [];

  source: LocalDataSource = new LocalDataSource();
  settings = {
    actions: false,
    columns: {
      id: {
        title: 'מספר דו״ח מאמן',
        type: 'number',
      },
      'player.firstname': {
        title: 'שם שחקן',
        type: 'string',
        valuePrepareFunction: (cell,row) => {
          return row.player.firstname;
        },
      },
      player: {
        title: 'מספר מזהה',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.user_id;
        },
      },
      player_type: {
        title: 'סוג השחקן',
        type: 'string',
        valuePrepareFunction: (value) => {
          if (+value === 1) {
            return 'שוער'
          }else {
            return 'שחקן שדה'
          }
        },
      },
      updated_at: {
        title: 'עודכן בתאריך',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      created_at: {
        title: 'תאריך יצירת הדו״ח',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      due_date: {
        title: 'תאריך להגשה',
        type: 'html',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return '<p class="due-date" style="color: #ff0000!important;">' + new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm') + '</p>';
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
    },
  };

  public input: string = '<input type="checkbox">';


  constructor(public reportsService: ReportsService,
              public router: Router) {
    super(reportsService,router)
  }


  ngOnInit() {
    this.getReports();
    console.log('coaches-new');
  }

  onUserRowSelect(event) {
    console.log(event.data);
    this.reportService.setReport(event.data,'edit',true);
    this.router.navigate(['/pages/edit-report'])
  }

  getReports() {
    this.reports = [];
    this.reportService.getMy()
      .subscribe(res => {
        res['data'].filter(item => {
          if (item.status === 1 && item.comment !== null && item.rating !== []) {
            this.reports.push(item);
          }
        });
        this.source.load(this.reports);
      })
  }
}

@Component({
  selector: 'coaches-finished',
  template: `
    <nb-card>
      <nb-card-header>
        דוחות שהסתיימו
      </nb-card-header>
      <nb-card-body>

        <ng2-smart-table [settings]="settings" [source]="source">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./coachs-reports.component.scss']
})
export class CoachReportsFinishedComponent extends CoachsReportsComponent {

  reports: any = [];

  source: LocalDataSource = new LocalDataSource();
  settings = {
    actions: false,
    columns: {
      id: {
        title: 'מספר דו״ח מאמן',
        type: 'number',
      },
      'coach.firstname': {
        title: 'שם המאמן שבדק',
        type: 'string',
        valuePrepareFunction: (cell,row) => {
          return row.coach.firstname;
        },
      },
      'coach.email': {
        title: 'דוא״ל של מאמן שבדק',
        type: 'string',
        valuePrepareFunction: (cell,row) => {
          return row.coach.email;
        },
      },
      'player.firstname': {
        title: 'שם שחקן',
        type: 'string',
        valuePrepareFunction: (cell,row) => {
          return row.player.firstname;
        },
      },
      player_type: {
        title: 'סוג שחקן',
        type: 'number',
        valuePrepareFunction: (value) => {
          console.log(value);
          if (+value === 1) {
            return 'שוער'
          }else {
            return 'שחקן שדה'
          }
        },
      },
      updated_at: {
        title: 'עודכן בתאריך',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      rating: {
        title: 'ציון דו״ח',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value[0].result;
        },
      },
      created_at: {
        title: 'תאריך יצירת הדו״ח',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      due_date: {
        title: 'תאריך להגשה',
        type: 'html',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return '<p class="due-date" style="color: #ff0000!important;">' + new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm') + '</p>';
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
    },
  };

  constructor(public reportsService: ReportsService,
              public router: Router) {
    super(reportsService,router)
  }

  ngOnInit() {
    console.log('coaches-finished');
    this.reportsService.getMy()
      .subscribe(res => {
        let data = res['data'].filter(item => {
          if (item.status === 2 && item.state === 'published') {
            return item
          }
        });
        this.source.load(data);
      })
  }
}

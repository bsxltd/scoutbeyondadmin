import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachsReportsComponent } from './coachs-reports.component';

describe('CoachsReportsComponent', () => {
  let component: CoachsReportsComponent;
  let fixture: ComponentFixture<CoachsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

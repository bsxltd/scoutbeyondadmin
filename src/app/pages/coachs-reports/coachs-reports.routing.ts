import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  CoachNewReportsComponent, CoachReportsFinishedComponent,
  CoachReportsOnCheckComponent, CoachsReportsComponent
} from "./coachs-reports.component";

const routes: Routes = [{
  path: '',
  component: CoachsReportsComponent,
  children: [{
    path: 'coaches-reports',
    component: CoachsReportsComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoachsReportsRouting { }

export const routedComponents = [
  CoachNewReportsComponent,
  CoachReportsOnCheckComponent,
  CoachReportsFinishedComponent,
];

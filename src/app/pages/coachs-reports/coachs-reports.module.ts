import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import {MatButtonModule, MatCardModule, MatDialogModule, MatSliderModule} from '@angular/material';
import {NbBadgeModule} from '@nebular/theme';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {CoachsReportsRouting, routedComponents} from "./coachs-reports.routing";

@NgModule({
  imports: [
    ThemeModule,
    EditorModule,
    ToasterModule.forRoot(),
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    NbBadgeModule,
    MatSliderModule,
    Ng2SmartTableModule,
    CoachsReportsRouting
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
  entryComponents: [
  ],
})

export class CoachsReportsModule { }

import { Component, OnInit } from '@angular/core';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {ContentEditingService} from '../../@core/data/content-editing.service';

@Component({
  selector: 'ngx-content-editing',
  templateUrl: './content-editing.component.html',
  styleUrls: ['./content-editing.component.scss'],
})
export class ContentEditingComponent implements OnInit {
  config: ToasterConfig;
  contentList: any;
  constructor(public toasterService: ToasterService, public contentService: ContentEditingService) {
    this.contentService.get().subscribe(res => {
      this.contentList = res['data'];
    });
  }

  ngOnInit() {
  }

  public showToast (type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }
}

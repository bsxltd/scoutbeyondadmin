import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContentEditingComponent} from './content-editing.component';
import {ContentListComponent} from './content-list/content-list.component';
import {ContentListItemComponent} from './content-list-item/content-list-item.component';
import {ContentLiveComponent} from './content-live/content-live.component';


const routes: Routes = [
  {
    path: 'edit-content',
    component: ContentEditingComponent,
  }, {
    path: 'live-content',
    component: ContentLiveComponent,
  },
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentEditingRoutingModule {
}

export const routedComponents = [
  ContentEditingComponent,
  ContentListComponent,
  ContentListItemComponent,
  ContentLiveComponent,

];

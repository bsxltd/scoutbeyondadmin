import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ContentDialogComponent} from '../content-dialog/content-dialog.component';
import {ContentEditingService} from '../../../@core/data/content-editing.service';
import {ComfirmationPromptComponent} from '../../shared/comfirmation-prompt/comfirmation-prompt.component';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';

@Component({
  selector: 'ngx-content-list-item',
  templateUrl: './content-list-item.component.html',
  styleUrls: ['./content-list-item.component.scss'],
})
export class ContentListItemComponent implements OnInit {
  public image: any;
  config: ToasterConfig;
  @Input() content: any = {};
  @Output() contentDeleted = new EventEmitter<any>();
  constructor(public dialog: MatDialog,
              public contentService: ContentEditingService,
              public toasterService: ToasterService) {

  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ContentDialogComponent, {
      data: {content: this.content, image: null},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.content = result.content;
        this.image = result.image;
        this.updateContent ();
      }
    });
  }

  updateContent () {
    const data = {
      name: this.content.name,
      date: this.content.date,
      time: this.content.time,
      content_id: this.content.id,
    };
    if (this.image)  data['image'] = this.image;
    this.contentService.update(data).subscribe(res => {
      if (res['status'] === 'success') this.showToast(res['status'], null, 'Content edited successfully');
    });
  }
  deleteContent() {
    const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
      data: {content: this.content, state: 'content'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contentService.delete({content_id: this.content.id}).subscribe(res => {
          this.contentDeleted.emit(this.content.id);
          if (res['status'] === 'success') this.showToast(res['status'], null, 'Content removed successfully');
        });
      }
    });

  }
  public showToast (type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }
}

import { Component, OnInit } from '@angular/core';
import {ContentEditingService} from '../../../@core/data/content-editing.service';

@Component({
  selector: 'ngx-content-live',
  templateUrl: './content-live.component.html',
  styleUrls: ['./content-live.component.scss'],
})
export class ContentLiveComponent implements OnInit {
  public contentList: any;
  constructor(public contentService: ContentEditingService) {

    this.contentService.getLiveContent().subscribe(res => {
      this.contentList = res['data'];
    });
  }

  ngOnInit() {
  }

}

import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-content-list',
  templateUrl: './content-list.component.html',
  styleUrls: ['./content-list.component.scss'],
})
export class ContentListComponent implements OnInit {
  @Input() contentList: any = [];
  constructor() { }

  ngOnInit() {
  }

  contentDeleted(id) {
    this.contentList = this.contentList.filter(item => {
        return item.id !== id;
    });
  }
}

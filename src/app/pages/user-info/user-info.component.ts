import {Component, OnInit} from '@angular/core';
import {UserService} from "../../@core/data/users.service";
import {DateModel} from "./date.model";

@Component({
  selector: 'ngx-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  user: any;

  selectedLanguage: any;

  state: string = 'show';

  dateModel: DateModel;

  countries: any = [];

  cities: any = [];

  languages: any = [];
  contact: any = {
    contact_name: '',
    contact_email: '',
    contact_phone: ''
  };

  languageSelect: boolean = false;

  positionsField: any = [];
  positionsPreffered: any = [];

  userId: any;

  constructor(public userService: UserService) {
    this.dateModel = new DateModel();
  }

  ngOnInit() {
    this.getUser();
    this.getLang();
    this.getPositions();
    this.getCities();
    this.getCountries();
  }

  getUser() {
    this.user = this.userService.getUser();
    this.userService.getIndex(this.user.id)
      .subscribe(res => {
        this.user = res['data'];
        // this.user.languages.forEach(item => {
        //   this.languages.forEach(language => {
        //     if (item.id === language.id) {
        //       language.checked = true;
        //     }
        //   })
        // });
        // console.log(this.languages);
        this.getContacts();
        this.parseLanguage();
        this.dateModel.setUser(this.user, true);
      });
  }

  parseLanguage() {
    let temp = this.languages.filter(item => {
      if (item.checked === true) {
        console.log(item);
        return item;
      }
    });
    if (temp.length) {
      this.selectedLanguage = temp[0].name;
    } else {
      this.selectedLanguage = '';
    }
  }

  getPositions() {
    this.userService.getPositions()
      .subscribe(res => {
        res['success'].filter(item => {
          if (item.type === 'preffered') {
            this.positionsField.push(item)
          } else {
            this.positionsPreffered.push(item)
          }
        })
      })
  }

  getCountries() {
    this.userService.getCountry()
      .subscribe(res => {
        this.countries = res['success'];
      })
  }

  changeState(ev) {
    console.log(ev);
    if(ev.target.className === 'select'){
      if (this.languageSelect) {
        this.languageSelect = false;
      } else {
        this.languageSelect = true;
      }
    }

  }

  closeLanguage(ev){
    if (this.languageSelect) {
      this.languageSelect = false;
    } else {
      this.languageSelect = true;
    }
  }

  changeView() {
    if (this.state === 'edit') {
      this.state = 'show';
      this.getUser();
    } else {
      this.state = 'edit';
    }
  }

  getLang() {
    this.userService.getLanguage()
      .subscribe(res => {
        this.languages = res['success'];
        console.log(this.languages);
        this.languages.forEach(item => {
          item.checked = false;
        })
      })
  }

  getCities() {
    this.userService.getCities()
      .subscribe(res => {
        this.cities = res['success'];
      })
  }

  getContacts(){
    this.userService.getContact(this.user.player_id)
      .subscribe((res: any) => {
        for (let prop in this.contact){
          if (res.data[0].hasOwnProperty(prop)){
            this.contact[prop] = res.data[0][prop];
          }
        }
      })
  }

  transformLanguages() {
    let temp = [];
    this.languages.filter(item => {
      if (item.checked) {
        temp.push(item.id);
      }
    });
    return temp;
  }

  save() {
    this.dateModel.setUser(this.user, false);
    this.dateModel.transformToSend();
    this.user = this.dateModel.user;
    this.user.language_id = this.transformLanguages();
    if (this.userService.credentialCheck(this.user)) {
      const userData = this.dateModel.dataUpdate(this.user);
      this.userService.updateUser(userData)
        .subscribe(res => {
          this.updateContacts();
        })
    }
  }

  updateContacts(){
    let updateObj = {};
    Object.assign(updateObj, this.contact);
    for (let prop in updateObj){
      if (this.contact.hasOwnProperty(prop)){
        if (!updateObj[prop]){
          delete updateObj[prop];
        }
      }
    }
    this.userService.updateContact(updateObj, this.user.player_id)
      .subscribe(res => {
        console.log(res);
        this.state = 'show';
        this.getUser();
        this.languageSelect = false;
      })

  }


}

import * as moment from 'moment';
import {ToastrManager} from "ng6-toastr-notifications";

export interface FullDate {
  term: {
    years: any
    months: any,
    contractYears: any,
    contractMonth: any
  }
  birth: {
    year: string
    month: string
    day: string
    days: any
  }
  contract: {
    year: string
    month: string
    day: string
    days: any
  }
}

export class DateModel {
  user: any;
  public date: FullDate = {
    term: {
      years: [],
      months: [],
      contractYears: [],
      contractMonth: []
    },
    birth: {
      year: '',
      month: '',
      day: '',
      days: []
    },
    contract: {
      year: '',
      month: '',
      day: '',
      days: []
    }
  };

  constructor() {

    this._init();
  }

  _init() {
    this.getDateArrays();
    this.getMonth('02', 'birth');
    this.getMonth('02', 'contract');
  }

  setUser(user: any, right: boolean) {
    this.user = user;
    if (right) {
      this.transformDate();
    }
  }

  dataUpdate(user) {
    const userData = {
      user_id: user.id,
      firstname: user.firstname,
      lastname: user.lastname,
      country_id: user.country.id,
      city_id: user.city.id,
      experience_id: user.experience_id,
      strong_leg: user.strong_leg,
      field_position_id: user.field_position.id,
      preffered_position_id: user.preffered_position.id,
      is_active: user.is_active,
      language_id: user.language_id,
      height: user.height,
      weight: user.weight,
      gender_id: user.gender.id,
      intrested_in_foreign: user.intrested_in_foreign,
      has_agent: user.has_agent,
      has_contract: user.has_contract,
      about_me: user.about_me,
      popup: user.popup,
      parent_phone: user.parent_phone
    };
    if (!user.has_contract) {
    } else {
      userData['contract_date'] = user.contract_date;
    }
    if (user.date_of_birth !== '//' && user.date_of_birth !== null) {
      userData['date_of_birth'] = user.date_of_birth;
    }
    return userData
  }

  transformDate() {
    console.log(this.user);
    if (this.user.date_of_birth) {
      let arr = this.user.date_of_birth.split('/');
      this.date.birth.day = arr[0];
      this.date.birth.month = arr[1];
      this.date.birth.year = arr[2];
    }
    if (this.user.contract_date) {
      let arr2 = this.user.contract_date.split('/');
      this.date.contract.day = arr2[0];
      this.date.contract.month = arr2[1];
      this.date.contract.year = arr2[2];
    }
  }

  transformContractDate(ev) {
    if (ev) {
      this.date.contract.day = '';
      this.date.contract.month = '';
      this.date.contract.year = '';
    }
  }

  transformToSend() {
    this.user.date_of_birth = this.date.birth.year + '/' + this.date.birth.month + '/' + this.date.birth.day;
    this.user.contract_date = this.date.contract.year + '/' + this.date.contract.month + '/' + this.date.contract.day
  }

  getMonth(event, state) {
    let currentDay = new Date().getDate();
    console.log(currentDay);
    this.date[state].days = [];
    if (state == 'contract') {
      for (let i = +currentDay; i <= moment(event, "MM").daysInMonth(); i++) {
        if (i < 10) {
          this.date[state].days.push(`0${i}`);
        } else {
          this.date[state].days.push(`${i}`);
        }
      }
    } else {
      for (let i = 1; i <= moment(event, "MM").daysInMonth(); i++) {
        if (i < 10) {
          this.date[state].days.push(`0${i}`);
        } else {
          this.date[state].days.push(`${i}`);
        }
      }
    }

  }

  getDateArrays() {
    let currentYear = new Date().getFullYear();
    let currentMonth = new Date().getMonth();
    for (let i = 1940; i < currentYear; i++) {
      this.date.term.years.push(`${i}`);
    }
    for (let i = currentYear; i < currentYear + 50; i++) {
      this.date.term.contractYears.push(`${i}`);
    }
    for (let i = 1; i <= 12; i++) {
      if (i < 10) {
        this.date.term.months.push(`0${i}`);
      } else {
        this.date.term.months.push(`${i}`);
      }
    }

    for (let i = currentMonth + 1; i <= 12; i++) {
      if (i < 10) {
        this.date.term.contractMonth.push(`0${i}`);
      } else {
        this.date.term.contractMonth.push(`${i}`);
      }
    }
  }

}

import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ToastrManager} from "ng6-toastr-notifications";
import {MatDialog} from "@angular/material";
import {ToasterConfig, ToasterService} from "angular2-toaster";
import {UserService} from "../../@core/data/users.service";
import {LocalDataSource} from "ng2-smart-table";

@Component({
  selector: 'ngx-deleted-users',
  templateUrl: './deleted-users.component.html',
  styleUrls: ['./deleted-users.component.scss']
})
export class DeletedUsersComponent implements OnInit {

  public users: any = [];
  config: ToasterConfig;
  source: LocalDataSource = new LocalDataSource();

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [{
        title: '<i class="fas fa-trash-restore"></i>'
      }],
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      id: {
        title: 'מזהה משתמש',
        type: 'number',
      },
      firstname: {
        title: 'שם פרטי',
        type: 'string',
      },
      lastname: {
        title: 'שם משפחה',
        type: 'string',
      },
      email: {
        title: 'דוא"ל',
        type: 'string',
      },
      created_at: {
        title: 'תאריך הצטרפות',
        type: 'string',
      }
    },
  };

  constructor(private userService: UserService,
              public dialog: MatDialog,
              public toasterService: ToasterService,
              public toastr: ToastrManager,
              public router: Router) { }

  ngOnInit() {
    this.getUsers()
  }

  restore(ev: any){
    this.userService.restoreUser({user_id: ev.data.id})
      .subscribe(res => {
        this.toastr.successToastr('המשתמש שוחזר.');
        this.getUsers();
      })
  }

  getUsers(){
    this.userService.getDeletedUsers()
      .subscribe(res => {
        this.users = res['data'];
        this.source.load(this.users);
      })
  }

}

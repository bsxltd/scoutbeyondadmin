import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ScheduleService} from '../../@core/data/schedule.service';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import {DatePipe} from '@angular/common';
import {LocalDataSource, ViewCell} from 'ng2-smart-table';
import {ComfirmationPromptComponent} from '../shared/comfirmation-prompt/comfirmation-prompt.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'ngx-button-view',
  template: `
    <button class="btn btn-rectangle btn-success btn-md btn-margin"
            [disabled]="rowData.status"
            (click)="onClick()">
      Approve
    </button>
  `,
})
export class ButtonViewComponent implements ViewCell, OnInit {

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  constructor(public dialog: MatDialog) {

  }

  ngOnInit() {
  }

  onClick() {
    const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
      data: {schedule: this.rowData, state: 'schedule'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.save.emit(this.rowData);
      }
    });
  }
}

@Component({
  selector: 'ngx-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss'],
})
export class ScheduleComponent implements OnInit {

  public schedules: any;
  public schedulesCurrWeek = [];
  public schedulesNextWeek = [];
  public multiselect = [];
  public currentWeekBadge = 0;
  public nextWeekBadge = 0;
  state = 'current';
  currentWeekSource: LocalDataSource = new LocalDataSource();
  nextWeekSource: LocalDataSource = new LocalDataSource();

  config: ToasterConfig;
  settings = {
    actions: false,
    selectMode: 'multi',
    columns: {
      consultant: {
        title: 'Consultant',
        type: 'stirng',
        valuePrepareFunction: (consultant) => {
          return consultant.name;
        },
        filterFunction: (item, query) => {
          const namelc = item.name.toLowerCase();
          const querylc = query.toLowerCase();
          return namelc.includes(querylc);
        },
      },
      time_from: {
        title: 'Time from',
        type: 'string',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);

          return new DatePipe('en-EN').transform(raw, 'dd/MM/yyyy - HH:mm');
        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return -1 * direction;
          }
          if (timeA > timeB) {
            return direction;
          }
          return 0;
        },
      },
      time_to: {
        title: 'Time to',
        type: 'string',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);

          return new DatePipe('en-EN').transform(raw, 'dd/MM/yyyy - HH:mm');
        },
      },
      status: {
        title: 'Status',
        type: 'stirng',
        filter: false,
        valuePrepareFunction: (status) => {
          return status ? 'Approved' : 'Not approved';
        },
      },
      action: {
        title: 'Action',
        type: 'custom',
        filter: false,
        renderComponent: ButtonViewComponent,
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(data => {
            this.approve(data.id);
          });
        },
      },
    },
  };

  constructor(public scheduleService: ScheduleService,
              public toasterService: ToasterService,
              public dialog: MatDialog) {
    this.scheduleService.getTwoWeeks().subscribe(res => {
      this.schedules = res['data'];
      this.filterSchedule();

      this.nextWeekSource.load(this.schedulesNextWeek);
      this.currentWeekSource.load(this.schedulesCurrWeek);
    });
  }

  ngOnInit() {

  }

  aproveCheckedConfirmation() {
    if (!this.checkStatus()) {
      this.showToast('warning', null, 'You need to flag only not approved schedules!');
    } else {
      const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
        data: {state: 'schedule-checked'},
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.approveAllChecked();
        }
      });
    }
  }

  approveAllChecked() {
    const data = {
      schedule_id: [],
    };
    data.schedule_id = this.multiselect.map(item => {
      return item.id;
    });
    return this.scheduleService.approve(data).subscribe(res => {
      if (res['status'] === 'success') {
        if (this.state === 'current') {
          this.schedulesCurrWeek = this.schedulesCurrWeek.map(item => {
            this.multiselect.map(date => {
              if (item.id === date.id) {
                item.status = 'Approved';
              }
            });
            return item;
          });
          this.currentWeekSource.load(this.schedulesCurrWeek);
        } else {
          this.schedulesNextWeek = this.schedulesNextWeek.map(item => {
            this.multiselect.map(date => {
              if (item.id === date.id) {
                item.status = 'Approved';
              }
            });
            return item;
          });
          this.nextWeekSource.load(this.schedulesNextWeek);
        }
        this.showToast('success', null, res['message']);
      }
    });
  }

  changeState(state: string) {
    this.state = state;
  }

  filterSchedule() {
    const current_week = [];
    const next_week = [];
    for (const key in this.schedules.next_week) {
      if (this.schedules.next_week.hasOwnProperty(key)) {
        next_week.push(this.schedules.next_week[key]);
      }
    }
    for (const key in this.schedules.current_week) {
      if (this.schedules.current_week.hasOwnProperty(key)) {
        current_week.push(this.schedules.current_week[key]);
      }
    }
    current_week.map(item => {
      item.map(y => {
        this.schedulesCurrWeek.push(y);
      });
      return item;
    });
    next_week.map(item => {
      item.map(y => {
        this.schedulesNextWeek.push(y);
      });
      return item;
    });

    this.currentWeekBadge = this.schedulesCurrWeek.length;
    this.nextWeekBadge = this.schedulesNextWeek.length;
  }

  approve(id: string) {
    const data = {
      schedule_id: [id],
    };

    return this.scheduleService.approve(data).subscribe(res => {
      if (res['status'] === 'success') {
        if (this.state === 'current') {
          this.schedulesCurrWeek = this.schedulesCurrWeek.map(item => {
            if (item.id === id) {
              item.status = 'Approved';
            }
            return item;
          });
          this.currentWeekSource.load(this.schedulesCurrWeek);
        } else {
          this.schedulesNextWeek = this.schedulesNextWeek.map(item => {
            if (item.id === id) {
              item.status = 'Approved';
            }
            return item;
          });
          this.nextWeekSource.load(this.schedulesNextWeek);
        }
        this.showToast('success', null, res['message']);
      }
    });

  }

  rowClicked(event) {
    this.multiselect = event.selected;
    this.checkStatus();
  }

  checkStatus() {
    return this.multiselect.length ? this.multiselect.every(item => {
      return item.status === 0;
    }) : false;
  }

  public showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }
}

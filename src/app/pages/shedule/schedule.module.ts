import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import { routedComponents, ScheduleRoutingModule } from './schedule.routing';
import { MatListModule } from '@angular/material/list';
import {MatButtonModule, MatCardModule, MatDialogModule} from '@angular/material';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ButtonViewComponent} from './schedule.component';
import {MatMenuModule} from '@angular/material/menu';

@NgModule({
  imports: [
    ThemeModule,
    EditorModule,
    ToasterModule.forRoot(),
    ScheduleRoutingModule,
    Ng2SmartTableModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatDialogModule,
  ],
  declarations: [
    ...routedComponents,
    ButtonViewComponent,
  ],
  providers: [
  ],
  entryComponents: [
    ButtonViewComponent,
  ],
})

export class ScheduleModule { }

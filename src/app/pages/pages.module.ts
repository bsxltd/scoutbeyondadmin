import {NgModule} from '@angular/core';


import {PagesComponent} from './pages.component';
import {PagesRoutingModule} from './pages-routing.module';
import {ThemeModule} from '../@theme/theme.module';
import {PageEditingModule} from './page-editing/page-editing.module';
import { ContentEditingModule } from './content-editing/content-editing.module';
import { ComfirmationPromptComponent } from './shared/comfirmation-prompt/comfirmation-prompt.component';
import {MatButtonModule, MatSliderModule} from '@angular/material';
import {NbAuthModule} from '@nebular/auth';
import {ContentDialogComponent} from './content-editing/content-dialog/content-dialog.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {CalendarModule} from 'primeng/primeng';
import {ScheduleModule} from './shedule/schedule.module';
import {PanicLogsModule} from './panic-logs/panic-logs.module';
import {MatMenuModule} from '@angular/material/menu';
import {NbAccordionModule} from '@nebular/theme';
import {ServerListModule} from './server-list/server-list.module';
import {ServerListDialogComponent} from './server-list/server-list-dialog/server-list-dialog.component';
import {CoachesModule} from "./coaches/coaches.module";
import {AddCoachDialogComponent} from "./coaches/add-coach-dialog/add-coach-dialog.component";
import {EditSliderModule} from "./edit-slider/edit-slider.module";
import {AddSlidesComponent} from "./edit-slider/add-slides/add-slides.component";
import { EditTeamAddDialogComponent } from './edit-team/edit-team-add-dialog/edit-team-add-dialog.component';
import {EditTeamModule} from "./edit-team/edit-team.module";
import {ReportsModule} from "./reports/reports.module";
import {ReportsComponent} from "./reports/reports.component";
import { AdminVideoComponent } from './admin-video/admin-video.component';
import {AdminVideoModule} from "./admin-video/admin-video.module";
import { CoachsReportsComponent } from './coachs-reports/coachs-reports.component';
import {CoachsReportsModule} from "./coachs-reports/coachs-reports.module";
import { EditReportComponent } from './edit-report/edit-report.component';
import { UsersComponent } from './users/users.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { DeletedUsersComponent } from './deleted-users/deleted-users.component';
import { DeletedReportsComponent } from './deleted-reports/deleted-reports.component';
import { DeletedCoachesComponent } from './deleted-coaches/deleted-coaches.component';
import { HowItWorkComponent } from './how-it-work/how-it-work.component';
import { StaticTextComponent } from './static-text/static-text.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { EditMailComponent } from './edit-mail/edit-mail.component';
import { ScoutsComponent } from './scouts/scouts.component';
import { DropdownsComponent } from './dropdowns/dropdowns.component';
import { GenderComponent } from './gender/gender.component';
import { ImagesComponent } from './images/images.component';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    PageEditingModule,
    ContentEditingModule,
    MatButtonModule,
    NbAuthModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    CalendarModule,
    ScheduleModule,
    PanicLogsModule,
    ServerListModule,
    MatMenuModule,
    NbAccordionModule,
    MatSliderModule,
    CoachesModule,
    EditSliderModule,
    EditTeamModule,
    ReportsModule,
    AdminVideoModule,
    CoachsReportsModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    ComfirmationPromptComponent,
    ContentDialogComponent,
    AddCoachDialogComponent,
    AddSlidesComponent,
    ServerListDialogComponent,
    EditTeamAddDialogComponent,
    ReportsComponent,
    AdminVideoComponent,
    CoachsReportsComponent,
    EditReportComponent,
    UsersComponent,
    UserInfoComponent,
    DeletedUsersComponent,
    DeletedReportsComponent,
    DeletedCoachesComponent,
    HowItWorkComponent,
    StaticTextComponent,
    ContactUsComponent,
    EditMailComponent,
    ScoutsComponent,
    DropdownsComponent,
    GenderComponent,
    ImagesComponent,
  ],
  entryComponents: [
    ComfirmationPromptComponent,
    ContentDialogComponent,
    AddCoachDialogComponent,
    ServerListDialogComponent,
    AddSlidesComponent,
    EditTeamAddDialogComponent,
    ReportsComponent,
    AdminVideoComponent,
    CoachsReportsComponent,
    UserInfoComponent
  ]
})
export class PagesModule {
}

import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ToastrManager} from "ng6-toastr-notifications";
import {ToasterConfig, ToasterService} from "angular2-toaster";
import {MatDialog} from "@angular/material";
import {UserService} from "../../@core/data/users.service";
import {LocalDataSource} from "ng2-smart-table";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'ngx-deleted-reports',
  templateUrl: './deleted-reports.component.html',
  styleUrls: ['./deleted-reports.component.scss']
})
export class DeletedReportsComponent implements OnInit {

  public reports: any = [];
  config: ToasterConfig;
  source: LocalDataSource = new LocalDataSource();

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          title: '<i class="fas fa-trash-restore"></i>'
        }
      ],
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      id: {
        title: 'מספר דו״ח מאמן',
        type: 'number',
      },
      'player.firstname': {
        title: 'שם שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.firstname;
        },
      },
      player: {
        title: 'מספר מזהה',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.user_id;
        },
      },
      player_type: {
        title: 'סוג שחקן',
        type: 'string',
        valuePrepareFunction: (value) => {
          if (value === '1') {
            return 'שוער'
          } else {
            return 'שחקן שדה'
          }
        },
      },
      'player.email': {
        title: 'אימייל שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.email;
        },
      },
      updated_at: {
        title: 'עודכן בתאריך',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      due_date: {
        title: 'תאריך להגשה',
        type: 'html',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return '<p class="due-date" style="color: #ff0000!important;">' + new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm') + '</p>';
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      created_at: {
        title: 'תאריך יצירת הדו״ח',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      rating: {
        title: 'ציון דו״ח',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value[0].result;
        },
      },
      last_updated: {
        title: 'עודכן בתאריך',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      coach: {
        title: 'שם המאמן שבדק',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.firstname;
        },
      },
    }
  };

  constructor(private userService: UserService,
              public dialog: MatDialog,
              public toasterService: ToasterService,
              public toastr: ToastrManager,
              public router: Router) {
  }

  ngOnInit() {
    this.getReports()
  }

  restore(ev: any){
    console.log(ev);
    this.userService.restoreReport({report_id: ev.data.id})
      .subscribe(res => {
        this.getReports();
        window.location.reload();
      })
  }

  getReports() {
    this.userService.getDeletedReports()
      .subscribe(res => {
        this.reports = res['data'];
        console.log(this.reports);
        this.source.load(this.reports);
      })
  }

}

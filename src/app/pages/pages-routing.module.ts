import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {PagesComponent} from './pages.component';
import {
  AboutUsPageComponent,
  HomePageComponent, LanguagesComponent, MenuComponent, OurTeamPageComponent, PageEditingComponent, PrivacyComponent,
  ProfilePageComponent,
  ReportsPageComponent, SubjectComponent, TermsComponent, TypesComponent
} from './page-editing/page-editing.component';
import {CoachesComponent} from "./coaches/coaches.component";
import {EditSliderComponent} from "./edit-slider/edit-slider.component";
import {EditTeamComponent} from "./edit-team/edit-team.component";
import {
  NewReportsComponent, ReportsComponent, ReportsFinishedComponent,
  ReportsOnCheckComponent
} from "./reports/reports.component";
import {AdminVideoComponent} from "./admin-video/admin-video.component";
import {
  CoachNewReportsComponent, CoachReportsFinishedComponent, CoachReportsOnCheckComponent,
  CoachsReportsComponent
} from "./coachs-reports/coachs-reports.component";
import {EditReportComponent} from "./edit-report/edit-report.component";
import {RoleGuardService} from "../@core/data/role-guard.service";
import {UsersComponent} from "./users/users.component";
import {UserInfoComponent} from "./user-info/user-info.component";
import {DeletedUsersComponent} from "./deleted-users/deleted-users.component";
import {DeletedReportsComponent} from "./deleted-reports/deleted-reports.component";
import {DeletedCoachesComponent} from "./deleted-coaches/deleted-coaches.component";
import {HowItWorkComponent} from "./how-it-work/how-it-work.component";
import {StaticTextComponent} from "./static-text/static-text.component";
import {ContactUsComponent} from "./contact-us/contact-us.component";
import {EditMailComponent} from "./edit-mail/edit-mail.component";
import {ScoutsComponent} from "./scouts/scouts.component";
import {DropdownsComponent} from "./dropdowns/dropdowns.component";
import {GenderComponent} from "./gender/gender.component";
import {ImagesComponent} from "./images/images.component";

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'coaches-reports',
      component: CoachsReportsComponent,
      data: {role: 'coach'},
      children: [{
        path: '',
        redirectTo: 'coach-new-reports',
        pathMatch: 'full',
      }, {
        path: 'coach-new-reports',
        component: CoachNewReportsComponent,
      }, {
        path: 'coach-reports-on-check',
        component: CoachReportsOnCheckComponent,
      },
        {
          path: 'coach-finished-reports',
          component: CoachReportsFinishedComponent,
        }
      ],
    },
    {
      path: 'user-info',
      component: UserInfoComponent,
      data: {role: 'admin'}
    },
    {
      path: 'edit-report',
      component: EditReportComponent,
      data: {role: 'admin'}
    },
    {
      path: 'admin-video',
      canActivate: [RoleGuardService],
      component: AdminVideoComponent,
      data: {role: 'admin'}
    }, {
      path: 'coaches',
      component: CoachesComponent,
      data: {role: 'admin'}
    }, {
      path: 'reports',
      component: ReportsComponent,
      data: {role: 'admin'},
      children: [{
        path: '',
        redirectTo: 'new-reports',
        pathMatch: 'full',
      }, {
        path: 'new-reports',
        component: NewReportsComponent,
      }, {
        path: 'reports-on-check',
        component: ReportsOnCheckComponent,
      },
        {
          path: 'finished-reports',
          component: ReportsFinishedComponent,
        }
      ],
    },
    {
      path: 'users-list',
      component: UsersComponent,
      data: {role: 'admin'}
    },
    {
      path: 'users-list',
      component: UsersComponent,
      data: {role: 'admin'}
    },
    {
      path: 'scouts',
      component: ScoutsComponent,
      data: {role: 'admin'}
    },
    {
      path: 'deleted-users',
      component: DeletedUsersComponent,
      data: {role: 'admin'}
    },
    {
      path: 'deleted-coaches',
      component: DeletedCoachesComponent,
      data: {role: 'admin'}
    }, {
      path: 'deleted-reports',
      component: DeletedReportsComponent,
      data: {role: 'admin'}
    },
    {
      path: 'page-editing',
      component: PageEditingComponent,
      data: {role: 'admin'},
      children: [{
        path: '',
        redirectTo: 'about-us',
        pathMatch: 'full',
      }, {
        path: 'home-page',
        component: HomePageComponent,
      }, {
        path: 'reports-page',
        component: ReportsPageComponent,
      }, {
        path: 'our-team-page',
        component: OurTeamPageComponent,
      }, {
        path: 'profile-page',
        component: ProfilePageComponent,
      },
        {
          path: 'about-us-page',
          component: AboutUsPageComponent,
        },
        {
          path: 'contact-us-page',
          component: ContactUsComponent,
        }
        , {
          path: 'subjects',
          component: SubjectComponent,
        }
        , {
          path: 'menu-edit',
          component: MenuComponent,
        },
        {
          path: 'edit-slider',
          component: EditSliderComponent,
        },
        {
          path: 'edit-team',
          component: EditTeamComponent,
        },
        {
          path: 'terms',
          component: TermsComponent,
        }, {
          path: 'privacy',
          component: PrivacyComponent,
        }, {
          path: 'types',
          component: TypesComponent,
        }, {
          path: 'languages',
          component: LanguagesComponent,
        },
        {
          path: 'how-it-work',
          component: HowItWorkComponent
        },
        {
          path: 'static-text',
          component: StaticTextComponent
        },
        {
          path: 'edit-mail',
          component: EditMailComponent
        },
        {
          path: 'images',
          component: ImagesComponent
        },
        {
          path: 'dropdown',
          component: DropdownsComponent
        },
        {
          path: 'genders',
          component: GenderComponent
        }
      ],
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}

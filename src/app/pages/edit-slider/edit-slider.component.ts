import {Component, OnInit} from '@angular/core';
import {CoachService} from "../../@core/data/coach.service";
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from "angular2-toaster";
import {MatDialog} from "@angular/material";
import {AddCoachDialogComponent} from "../coaches/add-coach-dialog/add-coach-dialog.component";
import {SlidesService} from "../../@core/data/slides.service";
import {AddSlidesComponent} from "./add-slides/add-slides.component";

@Component({
  selector: 'ngx-edit-slider',
  templateUrl: './edit-slider.component.html',
  styleUrls: ['./edit-slider.component.scss']
})
export class EditSliderComponent implements OnInit {
  public slidesList: any = [];
  config: ToasterConfig;

  constructor(private slideService: SlidesService,
              public dialog: MatDialog,
              public toasterService: ToasterService) {
  }

  ngOnInit() {
    this.slideService.getSlides()
      .subscribe(res => {
        this.slidesList = res['data'];
      });

  }

  openCreateMaskDialog() {
    const dialogRef = this.dialog.open(AddSlidesComponent, {
      data: {slide: {}, image: null, state: 'create'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.slide.forEach(item => {
          const data = {
            ...item,
          };
          if (result.image !== '') {
            data.file = result.image
          }
          this.slideService.addSlide(data).subscribe(res => {
            if (res['success']) {
              this.showToast('success', null, 'Slide has been added successfully');
            } else {
              this.showToast('error', null, res['message']);
            }
          });
        });
      }
    });
  }

  public showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

}

import { EditSliderModule } from './edit-slider.module';

describe('EditSliderModule', () => {
  let editSliderModule: EditSliderModule;

  beforeEach(() => {
    editSliderModule = new EditSliderModule();
  });

  it('should create an instance', () => {
    expect(editSliderModule).toBeTruthy();
  });
});

import {AfterViewInit, Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {DomSanitizer} from "@angular/platform-browser";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'ngx-add-slides',
  templateUrl: './add-slides.component.html',
  styleUrls: ['./add-slides.component.scss']
})
export class AddSlidesComponent implements OnInit, AfterViewInit {

  @ViewChild('image') public imageElement: ElementRef;

  public input: any;

  public slide: any = {
    id: '',
    title: '',
    description: '',
    time: 0
  };

  public slideEng: any = {
    id: '',
    title: '',
    description: '',
    time: 0
  };

  public image: string = '';

  title: FormControl;
  description: FormControl;
  time: FormControl;
  titleEng: FormControl;
  descriptionEng: FormControl;
  timeEng: FormControl;
  state: string;

  constructor(
    public sanitize: DomSanitizer,
    public dialogRef: MatDialogRef<AddSlidesComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

    console.log(data);

    this.slide.title = data.slide.title;
    this.slide.description = data.slide.description;
    this.slide.time = data.slide.time;
    this.slide.file = data.image;
    this.slide.id = data.slide.id;
    this.slide.language_id = 2;

    this.slideEng.id = data.slide.idEng;
    this.slideEng.title = data.slide.titleEng;
    this.slideEng.description = data.slide.descriptionEng;
    this.slideEng.time = data.slide.timeEng;
    this.slideEng.file = data.image;
    this.slideEng.language_id = 1;

    this.title = new FormControl(this.slide.title, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.description = new FormControl(this.slide.description, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.time = new FormControl(this.slide.time, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.titleEng = new FormControl(this.slideEng.title, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.descriptionEng = new FormControl(this.slideEng.description, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.timeEng = new FormControl(this.slideEng.time, [
      Validators.required,
      Validators.minLength(1),
    ]);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.input = document.getElementById('input');
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    const imageSrc = this.sanitize.bypassSecurityTrustResourceUrl(URL.createObjectURL(file));
    console.log(file);
    const reader = new FileReader();
    reader.onload = () => this.image = reader.result;
    reader.readAsDataURL(file);
    if (imageSrc) {
      this.slide.file = imageSrc;
      this.slideEng.file = imageSrc;
    }
  }

  save() {
    if (!this.title.invalid && !this.description.invalid && !this.time.invalid &&
      !this.titleEng.invalid && !this.descriptionEng.invalid && !this.timeEng.invalid) {
      this.slide.title = this.title.value;
      this.slide.description = this.description.value;
      this.slide.time = this.time.value;
      this.slideEng.title = this.titleEng.value;
      this.slideEng.description = this.descriptionEng.value;
      this.slideEng.time = this.timeEng.value;
      this.dialogRef.close({slide: [this.slide, this.slideEng], image: this.image});
    }
  }

  close() {
    this.dialogRef.close();
  }
}

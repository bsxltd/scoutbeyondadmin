import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from "angular2-toaster";
import {ComfirmationPromptComponent} from "../../shared/comfirmation-prompt/comfirmation-prompt.component";
import {MatDialog} from "@angular/material";
import {AddSlidesComponent} from "../add-slides/add-slides.component";
import {SlidesService} from "../../../@core/data/slides.service";
import {debug} from "util";

@Component({
  selector: 'ngx-slides-list-item',
  templateUrl: './slides-list-item.component.html',
  styleUrls: ['./slides-list-item.component.scss']
})
export class SlidesListItemComponent implements OnInit {
  public image: any;
  config: ToasterConfig;
  @Input() public slide: any;

  @Output() public slideDeleted = new EventEmitter<any>();

  @Output() public reload: any = new EventEmitter<any>();

  constructor(public dialog: MatDialog,
              public slideService: SlidesService,
              public toasterService: ToasterService) {
  }

  ngOnInit() {
  }


  deleteSlide() {
    const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
      data: {slide: this.slide, state: 'slide'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.slideService.deleteSlide({id: this.slide.id}).subscribe(res => {
          if (res['success']) {
            this.showToast(res['status'], null, 'Slide has been deleted successfully');
            this.slideDeleted.emit(this.slide.id);
          } else {
            this.showToast(res['status'], null, res['message']);
          }
        });
      }
    });
  }

  editSlider() {
    const dialogRef = this.dialog.open(AddSlidesComponent, {
      data: {
        slide: {
          id: this.slide.slider[1].id,
          title: this.slide.slider[1].title,
          description: this.slide.slider[1].description,
          time: this.slide.slider[1].time,
          titleEng: this.slide.slider[0].title,
          descriptionEng: this.slide.slider[0].description,
          timeEng: this.slide.slider[0].time,
          idEng: this.slide.slider[0].id
        },
        image: this.slide.slider[1].file, state: 'create'
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.slide.forEach(item => {
          const data = {
            ...item,
          };
          debugger
          delete data.file;
          if (result.image !== ''){
            data.file = result.image
          }
          this.slideService.updateSlide(data, item.id).subscribe(res => {
            if (res['success']) {
              this.showToast('success', null, res['message']);
              this.reload.emit(res['data']);
            } else {
              this.showToast('error', null, res['message']);
            }
          });
        })
      }
    });
  }

  public showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

}

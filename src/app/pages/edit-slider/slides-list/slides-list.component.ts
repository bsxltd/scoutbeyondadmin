import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-slides-list',
  templateUrl: './slides-list.component.html',
  styleUrls: ['./slides-list.component.scss']
})
export class SlidesListComponent implements OnInit {

  @Input() slidesList: any;

  constructor() { }

  ngOnInit() {
  }

  handleSlideDeleted(id) {
    this.slidesList = this.slidesList.filter(item => {
      return item.id !== id;
    });
  }

  slideChanged(slide){
    this.slidesList.forEach(item =>{
      if(item.id === slide.key_id){
        item.slider.forEach(data => {
          if (data.id === slide.id) {
            data.title = slide.title;
            data.description = slide.description;
            data.time = slide.time;
            data.file = slide.file;
          }
        })
      }
    })
  }

}

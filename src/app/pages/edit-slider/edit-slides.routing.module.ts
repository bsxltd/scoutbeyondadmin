import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EditSliderComponent} from "./edit-slider.component";
import {SlidesListComponent} from "./slides-list/slides-list.component";
import {SlidesListItemComponent} from "./slides-list-item/slides-list-item.component";


const routes: Routes = [
  {
    path: 'edit-slides',
    component: EditSliderComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditSlidesRoutingModule {
}

export const routedComponents = [
  EditSliderComponent,
  SlidesListComponent,
  SlidesListItemComponent,
];

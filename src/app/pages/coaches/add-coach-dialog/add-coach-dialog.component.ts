import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ContentDialogComponent} from "../../content-editing/content-dialog/content-dialog.component";

@Component({
  selector: 'ngx-add-coach-dialog',
  templateUrl: './add-coach-dialog.component.html',
  styleUrls: ['./add-coach-dialog.component.scss']
})
export class AddCoachDialogComponent implements OnInit {
  public user = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
  };
  consultantForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<ContentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    if(data.state !== 'create'){
      this.user.firstname = data.coach.firstname;
      this.user.lastname = data.coach.lastname;
      this.user.email = data.coach.email;
      this.user.password = data.coach.password;
    }
  }

  ngOnInit() {
    this.consultantForm = new FormGroup({
      'firstname': new FormControl(this.user.firstname, [
        Validators.required,
        Validators.minLength(3),
      ]),
      'lastname': new FormControl(this.user.lastname, [
        Validators.required,
        Validators.minLength(3),
      ]),
      'password': new FormControl(this.user.password, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(14),
      ]),
      'email': new FormControl(this.user.email, [
        Validators.required,
        Validators.email,
      ]),
    });
  }

  save() {
    const value = this.consultantForm.value;

    this.user = {
      firstname: value.firstname,
      lastname: value.lastname,
      email: value.email,
      password: value.password,
    };
    if (this.consultantForm.status === 'VALID') {
      this.dialogRef.close(this.user);
    }
  }

  close() {
    this.dialogRef.close();
  }
}

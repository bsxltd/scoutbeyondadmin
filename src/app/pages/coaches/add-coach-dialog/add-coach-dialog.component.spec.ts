import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCoachDialogComponent } from './add-coach-dialog.component';

describe('AddCoachDialogComponent', () => {
  let component: AddCoachDialogComponent;
  let fixture: ComponentFixture<AddCoachDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCoachDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCoachDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

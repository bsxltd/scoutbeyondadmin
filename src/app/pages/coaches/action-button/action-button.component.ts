import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ComfirmationPromptComponent} from '../../shared/comfirmation-prompt/comfirmation-prompt.component';
import {AddCoachDialogComponent} from "../add-coach-dialog/add-coach-dialog.component";
import {BodyOutputType, ToasterConfig} from "angular2-toaster";
import {Toast} from "primeng/toast";

@Component({
  selector: 'ngx-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.scss'],
})
export class CoachesActionButtonComponent implements OnInit {

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  @Output() edited: EventEmitter<any> = new EventEmitter();

  constructor (public dialog: MatDialog) {}

  ngOnInit() {
  }

  removeCoache () {
    const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
      data: {coach: this.rowData, state: 'coach'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const data = {
          coach: this.rowData,
          state: 'coach',
        };
        this.save.emit(data);
      }
    });
  }

  editCoache(){
      const dialogRef = this.dialog.open(AddCoachDialogComponent, {
        data: {coach: this.rowData, state: 'edit'},
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        if (result) {
          const data = {
            coach_id: this.rowData.id,
            firstname: result.firstname,
            lastname: result.lastname,
            email: result.email,
            password: result.password,
          };

          const edit = {
            coach: data,
            state: 'edit',
          };
          this.save.emit(edit);
        }
      });
  }
}

import { CoachesModule } from './coaches.module';

describe('CoachesModule', () => {
  let coachesModule: CoachesModule;

  beforeEach(() => {
    coachesModule = new CoachesModule();
  });

  it('should create an instance', () => {
    expect(coachesModule).toBeTruthy();
  });
});

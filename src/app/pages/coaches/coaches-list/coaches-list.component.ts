import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-coaches-list',
  templateUrl: './coaches-list.component.html',
  styleUrls: ['./coaches-list.component.scss']
})
export class CoachesListComponent implements OnInit {

  @Input() coachesList: any;

  constructor() { }

  ngOnInit() {
  }

  handleCoachDeleted(id) {
    console.log(id);
    this.coachesList = this.coachesList.filter(item => {
      return item.id !== id;
    });
  }

}

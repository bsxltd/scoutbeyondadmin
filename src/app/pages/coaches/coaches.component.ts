import {Component, OnInit} from '@angular/core';
import {CoachService} from "../../@core/data/coach.service";
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from "angular2-toaster";
import {MatDialog} from "@angular/material";
import {AddCoachDialogComponent} from "./add-coach-dialog/add-coach-dialog.component";
import {LocalDataSource} from "ng2-smart-table";
import {DatePipe} from "@angular/common";
import {CoachesActionButtonComponent} from "./action-button/action-button.component";

@Component({
  selector: 'ngx-coaches',
  templateUrl: './coaches.component.html',
  styleUrls: ['./coaches.component.scss']
})
export class CoachesComponent implements OnInit {
  public coaches: any = [];
  config: ToasterConfig;
  source: LocalDataSource = new LocalDataSource();

  settings = {
    actions: false,
    columns: {
      id: {
        title: 'מזהה משתמש',
        type: 'number',
      },
      firstname: {
        title: 'שם פרטי',
        type: 'string',
      },
      lastname: {
        title: 'שם משפחה',
        type: 'string',
      },
      email: {
        title: 'דוא"ל',
        type: 'string',
      },
      created_at: {
        title: 'נוצר ב',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd/MM/yyyy - HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return -1 * direction;
          }
          if (timeA > timeB) {
            return direction;
          }
          return 0;
        },
      },
      action: {
        title: 'פעולות',
        type: 'custom',
        filter: false,
        renderComponent: CoachesActionButtonComponent,
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(data => {
            if (data.state === 'coach') {
              this.removeCoach(data.coach);
            }else {
              this.editConsultant(data.coach)
            }
          });
        },
      },
    },
  };


  constructor(private coachService: CoachService,
              public dialog: MatDialog,
              public toasterService: ToasterService) {

    //
  }

  ngOnInit() {
    this.coachService.getCoaches().subscribe(res => {
      this.coaches = res;
      this.source.load(this.coaches);
    });
  }

   openAddCoachDialog() {
    const dialogRef = this.dialog.open(AddCoachDialogComponent, {
      data: {state: 'create'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        const data = {
          firstname: result.firstname,
          lastname: result.lastname,
          email: result.email,
          password: result.password,
        };

        this.coachService.addCoach(data).subscribe(res => {
          if (res['success']) {
            this.coaches.push(res['data']);
            this.source.load(this.coaches);
            this.showToast(res['status'], null, 'Coach has been added successfully');
          } else {
            this.showToast(res['status'], null, res['message']);
          }
        });
      }
    });
  }

  editConsultant(user): void {
    console.log("user ",user);
    this.coachService.editCoach(user).subscribe(res => {
      this.coachService.getCoaches().subscribe(res => {
        this.coaches = res;
        this.source.load(this.coaches);
      });
    });
  }

  // addConsultant(): void {
  //   const dialogRef = this.dialog.open(ConsultantDialogComponent, {
  //     data: {user: {}, state: 'create'},
  //   });
  //
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.service.createConsultant(result.user).subscribe(res => {
  //         if (res['status'] === 'success') {
  //           this.consultants.push(res['data']);
  //           this.source.load(this.consultants);
  //           this.showToast(res['status'], null, 'Consultant has been created successful');
  //         } else {
  //           this.showToast(res['status'], null, res['message']);
  //         }
  //       });
  //     }
  //   });
  // }
  //
  removeCoach(coach): void {
    this.coachService.deleteCoach({ids: [coach.id]}).subscribe(res => {
      if (res['success']) {
        this.coaches = this.coaches.filter(item => {
          return item.id !== coach.id;
        });
        this.source.load(this.coaches);
        this.showToast(res['status'], null, 'Coache has been removed successful');
      } else {
        this.showToast(res['status'], null, res['message']);
      }
    });
  }

  public showToast(type: string, title: string, body: string): void {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

}

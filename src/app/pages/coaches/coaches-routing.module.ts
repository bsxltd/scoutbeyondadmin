import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CoachesComponent} from "./coaches.component";
import {CoachesListComponent} from "./coaches-list/coaches-list.component";
import {CoachesListItemComponent} from "./coaches-list-item/coaches-list-item.component";
import {CoachesActionButtonComponent} from "./action-button/action-button.component";


const routes: Routes = [
  {
    path: 'coaches',
    component: CoachesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoachesRoutingModule {
}

export const routedComponents = [
  CoachesComponent,
  CoachesListComponent,
  CoachesListItemComponent,
  CoachesActionButtonComponent
];

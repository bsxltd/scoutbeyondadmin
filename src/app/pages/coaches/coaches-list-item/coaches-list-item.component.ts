import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ComfirmationPromptComponent} from "../../shared/comfirmation-prompt/comfirmation-prompt.component";
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from "angular2-toaster";
import {MatDialog} from "@angular/material";
import {CoachService} from "../../../@core/data/coach.service";

@Component({
  selector: 'ngx-coaches-list-item',
  templateUrl: './coaches-list-item.component.html',
  styleUrls: ['./coaches-list-item.component.scss']
})
export class CoachesListItemComponent implements OnInit {
  public image: any;
  config: ToasterConfig;
  @Input() public coach: any;
  @Output() public coachDeleted = new EventEmitter<any>();

  constructor(public dialog: MatDialog,
              private coachService: CoachService,
              public toasterService: ToasterService) { }

  ngOnInit() {
  }


  deleteCoach() {
    const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
      data: {coach: this.coach, state: 'coach'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.coachService.deleteCoach({ids: [this.coach.id]}).subscribe(res => {
          if (res['success']) {
            this.coachDeleted.emit(this.coach.id);
            this.showToast(res['success'], null, res['message']);
            this.coachDeleted.emit(this.coach.id);
          } else {
            this.showToast(res['success'], null, res['message']);
          }
        });
      }
    });
  }

  public showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachesListItemComponent } from './coaches-list-item.component';

describe('CoachesListItemComponent', () => {
  let component: CoachesListItemComponent;
  let fixture: ComponentFixture<CoachesListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachesListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachesListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

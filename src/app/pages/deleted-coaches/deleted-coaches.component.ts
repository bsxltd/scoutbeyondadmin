import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from "angular2-toaster";
import {LocalDataSource} from "ng2-smart-table";
import {Router} from "@angular/router";
import {ToastrManager} from "ng6-toastr-notifications";
import {MatDialog} from "@angular/material";
import {UserService} from "../../@core/data/users.service";

@Component({
  selector: 'ngx-deleted-coaches',
  templateUrl: './deleted-coaches.component.html',
  styleUrls: ['./deleted-coaches.component.scss']
})
export class DeletedCoachesComponent implements OnInit {

  public coaches: any = [];
  config: ToasterConfig;
  source: LocalDataSource = new LocalDataSource();

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [{
        title: '<i class="fas fa-trash-restore"></i>'
      }],
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      id: {
        title: 'מזהה משתמש',
        type: 'number',
      },
      firstname: {
        title: 'שם פרטי',
        type: 'string',
      },
      lastname: {
        title: 'שם משפחה',
        type: 'string',
      },
      email: {
        title: 'דוא"ל',
        type: 'string',
      },
      created_at: {
        title: 'תאריך הצטרפות',
        type: 'string',
      }
    },
  };

  constructor(private userService: UserService,
              public dialog: MatDialog,
              public toasterService: ToasterService,
              public toastr: ToastrManager,
              public router: Router) { }

  ngOnInit() {
    this.getCoaches();
  }

  restore(ev: any){
    this.userService.restoreUser({user_id: ev.data.id})
      .subscribe(res => {
        this.getCoaches();
      })
  }

  getCoaches(){
    this.userService.getDeletedCoaches()
      .subscribe(res => {
        this.coaches = res['data'];
        this.source.load(this.coaches);
      })
  }
}

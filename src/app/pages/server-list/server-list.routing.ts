import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ServerListComponent} from './server-list.component';


const routes: Routes = [{
  path: 'server-list',
  component: ServerListComponent,
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServerListRoutingModule { }

export const routedComponents = [
  ServerListComponent,
];

import {Component, OnInit} from '@angular/core';
import {ToasterService, ToasterConfig, Toast, BodyOutputType} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {ServerListService} from '../../@core/data/server-list.service';
import {MatDialog} from '@angular/material';
import {ServerListDialogComponent} from './server-list-dialog/server-list-dialog.component';

@Component({
  selector: 'ngx-server-list',
  templateUrl: './server-list.component.html',
  styleUrls: ['./server-list.component.scss'],
})
export class ServerListComponent implements OnInit {
  public serverList: any = [];
  public config: ToasterConfig;

  constructor(public serverListService: ServerListService,
              public dialog: MatDialog,
              private toasterService: ToasterService) {
    this.serverListService.getServerList().subscribe(res => {
      this.serverList = res['data'];
    });

  }

  ngOnInit() {

  }

  openDialog(server, state): void {
    const dialogRef = this.dialog.open(ServerListDialogComponent, {
      data: {server: server, state: state},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (state === 'edit') {
          this.updateServer(result.server);

        } else {
          this.createServer(result.server);
        }
      }
    });
  }

  createServer(server) {
    this.serverListService.createServer(server).subscribe(res => {
      if (res['status'] === 'success') {
        this.serverList.push(res['data']);
        this.showToast(res['status'], null, 'Server has been created successfully');
      } else {
        this.showToast(res['status'], null, res['message']);
      }
    });
  }

  updateServer(server) {
    const data = {
      url: server.url,
      server_id: server.id,
      credential: server.credential,
      username: server.username,
    };
    this.serverListService.updateServer(data).subscribe(res => {
      if (res['status'] === 'success') {
        this.serverList = this.serverList.map(item => {
          if (item.id === res['data'].id) {
            item = res['data'];
          }
          return item;
        });
        this.showToast(res['status'], null, 'Server has been updated successfully');
      } else {
        this.showToast(res['status'], null, res['message']);
      }
    });
  }

  public showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

}

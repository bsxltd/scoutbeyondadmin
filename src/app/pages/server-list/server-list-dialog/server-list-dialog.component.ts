import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

interface Server {
  url: string;
  username: string;
  credential: string;
}

@Component({
  selector: 'ngx-server-list-dialog',
  templateUrl: './server-list-dialog.component.html',
  styleUrls: ['./server-list-dialog.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class ServerListDialogComponent implements OnInit {
  public server: Server = {
    url: '',
    username: '',
    credential: '',
  };

  constructor(
    public dialogRef: MatDialogRef<ServerListDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    for (const key in this.data.server) {
      if (this.data.server.hasOwnProperty(key)) {
        this.server[key] = this.data.server[key];
      }
    }
  }

  ngOnInit() {

  }

  save() {
    this.data.server = this.server;
    this.dialogRef.close({server: this.data.server});
  }

  close() {
    this.dialogRef.close();
  }
}

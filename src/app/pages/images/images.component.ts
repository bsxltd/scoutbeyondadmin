import {Component, OnInit} from '@angular/core';
import {PageEditingService} from "../../@core/data/page-editing.service";
import {ToastrManager} from "ng6-toastr-notifications";

@Component({
  selector: 'ngx-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

  images: any = [];
  userUploadedImage: any;

  constructor(private pageService: PageEditingService,
              private toast: ToastrManager) {
  }

  ngOnInit() {
    this.getImages();
  }

  getImages() {
    this.pageService.getAllImages()
      .subscribe((res: any) => {
        if (res.success) {
          this.images = res.data;
        }
      })
  }

  addImage(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => this.userUploadedImage = reader.result;
    reader.readAsDataURL(file);
  }

  updateImg(event: any, image: any, index: number) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      image.img = reader.result;
      this.updateImage(image.id, image.img, index);
    };
    reader.readAsDataURL(file);
  }

  uploadImage() {
    this.pageService.uploadImage({name: this.userUploadedImage})
      .subscribe((res: any) => {
        if (res.success) {
          this.images.push(res.data);
          this.userUploadedImage = null;
        }
      })
  }

  updateImage(id: number, img: string, index: number) {
    this.pageService.updateImage({name: img}, id)
      .subscribe((res: any) => {
        if (res.success) {
          this.images.splice(index, 1, res.data);
        }
      })
  }

  deleteImage(id: number, index: number) {
    this.pageService.deleteImage(id)
      .subscribe((res: any) => {
        console.log(res);
        if (res.success) {
          this.toast.successToastr(res.message);
          this.images.splice(index, 1);
        }
      });
  }

  copyLink(ev, img: string) {
    ev.view.navigator.clipboard.writeText(img).then(() => {
      this.toast.successToastr('Link copied');
    }).catch(err => {
      console.log(err);
    });
  }

}

import { Component, OnInit } from '@angular/core';
import {ContentEditingService} from "../../@core/data/content-editing.service";
import {PageEditingService} from "../../@core/data/page-editing.service";

@Component({
  selector: 'ngx-dropdowns',
  templateUrl: './dropdowns.component.html',
  styleUrls: ['./dropdowns.component.scss']
})
export class DropdownsComponent implements OnInit {

  source: any = [];
  settings = {
    attr: {
      class: 'change-table'
    },
    actions: {
      add: false,
      delete: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-check"></i>',
      cancelButtonContent: '<i class="fas fa-times"></i>'
    },
    columns: {
      key: {
        title: 'key',
        type: 'string',
        editable: false
      },
      heb: {
        title: 'heb',
        type: 'string',
      },
      eng: {
        title: 'eng',
        type: 'string',
      },
    },
  };
  constructor(public contentService: ContentEditingService,
              public  staticTextService: PageEditingService) { }

  ngOnInit() {
    this.getPosition();
  }

  getPosition(){
    this.contentService.getPosition()
      .subscribe(res => {
        this.source = res;
      })
  }

  onEditConfirm(event){
    console.log(event);
    event.confirm.resolve();
    for (let name in event.newData){
      if (event.newData.hasOwnProperty(name)){
        if (event.newData[name] !== event.data[name]){
          this.updateDropdown(event.newData[`${name}_id`], {position: event.newData[name]});
        }
      }
    }
    event.confirm.reject();
  }

  updateDropdown(id, position){
    this.staticTextService.updatePosition(id, position)
      .subscribe(res => {
        console.log(res);
      })
  }

}

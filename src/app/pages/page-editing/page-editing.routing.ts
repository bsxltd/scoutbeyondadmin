import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  AboutUsPageComponent,
  HomePageComponent, LanguagesComponent, MenuComponent, OurTeamPageComponent, PageEditingComponent, PrivacyComponent,
  ProfilePageComponent,
  ReportsPageComponent, SubjectComponent, TermsComponent, TypesComponent,
} from './page-editing.component';

const routes: Routes = [{
  path: '',
  component: PageEditingComponent,
  children: [{
    path: 'page-editing',
    component: PageEditingComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageEditingRoutingModule { }

export const routedComponents = [
  PageEditingComponent,
  HomePageComponent,
  ReportsPageComponent,
  OurTeamPageComponent,
  ProfilePageComponent,
  AboutUsPageComponent,
  MenuComponent,
  SubjectComponent,
  TermsComponent,
  PrivacyComponent,
  TypesComponent,
  LanguagesComponent
];

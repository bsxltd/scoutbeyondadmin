import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import {PageEditingRoutingModule, routedComponents} from './page-editing.routing';
import { EditorModule } from '@tinymce/tinymce-angular';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatFormFieldModule, MatInputModule,
  MatMenuModule, MatSliderModule
} from "@angular/material";
import {Ng2SmartTableModule} from "ng2-smart-table";
import {NbBadgeModule} from "../../../libs/@nebular/theme/components/badge/badge.module";
import {CoachesRoutingModule} from "../coaches/coaches-routing.module";
import {AngularEditorModule} from "@kolkov/angular-editor";

@NgModule({
  imports: [
    ThemeModule,
    PageEditingRoutingModule,
    EditorModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    Ng2SmartTableModule,
    MatMenuModule,
    MatInputModule,
    NbBadgeModule,
    CoachesRoutingModule,
    MatSliderModule,
    AngularEditorModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
})
export class PageEditingModule { }

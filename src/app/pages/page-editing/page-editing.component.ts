import {Component, OnInit} from '@angular/core';
import {PageEditingService} from '../../@core/data/page-editing.service';
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {ToastrManager} from "ng6-toastr-notifications";
import {LocalDataSource} from "ng2-smart-table";

export declare const ACTIVE: string;

@Component({
  selector: 'ngx-page-editing',
  templateUrl: './page-editing.component.html',
  styleUrls: ['./page-editing.component.scss'],
})
export class PageEditingComponent implements OnInit {
  public config: ToasterConfig;
  public tabs: any[] = [
    {
      title: 'דף הבית',
      route: '/pages/page-editing/home-page',
    },
    {
      title: 'דוחות-דף',
      route: '/pages/page-editing/reports-page',
    },
    {
      title: 'הצוות שלנו',
      route: '/pages/page-editing/our-team-page',
    },
    {
      title: 'עמוד פרופיל',
      route: '/pages/page-editing/profile-page',
    },
    {
      title: 'עלינו',
      route: '/pages/page-editing/about-us-page',
    },
  ];


  constructor(public pageEditService: PageEditingService, public toasterService: ToastrManager) {
  }

  ngOnInit() {
  }

}

@Component({
  selector: 'home-page',
  template: `
    <nb-card>
      <nb-card-body>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <p>שחקן</p>
              <input fieldSize="medium" placeholder="title" [(ngModel)]="typesData.field2" type="text" nbInput>
              <textarea [(ngModel)]="typesData.field3"></textarea>
            </div>
            <div class="type-item">
              <p>צופים</p>
              <input fieldSize="medium" placeholder="title" [(ngModel)]="typesData.field5" type="text" nbInput>
              <textarea nbInput [(ngModel)]="typesData.field6"></textarea>
            </div>
            <div class="type-item">
              <p>מאמן</p>
              <input fieldSize="medium" placeholder="title" [(ngModel)]="typesData.field8" type="text" nbInput>
              <textarea nbInput [(ngModel)]="typesData.field9"></textarea>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(typesData)">
              שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <p>שחקן</p>
              <input fieldSize="medium" placeholder="title" [(ngModel)]="typesDataEng.field2" type="text" nbInput>
              <textarea [(ngModel)]="typesDataEng.field3"></textarea>
            </div>
            <div class="type-item">
              <p>צופים</p>
              <input fieldSize="medium" placeholder="title" [(ngModel)]="typesDataEng.field5" type="text" nbInput>
              <textarea nbInput [(ngModel)]="typesDataEng.field6"></textarea>
            </div>
            <div class="type-item">
              <p>מאמן</p>
              <input fieldSize="medium" placeholder="title" [(ngModel)]="typesDataEng.field8" type="text" nbInput>
              <textarea nbInput [(ngModel)]="typesDataEng.field9"></textarea>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(typesDataEng)">
              שמירה
            </button>
          </div>
        </div>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <p>נתיב וידאו</p>
              <input [(ngModel)]="videoData.field1" type="text" nbInput>
              <input placeholder="Link" [(ngModel)]="videoData.field2" type="text" nbInput>
              <textarea nbInput [(ngModel)]="videoData.field3"></textarea>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(videoData)">
              שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <p>נתיב וידאו</p>
              <input [(ngModel)]="videoDataEng.field1" type="text" nbInput>
              <input placeholder="Link" [(ngModel)]="videoDataEng.field2" type="text" nbInput>
              <textarea nbInput [(ngModel)]="videoDataEng.field3"></textarea>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(videoDataEng)">
              שמירה
            </button>
          </div>
        </div>
        <div class="types-block" dir="rtl">
          <div class="type-item">
            <p>מונים</p>
            <input placeholder="" [(ngModel)]="countUsers.field1" type="number" nbInput>
            <input placeholder="" [(ngModel)]="countUsers.field2" type="number" nbInput>
            <input placeholder="" [(ngModel)]="countUsers.field3" type="number" nbInput>
          </div>
          <button class="btn btn-hero-primary" (click)="newText(countUsers)">
            שמירה
          </button>
        </div>

        <div class="types-block" dir="rtl">
          <div class="type-item">
            <p>נתיב קישורים חברתיים</p>
            <input placeholder="Twitter" [(ngModel)]="socialLink.field2" type="text" nbInput>
            <input placeholder="Youtube" [(ngModel)]="socialLink.field4" type="text" nbInput>
            <input placeholder="Facebook" [(ngModel)]="socialLink.field6" type="text" nbInput>
            <input placeholder="Instagram" [(ngModel)]="socialLink.field8" type="text" nbInput>
          </div>
          <button class="btn btn-hero-primary" (click)="newText(socialLink)">
            שמירה
          </button>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class HomePageComponent extends PageEditingComponent {
  config: ToasterConfig;
  typesData: any = {
    dynamic_text_id: 1,
    title: "TYPES/USERS/MAIN",
    field1: "PLAYER",
    field2: "",
    field3: "",
    field4: "SCOUT",
    field5: "",
    field6: "",
    field7: "COACH",
    field8: "",
    field9: "",
  };

  typesDataEng: any = {
    dynamic_text_id: 19,
    title: "TYPES/USERS/MAIN",
    field1: "PLAYER",
    field2: "",
    field3: "",
    field4: "SCOUT",
    field5: "",
    field6: "",
    field7: "COACH",
    field8: "",
    field9: "",
  };

  videoData: any = {
    dynamic_text_id: 18,
    title: "MAIN/VIDEO",
    field1: "",
    field2: "",
    field3: "",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net",
  };

  videoDataEng: any = {
    dynamic_text_id: 20,
    title: "MAIN/VIDEO",
    field1: "",
    field2: "",
    field3: "",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net",
  };

  countUsers: any = {
    dynamic_text_id: 10,
    title: "HOME/COUNTS",
    field1: null,
    field2: null,
    field3: null,
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "some",
  };

  countUsersEng: any = {
    dynamic_text_id: 33,
    title: "HOME/COUNTS",
    field1: null,
    field2: null,
    field3: null,
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "some",
  };

  socialLink: any = {
    dynamic_text_id: 3,
    title: "LINK/SOCIAL/MAIN",
    field1: "TWITTER",
    field2: "",
    field3: "YOUTUBE",
    field4: "",
    field5: "FACEBOOK",
    field6: "",
    field7: "INSTAGRAM",
    field8: "",
    field9: "some",
  };

  socialLinkEng: any = {
    dynamic_text_id: 32,
    title: "LINK/SOCIAL/MAIN",
    field1: "TWITTER",
    field2: "",
    field3: "YOUTUBE",
    field4: "",
    field5: "FACEBOOK",
    field6: "",
    field7: "INSTAGRAM",
    field8: "",
    field9: "some",
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getText();
  }

  getText() {
    this.pageEditService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          if (item.id === 1) {
            this.typesData.field2 = item.field2;
            this.typesData.field3 = item.field3;
            this.typesData.field5 = item.field5;
            this.typesData.field6 = item.field6;
            this.typesData.field8 = item.field8;
            this.typesData.field9 = item.field9;
          } else if (item.id === 18) {
            this.videoData.field1 = item.field1;
            this.videoData.field2 = item.field2;
            this.videoData.field3 = item.field3;
          } else if (item.id === 3) {
            this.socialLink.field2 = item.field2;
            this.socialLink.field4 = item.field4;
            this.socialLink.field6 = item.field6;
            this.socialLink.field8 = item.field8;
          } else if (item.id === 10) {
            this.countUsers.field1 = +item.field1;
            this.countUsers.field2 = +item.field2;
            this.countUsers.field3 = +item.field3;
          } else if (item.id === 19) {
            this.typesDataEng.field2 = item.field2;
            this.typesDataEng.field3 = item.field3;
            this.typesDataEng.field5 = item.field5;
            this.typesDataEng.field6 = item.field6;
            this.typesDataEng.field8 = item.field8;
            this.typesDataEng.field9 = item.field9;
          } else if (item.id === 20) {
            this.videoDataEng.field1 = item.field1;
            this.videoDataEng.field2 = item.field2;
            this.videoDataEng.field3 = item.field3;
          }
        })
      })
  }

  createYouTubeEmbedLink(link) {
    console.log(link);
    return link;
  }

  newText(data) {
    if (data.title === 'MAIN/VIDEO') {
      data.field2 = this.createYouTubeEmbedLink(data.field2);
    }
    if (data.title === 'HOME/COUNTS') {
      this.countUsersEng.field1 = data.field1;
      this.countUsersEng.field2 = data.field2;
      this.countUsersEng.field3 = data.field3;
      this.updateText(this.countUsersEng);
    }
    if (data.title === 'LINK/SOCIAL/MAIN'){
      this.socialLinkEng.field2 = data.field2;
      this.socialLinkEng.field4 = data.field4;
      this.socialLinkEng.field6 = data.field6;
      this.socialLinkEng.field8 = data.field8;
      this.updateText(this.socialLinkEng);
    }
    this.updateText(data);
  }

  updateText(data){
    this.pageEditingService.updateText(data)
      .subscribe(res => {
        if (res['success']) {
          this.toasterService.successToastr('Text has been updated');
        } else {
          this.toasterService.errorToastr('Invalid data');
        }
      }, err => {
        this.toasterService.errorToastr(err['message']);
      })
  }
}

@Component({
  selector: 'reports-page',
  template: `
    <nb-card>
      <nb-card-body>
        <!--<p>שלב 1</p>-->
        <!--<div class="flex-wrapper">-->
          <!--<div class="types-block" dir="rtl">-->
            <!--<div class="type-item">-->
              <!--<input placeholder="כותרת" [(ngModel)]="stepOne.field3" type="text" nbInput>-->
              <!--<input placeholder="כתוביות" [(ngModel)]="stepOne.field4" type="text" nbInput>-->
              <!--<textarea nbInput placeholder="תיאור" [(ngModel)]="stepOne.field5"></textarea>-->
              <!--<input placeholder="סוג שחקן 1" [(ngModel)]="stepOne.field6" type="text" nbInput>-->
              <!--<input placeholder="סוג שחקן 2" [(ngModel)]="stepOne.field7" type="text" nbInput>-->
              <!--<input placeholder="אשר טקסט" [(ngModel)]="stepOne.field9" type="text" nbInput>-->
            <!--</div>-->
            <!--<button class="btn btn-hero-primary" (click)="newText(stepOne)">-->
              <!--שמירה-->

            <!--</button>-->
          <!--</div>-->
          <!--<div class="types-block" dir="ltr">-->
            <!--<div class="type-item">-->
              <!--<input placeholder="כותרת" [(ngModel)]="stepOneEng.field3" type="text" nbInput>-->
              <!--<input placeholder="כתוביות" [(ngModel)]="stepOneEng.field4" type="text" nbInput>-->
              <!--<textarea nbInput placeholder="תיאור" [(ngModel)]="stepOneEng.field5"></textarea>-->
              <!--<input placeholder="סוג שחקן 1" [(ngModel)]="stepOneEng.field6" type="text" nbInput>-->
              <!--<input placeholder="סוג שחקן 2" [(ngModel)]="stepOneEng.field7" type="text" nbInput>-->
              <!--<input placeholder="אשר טקסט" [(ngModel)]="stepOneEng.field9" type="text" nbInput>-->
            <!--</div>-->
            <!--<button class="btn btn-hero-primary" (click)="newText(stepOneEng)">-->
              <!--שמירה-->

            <!--</button>-->
          <!--</div>-->
        <!--</div>-->
        <p>שלב 3</p>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <input placeholder="כותרת" [(ngModel)]="stepThird.field2" type="text" nbInput>
              <input placeholder="כתוביות" [(ngModel)]="stepThird.field3" type="text" nbInput>
              <textarea nbInput placeholder="תיאור" [(ngModel)]="stepThird.field4"></textarea>
              <input placeholder="ספירת התשלום" [(ngModel)]="stepThird.field6" type="text" nbInput>
              <input placeholder="דווח על זמן האימות" [(ngModel)]="stepThird.field7"
                     type="text" nbInput>
              <input placeholder="אשר טקסט" [(ngModel)]="stepThird.field8" type="text" nbInput>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(stepThird)">
              שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <input placeholder="כותרת" [(ngModel)]="stepThirdEng.field2" type="text" nbInput>
              <input placeholder="כתוביות" [(ngModel)]="stepThirdEng.field3" type="text" nbInput>
              <textarea nbInput placeholder="תיאור" [(ngModel)]="stepThirdEng.field4"></textarea>
              <input placeholder="ספירת התשלום" [(ngModel)]="stepThirdEng.field6" type="text" nbInput>
              <input placeholder="דווח על זמן האימות" [(ngModel)]="stepThirdEng.field7"
                     type="text" nbInput>
              <input placeholder="אשר טקסט" [(ngModel)]="stepThirdEng.field8" type="text" nbInput>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(stepThirdEng)">
              שמירה
            </button>
          </div>
        </div>
        <p>ערוך קישור YouTube וטקסט של מודעת באנר</p>
        <div class="types-block" dir="ltr">
          <div class="type-item">
            <input placeholder="youtube link" [(ngModel)]="reportYoutube.field1" type="text"
                   nbInput>
          </div>
          <button class="btn btn-hero-primary"
                  (click)="newText(reportYoutube)">שמירה
          </button>
        </div>
        <p>ערוך מחיר</p>
        <div class="types-block" dir="rtl" style="display: flex;
    flex-direction: row; width: 450px">
          <div class="type-item" style=" display: flex;
      width: 300px;
      flex-direction: column;">
            <input placeholder="כותרת באנר"
                   maxlength="900000"
                   max="900000"
                   min="1" [(ngModel)]="price"
                   type="number" nbInput>
          </div>
          <button class="btn btn-hero-primary" (click)="updatePrice()">שמירה
          </button>
        </div>

        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <input placeholder="כותרת" [(ngModel)]="reportPopup.field1" type="text" nbInput>
              <textarea nbInput placeholder="תיאור" [(ngModel)]="reportPopup.field2"></textarea>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(reportPopup)">
              שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <input placeholder="כותרת" [(ngModel)]="reportPopupEng.field1" type="text" nbInput>
              <textarea nbInput placeholder="תיאור" [(ngModel)]="reportPopupEng.field2"></textarea>
            </div>
            <button class="btn btn-hero-primary" (click)="newText(reportPopupEng)">
              שמירה
            </button>
          </div>
        </div>

        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <img [src]="banner.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="banner.title" type="text"
                     nbInput>
              <input type="file" (change)="bannerChange($event)">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.heb, banner)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <img [src]="bannerEng.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="bannerEng.title" type="text"
                     nbInput>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.eng, bannerEng)">שמירה
            </button>
          </div>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class ReportsPageComponent extends PageEditingComponent {

  // stepOne: any = {
  //   dynamic_text_id: 6,
  //   title: "REPORT/INFOS",
  //   field1: "net",
  //   field2: "net",
  //   field3: "",
  //   field4: "",
  //   field5: "",
  //   field6: "",
  //   field7: "",
  //   field8: "net",
  //   field9: "",
  // };

  stepOneEng: any = {
    dynamic_text_id: 21,
    title: "REPORT/INFOS",
    field1: "net",
    field2: "net",
    field3: "",
    field4: "",
    field5: "",
    field6: "",
    field7: "",
    field8: "net",
    field9: "",
  };

  stepThird: any = {
    dynamic_text_id: 7,
    title: "REPORT/PAYMENT",
    field1: "",
    field2: "",
    field3: "",
    field4: "",
    field5: "300",
    field6: "",
    field7: "",
    field8: "",
    field9: "some",
  };

  stepThirdEng: any = {
    dynamic_text_id: 22,
    title: "REPORT/PAYMENT",
    field1: "",
    field2: "",
    field3: "",
    field4: "",
    field5: "300",
    field6: "",
    field7: "",
    field8: "",
    field9: "some",
  };

  reportYoutube: any = {
    dynamic_text_id: 9,
    title: "REPORTS/BANNER/YOUTUBE",
    field1: "",
    field2: "net",
    field3: "net",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net",

  };
  reportYoutubeEng: any = {
    dynamic_text_id: 34,
    title: "REPORTS/BANNER/YOUTUBE",
    field1: "",
    field2: "net",
    field3: "net",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net",

  };

  banner: any = {
    title: "",
    photo: "",
    name: "Reports",
    language_id: '2'
  };

  bannerEng: any = {
    title: "",
    photo: "",
    name: "Reports",
    language_id: '1'
  };

  bannersId: any = {
    eng: '',
    heb: ''
  };

  reportPopup: any = {
    dynamic_text_id: 13,
    title: "REPORTS_PAYMENT/POPUP",
    field1: "",
    field2: "",
    field3: "net",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net",
  };

  reportPopupEng: any = {
    dynamic_text_id: 23,
    title: "REPORTS_PAYMENT/POPUP",
    field1: "",
    field2: "",
    field3: "net",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net",
  };
  price: number = 0;

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getText();
    this.getPrice();
    this.getBanners();
  }

  getText() {
    this.pageEditService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          /*if (item.id === 6) {
            this.stepOne.field3 = item.field3;
            this.stepOne.field4 = item.field4;
            this.stepOne.field5 = item.field5;
            this.stepOne.field6 = item.field6;
            this.stepOne.field7 = item.field7;
            this.stepOne.field9 = item.field9;
          } else*/ if (item.id === 7) {
            this.stepThird.field1 = item.field1;
            this.stepThird.field2 = item.field2;
            this.stepThird.field3 = item.field3;
            this.stepThird.field4 = item.field4;
            this.stepThird.field6 = item.field6;
            this.stepThird.field7 = item.field7;
            this.stepThird.field8 = item.field8;
          } else if (item.id === 9) {
            this.reportYoutube.field1 = item.field1;
          } else if (item.id === 13) {
            this.reportPopup.field1 = item.field1;
            this.reportPopup.field2 = item.field2;
          } /*else if (item.id === 21) {
            this.stepOneEng.field3 = item.field3;
            this.stepOneEng.field4 = item.field4;
            this.stepOneEng.field5 = item.field5;
            this.stepOneEng.field6 = item.field6;
            this.stepOneEng.field7 = item.field7;
            this.stepOneEng.field9 = item.field9;
          }*/ else if (item.id === 22) {
            this.stepThirdEng.field1 = item.field1;
            this.stepThirdEng.field2 = item.field2;
            this.stepThirdEng.field3 = item.field3;
            this.stepThirdEng.field4 = item.field4;
            this.stepThirdEng.field6 = item.field6;
            this.stepThirdEng.field7 = item.field7;
            this.stepThirdEng.field8 = item.field8;
          } else if (item.id === 23) {
            this.reportPopupEng.field1 = item.field1;
            this.reportPopupEng.field2 = item.field2;
          }
        })
      })
  }

  createYouTubeEmbedLink(link) {
    console.log(link);
    return link;
  }

  getPrice() {
    this.pageEditingService.getPrice()
      .subscribe(res => {
        this.price = res['success'][0]['total'];
      })
  }

  updateBanner(id, banner) {
    let obj = {};
    Object.assign(obj, banner);
    if (banner.photo.startsWith('https://')){
      delete obj['photo'];
    }
    this.pageEditService.updateBanners(id, obj)
      .subscribe(res => {
        this.toasterService.successToastr(res['message']);
      })
  }

  bannerChange(event): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = () => {
        this.banner.photo = reader.result;
        this.bannerEng.photo = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  getBanners() {
    this.pageEditService.getBanners()
      .subscribe(res => {
        res['success'].forEach(item => {
          if (item.name === 'Reports') {
            item.language_id === 2 ? this.banner.title = item.title : this.bannerEng.title = item.title;
            item.language_id === 2 ? this.banner.photo = item.photo : this.bannerEng.photo = item.photo;
            item.language_id === 2 ? this.bannersId.heb = item.id : this.bannersId.eng = item.id;
          }
        })
      })
  }

  updatePrice() {
    this.pageEditService.updatePrice({id: 1, currency: 'UAH', total: this.price})
      .subscribe(res => {
      })
  }

  newText(data) {
    if (data.title === 'REPORTS/BANNER/YOUTUBE') {
      data.field1 = this.createYouTubeEmbedLink(data.field1) + "?autoplay=1";
    }
    if (data.title === 'REPORTS/BANNER/YOUTUBE') {
      this.reportYoutubeEng.field1 = data.field1;
      this.updateText(this.reportYoutubeEng);
    }
    this.updateText(data);
  }

  updateText(data){
    this.pageEditingService.updateText(data)
      .subscribe(res => {
        if (res['success']) {
          this.toasterService.successToastr('Text has been updated');
        } else {
          this.toasterService.errorToastr('Invalid data');
        }
      }, err => {
        this.toasterService.errorToastr(err['message']);
      })
  }

}

@Component({
  selector: 'ngx-tab3',
  template: `
    <nb-card>
      <nb-card-body>

        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <input placeholder="כותרת באנר" [(ngModel)]="ourTeamData.field1" type="text" nbInput>
              <input placeholder="שם סעיף 1" [(ngModel)]="ourTeamData.field2" type="text"
                     nbInput>
              <input placeholder="שם סעיף 2" [(ngModel)]="ourTeamData.field3" type="text"
                     nbInput>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(ourTeamData)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <input placeholder="כותרת באנר" [(ngModel)]="ourTeamDataEng.field1" type="text" nbInput>
              <input placeholder="שם סעיף 1" [(ngModel)]="ourTeamDataEng.field2" type="text"
                     nbInput>
              <input placeholder="שם סעיף 2" [(ngModel)]="ourTeamDataEng.field3" type="text"
                     nbInput>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(ourTeamDataEng)">שמירה
            </button>
          </div>
        </div>

        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <img [src]="banner.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="banner.title" type="text"
                     nbInput>
              <input type="file" (change)="readURL($event)">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.heb, banner)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <img [src]="bannerEng.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="bannerEng.title" type="text"
                     nbInput>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.eng, bannerEng)">שמירה
            </button>
          </div>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class OurTeamPageComponent extends PageEditingComponent {

  ourTeamData: any = {
    dynamic_text_id: 5,
    title: "BANNER/OURTEAM",
    field1: "",
    field2: "",
    field3: "",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net"
  };

  ourTeamDataEng: any = {
    dynamic_text_id: 24,
    title: "BANNER/OURTEAM",
    field1: "",
    field2: "",
    field3: "",
    field4: "net",
    field5: "net",
    field6: "net",
    field7: "net",
    field8: "net",
    field9: "net"
  };

  banner: any = {
    title: "",
    photo: "",
    name: "OurTeam",
    language_id: '2'
  };

  bannerEng: any = {
    title: "",
    photo: "",
    name: "OurTeam",
    language_id: '1'
  };

  bannersId: any = {
    eng: '',
    heb: ''
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getText();
    this.getBanners();
  }

  getText() {
    this.pageEditService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          if (item.id === 5) {
            this.ourTeamData.field1 = item.field1;
            this.ourTeamData.field2 = item.field2;
            this.ourTeamData.field3 = item.field3;
          } else if (item.id === 24) {
            this.ourTeamDataEng.field1 = item.field1;
            this.ourTeamDataEng.field2 = item.field2;
            this.ourTeamDataEng.field3 = item.field3;
          }
        })
      })
  }

  updateBanner(id, banner) {
    console.log(banner);
    if (banner.photo.startsWith('https://')) {
      const obj = {};
      Object.assign(obj, banner);
      let image = banner.photo.match(/(\/image\w+\W+\w+)/)[0].split('/');
      obj['photo'] = image[1];
      console.log(obj);
      this.pageEditService.updateBanners(id, obj)
        .subscribe(res => {
          this.toasterService.successToastr(res['message']);
        })
    } else {
      this.pageEditService.updateBanners(id, banner)
        .subscribe(res => {
          this.toasterService.successToastr(res['message']);
        })
    }
  }

  readURL(event): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = () => {
        this.banner.photo = reader.result;
        this.bannerEng.photo = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  getBanners() {
    this.pageEditService.getBanners()
      .subscribe(res => {
        res['success'].forEach(item => {
          if (item.name === 'OurTeam') {
            item.language_id === 2 ? this.banner.title = item.title : this.bannerEng.title = item.title;
            item.language_id === 2 ? this.banner.photo = item.photo : this.bannerEng.photo = item.photo;
            item.language_id === 2 ? this.bannersId.heb = item.id : this.bannersId.eng = item.id;
          }
        })
      })
  }

  newText(data) {
    this.pageEditingService.updateText(data)
      .subscribe(res => {
        if (res['success']) {
          this.toasterService.successToastr('Text has been updated');
        } else {
          this.toasterService.errorToastr('Invalid data');
        }
      }, err => {
        this.toasterService.errorToastr(err['message']);
      })
  }

}

@Component({
  selector: 'ngx-tab4',
  template: `
    <nb-card>
      <nb-card-body>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="profileData.field2">
              <textarea nbInput placeholder="טקסט קופץ" [(ngModel)]="profileData.field1"></textarea>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(profileData)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="profileDataEng.field2">
              <textarea nbInput placeholder="טקסט קופץ" [(ngModel)]="profileDataEng.field1"></textarea>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(profileDataEng)">שמירה
            </button>
          </div>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class ProfilePageComponent extends PageEditingComponent {

  profileData: any = {
    dynamic_text_id: 4,
    title: "POPUP/PROFILE/TEXT",
    field1: "",
    field2: "",
    field3: "field",
    field4: "field",
    field5: "field",
    field6: "field",
    field7: "field",
    field8: "field",
    field9: "field",
  };

  profileDataEng: any = {
    dynamic_text_id: 25,
    title: "POPUP/PROFILE/TEXT",
    field1: "",
    field2: "",
    field3: "field",
    field4: "field",
    field5: "field",
    field6: "field",
    field7: "field",
    field8: "field",
    field9: "field",
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getText();
  }

  getText() {
    this.pageEditService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          if (item.id === 4) {
            this.profileData.field1 = item.field1;
            this.profileData.field2 = item.field2;
          } else if (item.id === 25) {
            this.profileDataEng.field1 = item.field1;
            this.profileDataEng.field2 = item.field2;
          }
        })
      })
  }

  newText(data) {
    this.pageEditingService.updateText(data)
      .subscribe(res => {
        if (res['success']) {
          this.toasterService.successToastr('Text has been updated');
        } else {
          this.toasterService.errorToastr('Invalid data');
        }
      }, err => {
        this.toasterService.errorToastr(err['message']);
      })
  }

}

@Component({
  selector: 'ngx-tab5',
  template: `
    <nb-card>
      <nb-card-body dir="rtl">
        <div *ngFor="let item of aboutUsText">
          <div class="flex-wrapper">
            <div class="types-block" dir="rtl">
              <div class="type-item">
                <input placeholder="כותרת" [(ngModel)]="item.about_us[1].title" type="text" nbInput>
                <input placeholder="כותרת" [(ngModel)]="item.about_us[1].subtitle" type="text" nbInput>
                <textarea nbInput placeholder="תיאור" [(ngModel)]="item.about_us[1].description"></textarea>
                <img [src]="item.about_us[1].photo">
                <input type="file" (change)="changePhoto($event, item)">
              </div>
              <button class="btn btn-hero-primary" (click)="updateText(item.about_us[1])">
                שמירה
              </button>
            </div>
            <div class="types-block" dir="ltr">
              <div class="type-item">
                <input placeholder="כותרת" type="text" [(ngModel)]="item.about_us[0].title" nbInput>
                <input placeholder="כותרת" type="text" [(ngModel)]="item.about_us[0].subtitle" nbInput>
                <textarea nbInput placeholder="תיאור" [(ngModel)]="item.about_us[0].description"></textarea>
                <img [src]="item.about_us[0].photo">
              </div>
              <button class="btn btn-hero-primary" (click)="updateText(item.about_us[0])">
                שמירה
              </button>
            </div>
          </div>
        </div>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <img [src]="banner.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="banner.title" type="text"
                     nbInput>
              <input type="file" (change)="changePhoto($event)">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.heb, banner)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <img [src]="bannerEng.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="bannerEng.title" type="text"
                     nbInput>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.eng, bannerEng)">שמירה
            </button>
          </div>
        </div>
      </nb-card-body>
    </nb-card>

  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class AboutUsPageComponent extends PageEditingComponent {

  aboutUsText: any = [];
  banner: any = {
    title: "",
    photo: "",
    name: "about_us",
    language_id: '2'
  };

  bannerEng: any = {
    title: "",
    photo: "",
    name: "about_us",
    language_id: '1'
  };

  bannersId: any = {
    eng: '',
    heb: ''
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getBanners();
    this.getText();
  }


  getText() {
    this.pageEditingService.getAboutUs()
      .subscribe(res => {
        this.aboutUsText = res['data'];
      });
  }

  updateText(data) {
    console.log(data);
    let obj = {};
    Object.assign(obj, data);
    if (data.photo.startsWith('https://')){
      delete obj['photo'];
    }
    this.pageEditingService.updateAboutUs(data.id, obj)
      .subscribe(() => {
        this.toasterService.successToastr('Successfuly updated');
      }, () => {
        this.toasterService.errorToastr('Error updating')
      })
  }

  updateBanner(id, banner) {
    let obj = {};
    Object.assign(obj, banner);
    if (banner.photo.startsWith('https://')){
      delete obj['photo'];
    }
    this.pageEditingService.updateBanners(id, obj)
      .subscribe(res => {
        this.toasterService.successToastr(res['message']);
      })
  }

  changePhoto(event, item) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = () => {
        if (item) {
          item.about_us[1].photo = reader.result;
          item.about_us[0].photo = reader.result;
        } else {
          this.banner.photo = reader.result;
          this.bannerEng.photo = reader.result;
        }
      };
      reader.readAsDataURL(file);
    }
  }

  getBanners() {
    this.pageEditingService.getBanners()
      .subscribe(res => {
        res['success'].forEach(item => {
          if (item.name === 'about_us') {
            item.language_id === 2 ? this.banner.title = item.title : this.bannerEng.title = item.title;
            item.language_id === 2 ? this.banner.photo = item.photo : this.bannerEng.photo = item.photo;
            item.language_id === 2 ? this.bannersId.heb = item.id : this.bannersId.eng = item.id;
          }
        })
      })
  }
}

@Component({
  selector: 'ngx-tab6',
  template: `
    <nb-card>
      <!--<nb-card-header class="menu-header">-->
      <!--<input class="menu-input" nbInput type="text" [(ngModel)]="menuItem.name">-->
      <!--<input class="menu-input" nbInput type="text" [(ngModel)]="menuItem.url">-->
      <!--<input class="menu-input" placeholder="מיקום: למעלה / למטה" nbInput type="text" [(ngModel)]="menuItem.location">-->
      <!--<button nbButton (click)="createMenu()">ליצור</button>-->
      <!--</nb-card-header>-->
      <nb-card-body dir="rtl">
        <ng2-smart-table [settings]="settings" [source]="source" (deleteConfirm)="onDeleteConfirm($event)"
                         (editConfirm)="onEditConfirm($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class MenuComponent extends PageEditingComponent {

  config: ToasterConfig;
  source: any;

  menuItem: any = {
    name: '',
    url: '',
    location: '',
  };

  settings = {
    attr: {
      class: 'change-table'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-check"></i>',
      cancelButtonContent: '<i class="fas fa-times"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="far fa-trash-alt"></i>',
      confirmDelete: true,
      name: 'פעולות'
    },
    actions: {
      add: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      key: {
        title: 'מזהה',
        type: 'number',
        editable: false
      },
      heb: {
        title: 'heb',
        type: 'string',
      },
      eng: {
        title: 'eng',
        type: 'string',
      },
      location: {
        title: 'מקום',
        type: 'string',
        editor: {
          type: 'list',
          config: {
            list: [{value: 'top', title: 'top'}, {value: 'down', title: 'down'}]
          }
        }
      }
    },
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getMenu()
  }

  getMenu() {
    this.pageEditingService.getMenu()
      .subscribe(res => {
        this.source = res;
      })
  }

  // createMenu() {
  //   this.pageEditingService.createMenu({
  //     name: this.menuItem.name,
  //     url: this.menuItem.url,
  //     location: this.menuItem.location
  //   }).subscribe(res => {
  //     this.getMenu();
  //     this.menuItem.name = '';
  //     this.menuItem.url = '';
  //     this.menuItem.location = '';
  //   })
  // }

  onEditConfirm(ev) {
    ev.confirm.resolve();
    console.log(ev);
    for (let prop in ev.newData) {
      if (ev.newData.hasOwnProperty(prop)) {
        let update = {name: '', location: ''};
        let id = '';
        if (prop === 'eng' || prop === 'heb') {
          if (ev.newData[prop] !== ev.data[prop]) {
            update.name = ev.newData[prop];
            id = ev.newData[`${prop}_id`];
          }
        }
        if (prop === 'location' && ev.newData[prop] !== ev.data[prop]) {
          update.location = ev.newData[prop];
          if (update.name === '') {
            this.updateMenu(ev.newData.eng_id, {name: ev.newData.eng, location: update.location});
            this.updateMenu(ev.newData.heb_id, {name: ev.newData.heb, location: update.location});
          }
        }
        if (update.name !== '' && id !== '') {
          this.updateMenu(id, {name: update.name, location: ev.newData.location});
        }
      }
    }
    ev.confirm.reject();
  }

  onDeleteConfirm(event) {

    if (window.confirm('אתה בטוח שאתה רוצה למחוק?\n')) {
      event.confirm.resolve();
      this.menuItem.id = event.data.id;
      this.pageEditingService.deleteMenu(this.menuItem.id)
        .subscribe(res => {
          console.log(res);
        })
    } else {
      event.confirm.reject();
    }
  }

  updateMenu(id, data) {
    this.pageEditingService.updateMenu(id, data)
      .subscribe(res => {
        console.log(res);
      })
  }

}

@Component({
  selector: 'ngx-tab6',
  template: `
    <nb-card>
      <nb-card-body dir="rtl">
        <ng2-smart-table [settings]="settings" [source]="source" (deleteConfirm)="onDeleteConfirm($event)"
                         (editConfirm)="onEditConfirm($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class SubjectComponent extends PageEditingComponent {

  public subjects: any = [];
  config: ToasterConfig;
  source: any;

  subject: any = {
    name: '',
    id: null
  };

  settings = {
    attr: {
      class: 'change-table'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-check"></i>',
      cancelButtonContent: '<i class="fas fa-times"></i>'
    },
    delete: {
      deleteButtonContent: '<i class="far fa-trash-alt"></i>',
      confirmDelete: true,
      name: 'פעולות'
    },
    actions: {
      add: false,
      delete: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      key: {
        title: 'מזהה',
        type: 'number',
        editable: false
      },
      heb: {
        title: 'heb',
        type: 'string',
      },
      eng: {
        title: 'eng',
        type: 'string',
      }
    },
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getSubjects()
  }

  getSubjects() {
    this.pageEditingService.getSubjects()
      .subscribe(res => {
        this.source = res;
      })
  }

  onEditConfirm(ev) {
    console.log(ev);
    ev.confirm.resolve();
    for (let name in ev.newData) {
      if (ev.newData.hasOwnProperty(name)) {
        if (ev.newData[name] !== ev.data[name]) {
          let data = {
            name: ev.newData[name],
            language_id: name === 'heb' ? 2 : 1,
            key_id: ev.newData['key_id']
          };
          this.updateSubject(data, ev.newData[`${name}_id`]);
        }
      }
    }
    ev.confirm.reject();
  }

  onDeleteConfirm(event) {
    if (window.confirm('אתה בטוח שאתה רוצה למחוק?\n')) {
      event.confirm.resolve();
      this.subject.id = event.data.id;
      this.pageEditingService.deleteSubject(this.subject.id)
        .subscribe(res => {
          console.log(res);
        })
    } else {
      event.confirm.reject();
    }
  }

  updateSubject(data, id) {
    this.pageEditingService.updateSubject(data, id)
      .subscribe(res => {
        console.log(res);
      })
  }

}

@Component({
  selector: 'ngx-tab6',
  template: `
    <nb-card>
      <nb-card-header class="menu-header">
      </nb-card-header>
      <nb-card-body dir="rtl">
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl" style="width: 100%">
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="termsPage.field1">
              <angular-editor [(ngModel)]="termsPage.field2"></angular-editor>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(termsPage)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr" style="width: 100%">
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="termsPageEng.field1">
              <angular-editor style="text-align: left" [(ngModel)]="termsPageEng.field2"></angular-editor>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(termsPageEng)">שמירה
            </button>
          </div>
        </div>
        <br>
        <br>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <img [src]="banner.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="banner.title" type="text"
                     nbInput>
              <input type="file" (change)="bannerChange($event)">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.heb, banner)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <img [src]="bannerEng.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="bannerEng.title" type="text"
                     nbInput>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.eng, bannerEng)">שמירה
            </button>
          </div>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class TermsComponent extends PageEditingComponent {

  termsPage: any = {
    "dynamic_text_id": 12,
    "title": "TERMS/TEXT",
    "field1": "",
    "field2": "",
    "field3": "net",
    "field4": "field",
    "field5": "field",
    "field6": "field",
    "field7": "field",
    "field8": "field",
    "field9": "field",
  };

  termsPageEng: any = {
    "dynamic_text_id": 26,
    "title": "TERMS/TEXT",
    "field1": "",
    "field2": "",
    "field3": "net",
    "field4": "field",
    "field5": "field",
    "field6": "field",
    "field7": "field",
    "field8": "field",
    "field9": "field",
  };

  banner: any = {
    title: "",
    photo: "",
    name: "Privacy",
    language_id: "2"
  };

  bannerEng: any = {
    title: "",
    photo: "",
    name: "Privacy",
    language_id: "1"
  };

  bannersId: any = {
    heb: '',
    eng: ''
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getText();
    this.getBanners();
  }

  updateBanner(id, banner) {
    if (banner.photo.startsWith('https://')) {
      const obj = {};
      Object.assign(obj, banner);
      let image = banner.photo.match(/(\/image\w+\W+\w+)/)[0].split('/');
      obj['photo'] = image[1];
      console.log(obj);
      this.pageEditService.updateBanners(id, obj)
        .subscribe(res => {
          this.toasterService.successToastr(res['message']);
        })
    } else {
      this.pageEditService.updateBanners(id, banner)
        .subscribe(res => {
          this.toasterService.successToastr(res['message']);
        })
    }
  }

  bannerChange(event): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = () => {
        this.banner.photo = reader.result;
        this.bannerEng.photo = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  getBanners() {
    this.pageEditService.getBanners()
      .subscribe(res => {
        res['success'].forEach(item => {
          if (item.name === 'Privacy') {
            item.language_id === 2 ? this.banner.title = item.title : this.bannerEng.title = item.title;
            item.language_id === 2 ? this.banner.photo = item.photo : this.bannerEng.photo = item.photo;
            item.language_id === 2 ? this.bannersId.heb = item.id : this.bannersId.eng = item.id;
          }
        })
      })
  }


  getText() {
    this.pageEditService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          if (item.id === 12) {
            this.termsPage.field1 = item.field1;
            this.termsPage.field2 = item.field2;
          } else if (item.id === 26) {
            this.termsPageEng.field1 = item.field1;
            this.termsPageEng.field2 = item.field2;
          }
        })
      })
  }

  newText(data) {
    this.pageEditingService.updateText(data)
      .subscribe(res => {
        if (res['success']) {
          this.toasterService.successToastr('Text has been updated');
        } else {
          this.toasterService.errorToastr('Invalid data');
        }
      }, err => {
        this.toasterService.errorToastr(err['message']);
      })
  }

}

@Component({
  selector: 'ngx-tab6',
  template: `
    <nb-card>
      <nb-card-header class="menu-header">
      </nb-card-header>
      <nb-card-body dir="rtl">
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl" style="width: 100%">
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="privacy.field1">
              <angular-editor [(ngModel)]="privacy.field2"></angular-editor>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(privacy)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr" style="width: 100%">
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="privacyEng.field1">
              <angular-editor style="text-align: left" [(ngModel)]="privacyEng.field2"></angular-editor>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(privacyEng)">שמירה
            </button>
          </div>
        </div>
        <br>
        <br>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <div class="type-item">
              <img [src]="banner.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="banner.title" type="text"
                     nbInput>
              <input type="file" (change)="bannerChange($event)">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.heb, banner)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <div class="type-item">
              <img [src]="bannerEng.photo">
              <input placeholder="שם סעיף 1" [(ngModel)]="bannerEng.title" type="text"
                     nbInput>
            </div>
            <button class="btn btn-hero-primary"
                    (click)="updateBanner(bannersId.eng, bannerEng)">שמירה
            </button>
          </div>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class PrivacyComponent extends PageEditingComponent {

  privacy: any = {
    "dynamic_text_id": 14,
    "title": "PRIVACY/PAGE",
    "field1": "",
    "field2": "",
    "field3": "net",
    "field4": "net",
    "field5": "net",
    "field6": "net",
    "field7": "net",
    "field8": "net",
    "field9": "net",
  };

  privacyEng: any = {
    "dynamic_text_id": 27,
    "title": "PRIVACY/PAGE",
    "field1": "",
    "field2": "",
    "field3": "net",
    "field4": "net",
    "field5": "net",
    "field6": "net",
    "field7": "net",
    "field8": "net",
    "field9": "net",
  };

  banner: any = {
    title: "",
    photo: "",
    name: "Policy",
    language_id: '2'
  };

  bannerEng: any = {
    title: "",
    photo: "",
    name: "Policy",
    language_id: '1'
  };

  bannersId: any = {
    heb: '',
    eng: ''
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getText();
    this.getBanners();
  }

  updateBanner(id, banner) {
    if (banner.photo.startsWith('https://')) {
      const obj = {};
      Object.assign(obj, banner);
      let image = banner.photo.match(/(\/image\w+\W+\w+)/)[0].split('/');
      obj['photo'] = image[1];
      console.log(obj);
      this.pageEditService.updateBanners(id, obj)
        .subscribe(res => {
          this.toasterService.successToastr(res['message']);
        })
    } else {
      this.pageEditService.updateBanners(id, banner)
        .subscribe(res => {
          this.toasterService.successToastr(res['message']);
        })
    }
  }

  bannerChange(event): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = () => {
        this.banner.photo = reader.result;
        this.bannerEng.photo = reader.result;
      };
      reader.readAsDataURL(file);
    }
  }

  getBanners() {
    this.pageEditService.getBanners()
      .subscribe(res => {
        res['success'].forEach(item => {
          if (item.name === 'Policy') {
            item.language_id === 2 ? this.banner.title = item.title : this.bannerEng.title = item.title;
            item.language_id === 2 ? this.banner.photo = item.photo : this.bannerEng.photo = item.photo;
            item.language_id === 2 ? this.bannersId.heb = item.id : this.bannersId.eng = item.id;
          }
        })
      })
  }

  getText() {
    this.pageEditService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          if (item.id === 14) {
            this.privacy.field1 = item.field1;
            this.privacy.field2 = item.field2;
          } else if (item.id === 27) {
            this.privacyEng.field1 = item.field1;
            this.privacyEng.field2 = item.field2;
          }
        })
      })
  }


  newText(data) {
    this.pageEditingService.updateText(data)
      .subscribe(res => {
        if (res['success']) {
          this.toasterService.successToastr('Text has been updated');
        } else {
          this.toasterService.errorToastr('Invalid data');
        }
      }, err => {
        this.toasterService.errorToastr(err['message']);
      })
  }

}

@Component({
  selector: 'ngx-tab6',
  template: `
    <nb-card>
      <nb-card-header class="menu-header">
      </nb-card-header>
      <nb-card-body dir="rtl">
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <p>שוער</p>
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOne.field1">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOne.field2">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOne.field3">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOne.field4">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOne.field5">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(typeOne)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <p>שוער</p>
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOneEng.field1">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOneEng.field2">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOneEng.field3">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOneEng.field4">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeOneEng.field5">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(typeOneEng)">שמירה
            </button>
          </div>
        </div>
        <br>
        <br>
        <div class="flex-wrapper">
          <div class="types-block" dir="rtl">
            <p>שחקן שדה</p>
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwo.field1">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwo.field2">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwo.field3">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwo.field4">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwo.field5">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(typeTwo)">שמירה
            </button>
          </div>
          <div class="types-block" dir="ltr">
            <p>שחקן שדה</p>
            <div class="type-item">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwoEng.field1">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwoEng.field2">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwoEng.field3">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwoEng.field4">
              <input nbInput placeholder="טקסט קופץ" [(ngModel)]="typeTwoEng.field5">
            </div>
            <button class="btn btn-hero-primary"
                    (click)="newText(typeTwoEng)">שמירה
            </button>
          </div>
        </div>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class TypesComponent extends PageEditingComponent {

  typeOne: any = {
    "dynamic_text_id": 15,
    "title": "REPORT/STEP_2/TYPE_1",
    "field1": "",
    "field2": "",
    "field3": "",
    "field4": "",
    "field5": "",
    "field6": "field",
    "field7": "field",
    "field8": "field",
    "field9": "field",
  };

  typeOneEng: any = {
    "dynamic_text_id": 28,
    "title": "REPORT/STEP_2/TYPE_1",
    "field1": "",
    "field2": "",
    "field3": "",
    "field4": "",
    "field5": "",
    "field6": "field",
    "field7": "field",
    "field8": "field",
    "field9": "field",
  };

  typeTwo: any = {
    "dynamic_text_id": 16,
    "title": "REPORT/STEP_2/TYPE_2",
    "field1": "",
    "field2": "",
    "field3": "",
    "field4": "",
    "field5": "",
    "field6": "field",
    "field7": "field",
    "field8": "field",
    "field9": "field",
  };

  typeTwoEng: any = {
    "dynamic_text_id": 29,
    "title": "REPORT/STEP_2/TYPE_2",
    "field1": "",
    "field2": "",
    "field3": "",
    "field4": "",
    "field5": "",
    "field6": "field",
    "field7": "field",
    "field8": "field",
    "field9": "field",
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getText();
  }

  getText() {
    this.pageEditService.getText()
      .subscribe(res => {
        res['data'].forEach(item => {
          if (item.id === 15) {
            this.typeOne.field1 = item.field1;
            this.typeOne.field2 = item.field2;
            this.typeOne.field3 = item.field3;
            this.typeOne.field4 = item.field4;
            this.typeOne.field5 = item.field5;
          } else if (item.id === 16) {
            this.typeTwo.field1 = item.field1;
            this.typeTwo.field2 = item.field2;
            this.typeTwo.field3 = item.field3;
            this.typeTwo.field4 = item.field4;
            this.typeTwo.field5 = item.field5;
          } else if (item.id === 28) {
            this.typeOneEng.field1 = item.field1;
            this.typeOneEng.field2 = item.field2;
            this.typeOneEng.field3 = item.field3;
            this.typeOneEng.field4 = item.field4;
            this.typeOneEng.field5 = item.field5;
          } else if (item.id === 29) {
            this.typeTwoEng.field1 = item.field1;
            this.typeTwoEng.field2 = item.field2;
            this.typeTwoEng.field3 = item.field3;
            this.typeTwoEng.field4 = item.field4;
            this.typeTwoEng.field5 = item.field5;
          }
        })
      })
  }


  newText(data) {
    this.pageEditingService.updateText(data)
      .subscribe(res => {
        if (res['success']) {
          this.toasterService.successToastr('Text has been updated');
        } else {
          this.toasterService.errorToastr('Invalid data');
        }
      }, err => {
        this.toasterService.errorToastr(err['message']);
      })
  }

}

@Component({
  selector: 'ngx-tab6',
  template: `
    <nb-card>
      <!--<nb-card-header class="menu-header">-->
      <!--<input class="menu-input" nbInput type="text" [(ngModel)]="language.name">-->
      <!--<button nbButton (click)="createMenu()">ליצור</button>-->
      <!--<button nbButton (click)="updateMenu()">שמירה</button>-->
      <!--</nb-card-header>-->
      <nb-card-body dir="rtl">
        <ng2-smart-table [settings]="settings" [source]="source" (deleteConfirm)="onDeleteConfirm($event)"
                         (editConfirm)="onEditConfirm($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./page-editing.component.scss'],
})
export class LanguagesComponent extends PageEditingComponent {

  config: ToasterConfig;
  source: any;

  settings = {
    attr: {
      class: 'change-table'
    },
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true,
      name: 'פעולות'
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="far fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-check"></i>',
      cancelButtonContent: '<i class="fas fa-times"></i>'
    },
    actions: {
      add: false,
      delete: false,
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      key: {
        title: 'מזהה',
        type: 'number',
        editable: false
      },
      heb: {
        title: 'heb',
        type: 'string',
      },
      eng: {
        title: 'eng',
        type: 'string',
      }
    },
  };

  constructor(public pageEditingService: PageEditingService, public toasterService: ToastrManager) {
    super(pageEditingService, toasterService);
    this.getSubjects()
  }

  getSubjects() {
    this.pageEditingService.getLanguages()
      .subscribe(res => {
        console.log(res);
        this.source = res;
      })
  }

  // createMenu() {
  //   this.pageEditingService.createLanguages({
  //     name: this.language.name,
  //   }).subscribe(res => {
  //     this.getSubjects();
  //     this.language.name = '';
  //   })
  // }

  onEditConfirm(ev) {
    console.log(ev);
    ev.confirm.resolve();
    for (let name in ev.newData) {
      if (ev.newData.hasOwnProperty(name)) {
        if (ev.newData[name] !== ev.data[name]) {
          this.updateMenu(ev.newData[`${name}_id`], {name: ev.newData[name]});
        }
      }
    }
    ev.confirm.reject();
  }

  // onDeleteConfirm(event) {
  //   if (window.confirm('אתה בטוח שאתה רוצה למחוק?\n')) {
  //     event.confirm.resolve();
  //     this.pageEditingService.deleteLanguages(this.language.id)
  //       .subscribe(res => {
  //         console.log(res);
  //       })
  //   } else {
  //     event.confirm.reject();
  //   }
  // }

  updateMenu(id, data) {
    this.pageEditingService.updateLanguages(data, id)
      .subscribe(res => {
        console.log(res);
      })
  }
}

import {Component, OnInit} from '@angular/core';
import {ToasterConfig} from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
import {LocalDataSource} from "ng2-smart-table";
import {ReportsService} from "../../@core/data/reports.service";
import {DomSanitizer} from "@angular/platform-browser";
import {CoachService} from "../../@core/data/coach.service";
import {Router} from "@angular/router";
import * as moment from 'moment';
import {DatePipe} from "@angular/common";

@Component({
  selector: 'ngx-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  public aboutUs: any = '';
  public termsConditions: any = '';
  public config: ToasterConfig;

  public tabs: any = [
    {
      title: 'דוחות חדשים',
      route: '/pages/reports/new-reports',
    },
    {
      title: 'דוחות בתהליך',
      route: '/pages/reports/reports-on-check',
    },
    {
      title: 'דוחות שהוגשו',
      route: '/pages/reports/finished-reports',
    },
  ];

  public editorInit = {};

  constructor(public reportsService: ReportsService, public coachService: CoachService,) {

  }

  ngOnInit() {
  }
}

@Component({
  selector: 'ngx-tab1',
  template: `
    <nb-card>
      <nb-card-header>
        הקצה אל
        <select placeholder="Select Coach" [(ngModel)]="selectedItem" (change)="onMenuItemSelected($event)">
          <option *ngFor="let coach of coachs" [value]="coach.id">{{coach.firstname}}</option>
          <!--<nb-option value="something">Sonething</nb-option>-->
        </select>
      </nb-card-header>
      <nb-card-body>

        <ng2-smart-table [settings]="settings" [source]="source" (custom)="goTo($event)"
                         (userRowSelect)="onUserRowSelect($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./reports.component.scss']
})
export class NewReportsComponent extends ReportsComponent {

  coachs: any = [];
  selectedItem: any;

  selectedIds: any = [];

  source: LocalDataSource = new LocalDataSource();
  settings = {
    selectMode: 'multi',
    actions: {
      add: false,
      edit: false,
      delete: false,
      select: true,
      custom: [
        { title: '<i class="fas fa-arrow-circle-left"></i>'
        }
      ],
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      id: {
        title: 'מספר דו״ח מאמן',
        type: 'number',
      },
      'player.firstname': {
        title: 'שם שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.firstname;
        },
      },
      player: {
        title: 'מספר מזהה',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.user_id;
        },
      },
      player_type: {
        title: 'סוג שחקן',
        type: 'string',
        valuePrepareFunction: (value) => {
          if (+value === 1) {
            return 'שוער'
          } else {
            return 'שחקן שדה'
          }
        },
      },
      'player.email': {
        title: 'אימייל שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.email;
        },
      },
      created_at: {
        title: 'תאריך יצירת הדו״ח',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      due_date: {
        title: 'תאריך להגשה',
        type: 'html',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return '<p class="due-date" style="color: #ff0000!important;">' + new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm') + '</p>';
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
    },
  };

  constructor(public reportsService: ReportsService,
              public coachService: CoachService,
              public router: Router) {
    super(reportsService, coachService)
  }

  ngOnInit() {
    this.getReports();
    this.getCoachs()
  }

  getReports() {
    this.reportsService.getAll()
      .subscribe(res => {
        let data = res['data'].filter(item => {
          if (item.status === 0 && item.state === 'published') {
            return item
          }
        });
        this.source.load(data);
      });
  }

  onUserRowSelect(event) {
    this.selectedIds = [];
    event.selected.filter(item => {
      this.selectedIds.push(item.id);
    })
  }

  goTo(event){
    this.reportsService.setReport(event.data,'show',true);
    this.router.navigate(['/pages/edit-report']);
  }


  onMenuItemSelected(event) {
    if (!this.selectedIds.length) {
      alert('Please,select reports');
    } else {
      if (confirm('Are you sure?')) {
        let coachId = +event.target.value;
        this.coachService.appointCoach({coach_id: coachId, status: "1", id: this.selectedIds})
          .subscribe(res => {
            this.getReports();
            window.location.reload();
          })
      }
    }
  }

  getCoachs() {
    this.coachService.getCoaches()
      .subscribe(res => {
        this.coachs = res;
      })
  }
}

@Component({
  selector: 'ngx-tab2',
  template: `
    <nb-card>
      <nb-card-header>
        הקצה אל
        <select placeholder="Select Coach" [(ngModel)]="selectedItem" (change)="onMenuItemSelected($event)">
          <option *ngFor="let coach of coachs" [value]="coach.id">{{coach.firstname}}</option>
          <!--<nb-option value="something">Sonething</nb-option>-->
        </select>
      </nb-card-header>
      <nb-card-body>

        <ng2-smart-table [settings]="settings" [source]="source" (userRowSelect)="onUserRowSelect($event)" (custom)="goTo($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./reports.component.scss']

})
export class ReportsOnCheckComponent extends ReportsComponent {

  coachs: any = [];
  selectedItem: any;

  selectedIds: any = [];

  source: LocalDataSource = new LocalDataSource();
  settings = {
    selectMode: 'multi',
    actions: {
      add: false,
      edit: false,
      delete: false,
      select: true,
      custom: [
        { title: '<i class="fas fa-arrow-circle-left"></i>'
        }
          ],
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      id: {
        title: 'מספר דו״ח מאמן',
        type: 'number',
      },
      'player.firstname': {
        title: 'שם שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.firstname;
        },
      },
      player: {
        title: 'מספר מזהה',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.user_id;
        },
      },
      player_type: {
        title: 'סוג שחקן',
        type: 'string',
        valuePrepareFunction: (value) => {
          if (+value === 1) {
            return 'שוער'
          } else {
            return 'שחקן שדה'
          }
        },
      },
      updated_at: {
        title: 'עודכן בתאריך',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      created_at: {
        title: 'תאריך יצירת הדו״ח',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      due_date: {
        title: 'תאריך להגשה',
        type: 'html',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return '<p class="due-date" style="color: #ff0000!important;">' + new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm') + '</p>';
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      coach: {
        title: 'שם המאמן שבדק',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.firstname;
        },
      },
    },
  };

  public input: string = '<input type="checkbox">';

  constructor(public reportsService: ReportsService,
              public coachService: CoachService,
              public _sanitizer: DomSanitizer,
              public router: Router) {
    super(reportsService, coachService)
  }


  ngOnInit() {
    this.getReports();
    this.getCoachs()
  }

  getReports() {
    this.reportsService.getAll()
      .subscribe(res => {
        const data = res['data'].filter(item => {
          if (item.status === 1 && item.state === 'published') {
            return item
          }
        });
        this.source.load(data);
      })
  }

  goTo(event){
    this.reportsService.setReport(event.data,'show',false);
    this.router.navigate(['/pages/edit-report']);
  }

  onUserRowSelect(event) {

    this.selectedIds = [];
    event.selected.filter(item => {
      this.selectedIds.push(item.id);
    })
  }

  onMenuItemSelected(event) {
    if (!this.selectedIds.length) {
      alert('Please,select reports');
    } else {
      if (confirm('Are you sure?')) {
        let coachId = +event.target.value;
        this.coachService.appointCoach({coach_id: coachId, status: 1, id: this.selectedIds})
          .subscribe(res => {
            this.getReports();
            window.location.reload();
          })
      }
    }
  }

  getCoachs() {
    this.coachService.getCoaches()
      .subscribe(res => {
        this.coachs = res;
      })
  }
}

@Component({
  selector: 'ngx-tab3',
  template: `
    <nb-card>
      <nb-card-header>
        דוחות שהסתיימו
      </nb-card-header>
      <nb-card-body>

        <ng2-smart-table [settings]="settings" [source]="source" (deleteConfirm)="onDeleteConfirm($event)" (custom)="goTo($event)">
        </ng2-smart-table>
      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./reports.component.scss']

})
export class ReportsFinishedComponent extends ReportsComponent {

  reports: any = [];

  source: LocalDataSource = new LocalDataSource();
  settings = {
    delete: {
      deleteButtonContent: '<i class="ion-trash-a"></i>',
      confirmDelete: true,
      name: 'פעולות'
    },
    actions: {
      add: false,
      edit: false,
      custom: [
        { title: '<i class="fas fa-arrow-circle-left"></i>'
        }
      ],
      position: 'right',
      columnTitle: 'פעולות'
    },
    columns: {
      id: {
        title: 'מספר דו״ח מאמן',
        type: 'number',
      },
      'player.firstname': {
        title: 'שם שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.firstname;
        },
      },
      player: {
        title: 'מספר מזהה',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.user_id;
        },
      },
      player_type: {
        title: 'סוג שחקן',
        type: 'string',
        valuePrepareFunction: (value) => {
          if (+value === 1) {
            return 'שוער'
          } else {
            return 'שחקן שדה'
          }
        },
      },
      'player.email': {
        title: 'אימייל שחקן',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.player.email;
        },
      },
      updated_at: {
        title: 'עודכן בתאריך',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      due_date: {
        title: 'תאריך להגשה',
        type: 'html',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return '<p class="due-date" style="color: #ff0000!important;">' + new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm') + '</p>';
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      created_at: {
        title: 'תאריך יצירת הדו״ח',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      rating: {
        title: 'ציון דו״ח',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value[0].result;
        },
      },
      last_updated: {
        title: 'עודכן בתאריך',
        type: 'number',
        sort: true,
        sortDirection: 'desc',
        valuePrepareFunction: (date) => {
          if (date) {
            const raw = new Date(date);

            return new DatePipe('en-EN').transform(raw, 'dd.MM.yyyy HH:mm');
          }

        },
        compareFunction: (direction: any, a: any, b: any) => {
          const timeA = new Date(a).getTime();
          const timeB = new Date(b).getTime();
          if (timeA < timeB) {
            return direction;
          }
          if (timeA > timeB) {
            return -1 * direction;
          }
          return 0;
        },
      },
      coach: {
        title: 'שם המאמן שבדק',
        type: 'number',
        valuePrepareFunction: (value) => {
          return value.firstname;
        },
      },
    },
  };

  constructor(public reportsService: ReportsService,
              public coachService: CoachService,
              public router: Router) {
    super(reportsService, coachService)
  }

  ngOnInit() {
    this.reportsService.getAll()
      .subscribe(res => {
        let data = res['data'].filter(item => {
          if (item.status === 2 && item.state === 'published') {
            return item
          }
        });
        this.source.load(data);
      })
  }

  goTo(event){
    this.reportsService.setReport(event.data,'show',false);
    this.router.navigate(['/pages/edit-report']);
  }

  onDeleteConfirm(event):void{
    if (window.confirm('אתה בטוח שאתה רוצה למחוק?')) {
      event.confirm.resolve();
      this.reportsService.deleteReports({ids: [event['data']['id']]})
        .subscribe(res =>{
          console.log(res);
        })
    } else {
      event.confirm.reject();
    }
  }
}

import {ChangeDetectorRef, ElementRef, forwardRef, NgModule} from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import {ReportsRouting, routedComponents} from "./reports.routing";
import {Ng2SmartTableModule} from "ng2-smart-table";
import {ReportsService} from "../../@core/data/reports.service";
import {NbSelectModule} from "../../../libs/@nebular/theme/components/select/select.module";
import {NbSelectComponent} from "../../../libs/@nebular/theme/components/select/select.component";
import {NG_VALUE_ACCESSOR} from "@angular/forms";

@NgModule({
  imports: [
    ThemeModule,
    ReportsRouting,
    EditorModule,
    ToasterModule.forRoot(),
    Ng2SmartTableModule,
    NbSelectModule
  ],
  declarations: [
    ...routedComponents,
  ],
  exports: [NbSelectModule],
  providers: [
    ReportsService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NbSelectComponent),
      multi: false,
    }],
})
export class ReportsModule { }

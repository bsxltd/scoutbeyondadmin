import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {NewReportsComponent, ReportsOnCheckComponent, ReportsFinishedComponent, ReportsComponent} from './reports.component';

const routes: Routes = [{
  path: '',
  component: ReportsComponent,
  children: [{
    path: 'reports',
    component: ReportsComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRouting { }

export const routedComponents = [
  NewReportsComponent,
  ReportsOnCheckComponent,
  ReportsFinishedComponent,
];

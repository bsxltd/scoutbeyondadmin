import {Component, OnInit} from '@angular/core';
import {HowItWorkService} from "../../@core/data/how-it-work.service";
import {ToastrManager} from "ng6-toastr-notifications";
import {PageEditingService} from "../../@core/data/page-editing.service";

@Component({
  selector: 'ngx-how-it-work',
  templateUrl: './how-it-work.component.html',
  styleUrls: ['./how-it-work.component.scss']
})
export class HowItWorkComponent implements OnInit {

  list: any;
  banner: any = {
    title: "",
    photo: "",
    name: "how_it_work",
    language_id: '2'
  };

  bannerEng: any = {
    title: "",
    photo: "",
    name: "how_it_work",
    language_id: '1'
  };

  bannersId: any = {
    eng: '',
    heb: ''
  };

  constructor(public howItWorkService: HowItWorkService,
              public toasterService: ToastrManager,
              public pageEditingService: PageEditingService) {
  }

  ngOnInit() {
    this.getItems();
    this.getBanners();
  }

  getItems() {
    this.howItWorkService.getAll()
      .subscribe(res => {
        this.list = res['success'];
      })
  }

  updateText(data) {
    console.log(data);
    let obj = {};
    Object.assign(obj, data);
    if (data.photo.startsWith('https://')){
      delete obj['photo'];
    }
    this.howItWorkService.update(data.id, obj)
      .subscribe(() => {
        this.toasterService.successToastr('Successfuly updated');
      }, () => {
        this.toasterService.errorToastr('Error updating')
      })
  }

  changePhoto(event, item) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = () => {
        if (item) {
          item.how_it_work[1].photo = reader.result;
          item.how_it_work[0].photo = reader.result;
        } else {
          this.banner.photo = reader.result;
          this.bannerEng.photo = reader.result;
        }
      };
      reader.readAsDataURL(file);
    }
  }

  updateBanner(id, banner) {
    let obj = {};
    Object.assign(obj, banner);
    if (banner.photo.startsWith('https://')){
      delete obj['photo'];
    }
    this.pageEditingService.updateBanners(id, obj)
      .subscribe(res => {
        this.toasterService.successToastr(res['message']);
      })
  }

  getBanners() {
    this.pageEditingService.getBanners()
      .subscribe(res => {
        res['success'].forEach(item => {
          if (item.name === 'how_it_work') {
            item.language_id === 2 ? this.banner.title = item.title : this.bannerEng.title = item.title;
            item.language_id === 2 ? this.banner.photo = item.photo : this.bannerEng.photo = item.photo;
            item.language_id === 2 ? this.bannersId.heb = item.id : this.bannersId.eng = item.id;
          }
        })
      })
  }
}

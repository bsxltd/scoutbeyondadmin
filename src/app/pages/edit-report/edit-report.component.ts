import {Component, OnInit} from '@angular/core';
import {ReportsService} from "../../@core/data/reports.service";
import {Router} from "@angular/router";
import {DomSanitizer} from "@angular/platform-browser";
import {VimeoService} from "../../@core/data/vimeo.service";
import {BehaviorSubject} from "rxjs";
import {ToastrManager} from "ng6-toastr-notifications";

@Component({
  selector: 'ngx-edit-report',
  templateUrl: './edit-report.component.html',
  styleUrls: ['./edit-report.component.scss']
})
export class EditReportComponent implements OnInit {

  loading: boolean = false;

  report: any = {};

  ratings: any = [];

  commentsData: any = {
    report_id: null,
    comment1: null,
    comment2: null,
    comment3: null,
    comment4: null
  };

  ratingsData: any = {
    report_id: null,
    rating1: 0,
    rating2: 0,
    rating3: 0,
    rating4: 0,
    status: "2"
  };

  sections: any = {
    'section[1]': [],
    'section[2]': [],
    'section[3]': [],
    'section[4]': [],
    'section[5]': []
  };

  state: string = '';

  uploadVideo: any = {
    section: '',
    file: null
  };

  right: boolean;

  progress: any;

  progress_subject = new BehaviorSubject(this.progress);
  progress_subject$ = this.progress_subject.asObservable();

  constructor(public reportService: ReportsService,
              public router: Router,
              public vimeoService: VimeoService,
              public toastr: ToastrManager){
  }

  ngOnInit() {
    this.parseRating();
    this.getVideo();
    this.getProgress();
  }

  parseRating() {
    for (let i = 0; i <= 5; i += 0.1) {
      this.ratings.push(+i.toFixed(1));
    }
  }

  getProgress() {
    this.progress_subject$.subscribe(res => {
      if (res) {
        this.sections[`section[${res['section']}]`][res['id']]['info']['progress'] = res['progress'];
        this.sections[`section[${res['section']}]`][res['id']]['info']['exist'] = res['exist'];
      }
    })
  }

  newFileSend(file, path) {
    const fd = new FormData();
    fd.append(`section[${file.section}][]`, path);
    fd.append('report_id', this.report.id);
    return fd;
  }

  getNewVideo(ev) {
    console.log(ev);
    this.uploadVideo.file = ev.target.files[0];
  }

  addNew() {
    if (this.uploadVideo.section !== '' && this.uploadVideo.file != null) {
      this.loading = true;
      const file = this.uploadVideo.file;
      this.uploadVideo.file = null;
      if (this.reportService.sizeCheck(this.sections, this.uploadVideo.section)) {
        file.info = {
          section: this.uploadVideo.section,
          id: this.sections[`section[${this.uploadVideo.section}]`].length,
          progress: 0,
          exist: false,
        };
        file.section = Number(this.uploadVideo.section);
        this.loading = false;
        this.vimeoService.createVideo(file).subscribe(res => {
          const url = res['body']['upload']['upload_link'];
          file.path = res['body']['uri'];
          this.sections[`section[${file.info.section}]`].push(file);
          this.addVideo(file, url, res['body']['uri']).then(() => {
          });
        });
      } else {
        this.loading = false;
      }
    } else this.toastr.errorToastr('בחר קטע וסרטון');
  }

  getVideo() {
    this.sections = {
      'section[1]': [],
      'section[2]': [],
      'section[3]': [],
      'section[4]': [],
      'section[5]': []
    };
    this.state = this.reportService.getState();
    console.log(this.state);
    this.right = this.reportService.getRight();
    this.report = this.reportService.getReport();
    if (JSON.stringify(this.report) !== '{}') {
      this.saveReportLS();
      this.setReportData();
    } else {
      this.report = JSON.parse(localStorage.getItem('report'));
      this.setReportData();
    }
  }

  saveReportLS(){
    localStorage.setItem('report', JSON.stringify(this.report));
  }

  setReportData() {
    if (this.report.comment != null) {
      this.commentsData.comment1 = this.report.comment.comment1;
      this.commentsData.comment2 = this.report.comment.comment2;
      this.commentsData.comment3 = this.report.comment.comment3;
      this.commentsData.comment4 = this.report.comment.comment4;
    }
    if (this.report.rating.length) {
      this.ratingsData.rating1 = this.report.rating[0].rating1;
      this.ratingsData.rating2 = this.report.rating[0].rating2;
      this.ratingsData.rating3 = this.report.rating[0].rating3;
      this.ratingsData.rating4 = this.report.rating[0].rating4;
    }
    this.report.videos.forEach(item => {
      item.info = {
        section: item.section,
        id: item.id,
        progress: 100,
        exist: true
      };
      this.sections[`section[${item.section}]`].push(item);
    });
  }

  async addVideo(content, url, checkUrl?) {
    const end = content.size;
    const xhr = new XMLHttpRequest();
    xhr.open('PUT', url, true);
    xhr.setRequestHeader('Content-Type', 'application/octet-stream');
    xhr.upload.onprogress = (event) => {
      let score = Math.round(event.loaded / event.total * 100);
      this.progress_subject.next({section: content.info.section, id: content.info.id, progress: score, exist: false});
      if (event.loaded === event.total) {
        let interval = setInterval(() => {
          this.vimeoService.notCallbackVimeo(checkUrl).subscribe(res => {
            if (!this.sections[`section[${content.info.section}]`][content.info.id].info.exist) {
              if (res['body']['transcode']['status'] === 'complete') {
                this.vimeoService.getLink(checkUrl)
                  .subscribe(result => {
                    this.sections[`section[${content.info.section}]`][content.info.id].path = result['body']['files'][0]['link'];
                    const fd = this.newFileSend(content, this.sections[`section[${content.info.section}]`][content.info.id].path);
                    this.reportService.addVideo(fd)
                      .subscribe(res => {
                        this.addIdForVideo(res['data']['videos']);
                        this.uploadVideo.section = '';
                      });
                    this.progress_subject.next({
                      section: content.info.section,
                      id: content.info.id,
                      progress: 100,
                      exist: true
                    });
                  });
                clearInterval(interval);
              }
            }
          });
        }, 10000);
      }
    };
    xhr.setRequestHeader('Content-Range', 'bytes ' + 0 + '-' + (end - 1) + '/' + content.size);
    await xhr.send(content);
  }

  async sendFile(content, url, checkUrl?, video?) {
    let end = content.size;
    let xhr = new XMLHttpRequest();
    let fd = new FormData();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == XMLHttpRequest.DONE) {
        console.log('Response', xhr.responseText);
      }
    };
    xhr.open('PUT', url, true);
    xhr.setRequestHeader('Content-Type', 'application/octet-stream');
    xhr.upload.onprogress = (event) => {
      let index = this.sections[`section[${video.section}]`].findIndex(item => item.id === video.id);
      let score = Math.round(event.loaded / event.total * 100);
      this.progress_subject.next({section: video.section, id: index, progress: score, exist: false});
      if (event.loaded === event.total) {
        let interval = setInterval(() => {
          this.vimeoService.notCallbackVimeo(checkUrl).subscribe(res => {
            let index = this.sections[`section[${video.section}]`].findIndex(item => item.id === video.id);
            if (!this.sections[`section[${video.section}]`][index].info.exist) {
              if (res['body']['transcode']['status'] === 'complete') {
                this.vimeoService.getLink(checkUrl)
                  .subscribe(result => {
                    this.sections[`section[${video.section}]`][index].path = result['body']['files'][0]['link'];
                    fd.append('video_id', video.id);
                    fd.append('path', this.sections[`section[${video.section}]`][index].path);
                    fd.append('file', this.sections[`section[${video.section}]`][index].path);
                    this.reportService.updateVideo(fd)
                      .subscribe(res => {
                      });
                    this.progress_subject.next({
                      section: video.section,
                      id: index,
                      progress: 100,
                      exist: true
                    });
                  });
                clearInterval(interval);
              }
            }
          });
        }, 10000);
      }
    };
    xhr.setRequestHeader('Content-Range', 'bytes ' + 0 + '-' + (end - 1) + '/' + content.size);
    await xhr.send(content);
  }

  updateVideo(event, video) {
    this.loading = true;
    let file = event.target.files[0];
    this.sections[`section[${video.section}]`].forEach(item => {
      if (item.id === video.id) {
        item.info = {
          section: video.section,
          id: video.id,
          progress: 0,
          exist: false
        };
      }
    });
    this.loading = false;
    this.vimeoService.createVideo(file).subscribe(res => {
      let url = res['body']['upload']['upload_link'];
      file.path = res['body']['uri'];
      this.sendFile(file, url, res['body']['uri'], video).then(() => {
      });
    });
  }

  addIdForVideo(videos) {
    videos.forEach(video => {
      this.sections[`section[${video.section}]`].forEach(item => {
        if (item.path === video.path) {
          item.id = video.id;
        }
      });
    });
  }

  updateReport() {
    if (confirm('\n' +
      'האם אתה בטוח כי סיימת את הדו״ח?\n' +
      '\n' +
      '.לאחר שליחת הדו״ח לא ניתן לערוך אותו שוב\n' +
      'מומלץ לעבור עליו בשנית \n')) {
      this.commentsData.report_id = this.report.id;
      this.ratingsData.report_id = this.report.id;
      if (this.reportService.checkRating(this.ratingsData)) {
        if (this.reportService.checkComment(this.commentsData)) {
          this.reportService.updateComment(this.commentsData)
            .subscribe(res => {
              this.reportService.updateRating(this.ratingsData)
                .subscribe(res => {
                  this.router.navigate(['/pages/coaches-reports']);
                  setTimeout(() => {
                    window.location.reload()
                  }, 500);
                })
            })
        }
      }
    }
  }

  saveReport() {
    this.commentsData.report_id = this.report.id;
    this.ratingsData.report_id = this.report.id;
    this.ratingsData.status = '1';
    this.reportService.updateComment(this.commentsData)
      .subscribe(() => {
        this.reportService.updateRating(this.ratingsData)
          .subscribe(() => {
            this.router.navigate(['/pages/coaches-reports']);
            setTimeout(() => {
              window.location.reload()
            }, 500);
          })
      })
  }

  deleteVideo(video?, index?) {
    if (confirm('האם אתה בטוח שתרצה למחוק את הסירטון? ')) {
      let idVideo = video.path.match(/\d+/);
      this.reportService.deleteVideo({id: video.id})
        .subscribe(() => {
          this.vimeoService.deleteVideo(idVideo[0])
            .subscribe(() => {
            });
        });
      this.sections[`section[${video.section}]`].splice(index, 1);
      this.report.videos = this.report.videos.filter(item => item.id !== video.id);
      this.reportService.setOnlyReport(this.report);
      this.saveReportLS();
    }

  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTeamAddDialogComponent } from './edit-team-add-dialog.component';

describe('EditTeamAddDialogComponent', () => {
  let component: EditTeamAddDialogComponent;
  let fixture: ComponentFixture<EditTeamAddDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTeamAddDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTeamAddDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {DomSanitizer} from "@angular/platform-browser";
import {AddSlidesComponent} from "../../edit-slider/add-slides/add-slides.component";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'ngx-edit-team-add-dialog',
  templateUrl: './edit-team-add-dialog.component.html',
  styleUrls: ['./edit-team-add-dialog.component.scss']
})
export class EditTeamAddDialogComponent implements OnInit {

  @ViewChild('image') public imageElement: ElementRef;

  public team: any = {
    full_name: '',
    description: '',
    country: '',
    section: 1,
  };
  public image: any;
  photo: any;
  full_name: FormControl;
  description: FormControl;
  country: FormControl;
  section: FormControl;
  state: string;

  constructor(
    public sanitize: DomSanitizer,
    public dialogRef: MatDialogRef<AddSlidesComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    console.log(data);
    this.team.section = data.team.section;
    this.team.full_name = data.team.full_name;
    this.team.description = data.team.description;
    this.team.country = data.team.country;
    this.photo = data.image;

    this.full_name = new FormControl(this.team.full_name, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.description = new FormControl(this.team.description, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.country = new FormControl(this.team.country, [
      Validators.required,
      Validators.minLength(1),
    ]);

    this.section = new FormControl(this.team.section, [
      Validators.required,
      Validators.minLength(1),
    ]);
  }

  ngOnInit() {
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    const imageSrc = this.sanitize.bypassSecurityTrustResourceUrl(URL.createObjectURL(file));
    const reader = new FileReader();
    reader.onload = e => this.image = reader.result;
    reader.readAsDataURL(file);
    if (imageSrc) this.photo = imageSrc;
  }

  save() {
    if (!this.full_name.invalid && !this.description.invalid && !this.country.invalid && !this.section.invalid) {
      this.team.full_name = this.full_name.value;
      this.team.description = this.description.value;
      this.team.country = this.country.value;
      this.team.section = this.section.value;
      this.dialogRef.close({team: this.team, image: this.image});
    }
  }

  close() {
    this.dialogRef.close();
  }
}

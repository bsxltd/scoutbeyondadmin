import { EditTeamModule } from './edit-team.module';

describe('EditTeamModule', () => {
  let editTeamModule: EditTeamModule;

  beforeEach(() => {
    editTeamModule = new EditTeamModule();
  });

  it('should create an instance', () => {
    expect(editTeamModule).toBeTruthy();
  });
});

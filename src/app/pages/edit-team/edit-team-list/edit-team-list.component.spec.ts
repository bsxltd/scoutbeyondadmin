import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTeamListComponent } from './edit-team-list.component';

describe('EditTeamListComponent', () => {
  let component: EditTeamListComponent;
  let fixture: ComponentFixture<EditTeamListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTeamListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTeamListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

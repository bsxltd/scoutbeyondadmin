import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-edit-team-list',
  templateUrl: './edit-team-list.component.html',
  styleUrls: ['./edit-team-list.component.scss']
})
export class EditTeamListComponent implements OnInit {
  @Input() teamsList: any;

  constructor() { }

  ngOnInit() {
  }

  handleTeamDeleted(id) {
    this.teamsList = this.teamsList.filter(item => {
      return item.id !== id;
    });
  }

  teamUpdated(team){
    this.teamsList.forEach(item => {
      if(item.id === team.id){
        item.full_name = team.full_name;
        item.photo = team.photo;
        item.country = team.country;
        item.section = team.section;
      }
    })
  }

}

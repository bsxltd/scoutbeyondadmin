import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EditTeamComponent} from "./edit-team.component";
import {EditTeamListComponent} from "./edit-team-list/edit-team-list.component";
import {EditTeamListItemComponent} from "./edit-team-list-item/edit-team-list-item.component";


const routes: Routes = [
  {
    path: 'edit-team',
    component: EditTeamComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditTeamRoutingModule {
}

export const routedComponents = [
  EditTeamComponent,
  EditTeamListComponent,
  EditTeamListItemComponent
];

import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import {MatButtonModule, MatCardModule, MatDialogModule, MatSliderModule} from '@angular/material';
import {NbBadgeModule} from '@nebular/theme';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {EditTeamRoutingModule, routedComponents} from "./edit-team.routing";

@NgModule({
  imports: [
    ThemeModule,
    EditorModule,
    ToasterModule.forRoot(),
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    NbBadgeModule,
    MatSliderModule,
    EditTeamRoutingModule
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
  entryComponents: [
  ],
})

export class EditTeamModule { }

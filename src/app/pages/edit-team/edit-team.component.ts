import { Component, OnInit } from '@angular/core';
import {SlidesService} from "../../@core/data/slides.service";
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from "angular2-toaster";
import {MatDialog} from "@angular/material";
import {EditTeamAddDialogComponent} from "./edit-team-add-dialog/edit-team-add-dialog.component";
import {EditTeamService} from "../../@core/data/edit-team.service";

@Component({
  selector: 'ngx-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.scss']
})
export class EditTeamComponent implements OnInit {

  public teamsList:any = [];
  config: ToasterConfig;

  constructor(private teamService: EditTeamService,
              public dialog: MatDialog,
              public toasterService: ToasterService) {
  }

  ngOnInit() {
    this.teamService.getTeams().subscribe(res => {
      this.teamsList = res['success'];
    });

  }

  openCreateMaskDialog() {
    const dialogRef = this.dialog.open(EditTeamAddDialogComponent, {
      data: {team: {}, image: null, state: 'create'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const data = {
          ...result.team,
          photo: result.image
        };

        this.teamService.addTeam(data).subscribe(res => {
          if (res['success']) {
            this.teamsList.push(res['data']);
            this.showToast(res['status'], null, 'Slide has been added successfully');
          } else {
            this.showToast(res['status'], null, res['message']);
          }
        });
      }
    });
  }

  public showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }
}

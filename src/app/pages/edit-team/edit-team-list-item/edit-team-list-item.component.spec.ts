import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTeamListItemComponent } from './edit-team-list-item.component';

describe('EditTeamListItemComponent', () => {
  let component: EditTeamListItemComponent;
  let fixture: ComponentFixture<EditTeamListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTeamListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTeamListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

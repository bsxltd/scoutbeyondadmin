import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SlidesService} from "../../../@core/data/slides.service";
import {BodyOutputType, Toast, ToasterConfig, ToasterService} from "angular2-toaster";
import {MatDialog} from "@angular/material";
import {ComfirmationPromptComponent} from "../../shared/comfirmation-prompt/comfirmation-prompt.component";
import {EditTeamService} from "../../../@core/data/edit-team.service";
import {EditTeamAddDialogComponent} from "../edit-team-add-dialog/edit-team-add-dialog.component";

@Component({
  selector: 'ngx-edit-team-list-item',
  templateUrl: './edit-team-list-item.component.html',
  styleUrls: ['./edit-team-list-item.component.scss']
})
export class EditTeamListItemComponent implements OnInit {
  public image: any;
  config: ToasterConfig;
  @Input() public team: any;

  @Output() public teamDeleted = new EventEmitter <any>();
  @Output() public reload = new EventEmitter <any>();

  constructor(public dialog: MatDialog,
              public teamService: EditTeamService,
              public toasterService: ToasterService) { }

  ngOnInit() {
  }


  deleteTeam() {
    const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
      data: {team: this.team, state: 'slide'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.teamService.deleteTeam({id: this.team.id}).subscribe(res => {
          if (res['success']) {
            this.showToast(res['status'], null, 'Team has been deleted successfully');
            this.teamDeleted.emit(this.team.id);
          } else {
            this.showToast(res['status'], null, res['message']);
          }
        });
      }
    });
  }

  updateTeam() {
    const dialogRef = this.dialog.open(EditTeamAddDialogComponent, {
      data: {team: this.team, image: this.team.photo, state: 'create'},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const data = {
          ...result.team,
          photo: result.image
        };

        this.teamService.updateTeam(data,this.team.id).subscribe(res => {
          if (res['success']) {
            this.reload.emit(res['data']);
            this.showToast(res['status'], null, 'Slide has been updated successfully');
          } else {
            this.showToast(res['status'], null, res['message']);
          }
        });
      }
    });
  }

  public showToast(type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }
}

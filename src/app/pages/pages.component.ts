import {Component, OnInit,} from '@angular/core';

import {MENU_ITEMS} from './pages-menu';
import {AuthService} from "../@core/data/auth.service";
import {ReportsService} from "../@core/data/reports.service";


@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent implements OnInit {
  user = {};
  menu = MENU_ITEMS;

  count = {
    new: [],
    onCheck: [],
    finished: []
  };


  constructor(private auth: AuthService,
              public reportsService: ReportsService) {

  }

  ngOnInit() {
    this.getReports();
  }

  getReports(){
    this.count = {
      new: [],
      onCheck: [],
      finished: []
    };
    this.user = this.auth.getUser();
    if(this.user['role'] === 'admin'){
      this.reportsService.getAll()
        .subscribe(res => {
          res['data'].filter(item => {
            if(+item.status === 0 && item.state === 'published'){
              this.count.new.push(item);
            }else if(+item.status === 1 && item.state === 'published'){
              this.count.onCheck.push(item);
            }else if(+item.status === 2 && item.state === 'published'){
              this.count.finished.push(item);
            }
          });
          this.filterMenuItems();
        })
    } else {
      this.reportsService.getMy()
        .subscribe(res => {
          res['data'].filter(item => {
            if (item.status === 1 && item.comment === null) {
              this.count.new.push(item);
            }else if (item.status === 1 && item.comment !== null && item.rating !== []) {
              this.count.onCheck.push(item);
            }else if (item.status === 2 && item.state === 'published') {
              this.count.finished.push(item)
            }
          });
          this.filterMenuItems();

        })
    }
  }

  filterMenuItems () {
    this.menu = MENU_ITEMS.filter(item => {
      if(this.user['role'] === 'admin'){
        if(item.link === '/pages/reports'){
          item.children[0].title = `( ${item.children[0].title} ( ${this.count.new.length}`;
          item.children[1].title = `( ${item.children[1].title} ( ${this.count.onCheck.length}`;
          item.children[2].title = `( ${item.children[2].title} ( ${this.count.finished.length}`;
        }
      }else {
        if(item.link === '/pages/coaches-reports'){
          item.children[0].title = `( ${item.children[0].title} ( ${this.count.new.length}`;
          item.children[1].title = `( ${item.children[1].title} ( ${this.count.onCheck.length}`;
          item.children[2].title = `( ${item.children[2].title} ( ${this.count.finished.length}`;
        }
      }
      if(item.role === this.user['role']){
        return item
      }

      });
  }

}
